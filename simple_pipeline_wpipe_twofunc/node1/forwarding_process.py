import socket
import os
import shutil
import time
import sys
import ctypes
import stat 
from threading import Thread
from threading import Event

# Define the data to be sent 
data_to_send = { 'reader1': ['out1']}

# Define the mapping between the functions and the nodes
functions_to_nodes = { 'reader1': '10.0.26.207' }

# Set the maximum buffer size ( max tcp/ip socket buffer size and pipe)
SOCKET_BUFF_SIZE = 8096
PIPE_BUFF_SIZE = 8096

# Set function name
FUNCTION_NAME = 'writer1' 

def on_new_server(server_ip, client_idx, sockets):
    print("P1-CLIENT%d: Server IP: %s" % (client_idx, server_ip))

    # Define the server
    server = (server_ip, 4010)
    
    # Create a TCP/IP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port where the server is listening
    s.connect(server)
    print("P1-CLIENT%d: Client connected and started!" % client_idx)

    # Sending the task name length
    print("P1-CLIENT%d: Sending my name length on socket.." % client_idx)
    s.sendall(len(FUNCTION_NAME).to_bytes(8, "big"))

    # Sending the task name
    print("P1-CLIENT%d: Sending my name on socket.." % client_idx)
    s.sendall(FUNCTION_NAME.encode())

    # Store the socket connetion for the communication
    sockets.append(s)
    
def send_data_to(ready_event, sent_event, data_bytes_addr, data_length_bytes_addr, all_names, function, list_objs, client_idx, sockets):
    # Current socket
    s = sockets[client_idx]

    # Send all data objects to the following function
    for name in all_names:
        if name in list_objs:
            # Send data object dimension 
            ready_event.wait()
            data_length_bytes = ctypes.cast(data_length_bytes_addr, ctypes.py_object).value
            data_length = int.from_bytes(data_length_bytes, "big")
            print("P1-CLIENT%d: Sending data object %s dimension of size %d on socket.." % (client_idx, name, data_length))
            s.sendall(data_length_bytes)
            counter = 0 
            while counter < data_length:
                # Receive ready event
                ready_event.wait()
                # Send data object
                data_bytes = ctypes.cast(data_bytes_addr, ctypes.py_object).value
                s.sendall(data_bytes)
                counter += len(data_bytes)
                sent_event.set()
        else:
            # Iterate for the whole data object
            ready_event.wait()
            data_length_bytes = ctypes.cast(data_length_bytes_addr, ctypes.py_object).value
            data_length = int.from_bytes(data_length_bytes, "big")
            counter = 0
            while counter < data_length:
                ready_event.wait()
                sent_event.set()
    print("P1-CLIENT%d: Finished to transfer data on socket!" % client_idx)   

def main():
    sockets = []
    data_bytes = bytearray(PIPE_BUFF_SIZE)
    data_bytes_addr = id(data_bytes)
    data_length_bytes = bytearray(8)
    data_length_bytes_addr = id(data_length_bytes)
    
    # Create ready and sent event
    ready_event = Event()
    sent_event = Event()
    
    # Create all the client
    threads = []
    for idx, function in enumerate(data_to_send):
        threads.append(Thread(target=on_new_server, args=(functions_to_nodes[function], idx, sockets)))
        threads[-1].start()
    
    # Join all the threads
    [t.join() for t in threads]
    
    # Search for all data objects names
    all_names = [val for sub_list in data_to_send.values() for val in sub_list]
    all_names = list(dict.fromkeys(all_names))
    
    # Create threads
    threads = []
    for idx, function in enumerate(data_to_send):
        threads.append(Thread(target=send_data_to, args=(ready_event, sent_event, data_bytes_addr, data_length_bytes_addr, all_names, function, data_to_send[function], idx, sockets)))
        threads[-1].start()
    print("P1: All clients are set up!")
   
    # If data pipe does not exist, create it
    pipe_path = "/mnt/mem/vol2/data_pipe"
    if not os.path.exists(pipe_path):
        cmd = "mkfifo " + pipe_path
        os.system(cmd)
    elif not stat.S_ISFIFO(os.stat(pipe_path).st_mode):
        cmd = 'rm ' + pipe_path
        os.system(cmd)
        cmd = 'mkfifo ' + pipe_path
        os.system(cmd)
    
    # Read data object
    with open(pipe_path, "rb") as data_fifo:
        for data_obj in all_names:
            # Receive data object length
            print("P1: Receiving data object '%s' dimension from container..." % data_obj)
            packet_dim = data_fifo.read(8)
            data_length_bytes[:] = packet_dim
            data_length_bytes_addr = id(data_length_bytes)
            data_length = int.from_bytes(data_length_bytes, "big")
            print("P1: Data length %d received from container!" % data_length)
            counter = 0
            # Receive n-1 times full PIPE_BUFF_SIZE packets
            while counter + PIPE_BUFF_SIZE <= data_length:
                packet_data = data_fifo.read(PIPE_BUFF_SIZE)
                data_bytes[:] = packet_data          
                ready_event.set()
                counter += PIPE_BUFF_SIZE
                for i in range(len(data_to_send)):
                    sent_event.wait()
            if data_length - counter > 0:
                packet_data = data_fifo.read(PIPE_BUFF_SIZE)
                data_bytes[:len(packet_data)] = packet_data
                data_bytes[len(packet_data):] = bytearray(b'\x01' * (PIPE_BUFF_SIZE - len(packet_data)))
                ready_event.set()
                counter += len(packet_data)
                for i in range(len(data_to_send)):
                    sent_event.wait()
            print("P1: Data objcet '%s' received from container!" % data_obj)

    # Join all the threads
    [t.join() for t in threads]
    print("P1: All clients have finished to send data!")

    # Close the socket connections
    for idx, s in enumerate(sockets):
        print("P1-CLIENT%d: Closing connection..." % idx)
        s.close()

if __name__ == '__main__':
    main()
