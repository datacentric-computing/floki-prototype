#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>

using namespace std;

namespace fs = std::filesystem;

const int SOCKET_BUFF_SIZE = 8192;
const int PIPE_BUFF_SIZE = 8192;
const int SERVER_PORT = 4010;
const int NUM_THREAD = 1;

const list<string> out_objs_names = {"outA", "outB"};

const map<string, list<string>> data_to_send = {
    {"f2", {"outA", "outB"}}
};

const map<string, string> functions_to_nodes = {
    {"f2", "10.0.26.207" }
};

const char *sender_name = "f1";

bool data_ready_flags[NUM_THREAD] = {false};
bool sent_data_flags[NUM_THREAD] = {false};

unsigned char pipe_data_buffer[PIPE_BUFF_SIZE];
unsigned char pipe_dim_buffer[8];
uint64_t data_length = 0;

int open_and_connect_to_socket(string server_ip, int thread_id){
    // Open socket client connection
    struct sockaddr_in server;
    int port = 4010;
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd < 0){
        printf("SENDER-THREAD%d: : ERROR in opening the socket!\n", thread_id);
        exit(EXIT_FAILURE);
    }   
    printf("SENDER-THREAD%d: Successfully opened the socket!\n", thread_id);
    // Connect to socket server
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    server.sin_port = htons(port);
    if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
        printf("SENDER-THREAD%d: : ERROR in connecting to socket!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    printf("SENDER-THREAD%d: Connected to socket!\n", thread_id);
    return sock_fd;
}

int open_pipe(){
    char *pipe_path = "/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        mkfifo(pipe_path, 0644);
        printf("Main: Created data_pipe!\n");
    }
    // Open the pipe
    int pipe_fd = open(pipe_path, O_RDONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: data_pipe opened successfully!\n");
    return pipe_fd;
}

void send_function_name_over_network(int sock_fd, int thread_id){
    // Send function name length
    uint64_t name_length = strlen(sender_name);
    unsigned char *name_length_buff = static_cast<unsigned char*>(static_cast<void*>(&name_length));
    if (send(sock_fd,  name_length_buff, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name length!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    // Send function name
    if (send(sock_fd, sender_name, strlen(sender_name), 0) < 0){
        printf("SENDER-THREAD%d: : ERROR in sending function name!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    printf("SENDER-THREAD%d: Sent writer function name %s!\n", thread_id, sender_name);
}

void send_data_dim_over_network(int sock_fd, int thread_id, const char *data_obj_name, int count){
    if(count == 0){
        unsigned char ready[1] = {0x01};
        if (send(sock_fd, ready, 1, 0) < 0){
            printf("SENDER-THREAD%d: : ERROR in sending ready flag!\n", thread_id);
            exit(EXIT_FAILURE);
        }
    }    
    while(data_ready_flags[thread_id] == false);
    data_ready_flags[thread_id] = false;
    if (send(sock_fd, pipe_dim_buffer, 8, 0) < 0){
        printf("SENDER-THREAD%d: : ERROR in sending data object %s dimension!\n", thread_id, data_obj_name);
        exit(EXIT_FAILURE);
    }
    printf("SENDER-THREAD%d: Sent %s data object %s dimension!\n", thread_id, data_obj_name, data_obj_name);
    sent_data_flags[thread_id] = true;
}

void send_data_over_network(int sock_fd, int thread_id, const char *data_obj_name){
    uint64_t counter = 0;
    while (counter + PIPE_BUFF_SIZE < data_length){
        // Send full data packets to socket
        while(data_ready_flags[thread_id] == false);
        data_ready_flags[thread_id] = false;
        if (send(sock_fd, pipe_data_buffer, PIPE_BUFF_SIZE, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending data object %s!\n", thread_id, data_obj_name);
            exit(EXIT_FAILURE);
        }
        sent_data_flags[thread_id] = true;
        counter += PIPE_BUFF_SIZE;
    }
    if (data_length - counter > 0){
        // Send last not full data packets to socket
        while(data_ready_flags[thread_id] == false);
        data_ready_flags[thread_id] = false;
        if (send(sock_fd, pipe_data_buffer, counter - data_length, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending last packet of %s data object!\n", thread_id, data_obj_name);
            exit(EXIT_FAILURE);
        }
        //unique_lock<mutex> lck_sent(sent_data_mtx);
        sent_data_flags[thread_id] = true;
        counter += data_length - counter;
    }
    printf("SENDER-THREAD%d: Sent %s %lu/%lu bytes!\n", thread_id, data_obj_name, counter, data_length);
}

void send_data_to_function(int thread_id, string func_name, list<string> list_objs, string server_ip){
    printf("SENDER-THREAD%d: Sending data objects to function %s..\n", thread_id, func_name.c_str());
    // Open and connect to socket server
    int sock_fd = open_and_connect_to_socket(server_ip, thread_id);
    // Send the task name
    send_function_name_over_network(sock_fd, thread_id);
    // Send data dimension and data content
    std::list<string>::const_iterator it;
    int count = 0;
    for (it = list_objs.begin(); it != list_objs.end(); ++it){
        send_data_dim_over_network(sock_fd, thread_id, it->c_str(), count);
        send_data_over_network(sock_fd, thread_id, it->c_str());
        count++;
    }   
}

uint64_t get_data_length_from_buffer_bigendian(unsigned char* pipe_dim_buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (pipe_dim_buffer[7]) |
        (pipe_dim_buffer[6] << 8) |
        (pipe_dim_buffer[5] << 16)|
        (pipe_dim_buffer[4] << 24) |
        (pipe_dim_buffer[3] << 32)|
        (pipe_dim_buffer[2] << 40) |
        (pipe_dim_buffer[1] << 48)|
        (pipe_dim_buffer[0] << 56) );
}

uint64_t get_data_length_from_buffer_littleendian(unsigned char* pipe_dim_buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (pipe_dim_buffer[0]) |
        (pipe_dim_buffer[1] << 8) |
        (pipe_dim_buffer[2] << 16)|
        (pipe_dim_buffer[3] << 24) |
        (pipe_dim_buffer[4] << 32)|
        (pipe_dim_buffer[5] << 40) |
        (pipe_dim_buffer[6] << 48)|
        (pipe_dim_buffer[7] << 56) );
}
void read_data_dim_from_pipe(int pipe_fp, const char *data_obj_name){
    if(read(pipe_fp, pipe_dim_buffer, sizeof(pipe_dim_buffer)) < 0){
        printf("Main: ERROR Cannot read '%s' data object dimension from pipe!\n", data_obj_name);
        exit(EXIT_FAILURE);
    }
    data_length = get_data_length_from_buffer_bigendian(pipe_dim_buffer);
    printf("Main: Read '%s' data object dimension %lu from pipe!\n", data_obj_name, data_length);
    memset(data_ready_flags, 1, sizeof(data_ready_flags));
    while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != NUM_THREAD);
    memset(sent_data_flags, 0, sizeof sent_data_flags);
}

void read_data_from_pipe(int pipe_fp, const char *data_obj_name){
    printf("Main: Reading '%s' data object data from pipe..\n", data_obj_name);
    uint64_t counter = 0;
    int num_bytes = 0;
    while (counter + PIPE_BUFF_SIZE < data_length){
        // Read full data packets from pipe
        if(read(pipe_fp, pipe_data_buffer, PIPE_BUFF_SIZE) < 0){
            printf("Main: ERROR Cannot read data object packet from pipe!\n");
            exit(EXIT_FAILURE);
        }
        memset(data_ready_flags, 1, sizeof(data_ready_flags));
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != NUM_THREAD);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
        counter += PIPE_BUFF_SIZE;
    }
    if (data_length - counter > 0){
        // Read last not full data packet from pipe
        if(read(pipe_fp, pipe_data_buffer, data_length - counter) < 0){
            printf("Main: ERROR Cannot read last data object packet from pipe!\n");
            exit(EXIT_FAILURE);
        }
        memset(data_ready_flags, 1, sizeof(data_ready_flags));
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != NUM_THREAD);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
        counter += data_length - counter;
    }
    printf("Main: Read '%s' data object %lu/%lu bytes!\n", data_obj_name, counter, data_length);
}

int main (){
    thread threads[NUM_THREAD];
    for (int i=0; i<NUM_THREAD; ++i){
        auto it = data_to_send.begin();
        advance(it, i);
        string server_ip = functions_to_nodes.find(it->first)->second;
        threads[i] = thread(send_data_to_function, i, it->first, it->second, server_ip);
    }
    printf("Main: Created senders threads!\n");
    
    // Open pipe for reading
    int pipe_fp = open_pipe();

    // Read data objec from pipe
    std::list<string>::const_iterator it;
    for (it = out_objs_names.begin(); it != out_objs_names.end(); ++it){
        read_data_dim_from_pipe(pipe_fp, it->c_str());
        read_data_from_pipe(pipe_fp, it->c_str());
    }

    // Wait all threads to finish
    for (auto& th : threads) th.join();
    printf("Main: All senders threads have finished!\n");

    return 0;
}
