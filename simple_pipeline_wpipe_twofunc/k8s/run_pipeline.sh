#!/bin/bash

# Create all the memory pvs
kubectl apply -f pvs/pvMemNode1.yaml
kubectl apply -f pvs/pvMemNode2.yaml

# Create all the memory pvcs
kubectl apply -f pvcs/pvcNode1.yaml
kubectl apply -f pvcs/pvcNode2.yaml

# Apply the pipeline
kubectl apply -f pipeline.yaml

# Create the pipelinerun
kubectl create -f pipelinerun.yaml
