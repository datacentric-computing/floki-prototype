import os

# Writes data on local data named pipe
data = 'A' * 524288 + 'B' * 524288
data_dim = len(data)
data_as_bytes = str.encode(data)
print("Sending data dimension...")
data_fifo = open("data_pipe", 'wb')
data_dim_bin = data_dim.to_bytes(8, "big")
data_fifo.write(data_dim_bin)
print("Sent data dimension %d!" % data_dim)
print("Sending data...")
data_fifo.write(data_as_bytes)
print("Sent data!")
print("Closing data pipe")
data_fifo.close()
