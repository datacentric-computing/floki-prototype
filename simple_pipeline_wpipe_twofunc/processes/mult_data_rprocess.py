import os

buf_sz = 65536

# Read data from local data named pipe
# Open data pipe
data_fifo = open('data_pipe', 'rb')
print("Pipe opened!")
# Receive first data object
print("DATA1: Receiving data dimension...")
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
print("DATA1: Data length %d received!" % data_length)
print("DATA1: Receiving data...")
data1 = bytearray(0)
counter = 0
while counter < data_length:
    data1.extend(data_fifo.read(buf_sz))
    counter += buf_sz
print("DATA1: Received data!")
# Receive second data object
print("DATA2: Receiving data dimension...")
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
print("DATA2: Data length %d received!" % data_length)
print("DATA2: Receiving data...")
data2 = bytearray(0)
counter = 0
while counter < data_length:
    data2.extend(data_fifo.read(buf_sz))
    counter += buf_sz
print("DATA2: Received data!")
# Close data pipe
data_fifo.close()
