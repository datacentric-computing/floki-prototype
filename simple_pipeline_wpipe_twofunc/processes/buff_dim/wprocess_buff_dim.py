import os
import struct
import time 
import csv
from statistics import mean 

min_buf_sz = 4096
max_buf_sz = 65536
num_runs = 10

# Writes data on local data named pipe
half_gb = int(1024*1024*1024/2)
data = 'A' * half_gb + 'B' * half_gb
data_dim = len(data)
data_as_bytes = str.encode(data)
print("Sending data dimension...")
data_fifo = open("data_pipe", 'wb')
data_dim_bin = data_dim.to_bytes(8, "big")
data_fifo.write(data_dim_bin)
print("Sent data dimension %d!" % data_dim)
temp_buf_sz = min_buf_sz
write_times = []
pipe_stats = []
with open("buff_sizes_wtimes.csv", 'w') as fp:
    cols_names = ['buff_size', 'avg_time']
    writer = csv.writer(fp)
    writer.writerow(cols_names)
    while temp_buf_sz <= max_buf_sz:
        for i in range(num_runs):
            s_time = time.time()
            data_fifo.write(data_as_bytes)
            e_time = time.time()
            write_times.append(e_time - s_time)
        print("Buffer dimension %d avg total time: %f" % (temp_buf_sz, mean(write_times)))
        pipe_stats.append([temp_buf_sz, mean(write_times)])
        temp_buf_sz += min_buf_sz
    writer.writerows(pipe_stats)
