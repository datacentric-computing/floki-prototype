import os
import struct
import time
import csv
from statistics import mean

min_buf_sz = 4096
max_buf_sz = 65536

num_runs = 10

# Writes data on local data named pipe
data_fifo = open('data_pipe', 'rb')
print("Pipe opened!")
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
print("Data length %d received!" % data_length)
temp_buf_sz = min_buf_sz
read_times = []
pipe_stats = []
with open("buff_sizes_rtimes.csv", 'w') as fp:
    cols_names = ['buff_size', 'avg_time']
    writer = csv.writer(fp)
    writer.writerow(cols_names)
    while temp_buf_sz <= max_buf_sz:
        for i in range(num_runs):
            data = bytearray(0)
            counter = 0
            s_time = time.time()
            while counter < data_length:
                data.extend(data_fifo.read(temp_buf_sz))
                counter += temp_buf_sz
            e_time = time.time()
            read_times.append(e_time - s_time)
        print("Buffer dimension %d total time: %f" % (temp_buf_sz, mean(read_times)))
        pipe_stats.append([temp_buf_sz, mean(read_times)])
        temp_buf_sz += min_buf_sz    
    writer.writerows(pipe_stats)
