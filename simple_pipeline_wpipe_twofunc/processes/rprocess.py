import os

buf_sz = 65536

# Writes data on local data named pipe
data_fifo = open('data_pipe', 'rb')
print("Pipe opened!")
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
print("Data length %d received!" % data_length)
print("Receiving data...")
data = bytearray(0)
counter = 0
while counter < data_length:
    data.extend(data_fifo.read(buf_sz))
    counter += buf_sz
print("Data received!")
# Close data pipe
data_fifo.close()
