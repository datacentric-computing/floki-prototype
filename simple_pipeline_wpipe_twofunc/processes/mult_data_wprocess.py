import os
import time 
import csv
from statistics import mean 

# Writes data on local data named pipe
# Create two data objects
data1 = 'A' * 1024*1024*1024
data1_dim = len(data1)
data1_as_bytes = str.encode(data1)
data2 = 'B' * 1024*1024
data2_dim = len(data2)
data2_as_bytes = str.encode(data2)
# Open the data fifo
data_fifo = open("data_pipe", 'wb')
print("DATA1: Sending data dimension %d..." % data1_dim)
data1_dim_bin = data1_dim.to_bytes(8, "big")
data_fifo.write(data1_dim_bin)
print("DATA1: Sent data dimension %d!" % data1_dim)
print("DATA1: Sending data...")
data_fifo.write(data1_as_bytes)
print("DATA1: Sent data!")
# Send second data objcet
print("DATA2: Sending data dimension %d..." % data1_dim)
data2_dim_bin = data2_dim.to_bytes(8, "big")
data_fifo.write(data2_dim_bin)
print("DATA2: Sent data dimension %d!" % data2_dim)
print("DATA2: Sending data...")
data_fifo.write(data2_as_bytes)
print("DATA2: Sent data!")
# Close data pipe
data_fifo.close()
