#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <queue>
#include <atomic>
#include <vector>

using namespace std;

namespace fs = std::filesystem;

#define     SOCKET_BUFF_SIZE    8192
#define     PIPE_BUFF_SIZE      8192
#define     SERVER_PORT         4010
#define     NUM_THREADS_IN      1

const map<string, list<string>> data_to_recv = {
    {"f1", {"outA", "outB"}}
};

const char *this_node_ip = "10.0.26.207";

class ordered_lock {
    queue<reference_wrapper<condition_variable>> cond_var;
    mutex cond_var_lock;
    bool locked;
public:
    ordered_lock() : locked(false) {}
    void lock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if(locked){
            condition_variable signal;
            cond_var.emplace(std::ref(signal));
            signal.wait(acquire);
            cond_var.pop();
        }else{
            locked = true;
        }
    }
    void unlock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if (cond_var.empty()){
            locked = false;
        }else{
            cond_var.front().get().notify_one();
        }
    }
};

ordered_lock *global_lock = new ordered_lock();

int open_and_connect_to_socket(string server_ip, int thread_id){
    // Open socket client connection
    struct sockaddr_in server;
    int port = 4010;
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd < 0){
        printf("RECEIVER-THREAD%d: ERROR in opening the socket!\n", thread_id);
        exit(EXIT_FAILURE);
    }   
    printf("RECEIVER-THREAD%d: Successfully opened the socket!\n", thread_id);
    // Connect to socket server
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    server.sin_port = htons(port);
    if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
        printf("RECEIVER-THREAD%d: ERROR in connecting to socket!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    printf("RECEIVER-THREAD%d: Connected to socket!\n", thread_id);
    return sock_fd;
}

int open_pipe(string pipe_mode){
    char *pipe_path = "/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        mkfifo(pipe_path, 0644);
        printf("Main: Created data_pipe!\n");
    }
    // Open the pipe
    int pipe_fd = 0;
    if(strcmp(pipe_mode.c_str(), "read") == 0)
        pipe_fd = open("/mnt/mem/vol2/data_pipe", O_RDONLY);
    else
        pipe_fd = open("/mnt/mem/vol2/data_pipe", O_WRONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: data_pipe opened successfully!\n");
    return pipe_fd;
}

uint64_t get_length_from_buffer_socket_bigendian(unsigned char* buffer){
    return static_cast<uint64_t>(
        (buffer[7]) |
        (buffer[6] << 8) |
        (buffer[5] << 16) |
        (buffer[4] << 24) |
        (buffer[3] << 32) |
        (buffer[2] << 40) |
        (buffer[1] << 48) |
        (buffer[0] << 56));
}

uint64_t get_length_from_buffer_socket_littleendian(unsigned char* buffer){
    return static_cast<uint64_t>(
        (buffer[0]) |
        (buffer[1] << 8) |
        (buffer[2] << 16) |
        (buffer[3] << 24) |
        (buffer[4] << 32) |
        (buffer[5] << 40) |
        (buffer[6] << 48) |
        (buffer[7] << 56));
}

void get_n_bytes_from_socket(int socket_conn_fd, unsigned char *buffer, int n, int thread_id){
    int rec = 0;
    do{
        int nbytes = recv(socket_conn_fd, &buffer[rec], n-rec, 0);
        rec += nbytes;

    }while(rec < n);
}

string recv_sender_func_name_from_socket(int socket_conn_fd, int thread_id){
    unsigned char func_name_length_buff[8];
    // Receive function name length
    get_n_bytes_from_socket(socket_conn_fd, func_name_length_buff, 8, thread_id);
    uint64_t name_length = get_length_from_buffer_socket_littleendian(func_name_length_buff);
    // Receive function name
    unsigned char func_name_buff[name_length];
    char func_name[name_length + 1];
    get_n_bytes_from_socket(socket_conn_fd, func_name_buff, name_length, thread_id);
    memcpy(func_name, func_name_buff, sizeof(func_name_buff));
    func_name[sizeof(func_name_buff)] = 0;
    string func_name_string(func_name);
    printf("RECEIVER-THREAD%d: Received sender name %s!\n", thread_id, func_name_string.c_str());
    return func_name_string;
}

int recv_data_dim_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, int pipe_fd){
    unsigned char data_length_buff[8];
    get_n_bytes_from_socket(socket_conn_fd, data_length_buff, 8, thread_id);
    uint64_t data_length = get_length_from_buffer_socket_bigendian(data_length_buff);
    write(pipe_fd, data_length_buff, 8);
    printf("RECEIVER-THREAD%d: Received and sent to container %s data object length %lu!\n", thread_id, obj_name, data_length);
    return data_length;
}   

void recv_data_obj_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, uint64_t data_obj_length, int pipe_fd){
    uint64_t counter = 0;
    unsigned char data_obj_buff[SOCKET_BUFF_SIZE];
    while (counter + SOCKET_BUFF_SIZE < data_obj_length){
        // Receive full data packets from socket 
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, SOCKET_BUFF_SIZE, thread_id);
        // Send full data packets to pipe
        write(pipe_fd, data_obj_buff, PIPE_BUFF_SIZE);
        counter += SOCKET_BUFF_SIZE;
    }
    if(data_obj_length - counter > 0){
        // Receive not full data packet from socket
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, data_obj_length - counter, thread_id);  
        // Send last not full data packet to pipe
        write(pipe_fd, data_obj_buff, data_obj_length - counter);
        counter += data_obj_length - counter;
    }
    printf("RECEIVER-THREAD%d: %s data object received %lu/%lu bytes!\n", thread_id, obj_name, counter, data_obj_length);
}

bool check_data_ready(int socket_conn_fd){
    unsigned char ready[1];
    recv(socket_conn_fd, &ready, 1, 0);
    return ready;
}

void receive_data_from_function(int thread_id, int server_socket_fd, struct sockaddr_in server_adrr, int server_addr_len, int pipe_fd){
    uint64_t data_obj_length = 0;
    // Accept connetion 
    int socket_conn_fd;
    if ((socket_conn_fd = accept(server_socket_fd, (struct sockaddr *)&server_adrr, (socklen_t*)&server_addr_len))<0){
        printf("RECEIVER-THREAD%d: ERROR Cannot accept connection!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    printf("RECEIVER-THREAD%d: Accepted connection!\n", thread_id);
    // Receive sender function name
    string func_name = recv_sender_func_name_from_socket(socket_conn_fd, thread_id);
    // Receive the ready signal from the sender
    while (!check_data_ready(socket_conn_fd));
    // Lock the data_pipe
    global_lock->lock();
    printf("RECEIVER-THREAD%d: Got the lock!\n", thread_id);
    // Receive function data objects
    list<string> list_objs = data_to_recv.find(func_name.c_str())->second;
    list<string>::const_iterator it;
    for (it = list_objs.begin(); it != list_objs.end(); ++it){
        data_obj_length = recv_data_dim_from_socket(socket_conn_fd, thread_id, it->c_str(), pipe_fd);
        recv_data_obj_from_socket(socket_conn_fd, thread_id, it->c_str(), data_obj_length, pipe_fd);
    }
    // Release the lock on data_pipe
    global_lock->unlock();
    printf("RECEIVER-THREAD%d: Released the lock!\n", thread_id);
}

void setup_server_socket(int &server_socket_fd, struct sockaddr_in server_addr, int &server_addr_len){
    int server_socket;
    server_addr_len = sizeof(server_addr);
    // Define the server socket
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd < 0){
        printf("Main: ERROR in opening the server socket!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: Successfully opened the server socket!\n");
    server_addr.sin_addr.s_addr = inet_addr(this_node_ip);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    
    // Bind the socket to the address and port number
    bind(server_socket_fd, (struct sockaddr*)&server_addr, server_addr_len);

    // Listen on the socket (max 10 connections)
    if (listen(server_socket_fd, 10) == 0){
        printf("Main: Server socket listening..\n");
    }else{
        printf("Main: ERROR in server socket listening!\n");
        exit(EXIT_FAILURE);
    }
}

int main (){
    int server_socket_fd = 0;
    struct sockaddr_in server_addr;
    int server_addr_len = 0;

    // Define, bind and listen to the socket
    setup_server_socket(server_socket_fd, server_addr, server_addr_len);

    // Open data_pipe
    string pipe_mode = "write";
    int pipe_fd = open_pipe(pipe_mode);
    
    // Create the different receivers threads
    thread threads_in[NUM_THREADS_IN];
    for (int i = 0; i < NUM_THREADS_IN; ++i){
        threads_in[i] = thread(receive_data_from_function, i, server_socket_fd, server_addr, server_addr_len, pipe_fd);
    }
    printf("Main: Created all receivers threads!\n");
    
    // Wait all threads to finish
    for (auto& th : threads_in) th.join();
    printf("Main: All receivers threads have finished!\n");

    return 0;
}
