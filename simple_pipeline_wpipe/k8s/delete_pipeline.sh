#!/bin/bash

# Delete pipelineruns
kubectl delete pipelinerun --all 2> /dev/null

# Delete pipelines
kubectl delete pipeline --all 2> /dev/null

# Delete tasks
kubectl delete task --all 2> /dev/null

# Delete all the disk pvcs
kubectl delete pvc task1-pv-claim 2> /dev/null
kubectl delete pvc task2-pv-claim 2> /dev/null
kubectl delete pvc task3-pv-claim 2> /dev/null

# Delete all the memory pvs
kubectl delete pv tekton-mem-pv-hostpath1 2> /dev/null
kubectl delete pv tekton-mem-pv-hostpath2 2> /dev/null
kubectl delete pv tekton-mem-pv-hostpath3 2> /dev/null
