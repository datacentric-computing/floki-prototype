#!/bin/bash

# Check if all worker nodes have the volumes set up
workers_ips=('10.0.26.206' 
        '10.0.26.207' 
        '10.0.26.208'
) 

# Create hostpaths if they do not exist, and change their permissions and owner
for ip in "${workers_ips[@]}"; do 
    sshpass -p 'root' ssh -o LogLevel=QUIET -t root@${ip} "if [ ! -d /mnt/mem/vol2 ]; then echo 'Creating volume root folder on ${ip}...' && mkdir -p /mnt/mem/vol2; fi"
    # If mem configuration, mount the tmpfs
    sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} "if ! mount | grep /mnt/mem/vol2; then echo 'Mounting tmpfs on ${ip}...' && mount -t tmpfs -o size=5g tmpfs /mnt/mem/vol2; fi"
done
