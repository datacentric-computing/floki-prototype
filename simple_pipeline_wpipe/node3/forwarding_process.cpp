#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <queue>
#include <atomic>
#include <vector>

using namespace std;

namespace fs = std::filesystem;

#define     SOCKET_BUFF_SIZE    8192
#define     PIPE_BUFF_SIZE      8192
#define     SERVER_PORT         4010
#define     NUM_THREADS_IN      2

const map<string, list<string>> data_to_recv = {
    {"f1", {"outA"}},
    {"f2", {"outC"}}
};

const char *this_node_ip = "10.0.26.208";

class ordered_lock {
    queue<reference_wrapper<condition_variable>> cond_var;
    mutex cond_var_lock;
    bool locked;
public:
    ordered_lock() : locked(false) {}
    void lock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if(locked){
            condition_variable signal;
            cond_var.emplace(std::ref(signal));
            signal.wait(acquire);
            cond_var.pop();
        }else{
            locked = true;
        }
    }
    void unlock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if (cond_var.empty()){
            locked = false;
        }else{
            cond_var.front().get().notify_one();
        }
    }
};

ordered_lock *global_lock = new ordered_lock();

int open_pipe(string pipe_mode){
    char *pipe_path = (char *)"/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        mkfifo(pipe_path, 0644);
        printf("Main: Created data_pipe!\n");
    }
    // Open the pipe
    int pipe_fd = 0;
    if(strcmp(pipe_mode.c_str(), "read") == 0)
        pipe_fd = open(pipe_path, O_RDONLY);
    else
        pipe_fd = open(pipe_path, O_WRONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: data_pipe opened successfully in %s mode!\n", pipe_mode.c_str());
    return pipe_fd;
}

uint64_t get_length_from_buffer_socket_bigendian(unsigned char* buffer){
    return static_cast<uint64_t>(
        (buffer[7]) |
        (buffer[6] << 8) |
        (buffer[5] << 16) |
        (buffer[4] << 24) |
        (uint64_t(buffer[3]) << 32) |
        (uint64_t(buffer[2]) << 40) |
        (uint64_t(buffer[1]) << 48) |
        (uint64_t(buffer[0]) << 56));
}

uint64_t get_length_from_buffer_socket_littleendian(unsigned char* buffer){
    return static_cast<uint64_t>(
        (buffer[0]) |
        (buffer[1] << 8) |
        (buffer[2] << 16) |
        (buffer[3] << 24) |
        (uint64_t(buffer[4]) << 32) |
        (uint64_t(buffer[5]) << 40) |
        (uint64_t(buffer[6]) << 48) |
        (uint64_t(buffer[7]) << 56));
}

void get_n_bytes_from_socket(int socket_conn_fd, unsigned char *buffer, int n, int thread_id){
    int rec = 0;
    do{
        int nbytes = recv(socket_conn_fd, &buffer[rec], n-rec, 0);
        rec += nbytes;

    }while(rec < n);
}

string recv_sender_func_name_from_socket(int socket_conn_fd, int thread_id){
    unsigned char func_name_length_buff[8];
    // Receive function name length
    get_n_bytes_from_socket(socket_conn_fd, func_name_length_buff, 8, thread_id);
    uint64_t name_length = get_length_from_buffer_socket_littleendian(func_name_length_buff);
    // Receive function name
    unsigned char func_name_buff[name_length];
    char func_name[name_length + 1];
    get_n_bytes_from_socket(socket_conn_fd, func_name_buff, name_length, thread_id);
    memcpy(func_name, func_name_buff, sizeof(func_name_buff));
    func_name[sizeof(func_name_buff)] = 0;
    string func_name_string(func_name);
    printf("RECEIVER-THREAD%d: Received sender name %s!\n", thread_id, func_name_string.c_str());
    return func_name_string;
}

int recv_data_dim_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, int pipe_fd){
    unsigned char data_length_buff[8];
    get_n_bytes_from_socket(socket_conn_fd, data_length_buff, 8, thread_id);
    uint64_t data_length = get_length_from_buffer_socket_bigendian(data_length_buff);
    write(pipe_fd, data_length_buff, 8);
    printf("RECEIVER-THREAD%d: Received and sent to container %s data object length %lu!\n", thread_id, obj_name, data_length);
    return data_length;
}   

void recv_data_obj_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, uint64_t data_obj_length, int pipe_fd){
    uint64_t counter = 0;
    unsigned char data_obj_buff[SOCKET_BUFF_SIZE];
    while (counter + SOCKET_BUFF_SIZE <= data_obj_length){
        // Receive full data packets from socket 
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, SOCKET_BUFF_SIZE, thread_id);
        // Send full data packets to pipe
        write(pipe_fd, data_obj_buff, PIPE_BUFF_SIZE);
        counter += SOCKET_BUFF_SIZE;
    }
    if(data_obj_length - counter > 0){
        // Receive not full data packet from socket
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, data_obj_length - counter, thread_id);  
        // Send last not full data packet to pipe
        write(pipe_fd, data_obj_buff, data_obj_length - counter);
        counter += data_obj_length - counter;
    }
    printf("RECEIVER-THREAD%d: Received and sent to container %s data object (%lu/%lu bytes)!\n", thread_id, obj_name, counter, data_obj_length);
}

bool check_data_ready(int socket_conn_fd){
    unsigned char ready[1];
    recv(socket_conn_fd, &ready, 1, 0);
    return ready;
}

void receive_data_from_function(int thread_id, int client_socket_fd, int pipe_fd){
    // Receive sender function name
    string func_name = recv_sender_func_name_from_socket(client_socket_fd, thread_id);
    // Receive the ready signal from the sender
    while (!check_data_ready(client_socket_fd));
    // Lock the data_pipe
    global_lock->lock();
    printf("RECEIVER-THREAD%d: Got the lock!\n", thread_id);
    // Receive function data objects
    list<string> list_objs = data_to_recv.find(func_name.c_str())->second;
    list<string>::const_iterator it;
    for (it = list_objs.begin(); it != list_objs.end(); it++){
        uint64_t data_obj_length = recv_data_dim_from_socket(client_socket_fd, thread_id, it->c_str(), pipe_fd);
        recv_data_obj_from_socket(client_socket_fd, thread_id, it->c_str(), data_obj_length, pipe_fd);
    }
    // Release the lock on data_pipe
    global_lock->unlock();
    printf("RECEIVER-THREAD%d: Released the lock!\n", thread_id);
}

void setup_server_socket_and_get_clients_conn(int *client_sockets_fd){
    int server_socket_fd, max_sd, sd, activity, new_socket, address_len;
    struct sockaddr_in address;
    int count_clients = 0;
    //set of socket descriptors 
    fd_set readfds;
    // Initialise all client_socket[] to 0 so not checked 
    for (int i = 0; i < NUM_THREADS_IN; i++){
        client_sockets_fd[i] = 0;
    }
    // Create the server socket
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd == 0){
        printf("Main: ERROR in opening the server socket!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: Successfully opened the server socket!\n");
    // Define the type of server socket
    address.sin_addr.s_addr = inet_addr(this_node_ip);
    address.sin_family = AF_INET;
    address.sin_port = htons(SERVER_PORT);
    // Bind the socket to the address and port number
    if (bind(server_socket_fd, (struct sockaddr*)&address, sizeof(address)) < 0){
        printf("Main: ERROR in binding the server socket!\n");
    }
    // Listen on the socket (max 10 connections)
    if (listen(server_socket_fd, NUM_THREADS_IN) == 0){
        printf("Main: Server socket listening..\n");
    }else{
        printf("Main: ERROR in server socket listening!\n");
        exit(EXIT_FAILURE);
    }
    // Accept incoming connections
    address_len = sizeof(address);
    printf("Main: Waiting for connections..\n");
    while(count_clients < NUM_THREADS_IN){
        // Clear the socket set
        FD_ZERO(&readfds);
        // Add master socket to set 
        FD_SET(server_socket_fd, &readfds);
        max_sd = server_socket_fd;  
        //add child sockets to set
        for (int i = 0 ; i < NUM_THREADS_IN; i++){
            //socket descriptor 
            sd = client_sockets_fd[i];  
            //if valid socket descriptor then add to read list 
            if(sd > 0)
                FD_SET( sd , &readfds);  
            //highest file descriptor number, need it for the select function
            if(sd > max_sd) 
                max_sd = sd; 
        }
        //wait for an activity on one of the sockets
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
        if ((activity < 0) && (errno!=EINTR))
            printf("Main: ERROR in selecting client sockets!\n");
        //If something happened on the master socket, then its an incoming connection
        if (FD_ISSET(server_socket_fd, &readfds)){
            if ((new_socket = accept(server_socket_fd, (struct sockaddr *)&address, (socklen_t*)&address_len))<0)
                printf("Main: ERROR in accepting client socket!\n");
            printf("Main: New client connection from ip %s!\n", inet_ntoa(address.sin_addr));
            
            //add new socket to array of sockets 
            for (int i = 0; i < NUM_THREADS_IN; i++){
                if( client_sockets_fd[i] == 0 ){
                    client_sockets_fd[i] = new_socket;
                    count_clients++;
                    break;
                }
            }
        }
    }
}
int main (){
    int client_sockets_fd[NUM_THREADS_IN];
    // Define, bind and listen to the socket
    setup_server_socket_and_get_clients_conn(client_sockets_fd);
    // Open data_pipe
    string pipe_mode = "write";
    int pipe_fd = open_pipe(pipe_mode);
    
    // Create the different receivers threads
    thread threads_in[NUM_THREADS_IN];
    for (int i = 0; i < NUM_THREADS_IN; i++){
        threads_in[i] = thread(receive_data_from_function, i, client_sockets_fd[i], pipe_fd);
    }
    printf("Main: Created all receivers threads!\n");
    
    // Wait all threads to finish
    for (auto& th : threads_in) th.join();
    printf("Main: All receivers threads have finished!\n");
    
    // Close pipe
    close(pipe_fd);
    printf("Main: Closed data pipe!\n");

    return 0;
}



