#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <queue>
#include <algorithm>
#include <vector>

#define     SOCKET_BUFF_SIZE    8192
#define     PIPE_BUFF_SIZE      8192
#define     SERVER_PORT         4010
#define     NUM_THREADS_IN      1
#define     NUM_THREADS_OUT     1

using namespace std;
namespace fs = std::filesystem;

const map<string, list<string>> data_to_recv = {
    {"f1", {"outA","outB"}}
};

const list<string> out_objs_names = {"outC"};
const map<string, list<string>> data_to_send = {
    {"f3", {"outC"}}
};

const map<string, string> functions_to_nodes = {
    {"f3", "10.0.26.208"}
};

const char *sender_name = "f2";

bool data_ready_flags[NUM_THREADS_OUT] = {false};
bool sent_data_flags[NUM_THREADS_OUT] = {false};

unsigned char pipe_data_buffer[PIPE_BUFF_SIZE];
unsigned char pipe_dim_buffer[8];
uint64_t data_length = 0;

string current_obj_name = out_objs_names.front();;
bool sent_curr_data_obj[NUM_THREADS_OUT];

const char *this_node_ip = "10.0.26.207";

class ordered_lock {
    queue<reference_wrapper<condition_variable>> cond_var;
    mutex cond_var_lock;
    bool locked;
public:
    ordered_lock() : locked(false) {}
    void lock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if(locked){
            condition_variable signal;
            cond_var.emplace(std::ref(signal));
            signal.wait(acquire);
            cond_var.pop();
        }else{
            locked = true;
        }
    }
    void unlock(){
        unique_lock<mutex> acquire(cond_var_lock);
        if (cond_var.empty()){
            locked = false;
        }else{
            cond_var.front().get().notify_one();
        }
    }
};

ordered_lock *global_lock = new ordered_lock();

void open_and_connect_to_sockets_servers(int *sockets_out_fds){
    // Open socket client connection
    struct sockaddr_in server;
    int port = 4010;
    int sock_fd;
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        auto it = data_to_send.begin();
        advance(it, i);
        string server_ip = functions_to_nodes.find((it->first).c_str())->second;
        // Open socket client connection 
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_fd < 0){
            printf("Main: ERROR in opening the socket connection to %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        }   
        printf("Main: Successfully opened the socket connection to %s!\n", server_ip.c_str());
        // Connect to socket server
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(server_ip.c_str());
        server.sin_port = htons(port);
        if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
            printf("Main: ERROR in connecting to socket server at %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        }
        printf("Main: Connected to socket server at %s!\n", server_ip.c_str());
        sockets_out_fds[i] = sock_fd;
    }
}

int open_pipe(string pipe_mode){
    char *pipe_path = (char*)"/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        mkfifo(pipe_path, 0644);
        printf("Main: Created data_pipe!\n");
    }
    // Open the pipe
    int pipe_fd = 0;
    if(strcmp(pipe_mode.c_str(), "read") == 0)
        pipe_fd = open(pipe_path, O_RDONLY);
    else
        pipe_fd = open(pipe_path, O_WRONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: data_pipe opened successfully in %s mode!\n", pipe_mode.c_str());
    return pipe_fd;
}

void send_function_name_over_network(int sock_fd, int thread_id){
    // Send function name length
    uint64_t name_length = strlen(sender_name);
    unsigned char *name_length_buff = static_cast<unsigned char*>(static_cast<void*>(&name_length));
    if (send(sock_fd,  name_length_buff, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name length!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    // Send function name
    if (send(sock_fd, sender_name, strlen(sender_name), 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    printf("SENDER-THREAD%d: Sent writer function name %s!\n", thread_id, sender_name);
}

void send_data_dim_over_network(int sock_fd, int thread_id, const char *data_obj_name, int count){
    if(count == 0){
        unsigned char ready[1] = {0x01};
        if (send(sock_fd, ready, 1, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending ready flag!\n", thread_id);
            exit(EXIT_FAILURE);
        }
    }    
    while(data_ready_flags[thread_id] == false);
    data_ready_flags[thread_id] = false;
    if (send(sock_fd, pipe_dim_buffer, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending data object %s dimension!\n", thread_id, data_obj_name);
        exit(EXIT_FAILURE);
    }
    printf("SENDER-THREAD%d: Sent %s data object dimension %lu!\n", thread_id, data_obj_name, data_length);
    sent_data_flags[thread_id] = true;
}

void send_data_over_network(int sock_fd, int thread_id, const char *data_obj_name){
    uint64_t counter = 0;
    while (counter + PIPE_BUFF_SIZE <= data_length){
        // Send full data packets to socket
        while(data_ready_flags[thread_id] == false);
        data_ready_flags[thread_id] = false;
        if (send(sock_fd, pipe_data_buffer, PIPE_BUFF_SIZE, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending packet %lu-%lu/%lu of %s data object!\n", thread_id, counter, counter + PIPE_BUFF_SIZE, data_length, data_obj_name);
            exit(EXIT_FAILURE);
        }
        sent_data_flags[thread_id] = true;
        counter += PIPE_BUFF_SIZE;
    }
    if (data_length - counter > 0){
        // Send last not full data packets to socket
        while(data_ready_flags[thread_id] == false);
        data_ready_flags[thread_id] = false;
        if (send(sock_fd, pipe_data_buffer, counter - data_length, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending last packet of %s data object!\n", thread_id, data_obj_name);
            exit(EXIT_FAILURE);
        }
        //unique_lock<mutex> lck_sent(sent_data_mtx);
        sent_data_flags[thread_id] = true;
        counter += data_length - counter;
    }
    printf("SENDER-THREAD%d: Sent %s %lu/%lu bytes!\n", thread_id, data_obj_name, counter, data_length);
}

void send_data_to_function(int thread_id, string func_name, list<string> list_objs, int socket_out_fd){
    printf("SENDER-THREAD%d: Sending data objects to function %s..\n", thread_id, func_name.c_str());
    // Send the task name
    send_function_name_over_network(socket_out_fd, thread_id);
    // Send data dimension and data content
    std::list<string>::const_iterator it;
    int count = 0;
    for (it = list_objs.begin(); it != list_objs.end(); it++){
        while(accumulate(begin(sent_curr_data_obj), end(sent_curr_data_obj), 0) != 0);
        if (strcmp(current_obj_name.c_str(), it->c_str()) == 0){
            send_data_dim_over_network(socket_out_fd, thread_id, it->c_str(), count);
            send_data_over_network(socket_out_fd, thread_id, it->c_str());
            count++;
            sent_curr_data_obj[thread_id] = 1;
        }
    }   
}

uint64_t get_length_from_buffer_socket_bigendian(unsigned char* buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (buffer[7]) |
        (buffer[6] << 8) |
        (buffer[5] << 16) |
        (buffer[4] << 24) |
        (uint64_t(buffer[3]) << 32) |
        (uint64_t(buffer[2]) << 40) |
        (uint64_t(buffer[1]) << 48) |
        (uint64_t(buffer[0]) << 56) );
}

uint64_t get_length_from_buffer_socket_littleendian(unsigned char* buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (buffer[0]) |
        (buffer[1] << 8) |
        (buffer[2] << 16) |
        (buffer[3] << 24) |
        (uint64_t(buffer[4]) << 32) |
        (uint64_t(buffer[5]) << 40) |
        (uint64_t(buffer[6])  << 48) |
        (uint64_t(buffer[7]) << 56) );
}

void read_data_dim_from_pipe(int pipe_fd, const char *data_obj_name, int active_senders){
    if(read(pipe_fd, pipe_dim_buffer, sizeof(pipe_dim_buffer)) < 0){
        printf("Main: ERROR Cannot read '%s' data object dimension from pipe!\n", pipe_dim_buffer);
        exit(EXIT_FAILURE);
    }
    data_length = get_length_from_buffer_socket_bigendian(pipe_dim_buffer);
    printf("Main: Read '%s' data object dimension %lu from pipe!\n", data_obj_name, data_length);
    memset(data_ready_flags, 1, sizeof(data_ready_flags));
    while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
    memset(sent_data_flags, 0, sizeof sent_data_flags);
}

void read_data_from_pipe(int pipe_fd, const char *data_obj_name, int active_senders){
    printf("Main: Reading '%s' data object data from pipe..\n", data_obj_name);
    uint64_t counter = 0;
    while (counter + PIPE_BUFF_SIZE <= data_length){
        // Read full data packets from pipe
        if(read(pipe_fd, pipe_data_buffer, PIPE_BUFF_SIZE) < 0){
            printf("Main: ERROR Cannot read data object packet from pipe!\n");
            exit(EXIT_FAILURE);
        }
        memset(data_ready_flags, 1, sizeof(data_ready_flags));
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
        counter += PIPE_BUFF_SIZE;
    }
    if (data_length - counter > 0){
        // Read last not full data packet from pipe
        if(read(pipe_fd, pipe_data_buffer, data_length - counter) < 0){
            printf("Main: ERROR Cannot read last data object packet from pipe!\n");
            exit(EXIT_FAILURE);
        }
        memset(data_ready_flags, 1, sizeof(data_ready_flags));
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
        counter += data_length - counter;
    }
    printf("Main: Finished to read '%s' data object from pipe (%lu/%lu bytes)!\n", data_obj_name, counter, data_length);
}


void get_n_bytes_from_socket(int socket_conn_fd, unsigned char *buffer, int n, int thread_id){
    int rec = 0;
    do{
        int nbytes = recv(socket_conn_fd, &buffer[rec], n-rec, 0);
        rec += nbytes;

    }while(rec < n);
}

string recv_sender_func_name_from_socket(int socket_conn_fd, int thread_id){
    unsigned char func_name_length_buff[8];
    // Receive function name length
    get_n_bytes_from_socket(socket_conn_fd, func_name_length_buff, 8, thread_id);
    uint64_t name_length = get_length_from_buffer_socket_littleendian(func_name_length_buff);
    // Receive function name
    unsigned char func_name_buff[name_length];
    char func_name[name_length + 1];
    get_n_bytes_from_socket(socket_conn_fd, func_name_buff, name_length, thread_id);
    memcpy(func_name, func_name_buff, sizeof(func_name_buff));
    func_name[sizeof(func_name_buff)] = 0;
    string func_name_string(func_name);
    printf("RECEIVER-THREAD%d: Received sender name %s!\n", thread_id, func_name_string.c_str());
    return func_name_string;
}

int recv_data_dim_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, int pipe_fd){
    unsigned char data_length_buff[8];
    get_n_bytes_from_socket(socket_conn_fd, data_length_buff, 8, thread_id);
    uint64_t data_length = get_length_from_buffer_socket_bigendian(data_length_buff);
    write(pipe_fd, data_length_buff, 8);
    printf("RECEIVER-THREAD%d: Received and sent to container %s data object length %lu!\n", thread_id, obj_name, data_length);
    return data_length;
}   

void recv_data_obj_from_socket(int socket_conn_fd, int thread_id, const char *obj_name, uint64_t data_obj_length, int pipe_fd){
    uint64_t counter = 0;
    unsigned char data_obj_buff[SOCKET_BUFF_SIZE];
    while (counter + SOCKET_BUFF_SIZE <= data_obj_length){
        // Receive full data packets from socket 
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, SOCKET_BUFF_SIZE, thread_id);
        // Send full data packets to pipe
        int num_bytes_written = write(pipe_fd, data_obj_buff, PIPE_BUFF_SIZE);
        counter += SOCKET_BUFF_SIZE;
    }
    if(data_obj_length - counter > 0){
        // Receive not full data packet from socket
        get_n_bytes_from_socket(socket_conn_fd, data_obj_buff, data_obj_length - counter, thread_id);  
        // Send last not full data packet to pipe
        write(pipe_fd, data_obj_buff, data_obj_length - counter);
        counter += data_obj_length - counter;
    }
    printf("RECEIVER-THREAD%d: Received and sent to container %s data object (%lu/%lu bytes)!\n", thread_id, obj_name, counter, data_obj_length);
}

bool check_data_ready(int socket_conn_fd){
    unsigned char ready[1];
    recv(socket_conn_fd, &ready, 1, 0);
    return ready;
}

void receive_data_from_function(int thread_id, int client_socket_fd, int pipe_fd){
    // Receive sender function name
    string func_name = recv_sender_func_name_from_socket(client_socket_fd, thread_id);
    // Receive the ready signal from the sender
    while (!check_data_ready(client_socket_fd));
    // Lock the data_pipe
    global_lock->lock();
    printf("RECEIVER-THREAD%d: Got the lock!\n", thread_id);
    // Receive function data objects
    list<string> list_objs = data_to_recv.find(func_name.c_str())->second;
    list<string>::const_iterator it;
    for (it = list_objs.begin(); it != list_objs.end(); it++){
        uint64_t data_obj_length = recv_data_dim_from_socket(client_socket_fd, thread_id, it->c_str(), pipe_fd);
        recv_data_obj_from_socket(client_socket_fd, thread_id, it->c_str(), data_obj_length, pipe_fd);
    }
    // Release the lock on data_pipe
    global_lock->unlock();
    printf("RECEIVER-THREAD%d: Released the lock!\n", thread_id);
}

void setup_server_socket_and_get_clients_conn(int *client_sockets_fds){
    int server_socket_fd, max_sd, sd, activity, new_socket, address_len;
    int count_clients = 0;
    struct sockaddr_in address;
    // Set of socket descriptors
    fd_set readfds;
    // Initialise all client_socket_fds[] to 0 so not checked
    for (int i = 0; i < NUM_THREADS_IN; i++){ 
        client_sockets_fds[i] = 0;
    }
    // Create the server socket
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd == 0){ 
        printf("Main: ERROR in creating the server socket!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: Successfully created the server socket!\n");
    // Define the type of server socket
    address.sin_addr.s_addr = inet_addr(this_node_ip);
    address.sin_family = AF_INET;
    address.sin_port = htons(SERVER_PORT);
    // Bind the socket to the address and port number
    if (bind(server_socket_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        printf("Main: ERROR in binding the server socket!\n"); 
    }
    // Listen on the socket
    if (listen(server_socket_fd, NUM_THREADS_IN) == 0){
        printf("Main: Server socket listening..\n");  
    }else{
        printf("Main: ERROR in server socket listening!\n");
        exit(EXIT_FAILURE);
    }
    address_len = sizeof(address);
    while(count_clients < NUM_THREADS_IN){
        // Clear the socket set
        FD_ZERO(&readfds);
        // Add master socket to set
        FD_SET(server_socket_fd, &readfds); 
        max_sd = server_socket_fd;
        // Add child sockets to set 
        for (int i = 0 ; i < NUM_THREADS_IN; i++){
            // If valid socket descriptor then add to read list 
            if (client_sockets_fds[i] > 0){
                FD_SET( sd , &readfds);
            }
            // Highest file descriptor number
            if (client_sockets_fds[i] > max_sd){
                max_sd = sd;
            }
        }
        // Wait for an activity on one of the sockets 
        activity = select(max_sd+1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno!=EINTR)){
            printf("Main: ERROR in selecting client sockets!\n");
        }
        // If something happened on the master socket, then its an incoming connection 
        if (FD_ISSET(server_socket_fd, &readfds)){
            if ((new_socket = accept(server_socket_fd, (struct sockaddr *)&address, (socklen_t*)&address_len))<0){
                printf("Main: ERROR in accepting client socket!\n");
            }
            printf("Main: New client connection from ip %s!\n", inet_ntoa(address.sin_addr));
            // Add new socket to array of sockets 
            for (int i = 0; i < NUM_THREADS_IN; i++){ 
                if( client_sockets_fds[i] == 0 ){ 
                    client_sockets_fds[i] = new_socket;
                    count_clients++;   
                    break;
                }
            }
        }
    }
}


void compute_active_senders(vector<int> *active_senders){
    list<string>::const_iterator it_out_objs;
    map<string,list<string>>::const_iterator it_data_to_send;
    int num_active_senders;
    for (int i = 0; i < out_objs_names.size(); i++){
        num_active_senders = 0;
        it_out_objs = out_objs_names.begin();
        advance(it_out_objs, i);
        for (it_data_to_send = begin(data_to_send); it_data_to_send != end(data_to_send); it_data_to_send++){ 
            if (find(begin(it_data_to_send->second), end(it_data_to_send->second), it_out_objs->c_str()) != end(it_data_to_send->second)){
                num_active_senders += 1;
            }
        }
        active_senders->push_back(num_active_senders);
    }
}

int main (){
    int client_sockets_fds[NUM_THREADS_IN];
    int sockets_out_fds[NUM_THREADS_OUT];
    vector<int> active_senders;

    // Define, bind and listen to the server socket and get client connections
    setup_server_socket_and_get_clients_conn(client_sockets_fds);

    // Open and connect to sockets servers
    open_and_connect_to_sockets_servers(sockets_out_fds);

    // Compute output data objects active senders
    compute_active_senders(&active_senders);

    // Open data_pipe
    string pipe_mode = "write";
    int pipe_fd = open_pipe(pipe_mode);
    
    // Create the different receivers threads
    thread threads_in[NUM_THREADS_IN];
    for (int i = 0; i < NUM_THREADS_IN; i++){
        threads_in[i] = thread(receive_data_from_function, i, client_sockets_fds[i], pipe_fd);
    }
    printf("Main: Created all receivers threads!\n");
    
    // Wait all threads to finish
    for (auto& th : threads_in) th.join();
    printf("Main: All receivers threads have finished!\n");

    // Close pipe in read mode
    close(pipe_fd);
    printf("Main: Closed data pipe!\n");

    // Open pipe for reading
    pipe_mode = "read";
    pipe_fd = open_pipe(pipe_mode);

    thread threads_out[NUM_THREADS_OUT];
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        auto it_send = data_to_send.begin();
        advance(it_send, i);
        string server_ip = functions_to_nodes.find(it_send->first)->second;
        threads_out[i] = thread(send_data_to_function, i, it_send->first, it_send->second, sockets_out_fds[i]);
    }
    printf("Main: Created senders threads!\n");
    
    // Read data objec from pipe
    for (int j=0; j < out_objs_names.size(); j++){
        auto it_out = out_objs_names.begin();
        advance(it_out, j);
        current_obj_name = it_out->c_str();
        memset(sent_curr_data_obj, 0, sizeof(sent_curr_data_obj));
        read_data_dim_from_pipe(pipe_fd, it_out->c_str(), active_senders[j]);
        read_data_from_pipe(pipe_fd, it_out->c_str(), active_senders[j]);
        while(std::accumulate(std::begin(sent_curr_data_obj), std::end(sent_curr_data_obj), 0) != active_senders[j]);
        printf("Main: All senders have sent data object %s!\n", it_out->c_str());
    }

    // Wait all threads to finish
    for (auto& th : threads_out) th.join();
    printf("Main: All senders threads have finished!\n");
    
    // Close pipe in read mode
    close(pipe_fd);
    printf("Main: Closed data pipe!\n");
    
    return 0;
}
