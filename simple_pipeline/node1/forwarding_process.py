import socket
import os 
import sys
import getopt
from threading import Thread

# Define the data to be sent 
data_to_send = [(['out1'], 'f2'),
        (['out1', 'out2'], 'f3')]

# Define the mapping between the following functions and the nodes
functions_to_nodes = {'f2': '10.0.26.207',
        'f3': '10.0.26.208'}

# Define the configuration to use ('mem' or 'disk')
config = 'disk'                                      

def on_new_server(list_objs, function, client_idx):
    # Get the server IP address
    server_ip = functions_to_nodes[function]
    print("P1-CLIENT%d: Server IP: %s" % (client_idx, server_ip))

    # Define the server
    server = (server_ip, 4010)
    
    # Create a TCP/IP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port where the server is listening
    s.connect(server)
    print("P1-CLIENT%d: Client started!" % client_idx)

    print("P1-CLIENT%d: Sending function name..." % client_idx)
    s.send("_NAME_".encode())
    s.send("f1".encode())

    # Send all files necessary to the following function
    vol_path = '/mnt/' + config + '/vol2/out_data'
    for idx, file_name in enumerate(list_objs):
        # Send new file signal
        s.send("_NEW_".encode())
        print("P1-CLIENT%d: Sending file %s.." % (client_idx, file_name))
        file_to_send = open(vol_path + '/' + file_name + '.txt', 'rb')
        data = file_to_send.read(1024)
        while data:
            s.send(data)
            data = file_to_send.read(1024)
        file_to_send.close()
        # Send done signal
        if idx == len(list_objs) - 1:
            s.send("_DONE_".encode())
        print("P1-CLIENT%d: Finished to transfer data!" % client_idx)   
    print("P1-CLIENT%d: Closing connection..." % client_idx)
    s.close()

def main():

    # Read the sync pipe waiting for a ready signal
    with open("/mnt/" + config + "/vol2/sync_pipe", "r") as sync_pipe:
        print("P1: Waiting ready signal from X_E_CONT1...")
        while True:
            data = sync_pipe.read()
            if len(data) == 0 or data == '1':
                print("P1: Read the ready signal from X_E_CONT1!")
                break

    # Create all the clients
    threads = []
    for idx, data_function in enumerate(data_to_send):
        threads.append(Thread(target=on_new_server, args=(data_function[0], data_function[1], idx)))
        threads[-1].start()
    # Join all the threads before closing the socket
    [t.join for t in threads]

if __name__ == '__main__':
    main()
