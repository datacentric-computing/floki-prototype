import socket
import os 
from threading import Thread
import re

# Define the data to be received
data_to_recv = [(['out1', 'out2'], 'f1'), 
        (['out1'], 'f2')]

# Define the configuration to use ('mem' or 'disk')
config = 'disk'

def find_occurrences(string):
    # Finding the nth occurence of substring _NEW_
    occ_list = [m.start() for m in re.finditer(r"_NEW_", string)]
    return occ_list
   
def get_sender_name(data):
    start = data.find("_NAME_") + len("_NAME_")
    end = data.find("_NEW_")
    return data[start:end]

def on_new_client(clientsocket, address):
    # Receive the first packet of data
    data = (clientsocket.recv(1024)).decode()
    # Get the sender name
    sender_name = get_sender_name(data)

    # Write all data objects
    num_files = 0
    while True:
        # Find the number of new data objects in the current packet
        occ_list = find_occurrences(data)
        # Check the presence of new data objects
        if len(occ_list) == 0:
            # No new data objects, dump the data on the current file
            if "_DONE_" in data:
                # If done special string is present, remove it from data
                end = data.find("_DONE_")
                file.write(data[:end])
                file.close()
                print("P3: Communication finished!")
                break
            else:
                # Dump the entire chunk of data
                file.write(data)
        else:
            # Present at least one new data object
            if occ_list[0] > 0 and "_NAME_" not in data:
                # Present data belonging to the current data object, dump the data on the current file
                file.write(data[:occ_list[0]])
                file.close
            # Write the n new data objects
            for idx in range(len(occ_list)):
                num_files += 1
                # Open a new file 
                print("P3: Reading data %s..." % (sender_name + "_out" + str(num_files)))
                file_name = "/mnt/" + config + "/vol2/in_data/" + sender_name + "_out" + str(num_files) + ".txt"
                file = open(file_name, "w")
                # Write the data on the new file
                start = occ_list[idx] + len("_NEW_")
                if idx < len(occ_list) - 1:
                    # Current data object is not the last one in the packet
                    file.write(data[start:occ_list[idx+1]])
                    file.close()
                else:
                    if "_DONE_" in data:
                        done_pos = data.find("_DONE_")
                        file.write(data[start:done_pos])
                        file.close()
                        print("P3: Communication finished!")
                        clientsocket.close()
                        return
                    else:
                        file.write(data[start:])
        # Receive new packet of data
        data = clientsocket.recv(1024).decode()
    # Close socket connection
    clientsocket.close()

def main():
    host = '10.0.26.208'
    port = 4010

    # Create a TCP/IP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try: 
        # Bind the socket to the port
        s.bind((host, port))
    except socket.error as e:
        print(str(e))
    print("P3: Server started!")

    # Listen for incominc connections
    print("P3: Socket is listening...")
    s.listen(5)
    
    threads = []
    for i in range(len(data_to_recv)):
        # Wait for a connection
        print("P3: Waiting for a connection...")
        clientsocket, address = s.accept()
        print("P3: Connected to: " + address[0] + ":" + str(address[1]))
        threads.append(Thread(target=on_new_client, args=(clientsocket, address)))
        threads[-1].start()
    # Join all the threads before closing the socket
    [t.join() for t in threads]

    # Close the socket
    s.close()

    # Send ready signal to container X_S_CONT3
    print("P3: Writing ready signal to X_S_CONT3...")
    cmd = "echo 1 > /mnt/" + config + "/vol2/sync_pipe"
    print("P3: X_S_CONT3 read the ready signal!")
    os.system(cmd)

if __name__ == '__main__':
	main()
	
