#!/bin/bash

function usage(){
    echo "Usage: ./delete_pipeline.sh  -c 'configuration'"
    echo "  -c 'configuration'       specify configuration (mem or disk)"
}

# Check if an argument is given
if [ $# -lt 2 ]
then
    echo "ERROR: you have to provide the configuration to delete!"
    usage
    exit 1
fi

# Check if more than one argument is given
if [ $# -gt 2 ]
then
    echo echo "ERROR: you have to provide only one paramenter!"
    usage
    exit 1
fi

# Check the correctness of the given argument
key="$1"
if [[ $key == "-c" ]]
then
    config="$2"
    if ! [[ $config == mem || $config == disk ]]
    then
        echo "ERROR: you have to provide an allowed configuration!"
        usage
        exit 1
    fi
else
    echo "ERROR: you have to provide an allowed configuration!"
    usage
    exit 1
fi


# Delete pipelineruns
kubectl delete pipelinerun --all 2> /dev/null

# Delete pipelines
kubectl delete pipeline --all 2> /dev/null

# Delete tasks
kubectl delete task --all 2> /dev/null

# Delete all the disk pvcs
kubectl delete pvc task1-pv-claim 2> /dev/null
kubectl delete pvc task2-pv-claim 2> /dev/null
kubectl delete pvc task3-pv-claim 2> /dev/null

if [[ $config == mem ]]
then
    # Delete all the memory pvs
    kubectl delete pv tekton-mem-pv-hostpath1 2> /dev/null
    kubectl delete pv tekton-mem-pv-hostpath2 2> /dev/null
    kubectl delete pv tekton-mem-pv-hostpath3 2> /dev/null
else

    # Delete all the disk pvs
    kubectl delete pv tekton-disk-pv-hostpath1 2> /dev/null
    kubectl delete pv tekton-disk-pv-hostpath2 2> /dev/null
    kubectl delete pv tekton-disk-pv-hostpath3 2> /dev/null
fi
