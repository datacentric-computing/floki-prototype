#!/bin/bash

function usage(){
    echo "Usage: ./run.sh  -c 'configuration'"
    echo "  -c 'configuration'       specify configuration (mem or disk)"
}

# Check if an argument is given
if [ $# -lt 2 ]
then
    echo "ERROR: you have to provide the configuration to run!"
    usage
    exit 1
fi

# Check if more than one argument is given
if [ $# -gt 2 ]
then
    echo echo "ERROR: you have to provide only one paramenter!"
    usage
    exit 1
fi

# Check the correctness of the given argument
key="$1"
if [[ $key == "-c" ]]
then
    config="$2"
    if ! [[ $config == mem || $config == disk ]]
    then
        echo "ERROR: you have to provide an allowed configuration!"
        usage
        exit 1
    fi
else
    echo "ERROR: you have to provide an allowed configuration!"
    usage
    exit 1
fi

if [[ $config == mem ]]
then
    # Create all the memory pvs
    kubectl apply -f pvs/mem/pvMemNode1.yaml
    kubectl apply -f pvs/mem/pvMemNode2.yaml
    kubectl apply -f pvs/mem/pvMemNode3.yaml
else
    # Create all the disk pvs
    kubectl apply -f pvs/disk/pvDiskNode1.yaml
    kubectl apply -f pvs/disk/pvDiskNode2.yaml
    kubectl apply -f pvs/disk/pvDiskNode3.yaml
fi
# Create all the memory pvcs
kubectl apply -f pvcs/pvcNode1.yaml
kubectl apply -f pvcs/pvcNode2.yaml
kubectl apply -f pvcs/pvcNode3.yaml

# Apply the pipeline
kubectl apply -f pipeline.yaml

# Create the pipelinerun
kubectl create -f pipelinerun.yaml
