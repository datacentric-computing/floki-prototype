#!/bin/bash

function usage(){
    echo "Usage: ./check_hostpaths.sh  -c 'configuration'"
    echo "  -c 'configuration'       specify configuration (mem or disk)"
}

# Check if an argument is given
if [ $# -lt 2 ]
then
    echo "ERROR: you have to provide the configuration to check!"
    usage
    exit 1
fi

# Check if more than one argument is given
if [ $# -gt 2 ]
then
    echo echo "ERROR: you have to provide only one paramenter!"
    usage
    exit 1
fi

# Check the correctness of the given argument
key="$1"
if [[ $key == "-c" ]]
then
    config="$2"
    if ! [[ $config == mem || $config == disk ]]
    then
        echo "ERROR: you have to provide an allowed configuration!"
        usage
        exit 1
    fi
else
    echo "ERROR: you have to provide an allowed configuration!"
    usage
    exit 1
fi


# Check if all worker nodes have the volumes set up
workers_ips=('10.0.26.206' 
        '10.0.26.207' 
        '10.0.26.208'
) 

# Create hostpaths if they do not exist, and change their permissions and owner
for ip in "${workers_ips[@]}"; do 
    sshpass -p 'root' ssh -o LogLevel=QUIET -t root@${ip} "if [ ! -d /mnt/${config}/vol2 ]; then echo 'Creating volume root folder on ${ip}...' && mkdir -p /mnt/${config}/vol2; fi"
    # If mem configuration, mount the tmpfs
    if [[ $config == mem ]]; then
        sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} "if ! mount | grep /mnt/mem/vol2; then echo 'Mounting tmpfs on ${ip}...' && mount -t tmpfs -o size=5g tmpfs /mnt/mem/vol2; fi"
    fi
    sshpass -p 'root' ssh -o LogLevel=QUIET -t root@${ip} "if ! find /mnt/mem/vol2 -mindepth 1 -maxdepth 1 | read; then echo 'Creating volume subfolders and pipe on ${ip}...' && mkdir /mnt/${config}/vol2/in_data &&  mkdir /mnt/${config}/vol2/out_data && mkfifo /mnt/${config}/vol2/sync_pipe && chmod -R 777 /mnt/${config}/vol2 && chown -R vagrant:vagrant /mnt/${config}/vol2; fi"
done
