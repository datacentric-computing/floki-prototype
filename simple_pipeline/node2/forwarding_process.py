import socket
import os 
from threading import Thread
import re

host = '10.0.26.207'
port = 4010

# Define the data to be received
data_to_recv = [(['out1', 'out2'], 'f1')]

# Define the data to be sent
data_to_send = [(['out1'], 'f3')]

# Define the mapping between the following functions and the nodes
functions_to_nodes = {'f3': '10.0.26.208'}

# Define the configuration to use ('mem' or 'disk')
config = 'disk'


def find_occurrences(string):
    occ_list = [m.start() for m in re.finditer(r"_NEW_", string)]
    return occ_list

def get_sender_name(data):
    start = data.find("_NAME_") + len("_NAME_")
    end = data.find("_NEW_")
    return data[start:end]

def on_new_client(clientsocket, address):
    # Receive the first packet of data
    data = (clientsocket.recv(1024)).decode()
    # Get the sender name
    sender_name = get_sender_name(data)
    print("P2-SERVER: Function name %s" % sender_name)

    # Write all data objects
    num_files = 0
    while True:
        # Find the number of new data objects in the current packet
        occ_list = find_occurrences(data)
        # Check the presence of new data objects
        if len(occ_list) == 0:
            # No new data objects, dump the data on the current file
            if "_DONE_" in data:
                # If done special string is present, remove from data
                end = data.find("_DONE_")
                file.write(data[:end])
                file.close()
                print("P2-SERVER: Commnunication finished!")
                break
            else:
                # Dump the entire chunk of data
                file.write(data)
        else:
            # Present at least one new data object
            if occ_list[0] > 0 and "_NAME_" not in data:
                # Present data belonging to the current data object, dump the data on the current file
                file.write(data[:occ_list[0]])
                file.close()
            # Write the n new data objects
            for idx in range(len(occ_list)):
                num_files += 1 
                # Open a new file
                print("P2-SERVER: Reading data %s..." % (sender_name + '_out' + str(num_files)))
                file_name = "/mnt/" + config + "/vol2/in_data/" + sender_name + "_out" + str(num_files) + ".txt"
                file = open(file_name, "w")
                # Write the data on the new file
                start = occ_list[idx] + len("_NEW_")
                if idx < len(occ_list) - 1:
                     # Current data object is not the last one in the packet
                     file.write(data[start:occ_list[idx+1]])
                     file.close()
                else:
                     if "_DONE_" in data:
                         done_pos = data.find("_DONE_")
                         file.write(data[start:done_pos])
                         file.close()
                         print("P2-SERVERCommunication finished!")
                         clientsocket.close()
                         return 
                     else:
                        file.write(data[start:])
        # Receive new packet of data
        data = clientsocket.recv(1024).decode()
    # Close socket connection
    clientsocket.close()

def on_new_server(list_objs, function, client_idx):
    # Get the server IP address
    server_ip = functions_to_nodes[function]
    print("P2-CLIENT%d: Server IP: %s" % (client_idx, server_ip))
    
    # Define the server
    server = (server_ip, 4010)
    
    # Create a TCP/IP socket
    c_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    c_s.connect(server)
    print("P2-CLIENT%d: Client to %s:4010 started!" % (client_idx, server_ip))

    # Send the name of the function producing the data
    print("P2-CLIENT%d: Sending function name..." % client_idx)
    c_s.send("_NAME_".encode())
    c_s.send("f2".encode())

    # Send all files produced
    vol_path = '/mnt/' + config + '/vol2/out_data'
    for idx, file_name in enumerate(list_objs):
        # Send new file signal
        c_s.send("_NEW_".encode())
        print("P2-CLIENT%d: Sending file %s..." % (client_idx, file_name))
        file_to_send = open(vol_path + '/' + file_name + '.txt', 'rb')
        data = file_to_send.read(1024)
        while data:
            c_s.send(data)
            data = file_to_send.read(1024)
        file_to_send.close()
        # Send done signal
        if idx == len(list_objs) - 1:
            c_s.send("_DONE_".encode())
    print("P2-CLIENT%d: Finished to transfer data!" % client_idx)   
    print("P2-CLIENT%d: Closing connection..." % client_idx)
    c_s.close()

def main():
    # SERVER SOCKET
    # Create a TCP/IP socket
    s_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        # Bind the socket to the port
        s_s.bind((host, port))
    except socket.error as e:
        print(str(e))
    print("P2: Server started!")

    # Listen for incoming connections
    print("P2-SERVER: Socket is listening...")
    s_s.listen(5)
    
    threads = []
    for i in range(len(data_to_recv)):
        # Wait for a connection
        print("P2-SERVER: Waiting for a connection...")
        clientsocket, address = s_s.accept()
        print("P2-SERVER: Connectd to: " + address[0] + ":" + str(address[1]))
        threads.append(Thread(target=on_new_client, args=(clientsocket, address)))
        threads[-1].start()

    # Join all the threads
    [t.join for t in threads]
    
    # Close the server socket 
    s_s.close()
   
    # Send ready signal to X_S_CONT2
    print("P2: Writing ready signal to X_S_CONT2...")
    cmd = "echo 1 > /mnt/" + config + "/vol2/sync_pipe"
    os.system(cmd)
    print("P2: X_S_CONT2 read the ready signal!") 

    # Read the sync pipe waiting for a ready signal
    with open("/mnt/" + config + "/vol2/sync_pipe", "r") as sync_pipe:
        print("P2: Waiting ready signal from X_E_CONT2...")
        while True:
            data = sync_pipe.read()
            if len(data) == 0 or data == '1':
                print("P2: Read the ready signal from X_E_CONT2!")
                break

    # CLIENT SOCKETS
    threads = []
    for idx, data_function in enumerate(data_to_send):
        threads.append(Thread(target=on_new_server, args=(data_function[0], data_function[1], idx)))
        threads[-1].start()
    
    # Join all the threads
    [t.join for t in threads]

if __name__ == '__main__':
    main()
