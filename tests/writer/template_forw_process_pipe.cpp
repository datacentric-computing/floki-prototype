#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstdlib> 

using namespace std;

namespace fs = std::filesystem;

#define     NUM_RUNS            10
#define     SOCKET_BUFF_SIZE    65536
#define     PIPE_BUFF_SIZE      65536
#define     SERVER_PORT         4010
#define     NUM_THREADS_OUT     SET_NUM_THREADS_OUT

const list<string> out_objs_names = {
    OUT_OBJS_NAMES_LIST
};

const map<string, list<string>> data_to_send = {
    DATA_TO_SEND_LIST
};

const map<string, string> functions_to_nodes = {
    FUNCTIONS_TO_NODES_LIST
};

const char *sender_name = FUNCTION_NAME;

bool data_ready_flags[NUM_THREADS_OUT] = {false};
bool sent_data_flags[NUM_THREADS_OUT] = {false};

unsigned char pipe_data_buffer[PIPE_BUFF_SIZE];
unsigned char pipe_dim_buffer[8];
uint64_t data_length = 0;
uint64_t counter_pipe = 0;

string current_obj_name;
bool sent_curr_data_obj[NUM_THREADS_OUT];

list<string> dims {
    "1 MB",
    "2 MB",
    "4 MB", 
    "8 MB",
    "16 MB",
    "32 MB",
    "64 MB",
    "128 MB",
    "256 MB", 
    "512 MB",
    "1 GB",
    "2 GB",
    "4 GB", 
    "8 GB",
    "16 GB"
};

void get_socket_send_buff_size(int socket_fd){
    unsigned int size = 0;
    socklen_t socklen = sizeof(size);
    getsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, &socklen);
    printf("Main: Socket send buffer size is %u!\n", size);
}

void set_socket_send_buff_size(int socket_fd, size_t size){
    socklen_t socklen = sizeof(size);
#if defined(PRINT)
    get_socket_send_buff_size(socket_fd);
#endif 
    if (setsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, socklen) == -1) {
        printf("Main: Failed to set the socket send buffer size to %zu!\n", size);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT)
    get_socket_send_buff_size(socket_fd);
#endif 
}

void open_and_connect_to_sockets_servers(int *sockets_out_fds){
    struct sockaddr_in server;
    int port = 4010;
    int sock_fd;
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        auto it = data_to_send.begin();
        advance(it, i);
        // Retrieve the socket server ip
        string server_ip = functions_to_nodes.find(it->first)->second;
        // Open socket client connection
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_fd < 0){
            printf("Main: ERROR in opening the socket towards %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        }   
        printf("Main: Successfully opened the socket connection towards %s!\n", server_ip.c_str());
        // Connect to socket server
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(server_ip.c_str());
        server.sin_port = htons(port);
        if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
            printf("Main: ERROR in connecting to socket server %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        }
        printf("Main: Connected to socket server %s!\n", server_ip.c_str());
        set_socket_send_buff_size(sock_fd, (size_t)SOCKET_BUFF_SIZE);
        printf("Main: Set the socket send buffer size!\n");
        sockets_out_fds[i] = sock_fd;
    }
}

int open_pipe(){
    char *pipe_path = (char*)"/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        mkfifo(pipe_path, 0644);
#if defined(PRINT) 
        printf("Main: Created data_pipe!\n");
#endif
    }
    // Open the pipe
    int pipe_fd = open(pipe_path, O_RDONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    printf("Main: data_pipe opened successfully in read mode!\n");
#endif
    return pipe_fd;
}

void send_function_name_over_network(int sock_fd, int thread_id){
    // Send function name length
    uint64_t name_length = strlen(sender_name);
    unsigned char *name_length_buff = static_cast<unsigned char*>(static_cast<void*>(&name_length));
    if (send(sock_fd,  name_length_buff, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name length!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    // Send function name
    if (send(sock_fd, sender_name, strlen(sender_name), 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name!\n", thread_id);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    printf("SENDER-THREAD%d: Sent writer function name %s!\n", thread_id, sender_name);
#endif
}

void send_data_dim_over_network(int sock_fd, int thread_id, const char *data_obj_name, int count){
    // Notify to receiver forwarding process data are ready
    uint64_t counter_socket = 8;
    if(count == 0){
        unsigned char ready = 0x01;
        if (send(sock_fd, &ready, 1, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending ready flag!\n", thread_id);
            exit(EXIT_FAILURE);
        }
    }    
    while(counter_socket != counter_pipe);
    if (send(sock_fd, pipe_dim_buffer, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending data object %s dimension!\n", thread_id, data_obj_name);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    printf("SENDER-THREAD%d: Sent %s data object %s dimension!\n", thread_id, data_obj_name, data_obj_name);
#endif
    sent_data_flags[thread_id] = true;
}

void send_data_over_network(int sock_fd, int thread_id, const char *data_obj_name){
    uint64_t counter_socket = 0;
    uint64_t bytes_miss = 0;
    while (counter_socket + PIPE_BUFF_SIZE <= data_length){
        // Send full data packets to socket
        counter_socket += PIPE_BUFF_SIZE;
        while(counter_socket != counter_pipe);
        if (send(sock_fd, pipe_data_buffer, PIPE_BUFF_SIZE, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending data object %s!\n", thread_id, data_obj_name);
            exit(EXIT_FAILURE);
        }
        sent_data_flags[thread_id] = true;
    }
    if (data_length - counter_socket > 0){
        // Send last not full data packets to socket
        bytes_miss = data_length - counter_socket;
        counter_socket = data_length;
        while(counter_socket != counter_pipe);
        if (send(sock_fd, pipe_data_buffer, bytes_miss, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending last packet of %s data object!\n", thread_id, data_obj_name);
            exit(EXIT_FAILURE);
        }
        sent_data_flags[thread_id] = true;
    }
#if defined(PRINT) 
    printf("SENDER-THREAD%d: Sent %s %lu/%lu bytes!\n", thread_id, data_obj_name, counter_socket, data_length);
#endif
}

void send_data_to_function(int thread_id, string func_name, list<string> list_objs, int socket_out_fd){
#if defined(PRINT) 
    printf("SENDER-THREAD%d: Sending data objects to function %s..\n", thread_id, func_name.c_str());
#endif
    // Send data dimension and data content
    std::list<string>::const_iterator it;
    int count = 0;
    for (it = list_objs.begin(); it != list_objs.end(); it++){
        // Wait until main() sends data
        while(accumulate(begin(sent_curr_data_obj), end(sent_curr_data_obj), 0) != 0);
        if (strcmp(it->c_str(), current_obj_name.c_str()) == 0){
            send_data_dim_over_network(socket_out_fd, thread_id, it->c_str(), count);
            send_data_over_network(socket_out_fd, thread_id, it->c_str());
            count++;
            sent_curr_data_obj[thread_id] = 1;
        }
    }   
}

uint64_t get_data_length_from_buffer_bigendian(unsigned char* buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (uint64_t(buffer[7])) |
        (uint64_t(buffer[6]) << 8) |
        (uint64_t(buffer[5]) << 16) |
        (uint64_t(buffer[4]) << 24) |
        (uint64_t(buffer[3]) << 32) |
        (uint64_t(buffer[2]) << 40) |
        (uint64_t(buffer[1]) << 48) |
        (uint64_t(buffer[0]) << 56) );
}

uint64_t get_data_length_from_buffer_littleendian(unsigned char* buffer){
    // Get data length from 64 bits integer in big endian
    return static_cast<uint64_t>(
        (uint64_t(buffer[0])) |
        (uint64_t(buffer[1]) << 8) |
        (uint64_t(buffer[2]) << 16) |
        (uint64_t(buffer[3]) << 24) |
        (uint64_t(buffer[4]) << 32) |
        (uint64_t(buffer[5]) << 40) |
        (uint64_t(buffer[6]) << 48) |
        (uint64_t(buffer[7]) << 56) );
}

void get_n_bytes_from_pipe(int pipe_fd, unsigned char *buffer, int n){
    int recv_bytes = 0;
    do{
        int bytes = read(pipe_fd, &buffer[recv_bytes], n-recv_bytes);
        if(bytes < 0){
            printf("Main: ERROR in reading data from pipe!\n");
            exit(EXIT_FAILURE);
        }
        recv_bytes += bytes;
    }while(recv_bytes < n);
}

void read_data_dim_from_pipe(int pipe_fd, const char *data_obj_name, int active_senders){
    get_n_bytes_from_pipe(pipe_fd, pipe_dim_buffer, sizeof(pipe_dim_buffer));
    counter_pipe = sizeof(pipe_dim_buffer);
    data_length = get_data_length_from_buffer_bigendian(pipe_dim_buffer);
#if defined(PRINT) 
    printf("Main: Read '%s' data object dimension %lu from pipe!\n", data_obj_name, data_length);
#endif
    while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
    memset(sent_data_flags, 0, sizeof sent_data_flags);
}

void read_data_from_pipe(int pipe_fd, const char *data_obj_name, int active_senders){
    counter_pipe = 0;
    uint64_t bytes_miss = 0;
    while (counter_pipe + PIPE_BUFF_SIZE <= data_length){
        // Read full data packets from pipe
        get_n_bytes_from_pipe(pipe_fd, pipe_data_buffer, PIPE_BUFF_SIZE);
        counter_pipe += PIPE_BUFF_SIZE;
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
    }
    if (data_length - counter_pipe > 0){
        // Read last not full data packet from pipe
        bytes_miss = data_length - counter_pipe;
        get_n_bytes_from_pipe(pipe_fd, pipe_data_buffer, bytes_miss);
        counter_pipe += bytes_miss;
        while(std::accumulate(std::begin(sent_data_flags), std::end(sent_data_flags), 0) != active_senders);
        memset(sent_data_flags, 0, sizeof sent_data_flags);
    }
#if defined(PRINT) 
    printf("Main: Finished to read '%s' data object from pipe (%lu/%lu bytes)!\n", data_obj_name, counter_pipe, data_length);
#endif
}

void compute_active_senders(vector<int> *active_senders){
    list<string>::const_iterator it_out_objs;
    map<string,list<string>>::const_iterator it_data_to_send;
    int num_active_senders;
    for (int i = 0; i < out_objs_names.size(); i++){ 
        num_active_senders = 0;
        it_out_objs = out_objs_names.begin();
        advance(it_out_objs, i);
        for (it_data_to_send = begin(data_to_send); it_data_to_send != end(data_to_send); it_data_to_send++){
            if (find(begin(it_data_to_send->second), end(it_data_to_send->second), it_out_objs->c_str()) != end(it_data_to_send->second)){
                num_active_senders += 1;
            }
        }
        active_senders->push_back(num_active_senders);
    }
}

int main (){
    int sockets_out_fds[NUM_THREADS_OUT];    
    vector<int> active_senders;
    
    // Compute output data objects active senders 
    compute_active_senders(&active_senders);
    
    // Setup outpu socket connections
    open_and_connect_to_sockets_servers(sockets_out_fds);

    // Send the task name
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        send_function_name_over_network(sockets_out_fds[i], i);
    }
    
    // Run multiple data dimension
    list<string>::iterator it_data_dim;
    for (it_data_dim = dims.begin(); it_data_dim != dims.end(); it_data_dim++){
        printf("*************************** Testing dimension %s ***************************\n", it_data_dim->c_str());
        // Dimension without spaces 
        string data_dim = it_data_dim->c_str();
        data_dim.erase(remove(data_dim.begin(), data_dim.end(), ' ')); 
        // Create the network times folder if it does not exists 
        string dir_path = "/mnt/mem/vol2/timestamps/socket_pipe/NUM_WTASKStoNUM_RTASKS/" + data_dim;
        const fs::path dir_path_fs{dir_path};
        if (fs::exists(dir_path_fs)){
            for (auto& path: fs::directory_iterator(dir_path_fs)) {
                fs::remove_all(path);
            }
        }else{
            string cmd = "mkdir -p " + dir_path;
            system(cmd.c_str());
            printf("Main: Created timestamps local directory!\n");
        }
        // Run multiple times
        for (int i = 0; i < NUM_RUNS; i++){
            printf("RUN %d..\n", i+1);
            // Open pipe for reading
            int pipe_fd = open_pipe();
            current_obj_name = out_objs_names.front();
            thread threads_out[NUM_THREADS_OUT];
            for (int k = 0; k < NUM_THREADS_OUT; k++){
                auto it_send = data_to_send.begin();
                advance(it_send, k);
                string server_ip = functions_to_nodes.find(it_send->first)->second;
                threads_out[k] = thread(send_data_to_function, k, it_send->first, it_send->second, sockets_out_fds[k]);
            }
#if defined(PRINT) 
            printf("Main: Created senders threads!\n");
#endif      
            // Read data objec from pipe
            for (int j = 0; j < out_objs_names.size(); j++){ 
                auto it_out = out_objs_names.begin();
                advance(it_out, j);
                current_obj_name = it_out->c_str();
                memset(sent_curr_data_obj, 0, sizeof(sent_curr_data_obj));
                read_data_dim_from_pipe(pipe_fd, it_out->c_str(), active_senders[j]);
                read_data_from_pipe(pipe_fd, it_out->c_str(), active_senders[j]);
                while(std::accumulate(std::begin(sent_curr_data_obj), std::end(sent_curr_data_obj), 0) != active_senders[j]);
#if defined(PRINT) 
                printf("Main: All senders have sent data object %s!\n", it_out->c_str());
#endif
            }
            // Wait all threads to finish
            for (auto& th : threads_out) th.join();
#if defined(PRINT) 
            printf("Main: All senders threads have finished!\n");
#endif
            // Close pipe
            close(pipe_fd);
#if defined(PRINT) 
            printf("Main: Closed data pipe!\n");
#endif
        }
    }
    // Close socket connections
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        close(sockets_out_fds[i]);
    }
    return 0;
}
