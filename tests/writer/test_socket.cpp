#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstdlib> 
#include <fstream>
#include <sys/time.h>
#include <cassert>

using namespace std;

namespace fs = std::filesystem;

#define     NUM_RUNS            100
#define     NUM_BUFF_DIMS            8
#define     SERVER_PORT         4010

uint64_t DATA_LEN = (uint64_t)16*1024*1024*1024;

list<string> buffer_dims {
    "4K",
    "8K",
    "16K",
    "32K",
    "64K",
    "128K",
    "256K",
    "512K"
};

uint64_t buffer_sizes[] = {
    4*1024,
    8*1024,
    16*1024,
    32*1024,
    64*1024,
    128*1024,
    256*1024,
    512*1024
};

void get_socket_send_buff_size(int socket_fd){
    unsigned int size = 0;
    socklen_t socklen = sizeof(size);
    getsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, &socklen);
    printf("Main: Socket send buffer size is %u!\n", size);
}

void set_socket_send_buff_size(int socket_fd, size_t size){
    socklen_t socklen = sizeof(size);
#if defined(PRINT)
    get_socket_send_buff_size(socket_fd);
#endif
    if (setsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, socklen) == -1) {
        printf("Main: Failed to set the socket send buffer size to %zu!\n", size);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT)
    get_socket_send_buff_size(socket_fd);
#endif
}

int open_and_connect_to_sockets_servers(){
    struct sockaddr_in server;
    int sock_fd;
    // Retrieve the socket server ip
    string server_ip = "10.0.26.207";
    // Open socket client connection
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd < 0){
        printf("Main: ERROR in opening the socket towards %s!\n", server_ip.c_str());
        exit(EXIT_FAILURE);
    } 
    printf("Main: Successfully opened the socket connection towards %s!\n", server_ip.c_str());
    // Connect to socket server
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    server.sin_port = htons(SERVER_PORT);
    if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
        printf("Main: ERROR in connecting to socket server %s!\n", server_ip.c_str());
        exit(EXIT_FAILURE);
    }
    printf("Main: Connected to socket server %s!\n", server_ip.c_str());
    return sock_fd;
}

void dump_timestamp_to_file(struct timeval timestamps[NUM_BUFF_DIMS][NUM_RUNS]){
    for (int i = 0; i < NUM_BUFF_DIMS; i++){
        auto it_buff_dim = buffer_dims.begin();
        advance(it_buff_dim, i);
        string net_fpath = "socket_buffer/" + *it_buff_dim + "/net_start_ts.txt";
        ofstream ts_info;
        ts_info.open(net_fpath, ios_base::app);
        if(!ts_info){ 
            printf("Main: ERROR network file could not be opened!\n");
            exit(EXIT_FAILURE);
        }
        for (int j = 0; j < NUM_RUNS; j++){
            double timestamp_sec = timestamps[i][j].tv_sec + timestamps[i][j].tv_usec/1000000.0;
            ts_info << fixed << timestamp_sec << endl;
        }
        ts_info.close();
    }
#if defined(PRINT)
    printf("Main: Written network start timestamp to file!\n");
#endif
}

void sendBuffer(int socket_out_fd, unsigned char *data, int len){
    int bytes_sent = 0;
    do{
        int nbytes = send(socket_out_fd, &data[bytes_sent], len-bytes_sent, 0);
        bytes_sent += nbytes;

    }while(bytes_sent < len);
}

void send_data_over_network(int socket_out_fd, uint64_t buff_size, string buff_dim, unsigned char *data_buffer){
    uint64_t counter = 0;
    // Send data
    while (counter + buff_size <= DATA_LEN){
        // Send full data packets to socket
        sendBuffer(socket_out_fd, data_buffer, buff_size);
        counter = counter + buff_size;
    }
    if (DATA_LEN - counter > 0){
        // Send last not full data packets to socket
        sendBuffer(socket_out_fd, data_buffer, DATA_LEN - counter);
        counter = DATA_LEN;
    }
#if defined(PRINT)
    printf("Main: Sent %lu/%lu bytes!\n", counter, DATA_LEN);
#endif
}

int main (){
    int socket_out_fd; 
    // Setup output socket connections
    socket_out_fd = open_and_connect_to_sockets_servers();
    // Run multiple data dimension
    struct timeval timestamps[size(buffer_sizes)][NUM_RUNS];
    for (int i = 0; i < size(buffer_sizes); i++){
        auto it_buff_dim = buffer_dims.begin();
        advance(it_buff_dim, i);
        printf("******************* SOCKET BUFFER TEST WITH %s *******************\n", it_buff_dim->c_str());
        // Dimension without spaces 
        string buff_dim = it_buff_dim->c_str();
        // Create the network times folder if it does not exists 
        string dir_path = "socket_buffer/" + buff_dim;
        const fs::path dir_path_fs{dir_path};
        if (fs::exists(dir_path_fs)){
            for (auto& path: fs::directory_iterator(dir_path_fs)) {
                fs::remove_all(path);
            }
        }else{
            string cmd = "mkdir -p " + dir_path;
            system(cmd.c_str());
#if defined(PRINT)
            printf("Main: Created local socket buffer timestamps directory!\n");
#endif
        }
        // Set socekt writer buffer size
        set_socket_send_buff_size(socket_out_fd, (size_t)buffer_sizes[i]);
        // Create 1GB of data
        unsigned char *data_buffer = (unsigned char*)malloc(buffer_sizes[i]);
        memset(data_buffer, 1, buffer_sizes[i]);
        // Run multiple times 
        for (int j = 0; j < NUM_RUNS; j++){
            printf("RUN %d..\n", j+1);
            // Send data content
            gettimeofday(&timestamps[i][j], 0);
            send_data_over_network(socket_out_fd, buffer_sizes[i], buff_dim, data_buffer);   
        }
        // Free data array
        free(data_buffer);
    }
    // Dump timestamps to files 
    dump_timestamps_to_files(timestamps);
    // Close socket connections
    close(socket_out_fd);
#if defined(PRINT)
    printf("Main: Closed the socket connections!\n");
#endif
    return 0;
}
