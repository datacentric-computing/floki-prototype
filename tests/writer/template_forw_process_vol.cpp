#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstdlib> 
#include <fstream>
#include <sys/time.h>

using namespace std;

namespace fs = std::filesystem;

#define     NUM_RUNS            10
#define     NUM_THREADS_OUT     SET_NUM_THREADS_OUT
#define     SOCKET_BUFF_SIZE    65536
#define     SERVER_PORT         4010


const map<string, list<string>> data_to_send = {
    DATA_TO_SEND_LIST
};

const map<string, string> functions_to_nodes = {
    FUNCTIONS_TO_NODES_LIST
};

const char *sender_name = SET_FUNCTION_NAME;

string CONFIG = SET_CONFIG_VAL; 

list<string> dims {
    "1 MB",
    "2 MB",
    "4 MB", 
    "8 MB",
    "16 MB",
    "32 MB",
    "64 MB",
    "128 MB",
    "256 MB", 
    "512 MB",
    "1 GB",
    "2 GB",
    "4 GB", 
    "8 GB",
    "16 GB"
};

void get_socket_send_buff_size(int socket_fd){
    unsigned int size = 0;
    socklen_t socklen = sizeof(size);
    getsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, &socklen);
    printf("Main: Socket send buffer size is %u!\n", size);
}

void set_socket_send_buff_size(int socket_fd, size_t size){
    socklen_t socklen = sizeof(size);
#if defined(PRINT)
    get_socket_send_buff_size(socket_fd);
#endif
    if (setsockopt(socket_fd, SOL_SOCKET, SO_SNDBUF, &size, socklen) == -1) {
        printf("Main: Failed to set the socket send buffer size to %zu!\n", size);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    get_socket_send_buff_size(socket_fd);
#endif
}

void open_and_connect_to_sockets_servers(int *sockets_out_fds){
    struct sockaddr_in server;
    int sock_fd;
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        auto it = data_to_send.begin();
        advance(it, i);
        // Retrieve the socket server ip
        string server_ip = functions_to_nodes.find(it->first)->second;
        // Open socket client connection
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_fd < 0){
            printf("Main: ERROR in opening the socket towards %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        } 
        printf("Main: Successfully opened the socket connection towards %s!\n", server_ip.c_str());
        // Connect to socket server
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(server_ip.c_str());
        server.sin_port = htons(SERVER_PORT);
        if (connect(sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
            printf("Main: ERROR in connecting to socket server %s!\n", server_ip.c_str());
            exit(EXIT_FAILURE);
        }
        printf("Main: Connected to socket server %s!\n", server_ip.c_str());
        set_socket_send_buff_size(sock_fd, (size_t)SOCKET_BUFF_SIZE);
        printf("Main: Set the socket send buffer size!\n");
        sockets_out_fds[i] = sock_fd;
    }
}

int open_pipe(){
    string pipe_path = "/mnt/" + CONFIG + "/vol2/sync_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo((char *)&pipe_path[0])){
        mkfifo((char *)&pipe_path[0], 0644);
#if defined(PRINT)
        printf("Main: Created sync_pipe!\n");
#endif
    }
    // Open the pipe
    int pipe_fd = open((char *)&pipe_path[0], O_RDONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening sync_pipe!\n");
        exit(EXIT_FAILURE);
    }
#if defined(PRINT)
    printf("Main: sync_pipe opened successfully in read mode!\n");
#endif
    return pipe_fd;
}

void send_function_name_over_network(int sock_fd, int thread_id){
    // Send function name length
    uint64_t name_length = strlen(sender_name);
    unsigned char *name_length_buff = static_cast<unsigned char*>(static_cast<void*>(&name_length));
    if (send(sock_fd,  name_length_buff, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name length!\n", thread_id);
        exit(EXIT_FAILURE);
    }
    if (send(sock_fd, sender_name, strlen(sender_name), 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending function name!\n", thread_id);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT)
    printf("SENDER-THREAD%d: Sent writer function name %s!\n", thread_id, sender_name);
#endif
}

void dump_timestamp_to_file(int thread_id, string d_nospace){
    string net_fpath = "/mnt/" + CONFIG + "/vol2/timestamps/socket/NUM_WTASKStoNUM_RTASKS/" + d_nospace + "/netSET_REPLICA_" + to_string(thread_id+1) + "_start_ts.txt";
    ofstream net_info;
    net_info.open(net_fpath, ios_base::app);
    if(!net_info){ 
        printf("RECEIVER-THREAD%d: ERROR network file could not be opened!", thread_id);
        exit(EXIT_FAILURE);
    }
    struct timeval timestamp;
    gettimeofday(&timestamp, 0);
    double timestamp_sec = timestamp.tv_sec + timestamp.tv_usec/1000000.0;
    net_info << fixed << timestamp_sec << endl;
    net_info.close();
#if defined(PRINT)
     printf("RECEIVER-THREAD%d: Written network start timestamp to file!", thread_id);
#endif
}



uint64_t send_data_dim_over_network(int sock_fd, int thread_id, string data_obj_name, string vol_path, string d_nospace){
    unsigned char pipe_dim_buffer[8];
    //Dump the timestamp before writing
    dump_timestamp_to_file(thread_id, d_nospace);
    uint64_t data_length = fs::file_size((vol_path + "/" + data_obj_name + ".txt").c_str());
    memcpy(pipe_dim_buffer, (unsigned char*)&data_length, sizeof(uint64_t));
    if (send(sock_fd, pipe_dim_buffer, 8, 0) < 0){
        printf("SENDER-THREAD%d: ERROR in sending data object %s dimension!\n", thread_id, data_obj_name.c_str());
        exit(EXIT_FAILURE);
    }
#if defined(PRINT)
    printf("SENDER-THREAD%d: Sent %lu data object %s dimension!\n", thread_id, data_length, data_obj_name.c_str());
#endif
    return data_length;
}

void send_data_over_network(int sock_fd, int thread_id, string data_obj_name, string vol_path, uint64_t data_length){
    unsigned char data_buffer[SOCKET_BUFF_SIZE];
    uint64_t counter = 0;
    // Open data file
    ifstream data;
    const string data_fpath = "/mnt/" + CONFIG + "/vol2/out_data/" + data_obj_name + ".txt";
    data.open(data_fpath, ios::binary);
    if (!data){
        printf("SENDER-THREAD%d: ERROR file '%s.txt'could not be opened!", thread_id, data_obj_name.c_str());
        exit(EXIT_FAILURE);
    }
    // Send data
    while (counter + SOCKET_BUFF_SIZE <= data_length){
        // Read full data packet from file
        data.read(reinterpret_cast<char*>(data_buffer), SOCKET_BUFF_SIZE);
        // Send full data packets to socket
        if (send(sock_fd, data_buffer, SOCKET_BUFF_SIZE, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending data object %s!\n", thread_id, data_obj_name.c_str());
            exit(EXIT_FAILURE);
        }
        counter += SOCKET_BUFF_SIZE;
    }
    if (data_length - counter > 0){
        // Read full data packet from file
        data.read(reinterpret_cast<char*>(data_buffer), data_length - counter);
        // Send last not full data packets to socket
        if (send(sock_fd, data_buffer, data_length - counter, 0) < 0){
            printf("SENDER-THREAD%d: ERROR in sending last packet of %s data object!\n", thread_id, data_obj_name.c_str());
            exit(EXIT_FAILURE);
        }
    }
    // Close data file
    data.close();
#if defined(PRINT)
    printf("SENDER-THREAD%d: Sent %s %lu/%lu bytes!\n", thread_id, data_obj_name.c_str(), counter, data_length);
#endif
}

void send_data_to_function(int thread_id, string func_name, list<string> list_objs, int socket_out_fd, string d_nospace){
#if defined(PRINT)
    printf("SENDER-THREAD%d: Sending data objects to function %s..\n", thread_id, func_name.c_str());
#endif
    string vol_path = "/mnt/" + CONFIG + "/vol2/out_data";
    // Send data content
    std::list<string>::const_iterator it;
    for (it = list_objs.begin(); it != list_objs.end(); it++){
        uint64_t data_length = send_data_dim_over_network(socket_out_fd, thread_id, *it, vol_path, d_nospace);
        send_data_over_network(socket_out_fd, thread_id, *it, vol_path, data_length);
    }   
}

bool recv_ready_signal_from_container(int sync_pipe_fd){
    unsigned char ready;
    read(sync_pipe_fd, &ready, 1);
    return ready;
}

int main (){
    int sockets_out_fds[NUM_THREADS_OUT];    
    // Setup output socket connections
    open_and_connect_to_sockets_servers(sockets_out_fds);
    // Send the task name
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        send_function_name_over_network(sockets_out_fds[i], i);
    }
    // Run multiple data dimension
    list<string>::iterator it_data_dim;
    for (it_data_dim = dims.begin(); it_data_dim != dims.end(); it_data_dim++){
        printf("******************* SOCKET MEC WITH %s TEST ON %s *******************\n", CONFIG.data(), it_data_dim->c_str());
        // Dimension without spaces 
        string data_dim = it_data_dim->c_str();
        data_dim.erase(remove(data_dim.begin(), data_dim.end(), ' ')); 
        // Create the network times folder if it does not exists 
        string dir_path = "/mnt/" + CONFIG + "/vol2/timestamps/socket/NUM_WTASKStoNUM_RTASKS/" + data_dim;
        const fs::path dir_path_fs{dir_path};
        if (fs::exists(dir_path_fs)){
            for (auto& path: fs::directory_iterator(dir_path_fs)) {
                fs::remove_all(path);
            }
        }else{
            string cmd = "mkdir -p " + dir_path + " && chmod -R 777 " + dir_path;
            system(cmd.c_str());
#if defined(PRINT)
            printf("Main: Created local timestamps directory!\n");
#endif
        }
        // Run multiple times
        for (int i = 0; i < NUM_RUNS; i++){
            printf("RUN %d..\n", i+1);
            // Open pipe for reading
            int sync_pipe_fd = open_pipe();
            // Wait ready signal from container
            while(!recv_ready_signal_from_container(sync_pipe_fd));
            // Create and start the senders threads
            thread threads_out[NUM_THREADS_OUT];
            for (int k = 0; k < NUM_THREADS_OUT; k++){
                auto it_send = data_to_send.begin();
                advance(it_send, k);
                string server_ip = functions_to_nodes.find(it_send->first)->second;
                threads_out[k] = thread(send_data_to_function, k, it_send->first, it_send->second, sockets_out_fds[k], data_dim);
            }
#if defined(PRINT)
            printf("Main: Created senders threads!\n");
#endif
            // Wait all threads to finish
            for (auto& th : threads_out) th.join();
#if defined(PRINT)
            printf("Main: All senders threads have finished!\n");
#endif
            // Close pipe
            close(sync_pipe_fd);
#if defined(PRINT)
            printf("Main: Closed sync pipe!\n");
#endif
        }
    }
    // Close socket connections
    for (int i = 0; i < NUM_THREADS_OUT; i++){
        close(sockets_out_fds[i]);
    }
    printf("Main: Closed all socket connections!\n");
    return 0;
}
