import time
import os
import fcntl
import sys

NUM_RUNS = 100
DATA_DIM = 16*1024*1024*1024
PRINT = 0

F_SETPIPE_SZ = 1031
F_GETPIPE_SZ = 1032

buffer_dims = [ 
    "4K",
    "8K",
    "16K",
    "32K",
    "64K",
    "128K",
    "256K",
    "512K"
]

buffer_sizes = [
    4*1024,
    8*1024,
    16*1024,
    32*1024,
    64*1024,
    128*1024,
    256*1024,
    512*1024
]

def main():
    data = bytearray(b'\x01') * DATA_DIM
    with open("/mnt/mem/vol2/data_pipe", "wb") as data_pipe:
        for idx, dim in enumerate(buffer_dims):
            print("******************* PIPE BUFFER TEST WITH %s *******************" % dim)
            if not os.path.isdir("../pipe_buffer/timestamps/" + dim):
                os.makedirs("../pipe_buffer/timestamps/" + dim)
            pipe_buff_size = fcntl.fcntl(data_pipe, F_GETPIPE_SZ)
            if pipe_buff_size != buffer_sizes[idx]:
                fcntl.fcntl(data_pipe, F_SETPIPE_SZ, int(buffer_sizes[idx]))
                if PRINT:
                    pipe_buff_size = fcntl.fcntl(data_pipe, F_GETPIPE_SZ)
                    print("Current pipe buffer size is %s!" % str(pipe_buff_size))
            with open("../pipe_buffer/timestamps/" + dim + "/start_ts.txt", "w") as ts_file:
                for i in range(NUM_RUNS):
                    print("RUN %s" % str(i+1))
                    ts_file.write("%f\n" % time.time())
                    num_bytes = data_pipe.write(data)
                    if num_bytes != DATA_DIM:
                        print("ERROR: Written %d/%d bytes only!" % (num_bytes, DATA_DIM))
                        exit(1)
                    if PRINT:
                        print("Written %d/%d bytes on pipe!" % (num_bytes, DATA_DIM))

if __name__ == '__main__':
    main()
