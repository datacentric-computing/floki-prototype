#!/bin/bash

# Store the given parameters
NUM_WTASKS=$1
NUM_RTASKS=$2
CONFIG=$3

IP_ADDRESSES=( $(cat ../../../cluster_ips.txt | grep 'worker' | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}') )

# Create temp folder 
mkdir -p temp_files

# Create all write nodes forwarding processes
for (( i=1; i<=$NUM_WTASKS; i++)); do
    # Copy template file
    cp ../writer/template_forw_process_vol.cpp temp_files/temp_w${i}_forw_process_vol.cpp
    
    # Add to fowarding process file NUM_THREADS_OUT, NUM_WTASKS, and NUM_RTASKS
    sed -i "s/SET_NUM_THREADS_OUT/${NUM_RTASKS}/g" temp_files/temp_w${i}_forw_process_vol.cpp

    # Create list data_objs_names
    for (( r=1; r<=$NUM_RTASKS; r++)); do
        if [[ $r < $NUM_RTASKS ]]; then
            echo "    \"out${i}\"," >> temp_out_objs_names.txt
        else
            echo "    \"out${i}\"" >> temp_out_objs_names.txt
        fi
    done
    # Add out_objs_names content to forwarding process file
    sed -i -e '/OUT_OBJS_NAMES_LIST/{r temp_out_objs_names.txt' -e 'd}' temp_files/temp_w${i}_forw_process_vol.cpp
    rm temp_out_objs_names.txt 

    # Create list data_to_send
    for (( r=1; r<=$NUM_RTASKS; r++)); do
        if [[ $r < $NUM_RTASKS ]]; then
            echo "    {\"reader${r}\", {\"out${i}\"}}," >> temp_data_list.txt
        else
            echo "    {\"reader${r}\", {\"out${i}\"}}" >> temp_data_list.txt
        fi
    done
    # Add data_to_send object to forwarding process file
    sed -i -e '/DATA_TO_SEND_LIST/{r temp_data_list.txt' -e 'd}' temp_files/temp_w${i}_forw_process_vol.cpp
    rm temp_data_list.txt

    # Create list functions_to_nodes
    for (( r=1; r<=$NUM_RTASKS; r++)); do
        ind=$((r+NUM_WTASKS-1))
        if [[ $r < $NUM_RTASKS ]]; then
            echo "    {\"reader${r}\", \"${IP_ADDRESSES[${ind}]}\"}," >> temp_functions_list.txt
        else
            echo "    {\"reader${r}\", \"${IP_ADDRESSES[${ind}]}\"}" >> temp_functions_list.txt
        fi
    done
    # Add functions_to_nodes object to forwarding process file
    sed -i -e '/FUNCTIONS_TO_NODES_LIST/{r temp_functions_list.txt' -e 'd}' temp_files/temp_w${i}_forw_process_vol.cpp
    rm temp_functions_list.txt
    
    # Set NUM_WTASKS, NUM_RTASKS, REPLICA in forwarding process file
    sed -i "s/NUM_WTASKS/${NUM_WTASKS}/g" temp_files/temp_w${i}_forw_process_vol.cpp
    sed -i "s/NUM_RTASKS/${NUM_RTASKS}/g" temp_files/temp_w${i}_forw_process_vol.cpp
    sed -i "s/SET_FUNCTION_NAME/\"writer${i}\"/g" temp_files/temp_w${i}_forw_process_vol.cpp
    sed -i "s/SET_REPLICA/${i}/g" temp_files/temp_w${i}_forw_process_vol.cpp
    sed -i "s/SET_CONFIG_VAL/\"${CONFIG}\"/g" temp_files/temp_w${i}_forw_process_vol.cpp
    echo "temp_w${i}_forw_process_vol.cpp generated!"

    # Copy forwarding process to remote node
    idx=$((i - 1))
    node_ip=${IP_ADDRESSES[${idx}]}
    sshpass -p 'root' scp -r temp_files/temp_w${i}_forw_process_vol.cpp root@${node_ip}:/home/vagrant
    echo "temp_w${i}_forw_process_vol.cpp copied to node${i}!"
done

# Create all read nodes forwarding processes
for (( i=1; i<=$NUM_RTASKS; i++)); do
    # Copy template file
    cp ../reader/template_forw_process_vol.cpp temp_files/temp_r${i}_forw_process_vol.cpp
    
    sed -i "s/SET_NUM_THREADS_IN/${NUM_WTASKS}/g" temp_files/temp_r${i}_forw_process_vol.cpp
    # Create list data_to_send
    for (( w=1; w<=$NUM_WTASKS; w++)); do
        if [[ $w < $NUM_WTASKS ]]; then
            echo "    {\"writer${w}\", {\"out${w}\"}}," >> temp_data_list.txt
        else
            echo "    {\"writer${w}\", {\"out${w}\"}}" >> temp_data_list.txt
        fi
    done
    # Add data_to_send object to forwarding process file
    sed -i -e '/DATA_TO_RECV_LIST/{r temp_data_list.txt' -e 'd}' temp_files/temp_r${i}_forw_process_vol.cpp
    rm temp_data_list.txt

    # Set node ip
    idx=$((NUM_WTASKS + i - 1))
    node_ip=${IP_ADDRESSES[${idx}]}
    
    # Set NUM_WTASKS, NUM_RTASKS, REPLICA in forwarding process file
    sed -i "s/SET_NODE_IP/\"${node_ip}\"/g" temp_files/temp_r${i}_forw_process_vol.cpp
    sed -i "s/NUM_WTASKS/${NUM_WTASKS}/g" temp_files/temp_r${i}_forw_process_vol.cpp
    sed -i "s/NUM_RTASKS/${NUM_RTASKS}/g" temp_files/temp_r${i}_forw_process_vol.cpp
    sed -i "s/SET_REPLICA/${i}/g" temp_files/temp_r${i}_forw_process_vol.cpp
    sed -i "s/SET_CONFIG_VAL/\"${CONFIG}\"/g" temp_files/temp_r${i}_forw_process_vol.cpp
    echo "temp_r${i}_forw_process_vol.cpp generated!"

    # Copy forwarding process to remote node
    sshpass -p 'root' scp -r temp_files/temp_r${i}_forw_process_vol.cpp root@${node_ip}:/home/vagrant
    echo "temp_r${i}_forw_process_vol.cpp copied to node$((NUM_WTASKS+i))!"
done
