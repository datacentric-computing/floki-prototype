Name:         write-read-array-pipeline-run-zgfzx
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-30T01:55:18Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-30T01:55:18Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-zgfzx-read1-task-gnrsk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zgfzx-read2-task-4fjg5:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zgfzx-read3-task-nzpvp:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zgfzx-write1-task-2pwsz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zgfzx-write2-task-g9kwp:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zgfzx-write3-task-rwgt2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-30T01:56:37Z
  Resource Version:  55516437
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-zgfzx
  UID:               5ab15552-d709-49ae-b785-4a93f5af3a33
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
Status:
  Completion Time:  2022-03-30T01:56:36Z
  Conditions:
    Last Transition Time:  2022-03-30T01:56:36Z
    Message:               Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
  Start Time:  2022-03-30T01:55:18Z
  Task Runs:
    write-read-array-pipeline-run-zgfzx-read1-task-gnrsk:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-30T01:56:35Z
        Conditions:
          Last Transition Time:  2022-03-30T01:56:35Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-read1-task-gnrsk-pod-jczgn
        Start Time:              2022-03-30T01:55:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://26ba8898ad34287816d66e9e4e874f67456761556b1e9246713a6b8f9b2a90fc
            Exit Code:     0
            Finished At:   2022-03-30T01:56:29Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:03Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://19139862cacdce1a526d57aae578865ea371ffdab8a040c418ed8cb8e07e542c
            Exit Code:     0
            Finished At:   2022-03-30T01:56:35Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:30Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-zgfzx-read2-task-4fjg5:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-30T01:56:34Z
        Conditions:
          Last Transition Time:  2022-03-30T01:56:34Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-read2-task-4fjg5-pod-2k47w
        Start Time:              2022-03-30T01:55:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6441e2f0bd2e566de0df6445cb8e7e395ea1a376dd702de908d70401d3700961
          Name:       s-start
          Terminated:
            Container ID:  docker://6f86b3fa0581b3af0b83bb8316fc544aa5498e6641fee6c5b1dbdb34a0ea0849
            Exit Code:     0
            Finished At:   2022-03-30T01:56:28Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:03Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:af220ba516b0c154461915f76b5b170ff8f4fb35a5c80f7e94577117f39b743c
          Name:            read
          Terminated:
            Container ID:  docker://6d42f1f3140765e15375186a5d6f242889a72830107f02a9d1f775b5017d81b1
            Exit Code:     0
            Finished At:   2022-03-30T01:56:34Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:29Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-zgfzx-read3-task-nzpvp:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-30T01:56:36Z
        Conditions:
          Last Transition Time:  2022-03-30T01:56:36Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-read3-task-nzpvp-pod-cccwb
        Start Time:              2022-03-30T01:55:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
          Name:       s-start
          Terminated:
            Container ID:  docker://b79f7b443c8165bef20885588a95b7594d7c692d6a212a10c7d8fd3fbff4c69a
            Exit Code:     0
            Finished At:   2022-03-30T01:56:29Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:03Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://3982fac8112905a19e1d2e85c580dae9fa2c0986727f0af83cda430741c19ebb
            Exit Code:     0
            Finished At:   2022-03-30T01:56:36Z
            Reason:        Completed
            Started At:    2022-03-30T01:56:30Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-ws
    write-read-array-pipeline-run-zgfzx-write1-task-2pwsz:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-30T01:55:49Z
        Conditions:
          Last Transition Time:  2022-03-30T01:55:49Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-write1-task-2pwsz-pod-47tzz
        Start Time:              2022-03-30T01:55:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://8eaf0be989aa0244bb02b88550d9e6318e872dcdab284e7375c3fe2d65443ab1
            Exit Code:     0
            Finished At:   2022-03-30T01:55:46Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:34Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://55206fea7f35a801bb972e16c981f20e17584d77a8ba3d44a146595dc81f0cfa
            Exit Code:     0
            Finished At:   2022-03-30T01:55:48Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:47Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 2147483648
bytes = bytearray(b'\x01') * 2147483648
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/2GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-zgfzx-write2-task-g9kwp:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-30T01:55:46Z
        Conditions:
          Last Transition Time:  2022-03-30T01:55:46Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-write2-task-g9kwp-pod-pr5qs
        Start Time:              2022-03-30T01:55:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://64b7c37b9ff1f67619ba4fbab8eb52b029a7aac94377fc58a6906983f2481616
            Exit Code:     0
            Finished At:   2022-03-30T01:55:44Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:41Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://3ac737177d5c520e4203307d8bd47f267d787334e3f377a79d2462f593c187fa
            Exit Code:     0
            Finished At:   2022-03-30T01:55:45Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:44Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 2147483648
bytes = bytearray(b'\x01') * 2147483648
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/2GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-zgfzx-write3-task-rwgt2:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-30T01:55:46Z
        Conditions:
          Last Transition Time:  2022-03-30T01:55:46Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zgfzx-write3-task-rwgt2-pod-mdm7z
        Start Time:              2022-03-30T01:55:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://0536e4e250691b5a732f2004c20c9cac679289651fd0b6c66c02deb1e4b8521b
            Exit Code:     0
            Finished At:   2022-03-30T01:55:44Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:36Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://a55bc3faba41c2a19159cb620c1edce9a737dd4fb7e19dcb2776cb6314fdb592
            Exit Code:     0
            Finished At:   2022-03-30T01:55:45Z
            Reason:        Completed
            Started At:    2022-03-30T01:55:44Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 2147483648
bytes = bytearray(b'\x01') * 2147483648
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/2GB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/2GB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/2GB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    87s (x2 over 87s)  PipelineRun  
  Normal  Running    87s (x2 over 87s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    59s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    59s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    56s (x2 over 56s)  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    11s                PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    10s                PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
