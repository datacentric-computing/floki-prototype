Name:         write-read-array-pipeline-run-xq4r8
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-30T03:04:12Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-30T03:04:12Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-xq4r8-read1-task-4vjzz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-xq4r8-read2-task-hk6xr:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-xq4r8-read3-task-mnzt6:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-xq4r8-write1-task-2h6fk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-xq4r8-write2-task-rk5kg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-xq4r8-write3-task-d7d27:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-30T03:07:59Z
  Resource Version:  55634500
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-xq4r8
  UID:               3a3bb40a-38cc-483a-85c9-2fa825b02b77
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
Status:
  Completion Time:  2022-03-30T03:07:59Z
  Conditions:
    Last Transition Time:  2022-03-30T03:07:59Z
    Message:               Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
  Start Time:  2022-03-30T03:04:12Z
  Task Runs:
    write-read-array-pipeline-run-xq4r8-read1-task-4vjzz:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-30T03:07:56Z
        Conditions:
          Last Transition Time:  2022-03-30T03:07:56Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-read1-task-4vjzz-pod-hkqjw
        Start Time:              2022-03-30T03:05:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://f2a7d1fae85c238230cf9eb3088d032a21f521b420f0fd433145db76ea7d639a
            Exit Code:     0
            Finished At:   2022-03-30T03:07:23Z
            Reason:        Completed
            Started At:    2022-03-30T03:05:17Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://7a0233cd599b5fe152954feaa59433ab4804a1fe9b15d12ef6a31d3cef976b30
            Exit Code:     0
            Finished At:   2022-03-30T03:07:57Z
            Reason:        Completed
            Started At:    2022-03-30T03:07:24Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-xq4r8-read2-task-hk6xr:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-30T03:07:51Z
        Conditions:
          Last Transition Time:  2022-03-30T03:07:51Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-read2-task-hk6xr-pod-2jlvp
        Start Time:              2022-03-30T03:05:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6441e2f0bd2e566de0df6445cb8e7e395ea1a376dd702de908d70401d3700961
          Name:       s-start
          Terminated:
            Container ID:  docker://cb1359656652f9e6f82aeaef6867234b4bf6526ffa071bad8a239d4e1cb00ed7
            Exit Code:     0
            Finished At:   2022-03-30T03:07:27Z
            Reason:        Completed
            Started At:    2022-03-30T03:05:18Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:af220ba516b0c154461915f76b5b170ff8f4fb35a5c80f7e94577117f39b743c
          Name:            read
          Terminated:
            Container ID:  docker://00a62f87158a1231cef0855b3e4dfd16e662c4c0e4636317122837a36d15f773
            Exit Code:     0
            Finished At:   2022-03-30T03:07:50Z
            Reason:        Completed
            Started At:    2022-03-30T03:07:28Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-xq4r8-read3-task-mnzt6:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-30T03:07:59Z
        Conditions:
          Last Transition Time:  2022-03-30T03:07:59Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-read3-task-mnzt6-pod-spj5h
        Start Time:              2022-03-30T03:05:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
          Name:       s-start
          Terminated:
            Container ID:  docker://75d6978408779ee5d4b53fb60b1c97ff4cb6d9ffc5c403e9cf9432ea3b33eead
            Exit Code:     0
            Finished At:   2022-03-30T03:07:20Z
            Reason:        Completed
            Started At:    2022-03-30T03:05:17Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://88c8c06e600c7f4eca3986232c6679a81f0cefe2e921b90b732918921ddffff7
            Exit Code:     0
            Finished At:   2022-03-30T03:07:58Z
            Reason:        Completed
            Started At:    2022-03-30T03:07:21Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-ws
    write-read-array-pipeline-run-xq4r8-write1-task-2h6fk:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-30T03:05:04Z
        Conditions:
          Last Transition Time:  2022-03-30T03:05:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-write1-task-2h6fk-pod-5lkjv
        Start Time:              2022-03-30T03:04:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://358f10bb79da7f2da9cdcbfb3d9b7af0824dd41045b56120ae7afa214000053b
            Exit Code:     0
            Finished At:   2022-03-30T03:05:00Z
            Reason:        Completed
            Started At:    2022-03-30T03:04:31Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://ae67a4096a651775fe341d73e4647f07f7bb97e972140c123c2fddc526cfe002
            Exit Code:     0
            Finished At:   2022-03-30T03:05:02Z
            Reason:        Completed
            Started At:    2022-03-30T03:05:01Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8589934592
bytes = bytearray(b'\x01') * 8589934592
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-xq4r8-write2-task-rk5kg:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-30T03:04:39Z
        Conditions:
          Last Transition Time:  2022-03-30T03:04:39Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-write2-task-rk5kg-pod-6svl9
        Start Time:              2022-03-30T03:04:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://3e4ec6cf9295d68bdc15b37ad9f6d01d2b4937b80fe3b9eb6fc303429d7b4e54
            Exit Code:     0
            Finished At:   2022-03-30T03:04:37Z
            Reason:        Completed
            Started At:    2022-03-30T03:04:27Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://8dad4f6f1a8f46c8b2f717c1d3e31f46faa1eeebeea5dc556fd7db5dc4709f7e
            Exit Code:     0
            Finished At:   2022-03-30T03:04:38Z
            Reason:        Completed
            Started At:    2022-03-30T03:04:38Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8589934592
bytes = bytearray(b'\x01') * 8589934592
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-xq4r8-write3-task-d7d27:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-30T03:04:49Z
        Conditions:
          Last Transition Time:  2022-03-30T03:04:49Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-xq4r8-write3-task-d7d27-pod-wzb2b
        Start Time:              2022-03-30T03:04:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://b5901a12342f379bb33624568a5d98cb49d3d941e357d74bb37a5374d8fa95c1
            Exit Code:     0
            Finished At:   2022-03-30T03:04:46Z
            Reason:        Completed
            Started At:    2022-03-30T03:04:29Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://e1988e634c29645d134819f2a5c68d168b4e7e890051d028b78d653d5c65e667
            Exit Code:     0
            Finished At:   2022-03-30T03:04:47Z
            Reason:        Completed
            Started At:    2022-03-30T03:04:46Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8589934592
bytes = bytearray(b'\x01') * 8589934592
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8GB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8GB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8GB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
Events:
  Type    Reason     Age                  From         Message
  ----    ------     ----                 ----         -------
  Normal  Started    3m56s                PipelineRun  
  Normal  Running    3m56s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    3m29s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    3m19s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    3m4s (x2 over 3m4s)  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    17s                  PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    12s                  PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                   PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
