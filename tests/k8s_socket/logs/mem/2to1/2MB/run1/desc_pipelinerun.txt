Name:         write-read-array-pipeline-run-rph8x
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-12T20:21:13Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-12T20:21:13Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-rph8x-read1-task-msvq2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rph8x-write1-task-w7z5r:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rph8x-write2-task-nv7lm:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-12T20:21:37Z
  Resource Version:  16186642
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-rph8x
  UID:               6f3feb77-1be7-4fd0-9e32-6973ab5f72dc
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
Status:
  Completion Time:  2022-03-12T20:21:37Z
  Conditions:
    Last Transition Time:  2022-03-12T20:21:37Z
    Message:               Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
  Start Time:  2022-03-12T20:21:13Z
  Task Runs:
    write-read-array-pipeline-run-rph8x-read1-task-msvq2:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-12T20:21:37Z
        Conditions:
          Last Transition Time:  2022-03-12T20:21:37Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rph8x-read1-task-msvq2-pod-wrcx6
        Start Time:              2022-03-12T20:21:24Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://fe56ca0e02fcdc56919029589cef13b916823945d73cede5a8d17562ff947900
            Exit Code:     0
            Finished At:   2022-03-12T20:21:35Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:35Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://b980ade332013599be1cabbbeaefb1adc7b4d4f4974da9046e3c278c1d46865a
            Exit Code:     0
            Finished At:   2022-03-12T20:21:36Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:36Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
   del data
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
   del data
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-rph8x-write1-task-w7z5r:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-12T20:21:24Z
        Conditions:
          Last Transition Time:  2022-03-12T20:21:24Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rph8x-write1-task-w7z5r-pod-x2fr9
        Start Time:              2022-03-12T20:21:13Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://4e0036123ee4198a4762a0fa98242d9d2d6bbd35b5182b223989806fcf6ec78e
            Exit Code:     0
            Finished At:   2022-03-12T20:21:23Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:23Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://07c3c3ff13f63fcfd92f45049624d28c8bf64a66870e62590dbe05556e082ace
            Exit Code:     0
            Finished At:   2022-03-12T20:21:24Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:24Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f" % time.time())
# Create data object
string = 'x' * 2097152
# Store timestamp after data creation
with open("/data/timestamps/socket/2to1/2MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-rph8x-write2-task-nv7lm:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-12T20:21:22Z
        Conditions:
          Last Transition Time:  2022-03-12T20:21:22Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rph8x-write2-task-nv7lm-pod-wt5nd
        Start Time:              2022-03-12T20:21:13Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://51321b2cc695f5350b7a73455872c64aa1eea1a5aae8bbcc71dcaf65ff9ca468
            Exit Code:     0
            Finished At:   2022-03-12T20:21:21Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:21Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://8c21de4349e3e4577720034b07844101ee6fb0c8c6d54d4ecf2b748dd815fc53
            Exit Code:     0
            Finished At:   2022-03-12T20:21:21Z
            Reason:        Completed
            Started At:    2022-03-12T20:21:21Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f" % time.time())
# Create data object
string = 'x' * 2097152
# Store timestamp after data creation
with open("/data/timestamps/socket/2to1/2MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to1/2MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/2to1/2MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    32s (x2 over 32s)  PipelineRun  
  Normal  Running    32s (x2 over 32s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    23s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    21s (x2 over 21s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
