Name:         write-read-array-pipeline-run-2tzt9
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-24T13:50:41Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-24T13:50:41Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-2tzt9-read1-task-wjs7l:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2tzt9-read2-task-m65xg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2tzt9-write1-task-pvdfl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2tzt9-write2-task-t4nhq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-24T13:51:27Z
  Resource Version:  42924971
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-2tzt9
  UID:               8f724bed-5bd6-408f-b6ca-f7fe65c823a6
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
Status:
  Completion Time:  2022-03-24T13:51:26Z
  Conditions:
    Last Transition Time:  2022-03-24T13:51:26Z
    Message:               Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-03-24T13:50:41Z
  Task Runs:
    write-read-array-pipeline-run-2tzt9-read1-task-wjs7l:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-24T13:51:26Z
        Conditions:
          Last Transition Time:  2022-03-24T13:51:26Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2tzt9-read1-task-wjs7l-pod-67ft6
        Start Time:              2022-03-24T13:51:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://104d148a13832023b77eac2bfddf2e455164832bbf7515b6cd2b5b5820d88078
            Exit Code:     0
            Finished At:   2022-03-24T13:51:18Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:18Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://445b73ce54871757a5936c8bbfef40bdb44f58b6935e2a1ffbee9868686fda02
            Exit Code:     0
            Finished At:   2022-03-24T13:51:25Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:18Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-2tzt9-read2-task-m65xg:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-24T13:51:15Z
        Conditions:
          Last Transition Time:  2022-03-24T13:51:15Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2tzt9-read2-task-m65xg-pod-rs5nr
        Start Time:              2022-03-24T13:51:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://a49d269ba90fcf30a04f6f497199c2690e1d2f450f7d6254de0c8003fa05b626
            Exit Code:     0
            Finished At:   2022-03-24T13:51:13Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:13Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://37ff485d02c6bec497cd4977ff06c3723dd961c29d4df0e3d60922efb6046fe2
            Exit Code:     0
            Finished At:   2022-03-24T13:51:15Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:14Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-2tzt9-write1-task-pvdfl:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-24T13:51:04Z
        Conditions:
          Last Transition Time:  2022-03-24T13:51:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2tzt9-write1-task-pvdfl-pod-pkgww
        Start Time:              2022-03-24T13:50:41Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://9791a17cb71a8d892050534c8a4285b9be63f67a847365261604063792ed515b
            Exit Code:     0
            Finished At:   2022-03-24T13:51:02Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:01Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://cc99795ca6a9950b00676d79226340f3bf9e52163572ad5aa9422937da4b062e
            Exit Code:     0
            Finished At:   2022-03-24T13:51:03Z
            Reason:        Completed
            Started At:    2022-03-24T13:51:02Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 134217728
# Store timestamp after data creation
with open("/data/timestamps/socket/2to2/128MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-2tzt9-write2-task-t4nhq:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-24T13:50:53Z
        Conditions:
          Last Transition Time:  2022-03-24T13:50:53Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2tzt9-write2-task-t4nhq-pod-4lkml
        Start Time:              2022-03-24T13:50:41Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://2f545f4a6334effda1fc50661bd2bf71afcbb1ea81ab2506d7133ef317b6dcd9
            Exit Code:     0
            Finished At:   2022-03-24T13:50:51Z
            Reason:        Completed
            Started At:    2022-03-24T13:50:51Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://2b8a14049155d209a35934765b30a904c10ba1d93f7b1dbc7f26e48e90e8dd2c
            Exit Code:     0
            Finished At:   2022-03-24T13:50:52Z
            Reason:        Completed
            Started At:    2022-03-24T13:50:51Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 134217728
# Store timestamp after data creation
with open("/data/timestamps/socket/2to2/128MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/2to2/128MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/2to2/128MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    53s (x2 over 53s)  PipelineRun  
  Normal  Running    53s (x2 over 53s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    41s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    30s (x2 over 30s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    19s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
