Name:         write-read-array-pipeline-run-vddp8
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-15T09:14:15Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-15T09:14:15Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-vddp8-read1-task-d7pg7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vddp8-write1-task-s4xq7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vddp8-write2-task-r5r2w:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vddp8-write3-task-xlcx2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vddp8-write4-task-jb6tj:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-15T09:14:40Z
  Resource Version:  22029141
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-vddp8
  UID:               5dfc7248-92b8-403a-a8ec-9cdc049112b4
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-03-15T09:14:39Z
  Conditions:
    Last Transition Time:  2022-03-15T09:14:39Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
        write4-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-read1-ws
  Start Time:  2022-03-15T09:14:15Z
  Task Runs:
    write-read-array-pipeline-run-vddp8-read1-task-d7pg7:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-15T09:14:39Z
        Conditions:
          Last Transition Time:  2022-03-15T09:14:39Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vddp8-read1-task-d7pg7-pod-jpsc4
        Start Time:              2022-03-15T09:14:29Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://6c92854c77f6d79e04b9adc55268768a9c427dbeb69767df8773b23ed89ad041
            Exit Code:     0
            Finished At:   2022-03-15T09:14:38Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:38Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://42a2e9c2fda79d3b50800dfebf20f258c31b532cc95f46e33b09fe77a5a53777
            Exit Code:     0
            Finished At:   2022-03-15T09:14:39Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:38Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f/n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f/n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f/n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.txt", "r") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
with open("/data/in_data/out4.txt", "r") as fp_data4:
   data = fp_data4.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f/n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-vddp8-write1-task-s4xq7:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-15T09:14:28Z
        Conditions:
          Last Transition Time:  2022-03-15T09:14:28Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vddp8-write1-task-s4xq7-pod-75xsp
        Start Time:              2022-03-15T09:14:15Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://2298947fc19e72781bf4cbe43502ee1b49acaf69f8aa086e0c791fc0457fedef
            Exit Code:     0
            Finished At:   2022-03-15T09:14:26Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:26Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://3ceae4ab9f99d66edf389e118dc50e8e682b58ca6dafc6d248f54b89e218cc6b
            Exit Code:     0
            Finished At:   2022-03-15T09:14:26Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:26Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 33554432
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/32MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-vddp8-write2-task-r5r2w:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-15T09:14:29Z
        Conditions:
          Last Transition Time:  2022-03-15T09:14:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vddp8-write2-task-r5r2w-pod-x2p84
        Start Time:              2022-03-15T09:14:15Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://c83834a94281d14cafb5cbfe5b371157b8a384b2374fea2f53568cdc4712c0d2
            Exit Code:     0
            Finished At:   2022-03-15T09:14:27Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:27Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://784364b5368c37340e6c7458a2d9fe4c7578c36335753a88ad231ed526b5a213
            Exit Code:     0
            Finished At:   2022-03-15T09:14:28Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:28Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 33554432
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/32MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-vddp8-write3-task-xlcx2:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-15T09:14:29Z
        Conditions:
          Last Transition Time:  2022-03-15T09:14:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vddp8-write3-task-xlcx2-pod-hgx96
        Start Time:              2022-03-15T09:14:16Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://76d9d95f605e04db478611bf5976d2361e2daf27412f8c98b0b107b02afbea4f
            Exit Code:     0
            Finished At:   2022-03-15T09:14:27Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:27Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://e01af6d15b3983896aa064ca82ed1e08ae3b26cdfd95b7ea0377a97523a53b7b
            Exit Code:     0
            Finished At:   2022-03-15T09:14:28Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:28Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 33554432
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/32MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
    write-read-array-pipeline-run-vddp8-write4-task-jb6tj:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-15T09:14:25Z
        Conditions:
          Last Transition Time:  2022-03-15T09:14:25Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vddp8-write4-task-jb6tj-pod-t5v7x
        Start Time:              2022-03-15T09:14:16Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://cd58751d7564a3c682304a40d0605c65539c6b28b4d6cdf1c54e96048145e564
            Exit Code:     0
            Finished At:   2022-03-15T09:14:25Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:24Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://fd9e379ec66979725dbdc209614a14fcd1abeb14dba43afa74f4dd15ae75a507
            Exit Code:     0
            Finished At:   2022-03-15T09:14:25Z
            Reason:        Completed
            Started At:    2022-03-15T09:14:25Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 33554432
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/32MB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out4.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/32MB/w4sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/32MB/w4sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write4-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    32s                PipelineRun  
  Normal  Running    32s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    22s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    19s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    18s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    18s (x2 over 18s)  PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
