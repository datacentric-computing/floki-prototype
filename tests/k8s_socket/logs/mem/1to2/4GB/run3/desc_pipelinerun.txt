Name:         write-read-array-pipeline-run-bjr9g
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-04T18:47:13Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-04T18:47:13Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-bjr9g-read1-task-69vbv:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-bjr9g-read2-task-g8gsr:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-bjr9g-write1-task-t5wbc:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-04T18:48:22Z
  Resource Version:  7488785
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-bjr9g
  UID:               7d7a01cf-f65e-4f1f-9e9c-f49c920717f9
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
Status:
  Completion Time:  2022-03-04T18:48:22Z
  Conditions:
    Last Transition Time:  2022-03-04T18:48:22Z
    Message:               Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-03-04T18:47:13Z
  Task Runs:
    write-read-array-pipeline-run-bjr9g-read1-task-69vbv:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-04T18:48:19Z
        Conditions:
          Last Transition Time:  2022-03-04T18:48:19Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bjr9g-read1-task-69vbv-pod-9dsl4
        Start Time:              2022-03-04T18:47:38Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:       s-start
          Terminated:
            Container ID:  docker://532f0cc1ffcb59d77cd4985d8f01df8073431faf2e32a0830b3d715325a9b43e
            Exit Code:     0
            Finished At:   2022-03-04T18:48:11Z
            Reason:        Completed
            Started At:    2022-03-04T18:47:47Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:            read
          Terminated:
            Container ID:  docker://b16e042c86aedcf3b2b38404724a9ddd9dea255d22326aa012b10cee4126428c
            Exit Code:     0
            Finished At:   2022-03-04T18:48:18Z
            Reason:        Completed
            Started At:    2022-03-04T18:48:11Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-bjr9g-read2-task-g8gsr:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-04T18:48:22Z
        Conditions:
          Last Transition Time:  2022-03-04T18:48:22Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bjr9g-read2-task-g8gsr-pod-9fqtf
        Start Time:              2022-03-04T18:47:38Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:       s-start
          Terminated:
            Container ID:  docker://cc4db329fedc5b6fd8831a1dd2f3839fe0a09684bb0e616b15028eb818f0d995
            Exit Code:     0
            Finished At:   2022-03-04T18:48:13Z
            Reason:        Completed
            Started At:    2022-03-04T18:47:51Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:            read
          Terminated:
            Container ID:  docker://8eb02b09394f81be244052427440f5f078800c2ddee15cb7ea2e56ad6cdb4cfd
            Exit Code:     0
            Finished At:   2022-03-04T18:48:21Z
            Reason:        Completed
            Started At:    2022-03-04T18:48:13Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-bjr9g-write1-task-t5wbc:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-04T18:47:37Z
        Conditions:
          Last Transition Time:  2022-03-04T18:47:37Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bjr9g-write1-task-t5wbc-pod-mv2n2
        Start Time:              2022-03-04T18:47:13Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       write
          Terminated:
            Container ID:  docker://78b453d8b32911313677a21d785b311698bda62d210f5624c4c4a84c7ec7049d
            Exit Code:     0
            Finished At:   2022-03-04T18:47:36Z
            Reason:        Completed
            Started At:    2022-03-04T18:47:26Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:            s-end
          Terminated:
            Container ID:  docker://7cf9505bf27234a618a2f3f01e80fc7fc7400cb9e4cc4a6e93b2eb2e4b4b3254
            Exit Code:     0
            Finished At:   2022-03-04T18:47:36Z
            Reason:        Completed
            Started At:    2022-03-04T18:47:36Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 4294967296
# Store timestamp after data creation
with open("/data/timestamps/socket/1to2/4GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/4GB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/1to2/4GB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
Events:
  Type    Reason     Age   From         Message
  ----    ------     ----  ----         -------
  Normal  Started    76s   PipelineRun  
  Normal  Running    76s   PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    52s   PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    10s   PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
