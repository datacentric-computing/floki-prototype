Name:         write-read-array-pipeline-run-bsxs5
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-04T11:22:54Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-04T11:22:54Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-bsxs5-read1-task-zcz7q:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-bsxs5-read2-task-44xx4:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-bsxs5-write1-task-2xfwp:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-04T11:23:23Z
  Resource Version:  6760910
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-bsxs5
  UID:               db5bd145-55d0-40aa-8c6d-5b060169ad37
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
Status:
  Completion Time:  2022-03-04T11:23:23Z
  Conditions:
    Last Transition Time:  2022-03-04T11:23:23Z
    Message:               Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-03-04T11:22:54Z
  Task Runs:
    write-read-array-pipeline-run-bsxs5-read1-task-zcz7q:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-04T11:23:18Z
        Conditions:
          Last Transition Time:  2022-03-04T11:23:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bsxs5-read1-task-zcz7q-pod-klc95
        Start Time:              2022-03-04T11:23:10Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:       s-start
          Terminated:
            Container ID:  docker://ed18bde9864ad2fe11ab8a2a2aafad1664274b139b063161b02f9118a08c940d
            Exit Code:     0
            Finished At:   2022-03-04T11:23:18Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:18Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:            read
          Terminated:
            Container ID:  docker://afc62ff442fd2121cfe846e737bdaa4de9f9ffc916b88e73c30d9bd06215ca5e
            Exit Code:     0
            Finished At:   2022-03-04T11:23:18Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:18Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-bsxs5-read2-task-44xx4:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-04T11:23:23Z
        Conditions:
          Last Transition Time:  2022-03-04T11:23:23Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bsxs5-read2-task-44xx4-pod-zp56l
        Start Time:              2022-03-04T11:23:10Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:       s-start
          Terminated:
            Container ID:  docker://4d042d1b299471be933db10f9ea3bafc2792e9b1beedf6d60582a602191d80cd
            Exit Code:     0
            Finished At:   2022-03-04T11:23:21Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:21Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:            read
          Terminated:
            Container ID:  docker://3c04c495292e356ae17e097e7143b4e58f3285b2a5786ac4c944221899f6aa8d
            Exit Code:     0
            Finished At:   2022-03-04T11:23:21Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:21Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-bsxs5-write1-task-2xfwp:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-04T11:23:09Z
        Conditions:
          Last Transition Time:  2022-03-04T11:23:09Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-bsxs5-write1-task-2xfwp-pod-jfhpp
        Start Time:              2022-03-04T11:22:54Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       write
          Terminated:
            Container ID:  docker://a56b0f726d6dc20685c3ae4cb692643fc19a35c93cf5f328c397b186e0de3d32
            Exit Code:     0
            Finished At:   2022-03-04T11:23:08Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:08Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:6870fa6a910d1f4fa2ab485be14c7a34622064833ccbd9b381efba5a78447b03
          Name:            s-end
          Terminated:
            Container ID:  docker://3b98f970262b2c16429cd54ad1081ca36a32c40d8a0284e7d8e16cc824a93750
            Exit Code:     0
            Finished At:   2022-03-04T11:23:09Z
            Reason:        Completed
            Started At:    2022-03-04T11:23:09Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/1to2/8MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/1to2/8MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/1to2/8MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    35s (x2 over 35s)  PipelineRun  
  Normal  Running    35s (x2 over 35s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    20s (x2 over 20s)  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    11s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  6s                 PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
