Name:         write-read-array-pipeline-run-cr5f6
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-17T10:06:00Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-17T10:06:00Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-cr5f6-read1-task-pjb4s:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write1-task-xdqvn:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write2-task-smkr2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write3-task-8twcr:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write4-task-xqcsz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write5-task-4r9n6:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-cr5f6-write6-task-pg5jw:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-17T10:06:32Z
  Resource Version:  26729493
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-cr5f6
  UID:               56392ebe-af61-44c5-bb01-1d3dc2d8914d
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          write5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          write6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-write5-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-write6-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-03-17T10:06:32Z
  Conditions:
    Last Transition Time:  2022-03-17T10:06:32Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         write5-task
      Task Ref:
        Kind:  Task
        Name:  write5
      Workspaces:
        Name:       write5-ws
        Workspace:  task-write5-ws
      Name:         write6-task
      Task Ref:
        Kind:  Task
        Name:  write6
      Workspaces:
        Name:       write6-ws
        Workspace:  task-write6-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
        write4-task
        write5-task
        write6-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-write5-ws
      Name:    task-write6-ws
      Name:    task-read1-ws
  Start Time:  2022-03-17T10:06:00Z
  Task Runs:
    write-read-array-pipeline-run-cr5f6-read1-task-pjb4s:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-17T10:06:32Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:32Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-read1-task-pjb4s-pod-5v57f
        Start Time:              2022-03-17T10:06:15Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
          Name:       s-start
          Terminated:
            Container ID:  docker://e27d0571a433f9fd4232fb5b76b3c5d30711d8c758bad9444337fbe441052fd1
            Exit Code:     0
            Finished At:   2022-03-17T10:06:28Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:28Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://671a4f27286b79806717290db53dbb591dfa2d597cb4c16c6805e2519aa40905
            Exit Code:     0
            Finished At:   2022-03-17T10:06:31Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:29Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f/n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f/n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f/n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.txt", "r") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
with open("/data/in_data/out4.txt", "r") as fp_data4:
   data = fp_data4.read()
   del data
   gc.collect()
with open("/data/in_data/out5.txt", "r") as fp_data5:
   data = fp_data5.read()
   del data
   gc.collect()
with open("/data/in_data/out6.txt", "r") as fp_data6:
   data = fp_data6.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f/n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-cr5f6-write1-task-xdqvn:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-17T10:06:12Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:12Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write1-task-xdqvn-pod-644pd
        Start Time:              2022-03-17T10:06:00Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://cf8f9c5afed610e07c2a573f2a83f5e5df2b5f56f84df457cd5b24280b419e5f
            Exit Code:     0
            Finished At:   2022-03-17T10:06:10Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:09Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://d6b588a75d1154a3f84f466b6e4cac34f97fd05d7dff5b7bb4387640c1eed897
            Exit Code:     0
            Finished At:   2022-03-17T10:06:10Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:10Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-cr5f6-write2-task-smkr2:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-17T10:06:10Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:10Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write2-task-smkr2-pod-qqfbc
        Start Time:              2022-03-17T10:06:00Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://2ae31165e6aa7c21ebbca6c721ef4a09aa10bfba44ddbf5d2adb2bcf32e122ef
            Exit Code:     0
            Finished At:   2022-03-17T10:06:09Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:08Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://c360c3e0c3e1840944c24807916b0e01ecf47dd39da19b2f61de8b9d33ed481b
            Exit Code:     0
            Finished At:   2022-03-17T10:06:09Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:09Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-cr5f6-write3-task-8twcr:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-17T10:06:15Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:15Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write3-task-8twcr-pod-tnlct
        Start Time:              2022-03-17T10:06:00Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://994fd9635cd10b31589fda1ec0dc5ee6c2e536c34720d69f8118a781e84ee41a
            Exit Code:     0
            Finished At:   2022-03-17T10:06:14Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:10Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://b78c150dc0a43c7ec13f660a409ebf06290b6229157caef76bfab2dc9c060307
            Exit Code:     0
            Finished At:   2022-03-17T10:06:14Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
    write-read-array-pipeline-run-cr5f6-write4-task-xqcsz:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-17T10:06:11Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:11Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write4-task-xqcsz-pod-mct6c
        Start Time:              2022-03-17T10:06:01Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://9bf399b294c682993b47e1689a358282eb21fa28b1d0392f1959b8f01b1f5bec
            Exit Code:     0
            Finished At:   2022-03-17T10:06:09Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:09Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://c3bc9c349a411130a777344aa6f6a1350f7cf2f51277bcd777c7cdd6be58c27f
            Exit Code:     0
            Finished At:   2022-03-17T10:06:10Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:09Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out4.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w4sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w4sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write4-ws
    write-read-array-pipeline-run-cr5f6-write5-task-4r9n6:
      Pipeline Task Name:  write5-task
      Status:
        Completion Time:  2022-03-17T10:06:11Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:11Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write5-task-4r9n6-pod-zl496
        Start Time:              2022-03-17T10:06:01Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://3838fa6ede8ea2e9d49abd4862ca8cf91986cac50f199e830d4becc4ccb14dc4
            Exit Code:     0
            Finished At:   2022-03-17T10:06:10Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:10Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://352c65db367ce2ffc04464061ebc2e4d4ff87d5e72e33335dc8021977d077430
            Exit Code:     0
            Finished At:   2022-03-17T10:06:11Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:11Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w5cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w5cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out5.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w5cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w5sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w5sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write5-ws
    write-read-array-pipeline-run-cr5f6-write6-task-pg5jw:
      Pipeline Task Name:  write6-task
      Status:
        Completion Time:  2022-03-17T10:06:12Z
        Conditions:
          Last Transition Time:  2022-03-17T10:06:12Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-cr5f6-write6-task-pg5jw-pod-vtrlr
        Start Time:              2022-03-17T10:06:01Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://ac0fc68709eee72e5f422d1b2690c17727f3b8f4290bc28e8ec75f5b19bb3d22
            Exit Code:     0
            Finished At:   2022-03-17T10:06:10Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:10Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
          Name:            s-end
          Terminated:
            Container ID:  docker://9af424ac285f77c23b6304836a2d0b0d1ac82759d92ebe9b5ad18b4bc631cdc9
            Exit Code:     0
            Finished At:   2022-03-17T10:06:11Z
            Reason:        Completed
            Started At:    2022-03-17T10:06:11Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w6cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 268435456
# Store timestamp after data creation
with open("/data/timestamps/socket/6to1/256MB/w6cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out6.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w6cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/6to1/256MB/w6sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/6to1/256MB/w6sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write6-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    41s (x2 over 41s)  PipelineRun  
  Normal  Running    41s (x2 over 41s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    31s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    30s (x2 over 30s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    30s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    29s                PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    28s                PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    26s (x2 over 26s)  PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
