Name:         write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv
Namespace:    default
Priority:     0
Node:         k8s-worker-node7/10.0.26.216
Start Time:   Thu, 17 Mar 2022 11:46:08 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-4xw49
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-pipeline-run-4xw49-read1-task-2cr4c
Annotations:  cni.projectcalico.org/containerID: f525b1288dadd50b7cad132e32331c5cf4ced9731dbef4dc93547aeec9de535b
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.30.58
IPs:
  IP:           192.168.30.58
Controlled By:  TaskRun/write-read-array-pipeline-run-4xw49-read1-task-2cr4c
Init Containers:
  place-tools:
    Container ID:  docker://0e0eb064472f9e58a314910f248e0cd7b54a340c33b01390405886e1049633df
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:22 +0000
      Finished:     Thu, 17 Mar 2022 11:46:22 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://c373391bce4c82ae4397dd5adcf98c08a5d9bbe9bc36032cdff53b19bbba5b0b
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-9ctds"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvNnRvMS8yR0IvcjFzZW50X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3NlbnRfc190czoKICAgZnBfc2VudF9zX3RzLndyaXRlKCIlZi9uIiAlIHRpbWUudGltZSgpKQojIFdhaXQgZm9yIHRoZSByZWFkeSBzaWduYWwgZnJvbSBleHRlcm5hbCBwcm9jZXNzCndpdGggb3BlbigiL2RhdGEvc3luY19waXBlIiwgInJiIikgYXMgc3luY19waXBlX2ZpbGU6CiAgIHdoaWxlKGludC5mcm9tX2J5dGVzKHN5bmNfcGlwZV9maWxlLnJlYWQoMSksICJsaXR0bGUiKSAhPSAxKToKICAgICAgIGNvbnRpbnVlCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvNnRvMS8yR0IvcjFzZW50X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF9zZW50X2VfdHM6CiAgIGZwX3NlbnRfZV90cy53cml0ZSgiJWYvbiIgJSB0aW1lLnRpbWUoKSkK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      scriptfile="/tekton/scripts/script-1-g762l"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgZ2MKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0LzZ0bzEvMkdCL3IxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9zX3RzOgogICBmcF9yY29udF9zX3RzLndyaXRlKCIlZi9uIiAlIHRpbWUudGltZSgpKQojIFJlYWQgZGF0YSBvYmplY3QgZnJvbSByZW1vdGUgc2hhcmVkIHN0b3JhZ2UKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDEudHh0IiwgInIiKSBhcyBmcF9kYXRhMToKICAgZGF0YSA9IGZwX2RhdGExLnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDIudHh0IiwgInIiKSBhcyBmcF9kYXRhMjoKICAgZGF0YSA9IGZwX2RhdGEyLnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDMudHh0IiwgInIiKSBhcyBmcF9kYXRhMzoKICAgZGF0YSA9IGZwX2RhdGEzLnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDQudHh0IiwgInIiKSBhcyBmcF9kYXRhNDoKICAgZGF0YSA9IGZwX2RhdGE0LnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDUudHh0IiwgInIiKSBhcyBmcF9kYXRhNToKICAgZGF0YSA9IGZwX2RhdGE1LnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDYudHh0IiwgInIiKSBhcyBmcF9kYXRhNjoKICAgZGF0YSA9IGZwX2RhdGE2LnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldC82dG8xLzJHQi9yMWNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmL24iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:24 +0000
      Finished:     Thu, 17 Mar 2022 11:46:24 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://765cd26f36b51285d0ffe13e2d9f140c622e2fa8194fa32b816b4b12d4e5929b
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:26 +0000
      Finished:     Thu, 17 Mar 2022 11:46:26 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-s-start:
    Container ID:  docker://3aa2fb0628a63e528065eea8d6cd1c6c25da8a06269b76dd5851af8252f80fa4
    Image:         python
    Image ID:      docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-s-start
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-9ctds
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-17T11:46:35.494Z","type":3}]
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:29 +0000
      Finished:     Thu, 17 Mar 2022 11:47:16 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-98njf (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/run/1 from tekton-internal-run-1 (ro)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  step-read:
    Container ID:  docker://da2c19034bfa0042db575f3a9ef85d095f40ba21a63c8cf6ffae2991a4d89410
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/run/0/out
      -post_file
      /tekton/run/1/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/1
      -entrypoint
      /tekton/scripts/script-1-g762l
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-17T11:47:16.683Z","type":3}]
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:30 +0000
      Finished:     Thu, 17 Mar 2022 11:47:37 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-98njf (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-1 (rw)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (ro)
      /tekton/run/1 from tekton-internal-run-1 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://d6de0adbea90a5266a8ae5dae83b8e3ef51df058d80266fb23b0aee1cb882661
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:47:44 +0000
      Finished:     Thu, 17 Mar 2022 11:47:44 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 17 Mar 2022 11:46:31 +0000
      Finished:     Thu, 17 Mar 2022 11:47:43 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-s-start,step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-creds-init-home-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-98njf:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task7-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node7
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  104s               default-scheduler  Successfully assigned default/write-read-array-pipeline-run-4xw49-read1-task-2cr4c-pod-65vwv to k8s-worker-node7
  Normal   Pulled     92s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    91s                kubelet            Created container place-tools
  Normal   Started    90s                kubelet            Started container place-tools
  Normal   Pulled     89s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    89s                kubelet            Created container place-scripts
  Normal   Started    88s                kubelet            Started container place-scripts
  Normal   Pulled     87s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    87s                kubelet            Created container istio-init
  Normal   Started    86s                kubelet            Started container istio-init
  Normal   Pulled     84s                kubelet            Container image "python" already present on machine
  Normal   Pulled     83s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    83s                kubelet            Started container step-s-start
  Normal   Created    83s                kubelet            Created container step-s-start
  Normal   Created    82s                kubelet            Created container step-read
  Normal   Started    82s                kubelet            Started container step-read
  Normal   Pulled     82s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    14s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  10s (x2 over 12s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    9s (x2 over 81s)   kubelet            Created container istio-proxy
  Normal   Pulled     9s                 kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Started    8s (x2 over 81s)   kubelet            Started container istio-proxy
  Warning  Unhealthy  8s                 kubelet            Readiness probe failed: Get "http://192.168.30.58:15021/healthz/ready": dial tcp 192.168.30.58:15021: connect: connection refused
