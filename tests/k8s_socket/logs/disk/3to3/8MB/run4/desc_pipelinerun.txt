Name:         write-read-array-pipeline-run-kc4rd
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-29T13:43:30Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-29T13:43:30Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-kc4rd-read1-task-4xqfg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-kc4rd-read2-task-pgbkm:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-kc4rd-read3-task-ck64k:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-kc4rd-write1-task-n6bg5:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-kc4rd-write2-task-scqvn:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-kc4rd-write3-task-zhjvz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-29T13:44:05Z
  Resource Version:  54276836
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-kc4rd
  UID:               2b355d31-5320-4bed-a3fa-a143bebbdf31
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
Status:
  Completion Time:  2022-03-29T13:44:04Z
  Conditions:
    Last Transition Time:  2022-03-29T13:44:04Z
    Message:               Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
  Start Time:  2022-03-29T13:43:30Z
  Task Runs:
    write-read-array-pipeline-run-kc4rd-read1-task-4xqfg:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-29T13:44:04Z
        Conditions:
          Last Transition Time:  2022-03-29T13:44:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-read1-task-4xqfg-pod-zcw8g
        Start Time:              2022-03-29T13:43:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://44a9ca0906481a8dcfd73ead1cbb4bb52027b9ce4fefa3131f3f8eef1a96580d
            Exit Code:     0
            Finished At:   2022-03-29T13:44:02Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:01Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://205a626b9b17a3812fe53a1a40594126ed1eca1d2a8e712abb11ddf24835ae4a
            Exit Code:     0
            Finished At:   2022-03-29T13:44:04Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:03Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-kc4rd-read2-task-pgbkm:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-29T13:44:04Z
        Conditions:
          Last Transition Time:  2022-03-29T13:44:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-read2-task-pgbkm-pod-ngpb4
        Start Time:              2022-03-29T13:43:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:6441e2f0bd2e566de0df6445cb8e7e395ea1a376dd702de908d70401d3700961
          Name:       s-start
          Terminated:
            Container ID:  docker://968dbe4038c69bbdca1e2fc57c9a46d13d2786ad895ed5c6b0b224a25cd87182
            Exit Code:     0
            Finished At:   2022-03-29T13:44:02Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:02Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:af220ba516b0c154461915f76b5b170ff8f4fb35a5c80f7e94577117f39b743c
          Name:            read
          Terminated:
            Container ID:  docker://a08846475afa091b109969cf0d7dd6c8ae342fac720ecaa0654693bbbbaf9fde
            Exit Code:     0
            Finished At:   2022-03-29T13:44:04Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:02Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-kc4rd-read3-task-ck64k:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-29T13:44:04Z
        Conditions:
          Last Transition Time:  2022-03-29T13:44:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-read3-task-ck64k-pod-cm4hv
        Start Time:              2022-03-29T13:43:50Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:e71692a9512db7addf474eff98e84822d12ff4708b3aa298cb7fa15478aa76a8
          Name:       s-start
          Terminated:
            Container ID:  docker://93df91c7a2bc02a4d0b2acccbbff3c45356dfde6114951035e2175a5ac74c53d
            Exit Code:     0
            Finished At:   2022-03-29T13:44:03Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:02Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://b44afbdd65b847c9ddc33dd0e0549d3dfa79693941c3573648d8fea9abea2382
            Exit Code:     0
            Finished At:   2022-03-29T13:44:04Z
            Reason:        Completed
            Started At:    2022-03-29T13:44:04Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.bin", "rb") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.bin", "rb") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.bin", "rb") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-ws
    write-read-array-pipeline-run-kc4rd-write1-task-n6bg5:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-29T13:43:49Z
        Conditions:
          Last Transition Time:  2022-03-29T13:43:49Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-write1-task-n6bg5-pod-2t777
        Start Time:              2022-03-29T13:43:30Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://6c1748c1e03d99201f95a4aecb2f0cc0d7af61e0a41724c3392ed88d5f1bbddc
            Exit Code:     0
            Finished At:   2022-03-29T13:43:47Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:46Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://859a9c0ef05e4472a8a7952e2a3807c38d08327a18b26ddef421fc4f883fb8e9
            Exit Code:     0
            Finished At:   2022-03-29T13:43:49Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:48Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8388608
bytes = bytearray(b'\x01') * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-kc4rd-write2-task-scqvn:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-29T13:43:45Z
        Conditions:
          Last Transition Time:  2022-03-29T13:43:45Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-write2-task-scqvn-pod-9n8qf
        Start Time:              2022-03-29T13:43:30Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://7cb344cc67ee4a7196028986745153e0b3215473ed239e813e5c09cb757c965d
            Exit Code:     0
            Finished At:   2022-03-29T13:43:43Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:42Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://d2fa260b58f374e342ef619c457ce5d822af92a65848644b9ca0fc22fd1ce70a
            Exit Code:     0
            Finished At:   2022-03-29T13:43:44Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:44Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8388608
bytes = bytearray(b'\x01') * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-kc4rd-write3-task-zhjvz:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-29T13:43:50Z
        Conditions:
          Last Transition Time:  2022-03-29T13:43:50Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-kc4rd-write3-task-zhjvz-pod-s9l9f
        Start Time:              2022-03-29T13:43:31Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://d14631f27299973ffbf63cc9c3163d39d9293be4234a2ca49554eab7c0450b82
            Exit Code:     0
            Finished At:   2022-03-29T13:43:48Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:47Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://8c7b83c372c61bddfd04156103d24de419529e3893be7ea3920aa67dbc256593
            Exit Code:     0
            Finished At:   2022-03-29T13:43:49Z
            Reason:        Completed
            Started At:    2022-03-29T13:43:49Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
#string = 'x' * 8388608
bytes = bytearray(b'\x01') * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/3to3/8MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.bin", 'wb') as fp_data:
   fp_data.write(bytes)
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/3to3/8MB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/3to3/8MB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    41s                PipelineRun  
  Normal  Running    41s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    26s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    22s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    21s (x2 over 21s)  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    7s                 PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    7s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s                 PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
