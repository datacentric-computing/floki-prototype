Name:         write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm
Namespace:    default
Priority:     0
Node:         k8s-worker-node5/10.0.26.214
Start Time:   Tue, 29 Mar 2022 17:56:23 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-g2cb2
              tekton.dev/pipelineTask=read2-task
              tekton.dev/task=read2
              tekton.dev/taskRun=write-read-array-pipeline-run-g2cb2-read2-task-wk2fp
Annotations:  cni.projectcalico.org/containerID: 62337ce86dd4e2c892721bedad85f99769c362b92f0c4ff9f8e137efeb8a1452
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.43.178
IPs:
  IP:           192.168.43.178
Controlled By:  TaskRun/write-read-array-pipeline-run-g2cb2-read2-task-wk2fp
Init Containers:
  place-tools:
    Container ID:  docker://3c4ab62c03853bbd5bdf0600f7ed4e69bc4d2937faf45a2b1f7a5421cd3e4ca7
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:56:36 +0000
      Finished:     Tue, 29 Mar 2022 17:56:36 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://36c86f18f2b90d184c4221b504284e89e1dfbb9bdd5777bf2f38f8f123427f76
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-cx8w4"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvM3RvMy80R0IvcjJzZW50X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3NlbnRfc190czoKICAgZnBfc2VudF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdhaXQgZm9yIHRoZSByZWFkeSBzaWduYWwgZnJvbSBleHRlcm5hbCBwcm9jZXNzCndpdGggb3BlbigiL2RhdGEvc3luY19waXBlIiwgInJiIikgYXMgc3luY19waXBlX2ZpbGU6CiAgIHdoaWxlKGludC5mcm9tX2J5dGVzKHN5bmNfcGlwZV9maWxlLnJlYWQoMSksICJsaXR0bGUiKSAhPSAxKToKICAgICAgIGNvbnRpbnVlCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvM3RvMy80R0IvcjJzZW50X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF9zZW50X2VfdHM6CiAgIGZwX3NlbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      scriptfile="/tekton/scripts/script-1-zqz5w"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgZ2MKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0LzN0bzMvNEdCL3IyY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9zX3RzOgogICBmcF9yY29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFJlYWQgZGF0YSBvYmplY3QgZnJvbSByZW1vdGUgc2hhcmVkIHN0b3JhZ2UKd2l0aCBvcGVuKCIvZGF0YS9pbl9kYXRhL291dDEuYmluIiwgInJiIikgYXMgZnBfZGF0YTE6CiAgIGRhdGEgPSBmcF9kYXRhMS5yZWFkKCkKICAgZGVsIGRhdGEKICAgZ2MuY29sbGVjdCgpCndpdGggb3BlbigiL2RhdGEvaW5fZGF0YS9vdXQyLmJpbiIsICJyYiIpIGFzIGZwX2RhdGEyOgogICBkYXRhID0gZnBfZGF0YTIucmVhZCgpCiAgIGRlbCBkYXRhCiAgIGdjLmNvbGxlY3QoKQp3aXRoIG9wZW4oIi9kYXRhL2luX2RhdGEvb3V0My5iaW4iLCAicmIiKSBhcyBmcF9kYXRhMzoKICAgZGF0YSA9IGZwX2RhdGEzLnJlYWQoKQogICBkZWwgZGF0YQogICBnYy5jb2xsZWN0KCkKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldC8zdG8zLzRHQi9yMmNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:56:38 +0000
      Finished:     Tue, 29 Mar 2022 17:56:38 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://62e64b0251ae8bdc084cf0de5d454360aca869d01b0652d7ab5bd536daa2317a
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:56:40 +0000
      Finished:     Tue, 29 Mar 2022 17:56:41 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-s-start:
    Container ID:  docker://c5251b0db2198bc7ebf0e66707c43ae529b5bb985de3d974f9bcebef2cb44446
    Image:         python
    Image ID:      docker-pullable://python@sha256:6441e2f0bd2e566de0df6445cb8e7e395ea1a376dd702de908d70401d3700961
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-s-start
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-cx8w4
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-29T17:56:48.755Z","type":3}]
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:56:43 +0000
      Finished:     Tue, 29 Mar 2022 17:57:30 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-fl8p9 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/run/1 from tekton-internal-run-1 (ro)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  step-read:
    Container ID:  docker://3fec07662f97a63b14868c58286b1e6b29737c83bdbba2680961e997440fb119
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:af220ba516b0c154461915f76b5b170ff8f4fb35a5c80f7e94577117f39b743c
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/run/0/out
      -post_file
      /tekton/run/1/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/1
      -entrypoint
      /tekton/scripts/script-1-zqz5w
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-29T17:57:30.933Z","type":3}]
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:56:44 +0000
      Finished:     Tue, 29 Mar 2022 17:57:45 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-fl8p9 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-1 (rw)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (ro)
      /tekton/run/1 from tekton-internal-run-1 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://fc6469a1120c61f227abd5c3819511fdcbe12509f40c4924d96e5df3a7045f7e
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 17:57:54 +0000
      Finished:     Tue, 29 Mar 2022 17:57:54 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-s-start,step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-creds-init-home-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-fl8p9:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task5-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node5
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  104s               default-scheduler  Successfully assigned default/write-read-array-pipeline-run-g2cb2-read2-task-wk2fp-pod-zjtkm to k8s-worker-node5
  Normal   Pulled     90s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    89s                kubelet            Created container place-tools
  Normal   Started    88s                kubelet            Started container place-tools
  Normal   Pulled     87s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    86s                kubelet            Created container place-scripts
  Normal   Started    86s                kubelet            Started container place-scripts
  Normal   Pulled     85s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    84s                kubelet            Created container istio-init
  Normal   Started    84s                kubelet            Started container istio-init
  Normal   Pulled     82s                kubelet            Container image "python" already present on machine
  Normal   Pulled     81s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    81s                kubelet            Started container step-s-start
  Normal   Created    81s                kubelet            Created container step-s-start
  Normal   Created    80s                kubelet            Created container step-read
  Normal   Started    80s                kubelet            Started container step-read
  Normal   Pulled     80s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    16s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  11s (x3 over 15s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    10s (x2 over 79s)  kubelet            Created container istio-proxy
  Normal   Started    10s (x2 over 79s)  kubelet            Started container istio-proxy
  Normal   Pulled     10s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
