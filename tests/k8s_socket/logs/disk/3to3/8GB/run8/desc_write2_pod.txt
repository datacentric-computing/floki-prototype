Name:         write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf
Namespace:    default
Priority:     0
Node:         k8s-worker-node2/10.0.26.207
Start Time:   Tue, 29 Mar 2022 18:51:09 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-nqs7z
              tekton.dev/pipelineTask=write2-task
              tekton.dev/task=write2
              tekton.dev/taskRun=write-read-array-pipeline-run-nqs7z-write2-task-dccjt
Annotations:  cni.projectcalico.org/containerID: 9f1c187c5fd5b8b60a6cf546c315d88ee25416f5d17c4ac9f069b8b8d967a18a
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.219.75
IPs:
  IP:           192.168.219.75
Controlled By:  TaskRun/write-read-array-pipeline-run-nqs7z-write2-task-dccjt
Init Containers:
  place-tools:
    Container ID:  docker://12cca93caddc87fc66d9192ff83b5f36e127074f18a3fbe1060d9043af9952ef
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:14 +0000
      Finished:     Tue, 29 Mar 2022 18:51:15 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://9944a757c577ce92308906aea04443762a3d27b073bf36079dfe15fc8f2d4c4b
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-cxr2x"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvM3RvMy84R0IvdzJjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X3NfdHM6CiAgIGZwX3djb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiMgQ3JlYXRlIGRhdGEgb2JqZWN0CiNzdHJpbmcgPSAneCcgKiA4NTg5OTM0NTkyCmJ5dGVzID0gYnl0ZWFycmF5KGInXHgwMScpICogODU4OTkzNDU5MgojIFN0b3JlIHRpbWVzdGFtcCBhZnRlciBkYXRhIGNyZWF0aW9uCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvM3RvMy84R0IvdzJjb250X2RhdGFfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X2RhdGFfdHM6CiAgIGZwX3djb250X2RhdGFfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiMgV3JpdGUgZGF0YSBvYmplY3Qgb24gdGhlIGxvY2FsIHB2CndpdGggb3BlbigiL2RhdGEvb3V0X2RhdGEvb3V0Mi5iaW4iLCAnd2InKSBhcyBmcF9kYXRhOgogICBmcF9kYXRhLndyaXRlKGJ5dGVzKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0LzN0bzMvOEdCL3cyY29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZV90czoKICAgZnBfd2NvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJXUklURVI6IFdyaXR0ZW4gb3V0MSEiKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      scriptfile="/tekton/scripts/script-1-n2w2z"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgb3MKaW1wb3J0IHRpbWUKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0LzN0bzMvOEdCL3cyc2VudF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9zZW50X3NfdHM6CiAgIGZwX3NlbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZXMgcmVhZHkgc2lnbmFsIHRvIHN5bmMgcGlwZQpvcy5zeXN0ZW0oImVjaG8gMSA+IC9kYXRhL3N5bmNfcGlwZSIpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvM3RvMy84R0IvdzJzZW50X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF9zZW50X2VfdHM6CiAgIGZwX3NlbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJXUklURVI6IEZpbmlzaGVkISIpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:16 +0000
      Finished:     Tue, 29 Mar 2022 18:51:16 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://e546741217c7bff15e3407dae8f4df397b331b2907a4e13c7fbb1c8dbb16e22c
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:18 +0000
      Finished:     Tue, 29 Mar 2022 18:51:19 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://b3468a1538a08992c479def2d899ade97e1c0dfbf63076348985bc34324d97aa
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-cxr2x
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-29T18:51:25.931Z","type":3}]
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:19 +0000
      Finished:     Tue, 29 Mar 2022 18:51:39 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-j48d5 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/run/1 from tekton-internal-run-1 (ro)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  step-s-end:
    Container ID:  docker://c1dda639468f792fc7eda0a9b18b89b37fc7250798954696689c7c9d96790125
    Image:         python
    Image ID:      docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/run/0/out
      -post_file
      /tekton/run/1/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-s-end
      -step_metadata_dir_link
      /tekton/steps/1
      -entrypoint
      /tekton/scripts/script-1-n2w2z
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-29T18:51:40.030Z","type":3}]
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:20 +0000
      Finished:     Tue, 29 Mar 2022 18:51:45 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-j48d5 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-1 (rw)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (ro)
      /tekton/run/1 from tekton-internal-run-1 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://9d2fd07784cda07a52fdfbb8e85b796b74edeb708475e7dd7dc9c915e9950df3
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:52:10 +0000
      Finished:     Tue, 29 Mar 2022 18:52:10 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 29 Mar 2022 18:51:21 +0000
      Finished:     Tue, 29 Mar 2022 18:52:09 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write,step-s-end
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-creds-init-home-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-j48d5:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task2-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node2
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                    From               Message
  ----     ------     ----                   ----               -------
  Normal   Scheduled  4m56s                  default-scheduler  Successfully assigned default/write-read-array-pipeline-run-nqs7z-write2-task-dccjt-pod-ttftf to k8s-worker-node2
  Normal   Pulled     4m52s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    4m52s                  kubelet            Created container place-tools
  Normal   Started    4m52s                  kubelet            Started container place-tools
  Normal   Pulled     4m50s                  kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    4m50s                  kubelet            Created container place-scripts
  Normal   Started    4m50s                  kubelet            Started container place-scripts
  Normal   Pulled     4m49s                  kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    4m49s                  kubelet            Created container istio-init
  Normal   Started    4m48s                  kubelet            Started container istio-init
  Normal   Pulled     4m47s                  kubelet            Container image "python" already present on machine
  Normal   Pulled     4m47s                  kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    4m47s                  kubelet            Started container step-write
  Normal   Created    4m47s                  kubelet            Created container step-write
  Normal   Created    4m46s                  kubelet            Created container step-s-end
  Normal   Started    4m45s                  kubelet            Started container step-s-end
  Normal   Pulled     4m45s                  kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Started    4m45s                  kubelet            Started container istio-proxy
  Warning  Unhealthy  4m44s                  kubelet            Readiness probe failed: Get "http://192.168.219.75:15021/healthz/ready": dial tcp 192.168.219.75:15021: connect: connection refused
  Normal   Killing    4m2s                   kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  3m58s (x3 over 4m2s)   kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    3m57s (x2 over 4m45s)  kubelet            Created container istio-proxy
  Normal   Pulled     3m57s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
