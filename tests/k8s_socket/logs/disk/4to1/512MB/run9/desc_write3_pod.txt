Name:         write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq
Namespace:    default
Priority:     0
Node:         k8s-worker-node3/10.0.26.208
Start Time:   Mon, 14 Mar 2022 18:20:03 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-gbmz8
              tekton.dev/pipelineTask=write3-task
              tekton.dev/task=write3
              tekton.dev/taskRun=write-read-array-pipeline-run-gbmz8-write3-task-92l7x
Annotations:  cni.projectcalico.org/containerID: c30cd9382ddd25a3723dc784fe16e2e9811a1451db45a365b1ca1a5e713de5e7
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.198.240
IPs:
  IP:           192.168.198.240
Controlled By:  TaskRun/write-read-array-pipeline-run-gbmz8-write3-task-92l7x
Init Containers:
  place-tools:
    Container ID:  docker://ac89a5d1008faa6bae06cd35662eefb4dc5a996fa90ba313eb65f3f1fe49844a
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:06 +0000
      Finished:     Mon, 14 Mar 2022 18:20:07 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://f36d72bb42f0d9965e6156639832289e1b4cd48038b9aa1bbfa3daf4f775aaec
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-l2l6s"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvNHRvMS81MTJNQi93M2NvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfc190czoKICAgZnBfd2NvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBDcmVhdGUgZGF0YSBvYmplY3QKc3RyaW5nID0gJ3gnICogNTM2ODcwOTEyCiMgU3RvcmUgdGltZXN0YW1wIGFmdGVyIGRhdGEgY3JlYXRpb24Kd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldC80dG8xLzUxMk1CL3czY29udF9kYXRhX2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9kYXRhX3RzOgogICBmcF93Y29udF9kYXRhX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdyaXRlIGRhdGEgb2JqZWN0IG9uIHRoZSBsb2NhbCBwdgp3aXRoIG9wZW4oIi9kYXRhL291dF9kYXRhL291dDMudHh0IiwgJ3cnKSBhcyBmcF9kYXRhOgogICBmcF9kYXRhLndyaXRlKCIlcyIgJSBzdHJpbmcpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXQvNHRvMS81MTJNQi93M2NvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X2VfdHM6CiAgIGZwX3djb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiV1JJVEVSOiBXcml0dGVuIG91dDEhIikK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      scriptfile="/tekton/scripts/script-1-68cqg"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgb3MKaW1wb3J0IHRpbWUKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0LzR0bzEvNTEyTUIvdzNzZW50X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3NlbnRfc190czoKICAgZnBfc2VudF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdyaXRlcyByZWFkeSBzaWduYWwgdG8gc3luYyBwaXBlCm9zLnN5c3RlbSgiZWNobyAxID4gL2RhdGEvc3luY19waXBlIikKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldC80dG8xLzUxMk1CL3czc2VudF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfc2VudF9lX3RzOgogICBmcF9zZW50X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiV1JJVEVSOiBGaW5pc2hlZCEiKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:08 +0000
      Finished:     Mon, 14 Mar 2022 18:20:08 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://904e3bcae33cdef3626b60acd702d03f2443b3004fe821582d0ff707e400fda2
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:09 +0000
      Finished:     Mon, 14 Mar 2022 18:20:09 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://d5c4644bb66223665b576cd726918f3c28985d6e806b22090d91c8eba36a4adf
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-l2l6s
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-14T18:20:16.123Z","type":3}]
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:10 +0000
      Finished:     Mon, 14 Mar 2022 18:20:20 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-hbspb (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/run/1 from tekton-internal-run-1 (ro)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  step-s-end:
    Container ID:  docker://45f38d490cb4d8076e8e7f26330b3620b4f1840749ac26bb12c2dd3b14af7591
    Image:         python
    Image ID:      docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/run/0/out
      -post_file
      /tekton/run/1/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-s-end
      -step_metadata_dir_link
      /tekton/steps/1
      -entrypoint
      /tekton/scripts/script-1-68cqg
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-14T18:20:20.036Z","type":3}]
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:10 +0000
      Finished:     Mon, 14 Mar 2022 18:20:20 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-hbspb (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-1 (rw)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (ro)
      /tekton/run/1 from tekton-internal-run-1 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://7e87339ceea4836de675ab3f03339a572aa6119c02bea67d9fd9f49e09ebe57e
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:30 +0000
      Finished:     Mon, 14 Mar 2022 18:20:30 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 14 Mar 2022 18:20:11 +0000
      Finished:     Mon, 14 Mar 2022 18:20:30 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write,step-s-end
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-creds-init-home-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-1:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-hbspb:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task3-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node3
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  58s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-gbmz8-write3-task-92l7x-pod-cf6kq to k8s-worker-node3
  Normal   Pulled     56s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    55s                kubelet            Created container place-tools
  Normal   Started    55s                kubelet            Started container place-tools
  Normal   Pulled     54s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    53s                kubelet            Created container place-scripts
  Normal   Started    53s                kubelet            Started container place-scripts
  Normal   Pulled     53s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    52s                kubelet            Created container istio-init
  Normal   Started    52s                kubelet            Started container istio-init
  Normal   Pulled     51s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Pulled     51s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    51s                kubelet            Started container step-write
  Normal   Pulled     51s                kubelet            Container image "python" already present on machine
  Normal   Created    51s                kubelet            Created container step-s-end
  Normal   Started    51s                kubelet            Started container step-s-end
  Normal   Created    51s                kubelet            Created container step-write
  Normal   Killing    36s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  33s (x2 over 35s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    31s (x2 over 50s)  kubelet            Created container istio-proxy
  Normal   Started    31s (x2 over 50s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  31s                kubelet            Readiness probe failed: Get "http://192.168.198.240:15021/healthz/ready": dial tcp 192.168.198.240:15021: connect: connection refused
  Normal   Pulled     31s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
