Name:         write-read-array-pipeline-run-jdzg4
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-14T16:34:49Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-14T16:34:49Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-jdzg4-read1-task-kgbzm:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-jdzg4-write1-task-vsjkb:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-jdzg4-write2-task-mcrjk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-jdzg4-write3-task-44lxg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-jdzg4-write4-task-zjw47:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-14T16:35:14Z
  Resource Version:  20438820
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-jdzg4
  UID:               8ab9d6b0-6120-454b-85ea-8475738bbf12
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-03-14T16:35:14Z
  Conditions:
    Last Transition Time:  2022-03-14T16:35:14Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
        write4-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-read1-ws
  Start Time:  2022-03-14T16:34:49Z
  Task Runs:
    write-read-array-pipeline-run-jdzg4-read1-task-kgbzm:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-14T16:35:14Z
        Conditions:
          Last Transition Time:  2022-03-14T16:35:14Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-jdzg4-read1-task-kgbzm-pod-dlpbc
        Start Time:              2022-03-14T16:35:04Z
        Steps:
          Container:  step-s-start
          Image ID:   docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:       s-start
          Terminated:
            Container ID:  docker://da98a1a5a46c2ed13fdeb99974601d5effeb6841c5223abd0798253360de94e8
            Exit Code:     0
            Finished At:   2022-03-14T16:35:13Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:13Z
          Container:       step-read
          Image ID:        docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:            read
          Terminated:
            Container ID:  docker://9e7d8a42af10f220698fec6fc2568d6ba852c730dab44ca32c00a586bbde87f6
            Exit Code:     0
            Finished At:   2022-03-14T16:35:14Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:14Z
        Task Spec:
          Steps:
            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-start
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/r1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f/n" % time.time())
# Wait for the ready signal from external process
with open("/data/sync_pipe", "rb") as sync_pipe_file:
   while(int.from_bytes(sync_pipe_file.read(1), "little") != 1):
       continue
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/r1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f/n" % time.time())

            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import gc
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f/n" % time.time())
# Read data object from remote shared storage
with open("/data/in_data/out1.txt", "r") as fp_data1:
   data = fp_data1.read()
   del data
   gc.collect()
with open("/data/in_data/out2.txt", "r") as fp_data2:
   data = fp_data2.read()
   del data
   gc.collect()
with open("/data/in_data/out3.txt", "r") as fp_data3:
   data = fp_data3.read()
   del data
   gc.collect()
with open("/data/in_data/out4.txt", "r") as fp_data4:
   data = fp_data4.read()
   del data
   gc.collect()
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f/n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-jdzg4-write1-task-vsjkb:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-14T16:35:02Z
        Conditions:
          Last Transition Time:  2022-03-14T16:35:02Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-jdzg4-write1-task-vsjkb-pod-xvfqk
        Start Time:              2022-03-14T16:34:49Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://4d3adf7021a9f5e12a5aa67eed696b99e100c1e0f1149e36b80f3e522f507185
            Exit Code:     0
            Finished At:   2022-03-14T16:35:00Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:00Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://72485261fa291bfacf8d56ce0f7a6289d26a98f093b18b782aea7609b2d1b61b
            Exit Code:     0
            Finished At:   2022-03-14T16:35:01Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:00Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/8MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w1sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w1sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-jdzg4-write2-task-mcrjk:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-14T16:34:59Z
        Conditions:
          Last Transition Time:  2022-03-14T16:34:59Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-jdzg4-write2-task-mcrjk-pod-22lrg
        Start Time:              2022-03-14T16:34:49Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://6bd328ed63c2e9ce12d0a760c5da9461348e2d20d408b4c910209f3e339b6ce5
            Exit Code:     0
            Finished At:   2022-03-14T16:34:59Z
            Reason:        Completed
            Started At:    2022-03-14T16:34:58Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://37a6d3663685b58bbf5a24f9aadb0677120fa898836bff69773b434f0252dccd
            Exit Code:     0
            Finished At:   2022-03-14T16:34:59Z
            Reason:        Completed
            Started At:    2022-03-14T16:34:58Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/8MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w2sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w2sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-jdzg4-write3-task-44lxg:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-14T16:35:04Z
        Conditions:
          Last Transition Time:  2022-03-14T16:35:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-jdzg4-write3-task-44lxg-pod-57dhf
        Start Time:              2022-03-14T16:34:49Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://725ea6fe63a39f2fe0faa5ebda9cf7b9ab6c1adf6b05c021f6c1e8072099aa24
            Exit Code:     0
            Finished At:   2022-03-14T16:35:03Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:03Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://ecbdf8d7d8c2418498a0b3c3f23747bbcfd779d4d8080059a5cabddd7c7b1cd1
            Exit Code:     0
            Finished At:   2022-03-14T16:35:03Z
            Reason:        Completed
            Started At:    2022-03-14T16:35:03Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/8MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out3.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w3sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w3sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
    write-read-array-pipeline-run-jdzg4-write4-task-zjw47:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-14T16:34:58Z
        Conditions:
          Last Transition Time:  2022-03-14T16:34:58Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-jdzg4-write4-task-zjw47-pod-v4fw5
        Start Time:              2022-03-14T16:34:51Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://00801bd4bbced44f1262af41501de991d04dfd799b6861ddb0f8c22060750447
            Exit Code:     0
            Finished At:   2022-03-14T16:34:58Z
            Reason:        Completed
            Started At:    2022-03-14T16:34:57Z
          Container:       step-s-end
          Image ID:        docker-pullable://python@sha256:c90e15c86e2ebe71244a2a51bc7f094554422c159ce309a6faadb6debd5a6df0
          Name:            s-end
          Terminated:
            Container ID:  docker://f84ac84debc2a140537382bbe8431b9417bc858c261c1bc660859cb1deb72ecf
            Exit Code:     0
            Finished At:   2022-03-14T16:34:58Z
            Reason:        Completed
            Started At:    2022-03-14T16:34:58Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
string = 'x' * 8388608
# Store timestamp after data creation
with open("/data/timestamps/socket/4to1/8MB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the local pv
with open("/data/out_data/out4.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

            Image:              python
            Image Pull Policy:  IfNotPresent
            Name:               s-end
            Resources:
            Script:  #!/usr/bin/env python3
import os
import time
# Store initial timestamp
with open("/data/timestamps/socket/4to1/8MB/w4sent_start_ts.txt", 'a+') as fp_sent_s_ts:
   fp_sent_s_ts.write("%f\n" % time.time())
# Writes ready signal to sync pipe
os.system("echo 1 > /data/sync_pipe")
# Store final timestamp
with open("/data/timestamps/socket/4to1/8MB/w4sent_end_ts.txt", 'a+') as fp_sent_e_ts:
   fp_sent_e_ts.write("%f\n" % time.time())
#print("WRITER: Finished!")

          Workspaces:
            Mount Path:  /data
            Name:        write4-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    32s                PipelineRun  
  Normal  Running    32s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    23s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    22s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    19s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    17s (x2 over 17s)  PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
