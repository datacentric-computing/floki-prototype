Name:         write-read-array-pipeline-run-dn297
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-14T11:19:01Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-14T11:19:01Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-dn297-read1-task-62q48:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-dn297-read2-task-54zw5:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-dn297-read3-task-rbzml:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-dn297-write1-task-hp8bf:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-dn297-write2-task-tk8hx:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-dn297-write3-task-s928m:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-14T11:34:34Z
  Resource Version:  19963740
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-dn297
  UID:               6cceeb40-e7fd-446b-b3c7-7ba76013f0bb
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
Status:
  Completion Time:  2022-03-14T11:34:34Z
  Conditions:
    Last Transition Time:  2022-03-14T11:34:34Z
    Message:               Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-shared-ws
        Workspace:  shared-vol-ws
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-shared-ws
        Workspace:  shared-vol-ws
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-shared-ws
        Workspace:  shared-vol-ws
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-shared-ws
        Workspace:  shared-vol-ws
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-shared-ws
        Workspace:  shared-vol-ws
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-shared-ws
        Workspace:  shared-vol-ws
        Name:       read3-ws
        Workspace:  task-read3-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
  Start Time:  2022-03-14T11:19:01Z
  Task Runs:
    write-read-array-pipeline-run-dn297-read1-task-62q48:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-14T11:34:34Z
        Conditions:
          Last Transition Time:  2022-03-14T11:34:34Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-read1-task-62q48-pod-8qc67
        Start Time:              2022-03-14T11:26:04Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://e4ad59c856bff9481ef4b8bcf08ff39fcf398bda2b9eabf03f44b2f7a0438d70
            Exit Code:     0
            Finished At:   2022-03-14T11:34:32Z
            Reason:        Completed
            Started At:    2022-03-14T11:26:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out3.txt", "r") as fp_data:
   data = fp_data.read()
   del data
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-shared-ws
            Mount Path:  /timestamps
            Name:        read1-ws
    write-read-array-pipeline-run-dn297-read2-task-54zw5:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-14T11:34:26Z
        Conditions:
          Last Transition Time:  2022-03-14T11:34:26Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-read2-task-54zw5-pod-7h68h
        Start Time:              2022-03-14T11:26:04Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://53f0c47e0f99454cba9bc86e5970efa7470bab74d5ff052fc4745ef12d6a83bd
            Exit Code:     0
            Finished At:   2022-03-14T11:34:25Z
            Reason:        Completed
            Started At:    2022-03-14T11:26:13Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out3.txt", "r") as fp_data:
   data = fp_data.read()
   del data
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-shared-ws
            Mount Path:  /timestamps
            Name:        read2-ws
    write-read-array-pipeline-run-dn297-read3-task-rbzml:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-14T11:34:26Z
        Conditions:
          Last Transition Time:  2022-03-14T11:34:26Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-read3-task-rbzml-pod-hlrwz
        Start Time:              2022-03-14T11:26:04Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://6e9dab87429ac708b94fb210cc9c42b88482289a84408e7154844bb95bba3883
            Exit Code:     0
            Finished At:   2022-03-14T11:34:26Z
            Reason:        Completed
            Started At:    2022-03-14T11:26:13Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out3.txt", "r") as fp_data:
   data = fp_data.read()
   del data
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-shared-ws
            Mount Path:  /timestamps
            Name:        read3-ws
    write-read-array-pipeline-run-dn297-write1-task-hp8bf:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-14T11:25:59Z
        Conditions:
          Last Transition Time:  2022-03-14T11:25:59Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-write1-task-hp8bf-pod-dx7zw
        Start Time:              2022-03-14T11:19:01Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://7a37e3c09e2eaafe8fca0a27fca0e4bbcc08e97bdaa94d5ac041dcc04b261ac0
            Exit Code:     0
            Finished At:   2022-03-14T11:25:59Z
            Reason:        Completed
            Started At:    2022-03-14T11:19:10Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 17179869184
# Store timestamp after data creation 
with open("/timestamps/shared/3to3/16GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-shared-ws
            Mount Path:  /timestamps
            Name:        write1-ws
    write-read-array-pipeline-run-dn297-write2-task-tk8hx:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-14T11:25:54Z
        Conditions:
          Last Transition Time:  2022-03-14T11:25:54Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-write2-task-tk8hx-pod-7l9fs
        Start Time:              2022-03-14T11:19:01Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://aaa1885b810c7ac322257c463885dbf7ee4938f1ac36431adcc5168d892cb1aa
            Exit Code:     0
            Finished At:   2022-03-14T11:25:53Z
            Reason:        Completed
            Started At:    2022-03-14T11:19:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 17179869184
# Store timestamp after data creation 
with open("/timestamps/shared/3to3/16GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-shared-ws
            Mount Path:  /timestamps
            Name:        write2-ws
    write-read-array-pipeline-run-dn297-write3-task-s928m:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-14T11:26:04Z
        Conditions:
          Last Transition Time:  2022-03-14T11:26:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-dn297-write3-task-s928m-pod-nk9kj
        Start Time:              2022-03-14T11:19:02Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://c9f05739f3bf3f69cc240054dc3f3107b840f3e1bdfa09d1c8e1d52210b562b0
            Exit Code:     0
            Finished At:   2022-03-14T11:26:03Z
            Reason:        Completed
            Started At:    2022-03-14T11:19:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/3to3/16GB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 17179869184
# Store timestamp after data creation 
with open("/timestamps/shared/3to3/16GB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out3.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/3to3/16GB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-shared-ws
            Mount Path:  /timestamps
            Name:        write3-ws
Events:
  Type    Reason     Age    From         Message
  ----    ------     ----   ----         -------
  Normal  Started    15m    PipelineRun  
  Normal  Running    15m    PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    8m46s  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    8m41s  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    8m36s  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    14s    PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    14s    PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  6s     PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
