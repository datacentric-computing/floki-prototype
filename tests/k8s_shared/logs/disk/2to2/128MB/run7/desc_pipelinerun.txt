Name:         write-read-array-pipeline-run-mrkrm
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-13T18:20:38Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-13T18:20:38Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-mrkrm-read1-task-hmm9h:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-mrkrm-read2-task-bqf79:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-mrkrm-write1-task-v2x4q:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-mrkrm-write2-task-78jtm:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-13T18:21:16Z
  Resource Version:  18296265
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-mrkrm
  UID:               69371c81-6084-49fa-8dd8-3b10422ceb06
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
Status:
  Completion Time:  2022-03-13T18:21:16Z
  Conditions:
    Last Transition Time:  2022-03-13T18:21:16Z
    Message:               Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-shared-ws
        Workspace:  shared-vol-ws
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-shared-ws
        Workspace:  shared-vol-ws
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-shared-ws
        Workspace:  shared-vol-ws
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-shared-ws
        Workspace:  shared-vol-ws
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-03-13T18:20:38Z
  Task Runs:
    write-read-array-pipeline-run-mrkrm-read1-task-hmm9h:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-13T18:21:16Z
        Conditions:
          Last Transition Time:  2022-03-13T18:21:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-mrkrm-read1-task-hmm9h-pod-8dxxk
        Start Time:              2022-03-13T18:20:56Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://2659df2b7840cd3aefff458cc1dfad3a4b57ee5f925f6d9357c7995132b5f034
            Exit Code:     0
            Finished At:   2022-03-13T18:21:16Z
            Reason:        Completed
            Started At:    2022-03-13T18:21:05Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to2/128MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
   del data
# Store final timestamp
with open("/timestamps/shared/2to2/128MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-shared-ws
            Mount Path:  /timestamps
            Name:        read1-ws
    write-read-array-pipeline-run-mrkrm-read2-task-bqf79:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-13T18:21:12Z
        Conditions:
          Last Transition Time:  2022-03-13T18:21:12Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-mrkrm-read2-task-bqf79-pod-6j2pj
        Start Time:              2022-03-13T18:20:57Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://f07e8d50a0bade62f01bebe60da8b0b8f39f70bcc32d464f29ebe2ef25fc1b59
            Exit Code:     0
            Finished At:   2022-03-13T18:21:11Z
            Reason:        Completed
            Started At:    2022-03-13T18:21:03Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to2/128MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
   del data
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
   del data
# Store final timestamp
with open("/timestamps/shared/2to2/128MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-shared-ws
            Mount Path:  /timestamps
            Name:        read2-ws
    write-read-array-pipeline-run-mrkrm-write1-task-v2x4q:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-13T18:20:56Z
        Conditions:
          Last Transition Time:  2022-03-13T18:20:56Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-mrkrm-write1-task-v2x4q-pod-stjzm
        Start Time:              2022-03-13T18:20:38Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://c590142c13c632f6e8c4117c953e9f8c430324d03e05b000c0dd5834ad8f7c80
            Exit Code:     0
            Finished At:   2022-03-13T18:20:55Z
            Reason:        Completed
            Started At:    2022-03-13T18:20:49Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to2/128MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 134217728
# Store timestamp after data creation 
with open("/timestamps/shared/2to2/128MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/2to2/128MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-shared-ws
            Mount Path:  /timestamps
            Name:        write1-ws
    write-read-array-pipeline-run-mrkrm-write2-task-78jtm:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-13T18:20:54Z
        Conditions:
          Last Transition Time:  2022-03-13T18:20:54Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-mrkrm-write2-task-78jtm-pod-rd4q4
        Start Time:              2022-03-13T18:20:38Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://33e0526be9dcc7feb4ffd2ba1f04a82587cf652b996f41910c977d4f2ef8c6d4
            Exit Code:     0
            Finished At:   2022-03-13T18:20:54Z
            Reason:        Completed
            Started At:    2022-03-13T18:20:48Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to2/128MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 134217728
# Store timestamp after data creation 
with open("/timestamps/shared/2to2/128MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/2to2/128MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-shared-ws
            Mount Path:  /timestamps
            Name:        write2-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    47s                PipelineRun  
  Normal  Running    47s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    31s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    29s (x2 over 29s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    13s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
