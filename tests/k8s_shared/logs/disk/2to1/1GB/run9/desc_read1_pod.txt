Name:         write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl
Namespace:    default
Priority:     0
Node:         k8s-worker-node3/10.0.26.208
Start Time:   Fri, 04 Mar 2022 23:40:50 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-wqhc8
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-pipeline-run-wqhc8-read1-task-lcvpq
Annotations:  cni.projectcalico.org/containerID: b5437fcce7f5c8f43bf01ea9c4c929fe4a2620289ef3f82f573c817ca64fcded
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.198.217
IPs:
  IP:           192.168.198.217
Controlled By:  TaskRun/write-read-array-pipeline-run-wqhc8-read1-task-lcvpq
Init Containers:
  place-tools:
    Container ID:  docker://930ce58795b8b05a16024aff059e00e50fcd401b0261daa40f74dc4be06f4c4b
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:40:54 +0000
      Finished:     Fri, 04 Mar 2022 23:40:54 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  place-scripts:
    Container ID:  docker://2d5c99cbe7ad504edfd7fee59a934d15d279b3c1c9d3397fc124607f707955ab
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-xfln7"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzJ0bzEvMUdCL3IxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9zX3RzOgogICBmcF9yY29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFJlYWQgZGF0YSBvYmplY3QgZnJvbSByZW1vdGUgc2hhcmVkIHN0b3JhZ2UKd2l0aCBvcGVuKCIvZGF0YS9vdXQxLnR4dCIsICJyIikgYXMgZnBfZGF0YToKICAgZGF0YSA9IGZwX2RhdGEucmVhZCgpCndpdGggb3BlbigiL2RhdGEvb3V0Mi50eHQiLCAiciIpIGFzIGZwX2RhdGE6CiAgIGRhdGEgPSBmcF9kYXRhLnJlYWQoKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi90aW1lc3RhbXBzL3NoYXJlZC8ydG8xLzFHQi9yMWNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:40:56 +0000
      Finished:     Fri, 04 Mar 2022 23:40:56 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  istio-init:
    Container ID:  docker://5832ab59f2be8b370798d5c7281223c69f21d1b542ec940bb9532072a551a4a0
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:40:57 +0000
      Finished:     Fri, 04 Mar 2022 23:40:58 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Containers:
  step-read:
    Container ID:  docker://0496881a034c592cc9f5de8994b986d3c6bb88a608b0e9358b9dc2984e330b40
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-xfln7
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-04T23:41:03.933Z","type":3}]
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:40:59 +0000
      Finished:     Fri, 04 Mar 2022 23:41:14 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-6vzjl (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /timestamps from ws-4g7q7 (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://05dfbe230a12a405e9d787d16c38fbcfa8b10438d6693ae02080fb4a4ee81d80
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:41:23 +0000
      Finished:     Fri, 04 Mar 2022 23:41:23 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-6vzjl:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-ef076d7b04
    ReadOnly:   false
  ws-4g7q7:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task3-pv-claim
    ReadOnly:   false
  default-token-bn5jh:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-bn5jh
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node3
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age               From               Message
  ----     ------     ----              ----               -------
  Normal   Scheduled  38s               default-scheduler  Successfully assigned default/write-read-array-pipeline-run-wqhc8-read1-task-lcvpq-pod-xg8wl to k8s-worker-node3
  Normal   Pulled     36s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    36s               kubelet            Created container place-tools
  Normal   Started    35s               kubelet            Started container place-tools
  Normal   Created    34s               kubelet            Created container place-scripts
  Normal   Pulled     34s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Started    33s               kubelet            Started container place-scripts
  Normal   Pulled     32s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    32s               kubelet            Created container istio-init
  Normal   Started    32s               kubelet            Started container istio-init
  Normal   Created    31s               kubelet            Created container step-read
  Normal   Pulled     31s               kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    30s               kubelet            Started container step-read
  Normal   Pulled     30s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    13s               kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  9s (x3 over 13s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    7s (x2 over 29s)  kubelet            Created container istio-proxy
  Normal   Pulled     7s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  7s                kubelet            Readiness probe failed: Get "http://192.168.198.217:15021/healthz/ready": dial tcp 192.168.198.217:15021: connect: connection refused
  Normal   Started    6s (x2 over 29s)  kubelet            Started container istio-proxy
