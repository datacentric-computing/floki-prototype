Name:         write-read-array-pipeline-run-vgzkk
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-04T21:18:46Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-04T21:18:46Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-vgzkk-read1-task-r7htc:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vgzkk-write1-task-ts9sl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-vgzkk-write2-task-fssgj:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-04T21:19:22Z
  Resource Version:  7721854
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-vgzkk
  UID:               3f324aea-ec4c-4123-841d-e4148fb44805
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
Status:
  Completion Time:  2022-03-04T21:19:22Z
  Conditions:
    Last Transition Time:  2022-03-04T21:19:22Z
    Message:               Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-shared-ws
        Workspace:  shared-vol-ws
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-shared-ws
        Workspace:  shared-vol-ws
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-shared-ws
        Workspace:  shared-vol-ws
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
  Start Time:  2022-03-04T21:18:46Z
  Task Runs:
    write-read-array-pipeline-run-vgzkk-read1-task-r7htc:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-04T21:19:22Z
        Conditions:
          Last Transition Time:  2022-03-04T21:19:22Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vgzkk-read1-task-r7htc-pod-qzdfj
        Start Time:              2022-03-04T21:19:05Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       read
          Terminated:
            Container ID:  docker://6ed01bb74352cae19d548660fbc69af05ba4f01150aff596e526ca2508ba48ff
            Exit Code:     0
            Finished At:   2022-03-04T21:19:21Z
            Reason:        Completed
            Started At:    2022-03-04T21:19:21Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to1/1MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/timestamps/shared/2to1/1MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-shared-ws
            Mount Path:  /timestamps
            Name:        read1-ws
    write-read-array-pipeline-run-vgzkk-write1-task-ts9sl:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-04T21:19:04Z
        Conditions:
          Last Transition Time:  2022-03-04T21:19:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vgzkk-write1-task-ts9sl-pod-7vv5g
        Start Time:              2022-03-04T21:18:47Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       write
          Terminated:
            Container ID:  docker://80fddf8b0a589c3f7bfa7aa6aeb7e83ae2fe0cce7afa52675b06c845b1645849
            Exit Code:     0
            Finished At:   2022-03-04T21:19:04Z
            Reason:        Completed
            Started At:    2022-03-04T21:19:04Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to1/1MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1048576
# Store timestamp after data creation 
with open("/timestamps/shared/2to1/1MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/2to1/1MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-shared-ws
            Mount Path:  /timestamps
            Name:        write1-ws
    write-read-array-pipeline-run-vgzkk-write2-task-fssgj:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-04T21:18:55Z
        Conditions:
          Last Transition Time:  2022-03-04T21:18:55Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-vgzkk-write2-task-fssgj-pod-dwkbq
        Start Time:              2022-03-04T21:18:47Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       write
          Terminated:
            Container ID:  docker://2d95e16739aea83389bdc7bf5dbe870baf016c06b0308859f286cd95c71cb9b5
            Exit Code:     0
            Finished At:   2022-03-04T21:18:54Z
            Reason:        Completed
            Started At:    2022-03-04T21:18:54Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/2to1/1MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1048576
# Store timestamp after data creation 
with open("/timestamps/shared/2to1/1MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/2to1/1MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-shared-ws
            Mount Path:  /timestamps
            Name:        write2-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    44s                PipelineRun  
  Normal  Running    44s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    35s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    26s (x2 over 26s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
