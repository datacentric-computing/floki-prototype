Name:         write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9
Namespace:    default
Priority:     0
Node:         k8s-worker-node2/10.0.26.207
Start Time:   Fri, 04 Mar 2022 22:14:11 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-tg7k7
              tekton.dev/pipelineTask=write2-task
              tekton.dev/task=write2
              tekton.dev/taskRun=write-read-array-pipeline-run-tg7k7-write2-task-h9kkg
Annotations:  cni.projectcalico.org/containerID: d243b892f38147d22ef740da4a9882f5ad762e184e579febf16383fe259d26c8
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.219.70
IPs:
  IP:           192.168.219.70
Controlled By:  TaskRun/write-read-array-pipeline-run-tg7k7-write2-task-h9kkg
Init Containers:
  place-tools:
    Container ID:  docker://8b5860328b244c37422e6ad6cc3da574b457bfd7136eb103da5336746688e2c5
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 22:14:24 +0000
      Finished:     Fri, 04 Mar 2022 22:14:24 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  place-scripts:
    Container ID:  docker://8e15bf6f41fdd8dbc28f518a7dafb6104363d24f49cdb021a343e04a444f8636
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-sq2wx"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzJ0bzEvMTZNQi93MmNvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfc190czoKICAgZnBfd2NvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkgCiMgQ3JlYXRlIGRhdGEgb2JqZWN0CnN0cmluZyA9ICd4JyAqIDE2Nzc3MjE2CiMgU3RvcmUgdGltZXN0YW1wIGFmdGVyIGRhdGEgY3JlYXRpb24gCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzJ0bzEvMTZNQi93MmNvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiB0aGUgcmVtb3RlIHNoYXJlZCBzdG9yYWdlCndpdGggb3BlbigiL2RhdGEvb3V0Mi50eHQiLCAndycpIGFzIGZwX2RhdGE6CiAgIGZwX2RhdGEud3JpdGUoIiVzIiAlIHN0cmluZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvdGltZXN0YW1wcy9zaGFyZWQvMnRvMS8xNk1CL3cyY29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZV90czoKICAgZnBfd2NvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJXUklURVI6IFdyaXR0ZW4gb3V0MSEiKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 22:14:25 +0000
      Finished:     Fri, 04 Mar 2022 22:14:25 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  istio-init:
    Container ID:  docker://e28adc645e737425c84bde666054df4dd9e0de9f05396e75153dc8c277130027
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 22:14:26 +0000
      Finished:     Fri, 04 Mar 2022 22:14:26 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Containers:
  step-write:
    Container ID:  docker://66af075f1d54034f38db8d35364d05605a2f009acb3216f90574974acc6731e2
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-sq2wx
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-04T22:14:31.112Z","type":3}]
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 22:14:27 +0000
      Finished:     Fri, 04 Mar 2022 22:14:31 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-6x4bb (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /timestamps from ws-gns7z (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://c2cd5c2ef4eabc612be2cac68e7f89f8a053f7e8f6a5013448031d7a3ca2931c
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 22:14:38 +0000
      Finished:     Fri, 04 Mar 2022 22:14:38 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-6x4bb:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-bbb29bb572
    ReadOnly:   false
  ws-gns7z:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task2-pv-claim
    ReadOnly:   false
  default-token-bn5jh:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-bn5jh
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node2
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason                  Age                From               Message
  ----     ------                  ----               ----               -------
  Normal   Scheduled               50s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9 to k8s-worker-node2
  Warning  FailedCreatePodSandBox  48s                kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "bade36f2c29012ccbd78c81a3264a9f43de46b45aad14c741900c6db2719cd18" network for pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9": networkPlugin cni failed to set up pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Warning  FailedCreatePodSandBox  46s                kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "33dc4384c05782e0d49d376c0927d81c138b70ae3c733865524521186e10a180" network for pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9": networkPlugin cni failed to set up pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Warning  FailedCreatePodSandBox  44s                kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "e4b17ac7b329c21e018bb0d71dbc5acc672050ea72921ea599506cac5292c1fc" network for pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9": networkPlugin cni failed to set up pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Warning  FailedCreatePodSandBox  42s                kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "793b582a74d151941cb15dd2b572d302ad299e424474d87ac66b3b5ed43c66d5" network for pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9": networkPlugin cni failed to set up pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Normal   SandboxChanged          40s (x5 over 48s)  kubelet            Pod sandbox changed, it will be killed and re-created.
  Warning  FailedCreatePodSandBox  40s                kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "7ce16dcc29439c63b758e97d8dc952ddd4bfd814d2edc4ec017358b30ccdbdb4" network for pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9": networkPlugin cni failed to set up pod "write-read-array-pipeline-run-tg7k7-write2-task-h9kkg-pod-df5t9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Normal   Pulled                  39s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created                 39s                kubelet            Created container place-tools
  Normal   Started                 38s                kubelet            Started container place-tools
  Normal   Pulled                  38s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created                 38s                kubelet            Created container place-scripts
  Normal   Started                 37s                kubelet            Started container place-scripts
  Normal   Pulled                  37s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created                 37s                kubelet            Created container istio-init
  Normal   Started                 36s                kubelet            Started container istio-init
  Normal   Pulled                  36s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Created                 36s                kubelet            Created container step-write
  Normal   Started                 35s                kubelet            Started container step-write
  Normal   Pulled                  35s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created                 35s                kubelet            Created container istio-proxy
  Normal   Started                 35s                kubelet            Started container istio-proxy
