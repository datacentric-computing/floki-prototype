Name:         write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65
Namespace:    default
Priority:     0
Node:         k8s-worker-node2/10.0.26.207
Start Time:   Fri, 04 Mar 2022 23:18:33 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-2x9w8
              tekton.dev/pipelineTask=write2-task
              tekton.dev/task=write2
              tekton.dev/taskRun=write-read-array-pipeline-run-2x9w8-write2-task-vgtlf
Annotations:  cni.projectcalico.org/containerID: f108a8021facd93f7902c804c08c8a3fb3818dc03ed163f787b9df26158ad2d7
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.219.117
IPs:
  IP:           192.168.219.117
Controlled By:  TaskRun/write-read-array-pipeline-run-2x9w8-write2-task-vgtlf
Init Containers:
  place-tools:
    Container ID:  docker://3f7fe85b0e47e675d2c68273a533f80d861126f3c7ae4b27c0b536528e83c034
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:34 +0000
      Finished:     Fri, 04 Mar 2022 23:18:34 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  place-scripts:
    Container ID:  docker://f820c3a495a93c5f2587052f684261ea5fdc85a1af4752f050bf71cf9efb8b80
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-wc9cp"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzJ0bzEvNTEyTUIvdzJjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X3NfdHM6CiAgIGZwX3djb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpIAojIENyZWF0ZSBkYXRhIG9iamVjdApzdHJpbmcgPSAneCcgKiA1MzY4NzA5MTIKIyBTdG9yZSB0aW1lc3RhbXAgYWZ0ZXIgZGF0YSBjcmVhdGlvbiAKd2l0aCBvcGVuKCIvdGltZXN0YW1wcy9zaGFyZWQvMnRvMS81MTJNQi93MmNvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiB0aGUgcmVtb3RlIHNoYXJlZCBzdG9yYWdlCndpdGggb3BlbigiL2RhdGEvb3V0Mi50eHQiLCAndycpIGFzIGZwX2RhdGE6CiAgIGZwX2RhdGEud3JpdGUoIiVzIiAlIHN0cmluZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvdGltZXN0YW1wcy9zaGFyZWQvMnRvMS81MTJNQi93MmNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X2VfdHM6CiAgIGZwX3djb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiV1JJVEVSOiBXcml0dGVuIG91dDEhIikK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:35 +0000
      Finished:     Fri, 04 Mar 2022 23:18:35 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
  istio-init:
    Container ID:  docker://25184918ebe5b517461b331a540f0b35776a8fb8cd807a8f8f95ae425cd776d2
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:36 +0000
      Finished:     Fri, 04 Mar 2022 23:18:37 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Containers:
  step-write:
    Container ID:  docker://f590e55131b1deeee7a38da1ebe6a647bbe1ba1653e7e77dbbd6b90289e3292a
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-wc9cp
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-04T23:18:40.744Z","type":3}]
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:37 +0000
      Finished:     Fri, 04 Mar 2022 23:18:44 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-xfjbg (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /timestamps from ws-m98hf (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://3c1b529e12e17b14c423eea24031ec6faf27244d9b8b945bc4a5dbdc4d1ec2e9
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:51 +0000
      Finished:     Fri, 04 Mar 2022 23:18:51 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 04 Mar 2022 23:18:38 +0000
      Finished:     Fri, 04 Mar 2022 23:18:51 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-bn5jh (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-xfjbg:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-46d71fd812
    ReadOnly:   false
  ws-m98hf:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task2-pv-claim
    ReadOnly:   false
  default-token-bn5jh:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-bn5jh
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node2
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  58s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-2x9w8-write2-task-vgtlf-pod-jkc65 to k8s-worker-node2
  Normal   Pulled     57s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    57s                kubelet            Created container place-tools
  Normal   Started    57s                kubelet            Started container place-tools
  Normal   Pulled     56s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    56s                kubelet            Created container place-scripts
  Normal   Started    56s                kubelet            Started container place-scripts
  Normal   Pulled     55s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    55s                kubelet            Created container istio-init
  Normal   Started    55s                kubelet            Started container istio-init
  Normal   Pulled     54s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    54s                kubelet            Created container step-write
  Normal   Started    54s                kubelet            Started container step-write
  Normal   Pulled     54s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Killing    45s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  42s (x2 over 44s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    40s (x2 over 54s)  kubelet            Created container istio-proxy
  Normal   Started    40s (x2 over 53s)  kubelet            Started container istio-proxy
  Normal   Pulled     40s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  40s                kubelet            Readiness probe failed: Get "http://192.168.219.117:15021/healthz/ready": dial tcp 192.168.219.117:15021: connect: connection refused
