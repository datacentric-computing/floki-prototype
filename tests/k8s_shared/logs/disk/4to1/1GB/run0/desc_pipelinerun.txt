Name:         write-read-array-pipeline-run-2v8j5
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-09T04:52:18Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-09T04:52:18Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-2v8j5-read1-task-pk2c2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2v8j5-write1-task-55k89:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2v8j5-write2-task-62d2r:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2v8j5-write3-task-8f5p7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-2v8j5-write4-task-5zz9k:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-09T04:53:25Z
  Resource Version:  7724946
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-2v8j5
  UID:               d1863b1f-c24f-4423-815c-9b1d84c1675f
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-03-09T04:53:25Z
  Conditions:
    Last Transition Time:  2022-03-09T04:53:25Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-shared-ws
        Workspace:  shared-vol-ws
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-shared-ws
        Workspace:  shared-vol-ws
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-shared-ws
        Workspace:  shared-vol-ws
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-shared-ws
        Workspace:  shared-vol-ws
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
        write4-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-shared-ws
        Workspace:  shared-vol-ws
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-read1-ws
  Start Time:  2022-03-09T04:52:18Z
  Task Runs:
    write-read-array-pipeline-run-2v8j5-read1-task-pk2c2:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-09T04:53:25Z
        Conditions:
          Last Transition Time:  2022-03-09T04:53:25Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2v8j5-read1-task-pk2c2-pod-dnxw8
        Start Time:              2022-03-09T04:52:55Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://76f3d8fc78b3379b6fabb909b1ae1ccea01a59aa834d10ff5547f3a0a0c64832
            Exit Code:     0
            Finished At:   2022-03-09T04:53:25Z
            Reason:        Completed
            Started At:    2022-03-09T04:53:03Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/4to1/1GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out1.txt", "r") as fp_data:
   data = fp_data.read()
with open("/data/out2.txt", "r") as fp_data:
   data = fp_data.read()
with open("/data/out3.txt", "r") as fp_data:
   data = fp_data.read()
with open("/data/out4.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/timestamps/shared/4to1/1GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-shared-ws
            Mount Path:  /timestamps
            Name:        read1-ws
    write-read-array-pipeline-run-2v8j5-write1-task-55k89:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-09T04:52:51Z
        Conditions:
          Last Transition Time:  2022-03-09T04:52:51Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2v8j5-write1-task-55k89-pod-nzk6z
        Start Time:              2022-03-09T04:52:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://8e9c6f976dab2488d5c1400687ba08ddb90b510b7a71298a3eae18da2b991e38
            Exit Code:     0
            Finished At:   2022-03-09T04:52:50Z
            Reason:        Completed
            Started At:    2022-03-09T04:52:27Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/4to1/1GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1073741824
# Store timestamp after data creation 
with open("/timestamps/shared/4to1/1GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out1.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/4to1/1GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write1-shared-ws
            Mount Path:  /timestamps
            Name:        write1-ws
    write-read-array-pipeline-run-2v8j5-write2-task-62d2r:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-09T04:52:43Z
        Conditions:
          Last Transition Time:  2022-03-09T04:52:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2v8j5-write2-task-62d2r-pod-z96tv
        Start Time:              2022-03-09T04:52:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://6f62ab5e432593b35479e4abe42728494932f058608e95c742c4d42a15a6b8e4
            Exit Code:     0
            Finished At:   2022-03-09T04:52:43Z
            Reason:        Completed
            Started At:    2022-03-09T04:52:25Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/4to1/1GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1073741824
# Store timestamp after data creation 
with open("/timestamps/shared/4to1/1GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out2.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/4to1/1GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write2-shared-ws
            Mount Path:  /timestamps
            Name:        write2-ws
    write-read-array-pipeline-run-2v8j5-write3-task-8f5p7:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-09T04:52:55Z
        Conditions:
          Last Transition Time:  2022-03-09T04:52:55Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2v8j5-write3-task-8f5p7-pod-g25rn
        Start Time:              2022-03-09T04:52:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://e635d65eeb6c398733f5167f77b9aeaac17a11eda87350d0d6613e7de35836a8
            Exit Code:     0
            Finished At:   2022-03-09T04:52:54Z
            Reason:        Completed
            Started At:    2022-03-09T04:52:34Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/4to1/1GB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1073741824
# Store timestamp after data creation 
with open("/timestamps/shared/4to1/1GB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out3.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/4to1/1GB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write3-shared-ws
            Mount Path:  /timestamps
            Name:        write3-ws
    write-read-array-pipeline-run-2v8j5-write4-task-5zz9k:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-09T04:52:49Z
        Conditions:
          Last Transition Time:  2022-03-09T04:52:49Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-2v8j5-write4-task-5zz9k-pod-mm6qd
        Start Time:              2022-03-09T04:52:18Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://340a234b1fe499122243249f4fca95e378ba61817fd33d3ef80cab66dedb1f9d
            Exit Code:     0
            Finished At:   2022-03-09T04:52:49Z
            Reason:        Completed
            Started At:    2022-03-09T04:52:27Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/4to1/1GB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 1073741824
# Store timestamp after data creation 
with open("/timestamps/shared/4to1/1GB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out4.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/4to1/1GB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        write4-shared-ws
            Mount Path:  /timestamps
            Name:        write4-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    76s                PipelineRun  
  Normal  Running    76s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    50s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    45s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    43s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    39s (x2 over 39s)  PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
