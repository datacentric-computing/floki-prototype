Name:         write-read-array-pipeline-run-zwmdk
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-01-27T10:11:46Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-01-27T10:11:46Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-zwmdk-read-task:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-zwmdk-write-task:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-01-27T10:12:14Z
  Resource Version:  7808766
  UID:               a0eb9945-8ab8-4219-b116-b9725ff5d7b9
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-writer-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-reader-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
Status:
  Completion Time:  2022-01-27T10:12:14Z
  Conditions:
    Last Transition Time:  2022-01-27T10:12:14Z
    Message:               Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write-task
      Task Ref:
        Kind:  Task
        Name:  write
      Workspaces:
        Name:       writer-shared-ws
        Workspace:  shared-vol-ws
        Name:       writer-ws
        Workspace:  task-writer-ws
      Name:         read-task
      Run After:
        write-task
      Task Ref:
        Kind:  Task
        Name:  read
      Workspaces:
        Name:       reader-shared-ws
        Workspace:  shared-vol-ws
        Name:       reader-ws
        Workspace:  task-reader-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-writer-ws
      Name:    task-reader-ws
  Start Time:  2022-01-27T10:11:46Z
  Task Runs:
    Write - Read - Array - Pipeline - Run - Zwmdk - Read - Task:
      Pipeline Task Name:  read-task
      Status:
        Completion Time:  2022-01-27T10:12:14Z
        Conditions:
          Last Transition Time:  2022-01-27T10:12:14Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zwmdk-read-task-pod
        Start Time:              2022-01-27T10:12:01Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
          Name:       read
          Terminated:
            Container ID:  docker://64c4eb7f0442b925467f71a17bc362c4ee4ddb9eaafa74dd519804dab8bdd334
            Exit Code:     0
            Finished At:   2022-01-27T10:12:13Z
            Reason:        Completed
            Started At:    2022-01-27T10:12:10Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/512MB/rcont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/timestamps/shared/512MB/rcont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        reader-shared-ws
            Mount Path:  /timestamps
            Name:        reader-ws
    Write - Read - Array - Pipeline - Run - Zwmdk - Write - Task:
      Pipeline Task Name:  write-task
      Status:
        Completion Time:  2022-01-27T10:12:01Z
        Conditions:
          Last Transition Time:  2022-01-27T10:12:01Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-zwmdk-write-task-pod
        Start Time:              2022-01-27T10:11:46Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
          Name:       write
          Terminated:
            Container ID:  docker://aee47f7e8650292c0d7a743e4274d7cb5a5326bc42cc0575a074fce4763a8586
            Exit Code:     0
            Finished At:   2022-01-27T10:12:00Z
            Reason:        Completed
            Started At:    2022-01-27T10:11:56Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/512MB/wcont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 536870912
# Store timestamp after data creation 
with open("/timestamps/shared/512MB/wcont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/512MB/wcont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        writer-shared-ws
            Mount Path:  /timestamps
            Name:        writer-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    38s (x2 over 38s)  PipelineRun  
  Normal  Running    38s (x2 over 38s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    23s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  10s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
