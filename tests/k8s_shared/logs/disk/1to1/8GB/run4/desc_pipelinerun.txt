Name:         write-read-array-pipeline-run-gfvrc
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-01-27T11:37:06Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-01-27T11:37:06Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-gfvrc-read-task:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-gfvrc-write-task:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-01-27T11:39:13Z
  Resource Version:  7941147
  UID:               e39e2bbc-9417-4938-8ecc-336f99bf8ddf
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
  Timeout:                       1h0m0s
  Workspaces:
    Name:  shared-vol-ws
    Volume Claim Template:
      Metadata:
        Creation Timestamp:  <nil>
      Spec:
        Access Modes:
          ReadWriteMany
        Resources:
          Requests:
            Storage:  50Gi
      Status:
    Name:  task-writer-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-reader-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
Status:
  Completion Time:  2022-01-27T11:39:13Z
  Conditions:
    Last Transition Time:  2022-01-27T11:39:13Z
    Message:               Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write-task
      Task Ref:
        Kind:  Task
        Name:  write
      Workspaces:
        Name:       writer-shared-ws
        Workspace:  shared-vol-ws
        Name:       writer-ws
        Workspace:  task-writer-ws
      Name:         read-task
      Run After:
        write-task
      Task Ref:
        Kind:  Task
        Name:  read
      Workspaces:
        Name:       reader-shared-ws
        Workspace:  shared-vol-ws
        Name:       reader-ws
        Workspace:  task-reader-ws
    Workspaces:
      Name:    shared-vol-ws
      Name:    task-writer-ws
      Name:    task-reader-ws
  Start Time:  2022-01-27T11:37:06Z
  Task Runs:
    Write - Read - Array - Pipeline - Run - Gfvrc - Read - Task:
      Pipeline Task Name:  read-task
      Status:
        Completion Time:  2022-01-27T11:39:13Z
        Conditions:
          Last Transition Time:  2022-01-27T11:39:13Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-gfvrc-read-task-pod
        Start Time:              2022-01-27T11:38:17Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
          Name:       read
          Terminated:
            Container ID:  docker://7b680aece0a395a91166670df2e720ebe54652a119c379551aaee87443148f80
            Exit Code:     0
            Finished At:   2022-01-27T11:39:11Z
            Reason:        Completed
            Started At:    2022-01-27T11:38:25Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/8GB/rcont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
# Read data object from remote shared storage
with open("/data/out.txt", "r") as fp_data:
   data = fp_data.read()
# Store final timestamp
with open("/timestamps/shared/8GB/rcont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        reader-shared-ws
            Mount Path:  /timestamps
            Name:        reader-ws
    Write - Read - Array - Pipeline - Run - Gfvrc - Write - Task:
      Pipeline Task Name:  write-task
      Status:
        Completion Time:  2022-01-27T11:38:16Z
        Conditions:
          Last Transition Time:  2022-01-27T11:38:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-gfvrc-write-task-pod
        Start Time:              2022-01-27T11:37:06Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
          Name:       write
          Terminated:
            Container ID:  docker://15b58ed71c98074bfdafdfa383089dcf1b9806b1769804b23e05d3da75253c85
            Exit Code:     0
            Finished At:   2022-01-27T11:38:14Z
            Reason:        Completed
            Started At:    2022-01-27T11:37:13Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/timestamps/shared/8GB/wcont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Create data object
string = 'x' * 8589934592
# Store timestamp after data creation 
with open("/timestamps/shared/8GB/wcont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on the remote shared storage
with open("/data/out.txt", 'w') as fp_data:
   fp_data.write("%s" % string)
# Store final timestamp
with open("/timestamps/shared/8GB/wcont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /data
            Name:        writer-shared-ws
            Mount Path:  /timestamps
            Name:        writer-ws
Events:
  Type    Reason     Age                    From         Message
  ----    ------     ----                   ----         -------
  Normal  Started    2m17s (x2 over 2m17s)  PipelineRun  
  Normal  Running    2m17s (x2 over 2m17s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    67s (x2 over 67s)      PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  10s                    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
