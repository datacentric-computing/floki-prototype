Name:         write-read-array-pipeline-run-h9b7d-write-task-pod
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Thu, 27 Jan 2022 09:19:44 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-h9b7d-write-task-pod
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-h9b7d
              tekton.dev/pipelineTask=write-task
              tekton.dev/task=write
              tekton.dev/taskRun=write-read-array-pipeline-run-h9b7d-write-task
Annotations:  cni.projectcalico.org/containerID: dac2ba500a2cf77d6355ffdcd2de1fbb75954bd0d1eeb22a4e4a9cc2afa99e8e
              cni.projectcalico.org/podIP: 192.168.50.222/32
              cni.projectcalico.org/podIPs: 192.168.50.222/32
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 123c78a
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Running
IP:           192.168.50.222
IPs:
  IP:           192.168.50.222
Controlled By:  TaskRun/write-read-array-pipeline-run-h9b7d-write-task
Init Containers:
  place-tools:
    Container ID:  docker://befdac1887550ec3a29fccfdf29662a60f491da4da777489387f09fbda6658dc
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.30.0@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:19:47 +0000
      Finished:     Thu, 27 Jan 2022 09:19:47 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
  place-scripts:
    Container ID:  docker://08cdf4478bfbafd03e17c174574eb56951b7881708920099de2b4a204bd599a6
    Image:         gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-msn62"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzMyTUIvd2NvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfc190czoKICAgZnBfd2NvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkgCiMgQ3JlYXRlIGRhdGEgb2JqZWN0CnN0cmluZyA9ICd4JyAqIDMzNTU0NDMyCiMgU3RvcmUgdGltZXN0YW1wIGFmdGVyIGRhdGEgY3JlYXRpb24gCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzMyTUIvd2NvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiB0aGUgcmVtb3RlIHNoYXJlZCBzdG9yYWdlCndpdGggb3BlbigiL2RhdGEvb3V0LnR4dCIsICd3JykgYXMgZnBfZGF0YToKICAgZnBfZGF0YS53cml0ZSgiJXMiICUgc3RyaW5nKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi90aW1lc3RhbXBzL3NoYXJlZC8zMk1CL3djb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojcHJpbnQoIldSSVRFUjogV3JpdHRlbiBvdXQxISIpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:19:48 +0000
      Finished:     Thu, 27 Jan 2022 09:19:48 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
  istio-init:
    Container ID:  docker://ba31d730c09513e002c9badd5ed7bd80bfb139c053103f1236368a1f35b453c1
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:19:49 +0000
      Finished:     Thu, 27 Jan 2022 09:19:49 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
Containers:
  step-write:
    Container ID:  docker://c6fc034a8878ccbb5c0b2d075a17d67b09e54009014975df611d98556c65f32c
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-msn62
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-01-27T09:19:53.548Z","type":3}]
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:19:50 +0000
      Finished:     Thu, 27 Jan 2022 09:19:53 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-xwx9l (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /timestamps from ws-dfd5c (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://ccb7f46cdb28fa49805b43b241c372545ae8c5f6e0e2ed42edcf60f3f9b7c4cb
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Running
      Started:      Thu, 27 Jan 2022 09:19:50 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-h9b7d-write-task-pod (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-h9b7d-write-task-pod
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-h9b7d-write-task-pod
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-xwx9l:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-1762d59b00
    ReadOnly:   false
  ws-dfd5c:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-8g96z:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-8g96z
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  40s   default-scheduler  Successfully assigned default/write-read-array-pipeline-run-h9b7d-write-task-pod to k8s-worker-node1
  Normal  Pulled     38s   kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.30.0@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f" already present on machine
  Normal  Created    38s   kubelet            Created container place-tools
  Normal  Started    38s   kubelet            Started container place-tools
  Normal  Pulled     38s   kubelet            Container image "gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b" already present on machine
  Normal  Created    38s   kubelet            Created container place-scripts
  Normal  Started    37s   kubelet            Started container place-scripts
  Normal  Pulled     37s   kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal  Created    37s   kubelet            Created container istio-init
  Normal  Started    36s   kubelet            Started container istio-init
  Normal  Pulled     36s   kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal  Created    35s   kubelet            Created container step-write
  Normal  Started    35s   kubelet            Started container step-write
  Normal  Pulled     35s   kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal  Created    35s   kubelet            Created container istio-proxy
  Normal  Started    35s   kubelet            Started container istio-proxy
