Name:         write-read-array-pipeline-run-xwms8-write-task-pod
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Thu, 27 Jan 2022 09:59:10 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-xwms8-write-task-pod
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-xwms8
              tekton.dev/pipelineTask=write-task
              tekton.dev/task=write
              tekton.dev/taskRun=write-read-array-pipeline-run-xwms8-write-task
Annotations:  cni.projectcalico.org/containerID: 7ccbd8f3b3e6d4e0fc299c3274bec4a7249ae2b987c108d380b4070503d0ba44
              cni.projectcalico.org/podIP: 192.168.50.251/32
              cni.projectcalico.org/podIPs: 192.168.50.251/32
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 123c78a
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Running
IP:           192.168.50.251
IPs:
  IP:           192.168.50.251
Controlled By:  TaskRun/write-read-array-pipeline-run-xwms8-write-task
Init Containers:
  place-tools:
    Container ID:  docker://dd43c32157aed752743595874d230ebbd2fc4eeff1104d162b94932783a4c458
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.30.0@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:59:13 +0000
      Finished:     Thu, 27 Jan 2022 09:59:14 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
  place-scripts:
    Container ID:  docker://c947667b8816a64b52f02a5f3b3c35d9f0f9a31ad38897659d26992f8e07b617
    Image:         gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-4rs6s"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL3RpbWVzdGFtcHMvc2hhcmVkLzI1Nk1CL3djb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X3NfdHM6CiAgIGZwX3djb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpIAojIENyZWF0ZSBkYXRhIG9iamVjdApzdHJpbmcgPSAneCcgKiAyNjg0MzU0NTYKIyBTdG9yZSB0aW1lc3RhbXAgYWZ0ZXIgZGF0YSBjcmVhdGlvbiAKd2l0aCBvcGVuKCIvdGltZXN0YW1wcy9zaGFyZWQvMjU2TUIvd2NvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiB0aGUgcmVtb3RlIHNoYXJlZCBzdG9yYWdlCndpdGggb3BlbigiL2RhdGEvb3V0LnR4dCIsICd3JykgYXMgZnBfZGF0YToKICAgZnBfZGF0YS53cml0ZSgiJXMiICUgc3RyaW5nKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi90aW1lc3RhbXBzL3NoYXJlZC8yNTZNQi93Y29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZV90czoKICAgZnBfd2NvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJXUklURVI6IFdyaXR0ZW4gb3V0MSEiKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:59:15 +0000
      Finished:     Thu, 27 Jan 2022 09:59:15 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
  istio-init:
    Container ID:  docker://37491a7388c050c313d0ff534e57dbc97d229cf400b93f6b509fc6456c58932c
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:59:16 +0000
      Finished:     Thu, 27 Jan 2022 09:59:16 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
Containers:
  step-write:
    Container ID:  docker://42c0784e457c1b4e33ef3a81ec3666d8772297c4512efad0d7fda422bebe4f58
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:2a467dbb6542cf4d83e58e6e274d351f1f2d0f332ef81efefca53516b754d5f2
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-4rs6s
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-01-27T09:59:20.421Z","type":3}]
      Exit Code:    0
      Started:      Thu, 27 Jan 2022 09:59:17 +0000
      Finished:     Thu, 27 Jan 2022 09:59:22 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-dxvxd (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /timestamps from ws-dwsv2 (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://9365de9ca0d79857b2098c3b91719b469ff224246bffa554dd166ead64a23828
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Running
      Started:      Thu, 27 Jan 2022 09:59:17 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-xwms8-write-task-pod (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-xwms8-write-task-pod
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-xwms8-write-task-pod
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8g96z (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-dxvxd:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-aa7366557f
    ReadOnly:   false
  ws-dwsv2:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-8g96z:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-8g96z
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  40s   default-scheduler  Successfully assigned default/write-read-array-pipeline-run-xwms8-write-task-pod to k8s-worker-node1
  Normal  Pulled     39s   kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.30.0@sha256:34ee7658bb8a657584e1ada8e84121758cc5d067c1f0740873d614d07423886f" already present on machine
  Normal  Started    38s   kubelet            Started container place-tools
  Normal  Created    38s   kubelet            Created container place-tools
  Normal  Started    36s   kubelet            Started container place-scripts
  Normal  Pulled     36s   kubelet            Container image "gcr.io/distroless/base@sha256:cfdc553400d41b47fd231b028403469811fcdbc0e69d66ea8030c5a0b5fbac2b" already present on machine
  Normal  Created    36s   kubelet            Created container place-scripts
  Normal  Started    35s   kubelet            Started container istio-init
  Normal  Pulled     35s   kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal  Created    35s   kubelet            Created container istio-init
  Normal  Pulled     34s   kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal  Created    34s   kubelet            Created container step-write
  Normal  Started    34s   kubelet            Started container step-write
  Normal  Pulled     34s   kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal  Created    34s   kubelet            Created container istio-proxy
  Normal  Started    34s   kubelet            Started container istio-proxy
