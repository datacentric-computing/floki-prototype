NUM_TASKS=$1

# Delete pipelineruns
kubectl delete pipelinerun --all 2> /dev/null

# Delete pipelines
kubectl delete pipeline --all 2> /dev/null

# Delete tasks
kubectl delete task --all 2> /dev/null

# Delete pvcs and pvs
for (( i=0; i<=$NUM_TASKS; i++)); do
    kubectl delete pvc task${i}-pv-claim 2> /dev/null
    kubectl delete pv tekton-disk-pv-hostpath${i} 2> /dev/null
done
kubectl delete pv tekton-nfs-disk-pv 2> /dev/null
