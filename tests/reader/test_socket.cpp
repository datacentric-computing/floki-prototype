#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>           
#include <fcntl.h>
#include <thread>             
#include <mutex>              
#include <condition_variable>
#include <filesystem>            
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <map>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <cstdlib> 
#include <fstream>
#include <sys/time.h>

using namespace std;

namespace fs = std::filesystem;

#define     NUM_RUNS            100
#define     NUM_BUFF_DIMS       8
#define     SERVER_PORT         4010

const char *this_node_ip = "10.0.26.207";

uint64_t DATA_DIM = (uint64_t)16*1024*1024*1024;

list<string> buffer_dims {
    "4K",
    "8K",
    "16K",
    "32K",
    "64K",
    "128K",
    "256K",
    "512K"
};

int buffer_sizes[] = {
    4*1024,
    8*1024,
    16*1024,
    32*1024,
    64*1024,
    128*1024,
    256*1024,
    512*1024
};

void get_n_bytes_from_socket(int socket_conn_fd, unsigned char *buffer, int n){
    int rec = 0;
    do{
        int nbytes = recv(socket_conn_fd, &buffer[rec], n-rec, 0);
        rec += nbytes;

    }while(rec < n);
}

void dump_timestamp_to_file(struct timeval timestamps[NUM_DIMS][NUM_RUNS]){
    // Dump the timestamp when finished to read
    for (int i = 0; i < NUM_DIMS; i++){
        auto it_buff_dim = buff_dims.begin();
        advance(it_buff_dim, i);
        string net_fpath = "socket_buffer/" + *it_buff_dim + "/net_end_ts.txt";
        ofstream ts_info;
        ts_info.open(net_fpath, ios_base::app);
        if(!ts_info){
            printf("Main: ERROR network file could not be opened!\n");
            exit(EXIT_FAILURE);
        }
        for (int j = 0; j < NUM_RUNS; j++){
            double timestamp_sec = timestamp.tv_sec + timestamp.tv_usec/1000000.0; 
            ts_info << fixed << timestamp_sec << endl;
        }
        ts_info.close();
    }
#if defined(PRINT)
    printf("Main: Written network end timestamp to file!\n");
#endif
}

void recv_data_obj_from_socket(int client_socket_fd, int buff_size, string buff_dim, unsigned char *data_buffer){
    uint64_t counter = 0;
    while (counter + buff_size <= DATA_DIM){
        // Receive full data packets from socket 
        get_n_bytes_from_socket(client_socket_fd, &data[counter], buff_size);
        counter += buff_size;
    }
    if(DATA_DIM - counter > 0){
        // Receive last not full data packet from socket
        get_n_bytes_from_socket(client_socket_fd, &data[counter], DATA_DIM - counter);  
        counter += DATA_DIM - counter;
    }
#if defined(PRINT)
    printf("Main: Received %lu/%lu bytes!\n", counter, DATA_DIM);
#endif
}

void get_socket_receive_buff_size(int socket_fd){
    unsigned int size = 0;
    socklen_t socklen = sizeof(size);
    getsockopt(socket_fd, SOL_SOCKET, SO_RCVBUF, &size, &socklen); 
    printf("Main: Socket receive buffer size is %u \n", size);
}

void set_socket_receive_buff_size(int socket_fd, size_t size){
    socklen_t socklen = sizeof(size);
#if defined(PRINT)   
    get_socket_receive_buff_size(socket_fd);
#endif
    if (setsockopt(socket_fd, SOL_SOCKET, SO_RCVBUF, &size, socklen) == -1) {
        printf("Main: Failed to set the socket receive buffer size to %zu!\n", size);
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    get_socket_receive_buff_size(socket_fd);
#endif
}

int setup_server_socket_and_get_clients_conn(){
    int server_socket_fd, max_sd, sd, activity, new_socket, address_len;
    struct sockaddr_in address;
    int count_clients = 0;
    //set of socket descriptors 
    fd_set readfds;
    // Initialise all client_socket[] to 0 so not checked 
    int client_socket_fd = 0;
    // Create the server socket
    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_fd == 0){
        printf("Main: ERROR in opening the server socket!\n");
        exit(EXIT_FAILURE);
    }
    printf("Main: Successfully opened the server socket!\n");
    // Define the type of server socket
    address.sin_addr.s_addr = inet_addr(this_node_ip);
    address.sin_family = AF_INET;
    address.sin_port = htons(SERVER_PORT);
    // Bind the socket to the address and port number
    if (bind(server_socket_fd, (struct sockaddr*)&address, sizeof(address)) < 0){
        printf("Main: ERROR in binding the server socket!\n");
    }
    // Listen on the socket (max 10 connections)
    if (listen(server_socket_fd, 1) == 0){
        printf("Main: Server socket listening..\n");
    }else{
        printf("Main: ERROR in server socket listening!\n");
        exit(EXIT_FAILURE);
    }
    // Accept incoming connections
    address_len = sizeof(address);
    printf("Main: Waiting for connections..\n");
    while(count_clients < 1){
        // Clear the socket set
        FD_ZERO(&readfds);
        // Add master socket to set 
        FD_SET(server_socket_fd, &readfds);
        max_sd = server_socket_fd;
        // Add child socket to set
        sd = client_socket_fd;
        // If valid socket descriptor then add to read list 
        if(sd > 0)
            FD_SET( sd , &readfds);
        // Highest file descriptor number, need it for the select function
        if(sd > max_sd)
            max_sd = sd;
        // Wait for an activity on the socket
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
        if ((activity < 0) && (errno!=EINTR))
            printf("Main: ERROR in selecting client socket!\n");
        // If something happened on the master socket, then its an incoming connection
        if (FD_ISSET(server_socket_fd, &readfds)){
            if ((new_socket = accept(server_socket_fd, (struct sockaddr *)&address, (socklen_t*)&address_len))<0)
                printf("Main: ERROR in accepting client socket!\n");
            printf("Main: New client connection from ip %s!\n", inet_ntoa(address.sin_addr));
            client_socket_fd = new_socket;
            count_clients++;
        }
    }
    return client_socket_fd;
}

int main (){
    int client_socket_fd;
    // Define, bind and listen to the socket
    client_socket_fd = setup_server_socket_and_get_clients_conn();
    // Run multiple data dimension
    struct timeval timestamps[NUM_DIMS][NUM_RUNS];
    for (int i = 0; i < size(buffer_dims); i++){
        auto it_buff_dim = buffer_dims.begin();
        advance(it_buff_dim, i);
        printf("******************* SOCKET BUFFER TEST WITH %s *******************\n", it_buff_dim->c_str());
        // Dimension without spaces 
        string buff_dim = it_buff_dim->c_str();
        // Create the network times folder if it does not exists 
        string dir_path = "socket_buffer/" + buff_dim;
        const fs::path dir_path_fs{dir_path};
        if (fs::exists(dir_path_fs)){
            for (auto& path: fs::directory_iterator(dir_path_fs)) {
                fs::remove_all(path);
            }
        }else{
            string cmd = "mkdir -p " + dir_path;
            system(cmd.c_str());
#if defined(PRINT)
            printf("Main: Created local timestamps directory!\n");
#endif
        }
        // Setup socket receive buffer size
        set_socket_receive_buff_size(client_socket_fd, (uint64_t)buffer_sizes[i]);
        // Run multiple times
        for (int j = 0; j < NUM_RUNS; j++){
            printf("RUN %d..\n", j+1);       
            // Receive data content
            recv_data_obj_from_socket(client_socket_fd, buffer_sizes[i], buff_dim, data);
            gettimeofday(&timestamps[i][j], 0);
        }
    }
    // Dump timestamps to files 
    dump_timestamp_to_file(timestamps);
    // Close socket connections
    close(client_socket_fd);
#if defined(PRINT)
    printf("Main: Closed the socket connections!\n");
#endif
    return 0;
}

