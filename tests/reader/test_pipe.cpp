#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <filesystem>            
#include <sys/stat.h>
#include <list>
#include <string>
#include <numeric>
#include <cstring>
#include <cstdio>
#include <cstdlib> 
#include <fstream> 
#include <sys/time.h>

using namespace std;

namespace fs = std::filesystem;

#define     NUM_RUNS            100
#define     NUM_DATA_DIMS       8

uint64_t DATA_DIM = (uint64_t)16*1024*1024*1024;

list<string> buff_dims {
    "4K",
    "8K",
    "16K",
    "32K",
    "64K,"
    "128K",
    "256K",
    "512K"

uint64_t buffer_sizes[] = {
    4*1024,
    8*1024,
    16*1024,
    32*1024,
    64*1024,
    128*1024,
    256*1024,
    512*1024
};

int open_pipe(){
    char *pipe_path = (char*)"/mnt/mem/vol2/data_pipe";
    // Check if pipe exists, if not create it
    if (!fs::is_fifo(pipe_path)){
        int res = mkfifo(pipe_path, 0666);
        if (res != 0){
            printf("Main: ERROR could not create data_pipe!\n");
            exit(EXIT_FAILURE);
        }
#if defined(PRINT) 
        printf("Main: Created data_pipe!\n");
#endif
    }
    // Open the pipe
    int pipe_fd = open(pipe_path, O_RDONLY);
    if (pipe_fd < 0){
        printf("Main: ERROR opening data_pipe!\n");
        exit(EXIT_FAILURE);
    }
#if defined(PRINT) 
    printf("Main: data_pipe opened successfully in read mode!\n");
#endif
    return pipe_fd;
}

void dump_timestamp_to_file(struct timeval timestamps[NUM_DATA_DIMS][NUM_RUNS]){
    for (int i = 0; i < size(buff_dims); i++){
        auto it_buff_dim = buff_dims.begin();
        advance(it_buff_dim, i);
        string ts_fpath = "../pipe_buffer/timestamps/" + *it_buff_dim + "/end_ts.txt";
        ofstream ts_info;
        ts_info.open(ts_fpath, ios_base::app);
        if(!ts_info){
            printf("Main: ERROR timestamps file could not be opened!\n");
            exit(EXIT_FAILURE);
        }
        for (int j = 0; j < NUM_RUNS; j++){
            double timestamp_sec = timestamps[i][j].tv_sec + timestamps[i][j].tv_usec/1000000.0;
            ts_info << fixed << timestamp_sec << endl;
        }
        ts_info.close();
    }
#if defined(PRINT)
    printf("Main: Written start timestamp to file!\n");
#endif
}

int get_n_bytes_from_pipe(int pipe_fd, unsigned char *buffer, int n){
    int rec = 0;
    int num_read = 0;
    do{
        int n_bytes = read(pipe_fd, &buffer[rec], n-rec);
        num_read += 1;
        if(n_bytes < 0){
            printf("Main: ERROR Cannot read data object packet from pipe!\n");
            exit(EXIT_FAILURE);
        }
        rec += n_bytes;
    }while(rec < n);
    return num_read;
}

void read_data_from_pipe(int pipe_fd, uint64_t buff_size, string buff_dim){
    uint64_t counter = 0;
    int num_bytes = 0;
    uint64_t num_recv = 0;
    unsigned char data[buff_size];
    // Create buffer of data
    while (counter + buff_size <= DATA_DIM){
        //  Read full data packets from pipe
        num_recv += get_n_bytes_from_pipe(pipe_fd, data, buff_size);
        counter += buff_size;
    }
    if (DATA_DIM - counter > 0){
        // Read last not full data packet from pipe
        num_recv += get_n_bytes_from_pipe(pipe_fd, data, DATA_DIM - counter);
        counter = DATA_DIM;
    }
#if defined(PRINT) 
    printf("Main: Received %lu/%lu bytes!\n", counter, DATA_DIM);
#endif
}

int main (){
    // Run multiple buffer dimensions
    struct timeval timestamps[NUM_DATA_DIMS][NUM_RUNS];
    // Open pipe for reading
    int pipe_fd = open_pipe();
    for (int i = 0; i < size(buff_dims); i++){
        auto it_buff_dim = buff_dims.begin();
        advance(it_buff_dim, i);
        printf("******************* PIPE BUFFER TEST WITH %s *******************\n", it_buff_dim->c_str());
        // Read data objec from pipe
        long pipe_size = (long)fcntl(pipe_fd, F_GETPIPE_SZ);
        if (pipe_size != buffer_sizes[i]){
            int ret = fcntl(pipe_fd, F_SETPIPE_SZ, buffer_sizes[i]);
            long pipe_size2 = (long)fcntl(pipe_fd, F_GETPIPE_SZ);
#if defined(PRINT) 
            printf("Pipe buffer size is %ld\n", pipe_size2);
#endif
        }
        // Create the network times folder if it does not exists
        string buff_dim = it_buff_dim->c_str();
        fs::path working_dir(fs::current_path());
        string ts_folder = (working_dir.parent_path()).string() + "/pipe_buffer/timestamps/" + buff_dim; 
        const fs::path dir_path_fs{ts_folder};
        if (!fs::exists(dir_path_fs)){
            string cmd = "mkdir -p " + ts_folder;
            system(cmd.c_str());
#if defined(PRINT) 
            printf("Main: Created timestamps local directory!\n");
#endif
        }
        // Run multiple times
        for (int j = 0; j < NUM_RUNS; j++){
            printf("RUN %d..\n", j+1);
            read_data_from_pipe(pipe_fd, buffer_sizes[i], buff_dim);
            gettimeofday(&timestamps[i][j], 0);
        }
    }
    // Dumpo timestamps to files
    dump_timestamp_to_file(timestamps);
    // Close pipe
    close(pipe_fd);
#if defined(PRINT) 
    printf("Main: Closed data pipe!\n");
#endif
    return 0;
}
