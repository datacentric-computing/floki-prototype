Name:         write-read-array-cloud-pipeline-run-wtbh2
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-cloud-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-09T07:00:37Z
  Generate Name:       write-read-array-cloud-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-09T07:00:37Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-cloud-pipeline-run-wtbh2-read1-task-qrfdg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-read2-task-vwktk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-read3-task-x6vlt:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-read4-task-wvrsx:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-read5-task-9j6wq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-read6-task-cbhzn:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-wtbh2-write1-task-2qgfk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-09T07:01:32Z
  Resource Version:  211524805
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-cloud-pipeline-run-wtbh2
  UID:               f4b9e4f3-b0cd-4016-9c19-8c408d418f1e
Spec:
  Pipeline Ref:
    Name:                write-read-array-cloud-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  gcs-credentials
    Secret:
      Default Mode:  256
      Secret Name:   gcs-credentials
    Name:            task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read5-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read6-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-06-09T07:01:32Z
  Conditions:
    Last Transition Time:  2022-06-09T07:01:32Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read4-ws
        Workspace:  task-read4-ws
      Name:         read5-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read5
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read5-ws
        Workspace:  task-read5-ws
      Name:         read6-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read6
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read6-ws
        Workspace:  task-read6-ws
    Workspaces:
      Name:    gcs-credentials
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
      Name:    task-read5-ws
      Name:    task-read6-ws
  Start Time:  2022-06-09T07:00:37Z
  Task Runs:
    write-read-array-cloud-pipeline-run-wtbh2-read1-task-qrfdg:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-09T07:01:32Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:32Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read1-task-qrfd-k856f
        Start Time:              2022-06-09T07:01:11Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://6b3ebb6c463657860599c6ed12decce38b3e7af8b9fe4780ca8d84b32bce2a70
            Exit Code:     0
            Finished At:   2022-06-09T07:01:31Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:22Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-cloud-pipeline-run-wtbh2-read2-task-vwktk:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-09T07:01:32Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:32Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read2-task-vwkt-ctbkg
        Start Time:              2022-06-09T07:01:11Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://f65ce82674faf99b34c0bab6290a08e7a057e19fa858510fd058b7d56a798f5a
            Exit Code:     0
            Finished At:   2022-06-09T07:01:30Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:23Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-cloud-pipeline-run-wtbh2-read3-task-x6vlt:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-09T07:01:27Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:27Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read3-task-x6vl-7zgw5
        Start Time:              2022-06-09T07:01:11Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://d1bb7a7a9d31c50ca72208751013c768d5838544604ae2096aa1e83a9b930b4b
            Exit Code:     0
            Finished At:   2022-06-09T07:01:27Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:19Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-cloud-pipeline-run-wtbh2-read4-task-wvrsx:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-09T07:01:28Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:28Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read4-task-wvrs-lgjrx
        Start Time:              2022-06-09T07:01:11Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://78abcab81fd30c39d761f46135cb1eb3f6dcc6da466a2da46b559fc3f8536bad
            Exit Code:     0
            Finished At:   2022-06-09T07:01:28Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:20Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-cloud-pipeline-run-wtbh2-read5-task-9j6wq:
      Pipeline Task Name:  read5-task
      Status:
        Completion Time:  2022-06-09T07:01:28Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:28Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read5-task-9j6w-w2sqs
        Start Time:              2022-06-09T07:01:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://f82c2b910d0058ab7badc994aded93864b7081566256811f7c7a3a063ec56d74
            Exit Code:     0
            Finished At:   2022-06-09T07:01:27Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:19Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r5cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r5cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read5-ws
    write-read-array-cloud-pipeline-run-wtbh2-read6-task-cbhzn:
      Pipeline Task Name:  read6-task
      Status:
        Completion Time:  2022-06-09T07:01:28Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:28Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-read6-task-cbhz-7d5xd
        Start Time:              2022-06-09T07:01:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://b1372a6a0c2fd6ac50435b0d52d4ea60095d03b8cc0b484625c4870cd51d41e3
            Exit Code:     0
            Finished At:   2022-06-09T07:01:27Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:19Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r6cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum=None)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/r6cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read6-ws
    write-read-array-cloud-pipeline-run-wtbh2-write1-task-2qgfk:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-09T07:01:11Z
        Conditions:
          Last Transition Time:  2022-06-09T07:01:11Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-wtbh2-write1-task-2qg-bj8x7
        Start Time:              2022-06-09T07:00:37Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       write
          Terminated:
            Container ID:  docker://dca3b1d1b95914a533c5062b4303deaad50fed866c50012c211193e5c9f1dc86
            Exit Code:     0
            Finished At:   2022-06-09T07:01:09Z
            Reason:        Completed
            Started At:    2022-06-09T07:01:00Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage 
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Setup GCS credentials, bucket, and bucket
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json')
bucket = client.bucket('annastests')
blob = bucket.blob('out1.bin')
# Create data object
string = 'x' * 4194304
# Store timestamp after data creation 
with open("/local/timestamps/shared_cloud/1to6/4MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on cloud storage
blob.upload_from_string(string)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to6/4MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    64s                PipelineRun  
  Normal  Running    64s                PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    30s (x2 over 30s)  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    14s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    13s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    13s (x2 over 13s)  PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    13s                PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
