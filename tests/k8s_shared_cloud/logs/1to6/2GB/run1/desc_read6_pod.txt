Name:         write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl
Namespace:    default
Priority:     0
Node:         k8s-worker-node7/10.0.26.216
Start Time:   Thu, 09 Jun 2022 14:41:57 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-cloud-pipeline
              tekton.dev/pipelineRun=write-read-array-cloud-pipeline-run-gx8cr
              tekton.dev/pipelineTask=read6-task
              tekton.dev/task=read6
              tekton.dev/taskRun=write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6v
Annotations:  cni.projectcalico.org/containerID: 56ca9e7b91044c05aa9a498733d9722f044570019dfa94cf0b3256a31c94b380
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads an object from cloud
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.30.46
IPs:
  IP:           192.168.30.46
Controlled By:  TaskRun/write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6v
Init Containers:
  place-tools:
    Container ID:  docker://60cc8f510f932802128e04b7b9a83fb5fa414eca47ea013fe8bf09aee80df306
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 09 Jun 2022 14:41:59 +0000
      Finished:     Thu, 09 Jun 2022 14:41:59 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://14afac93271dffc4922251fc3803aca70bdac54c3e70f9567763b53eadad0e5a
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-b9pc5"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IHN1YnByb2Nlc3MKaW1wb3J0IHN5cwpzdWJwcm9jZXNzLmNoZWNrX2NhbGwoW3N5cy5leGVjdXRhYmxlLCAiLW0iLCAicGlwIiwgImluc3RhbGwiLCAiZ29vZ2xlLWNsb3VkLXN0b3JhZ2UiXSkKc3VicHJvY2Vzcy5jaGVja19jYWxsKFtzeXMuZXhlY3V0YWJsZSwgIi1tIiwgInBpcCIsICJpbnN0YWxsIiwgImdvb2dsZS1yZXN1bWFibGUtbWVkaWE9PTIuMy4zIl0pCmZyb20gZ29vZ2xlLmNsb3VkIGltcG9ydCBzdG9yYWdlCiMgU3RvcmUgaW5pdGlhbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfY2xvdWQvMXRvNi8yR0IvcjZjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X3NfdHM6CiAgIGZwX3Jjb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCmNsaWVudCA9IHN0b3JhZ2UuQ2xpZW50LmZyb21fc2VydmljZV9hY2NvdW50X2pzb24oJy93b3Jrc3BhY2UvY3JlZGVudGlhbHMvc2VydmljZV9hY2NvdW50X2djcy5qc29uJykgCmJ1Y2tldCA9IGNsaWVudC5idWNrZXQoJ2FubmFzdGVzdHMyJykKIyBSZWFkIGRhdGEgb2JqZWN0IGZyb20gcmVtb3RlIHNoYXJlZCBzdG9yYWdlCmJsb2IxID0gYnVja2V0LmdldF9ibG9iKCdvdXQxLmJpbicpCmRhdGEgPSBibG9iMS5kb3dubG9hZF9hc19ieXRlcyhjaGVja3N1bT1Ob25lKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9jbG91ZC8xdG82LzJHQi9yNmNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 09 Jun 2022 14:42:00 +0000
      Finished:     Thu, 09 Jun 2022 14:42:00 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://7b66b7904e4a54bb77dd4a1c6d5325a810afdc69f0f26aea68e50279e6a7dad7
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 09 Jun 2022 14:42:01 +0000
      Finished:     Thu, 09 Jun 2022 14:42:01 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://74a1002bf71260b3ed2d217cba319d02cb4531d98f284065e874a1a392b32fce
    Image:         gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Image ID:      docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-b9pc5
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-09T14:42:05.195Z","type":3}]
      Exit Code:    0
      Started:      Thu, 09 Jun 2022 14:42:02 +0000
      Finished:     Thu, 09 Jun 2022 14:44:38 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-qdplf (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
      /workspace/credentials from ws-2692b (rw)
  istio-proxy:
    Container ID:  docker://9748cead9b94ec7ab80063c68c910586f15ac641fbc6f3e5cf5a26ca0e96697d
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 09 Jun 2022 14:44:45 +0000
      Finished:     Thu, 09 Jun 2022 14:44:45 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-2692b:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  gcs-credentials
    Optional:    false
  ws-qdplf:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task7-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node7
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                 From               Message
  ----     ------     ----                ----               -------
  Normal   Scheduled  3m13s               default-scheduler  Successfully assigned default/write-read-array-cloud-pipeline-run-gx8cr-read6-task-r5x6-5pzhl to k8s-worker-node7
  Normal   Pulled     3m11s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    3m11s               kubelet            Created container place-tools
  Normal   Pulled     3m11s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    3m11s               kubelet            Created container place-scripts
  Normal   Started    3m11s               kubelet            Started container place-tools
  Normal   Started    3m10s               kubelet            Started container place-scripts
  Normal   Pulled     3m10s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    3m10s               kubelet            Created container istio-init
  Normal   Created    3m9s                kubelet            Created container step-read
  Normal   Pulled     3m9s                kubelet            Container image "gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9" already present on machine
  Normal   Started    3m9s                kubelet            Started container istio-init
  Normal   Started    3m8s                kubelet            Started container step-read
  Normal   Pulled     3m8s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    31s                 kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  26s (x3 over 30s)   kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    25s (x2 over 3m8s)  kubelet            Created container istio-proxy
  Normal   Started    25s (x2 over 3m8s)  kubelet            Started container istio-proxy
  Normal   Pulled     25s                 kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
