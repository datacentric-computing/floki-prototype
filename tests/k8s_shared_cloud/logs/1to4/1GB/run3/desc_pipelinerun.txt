Name:         write-read-array-cloud-pipeline-run-8w92z
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-cloud-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-08T06:53:17Z
  Generate Name:       write-read-array-cloud-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-08T06:53:17Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-cloud-pipeline-run-8w92z-read1-task-sw9pq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-8w92z-read2-task-lvqzd:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-8w92z-read3-task-qrmxs:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-8w92z-read4-task-fbg7k:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-8w92z-write1-task-4dzcl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-08T06:55:58Z
  Resource Version:  209292631
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-cloud-pipeline-run-8w92z
  UID:               b76e43d0-a95c-4030-980f-42a3e102613a
Spec:
  Pipeline Ref:
    Name:                write-read-array-cloud-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  gcs-credentials
    Secret:
      Default Mode:  256
      Secret Name:   gcs-credentials
    Name:            task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-06-08T06:55:57Z
  Conditions:
    Last Transition Time:  2022-06-08T06:55:57Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read4-ws
        Workspace:  task-read4-ws
    Workspaces:
      Name:    gcs-credentials
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
  Start Time:  2022-06-08T06:53:17Z
  Task Runs:
    write-read-array-cloud-pipeline-run-8w92z-read1-task-sw9pq:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-08T06:55:57Z
        Conditions:
          Last Transition Time:  2022-06-08T06:55:57Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-8w92z-read1-task-sw9p-6dxt4
        Start Time:              2022-06-08T06:54:41Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://8a76f13b37b06951002f36e6dd0b938ee957603f79fc76bcd1095e268cb545d9
            Exit Code:     0
            Finished At:   2022-06-08T06:55:56Z
            Reason:        Completed
            Started At:    2022-06-08T06:54:54Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-cloud-pipeline-run-8w92z-read2-task-lvqzd:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-08T06:55:52Z
        Conditions:
          Last Transition Time:  2022-06-08T06:55:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-8w92z-read2-task-lvqz-bgtpl
        Start Time:              2022-06-08T06:54:41Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://192ca765ac24b7781f516f0e2b9d37ec38bd56a08618b926ad8d06b97ff43b8e
            Exit Code:     0
            Finished At:   2022-06-08T06:55:52Z
            Reason:        Completed
            Started At:    2022-06-08T06:54:51Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-cloud-pipeline-run-8w92z-read3-task-qrmxs:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-08T06:55:53Z
        Conditions:
          Last Transition Time:  2022-06-08T06:55:53Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-8w92z-read3-task-qrmx-b6zxh
        Start Time:              2022-06-08T06:54:41Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://597c074beab13936e079f7d2312980251d294ca013136d6f68d67f8990ff0390
            Exit Code:     0
            Finished At:   2022-06-08T06:55:53Z
            Reason:        Completed
            Started At:    2022-06-08T06:54:50Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-cloud-pipeline-run-8w92z-read4-task-fbg7k:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-08T06:55:51Z
        Conditions:
          Last Transition Time:  2022-06-08T06:55:51Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-8w92z-read4-task-fbg7-rpqn9
        Start Time:              2022-06-08T06:54:41Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://1055dbee949951563320f3ff2fa2005b88c60f3ca0de9fd590bc87a7e7197d6a
            Exit Code:     0
            Finished At:   2022-06-08T06:55:52Z
            Reason:        Completed
            Started At:    2022-06-08T06:54:51Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-cloud-pipeline-run-8w92z-write1-task-4dzcl:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-08T06:54:41Z
        Conditions:
          Last Transition Time:  2022-06-08T06:54:41Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-8w92z-write1-task-4dz-rhc28
        Start Time:              2022-06-08T06:53:17Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       write
          Terminated:
            Container ID:  docker://6e23b73a70768fa6a7a646959daebbc5801a2f1dd462187944db77b333977ee8
            Exit Code:     0
            Finished At:   2022-06-08T06:54:40Z
            Reason:        Completed
            Started At:    2022-06-08T06:53:42Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage 
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Setup GCS credentials, bucket, and bucket
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json')
bucket = client.bucket('annastests')
blob = bucket.blob('out1.bin')
# Create data object
string = 'x' * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_cloud/1to4/1GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on cloud storage
blob.upload_from_string(string)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    2m49s              PipelineRun  
  Normal  Running    2m49s              PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    85s (x2 over 85s)  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    15s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    14s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    13s                PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
