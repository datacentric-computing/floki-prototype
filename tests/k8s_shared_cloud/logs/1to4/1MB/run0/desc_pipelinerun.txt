Name:         write-read-array-cloud-pipeline-run-bkmt9
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-cloud-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-07T21:18:39Z
  Generate Name:       write-read-array-cloud-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-07T21:18:39Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-cloud-pipeline-run-bkmt9-read1-task-97c9q:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-bkmt9-read2-task-vdl75:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-bkmt9-read3-task-7x7gl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-bkmt9-read4-task-b7tqp:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-bkmt9-write1-task-s72vc:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-07T21:20:20Z
  Resource Version:  208374471
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-cloud-pipeline-run-bkmt9
  UID:               5678a34c-8058-4399-a90d-5657483f00f6
Spec:
  Pipeline Ref:
    Name:                write-read-array-cloud-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  gcs-credentials
    Secret:
      Default Mode:  256
      Secret Name:   gcs-credentials
    Name:            task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-06-07T21:20:19Z
  Conditions:
    Last Transition Time:  2022-06-07T21:20:19Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read4-ws
        Workspace:  task-read4-ws
    Workspaces:
      Name:    gcs-credentials
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
  Start Time:  2022-06-07T21:18:39Z
  Task Runs:
    write-read-array-cloud-pipeline-run-bkmt9-read1-task-97c9q:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-07T21:19:32Z
        Conditions:
          Last Transition Time:  2022-06-07T21:19:32Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-bkmt9-read1-task-97c9-rc7l2
        Start Time:              2022-06-07T21:19:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://7041fbf8daf09e3be707f237c4d419e855ecba4edc549d80765e9e18d1a05dc5
            Exit Code:     0
            Finished At:   2022-06-07T21:19:31Z
            Reason:        Completed
            Started At:    2022-06-07T21:19:22Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-cloud-pipeline-run-bkmt9-read2-task-vdl75:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-07T21:19:35Z
        Conditions:
          Last Transition Time:  2022-06-07T21:19:35Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-bkmt9-read2-task-vdl7-56kpw
        Start Time:              2022-06-07T21:19:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://6040a8e15dfd79b6b64a81330e636cc81fcab6cc9906027d1ea9b19111a86602
            Exit Code:     0
            Finished At:   2022-06-07T21:19:34Z
            Reason:        Completed
            Started At:    2022-06-07T21:19:26Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-cloud-pipeline-run-bkmt9-read3-task-7x7gl:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-07T21:20:19Z
        Conditions:
          Last Transition Time:  2022-06-07T21:20:19Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-bkmt9-read3-task-7x7g-ztg8j
        Start Time:              2022-06-07T21:19:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://70d9211183b3cdb6bd27e74fe8d98d8c0053c3244d7a9b0d86204f4ad99b2cc9
            Exit Code:     0
            Finished At:   2022-06-07T21:20:20Z
            Reason:        Completed
            Started At:    2022-06-07T21:20:12Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-cloud-pipeline-run-bkmt9-read4-task-b7tqp:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-07T21:19:28Z
        Conditions:
          Last Transition Time:  2022-06-07T21:19:28Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-bkmt9-read4-task-b7tq-fdd6h
        Start Time:              2022-06-07T21:19:14Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://13214e62d0aa4ee2556afbe77fbd6d74adfef0cbb0be02e82732e2e8f9ddf42a
            Exit Code:     0
            Finished At:   2022-06-07T21:19:28Z
            Reason:        Completed
            Started At:    2022-06-07T21:19:21Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-cloud-pipeline-run-bkmt9-write1-task-s72vc:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-07T21:19:13Z
        Conditions:
          Last Transition Time:  2022-06-07T21:19:13Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-bkmt9-write1-task-s72-j84bx
        Start Time:              2022-06-07T21:18:40Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       write
          Terminated:
            Container ID:  docker://bae2d778a614ad3f2c4f7d608411344e9852a2d93e66b09a356805d8daa890c4
            Exit Code:     0
            Finished At:   2022-06-07T21:19:12Z
            Reason:        Completed
            Started At:    2022-06-07T21:19:02Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage 
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Setup GCS credentials, bucket, and bucket
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json')
bucket = client.bucket('annastests')
blob = bucket.blob('out1.bin')
# Create data object
string = 'x' * 1048576
# Store timestamp after data creation 
with open("/local/timestamps/shared_cloud/1to4/1MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on cloud storage
blob.upload_from_string(string)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/1MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    108s               PipelineRun  
  Normal  Running    108s               PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    74s (x2 over 74s)  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    59s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    55s                PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    52s                PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
