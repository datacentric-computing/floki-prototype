Name:         write-read-array-cloud-pipeline-run-fgjc6
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-cloud-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-08T09:09:35Z
  Generate Name:       write-read-array-cloud-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-08T09:09:35Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-cloud-pipeline-run-fgjc6-read1-task-nsxv9:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-fgjc6-read2-task-trqv7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-fgjc6-read3-task-v2zcv:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-fgjc6-read4-task-wgm6c:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-fgjc6-write1-task-d2667:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-08T09:16:17Z
  Resource Version:  209515211
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-cloud-pipeline-run-fgjc6
  UID:               c369a74f-badd-4fb1-887e-2c2bd03cfbab
Spec:
  Pipeline Ref:
    Name:                write-read-array-cloud-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  gcs-credentials
    Secret:
      Default Mode:  256
      Secret Name:   gcs-credentials
    Name:            task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-06-08T09:16:17Z
  Conditions:
    Last Transition Time:  2022-06-08T09:16:17Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read4-ws
        Workspace:  task-read4-ws
    Workspaces:
      Name:    gcs-credentials
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
  Start Time:  2022-06-08T09:09:35Z
  Task Runs:
    write-read-array-cloud-pipeline-run-fgjc6-read1-task-nsxv9:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-08T09:16:13Z
        Conditions:
          Last Transition Time:  2022-06-08T09:16:13Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-fgjc6-read1-task-nsxv-5jqxk
        Start Time:              2022-06-08T09:12:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://790ddfcc234e526ea1d064faf81c10a149dbf59be1db17e97be5db3cbe868cfe
            Exit Code:     0
            Finished At:   2022-06-08T09:16:12Z
            Reason:        Completed
            Started At:    2022-06-08T09:12:22Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-cloud-pipeline-run-fgjc6-read2-task-trqv7:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-08T09:16:09Z
        Conditions:
          Last Transition Time:  2022-06-08T09:16:09Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-fgjc6-read2-task-trqv-5vrwv
        Start Time:              2022-06-08T09:12:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://f78b5f380679a45e455fefb4b67ce8a8abe095738b5158b198591b4d6893d5e3
            Exit Code:     0
            Finished At:   2022-06-08T09:16:08Z
            Reason:        Completed
            Started At:    2022-06-08T09:12:23Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-cloud-pipeline-run-fgjc6-read3-task-v2zcv:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-08T09:16:11Z
        Conditions:
          Last Transition Time:  2022-06-08T09:16:11Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-fgjc6-read3-task-v2zc-h47xw
        Start Time:              2022-06-08T09:12:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://9ca270f9293f14d1add35f21bfab8ad65c1de0148a2bd070b6650962a6f1dec7
            Exit Code:     0
            Finished At:   2022-06-08T09:16:11Z
            Reason:        Completed
            Started At:    2022-06-08T09:12:21Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-cloud-pipeline-run-fgjc6-read4-task-wgm6c:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-08T09:16:17Z
        Conditions:
          Last Transition Time:  2022-06-08T09:16:17Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-fgjc6-read4-task-wgm6-csssn
        Start Time:              2022-06-08T09:12:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://a8dc05f99db125ce5731705ff40fece5e54958b76a2543c64426fb5f5a8c2919
            Exit Code:     0
            Finished At:   2022-06-08T09:16:16Z
            Reason:        Completed
            Started At:    2022-06-08T09:12:20Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-resumable-media==2.3.3"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_bytes(checksum='crc32c')
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-cloud-pipeline-run-fgjc6-write1-task-d2667:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-08T09:12:12Z
        Conditions:
          Last Transition Time:  2022-06-08T09:12:12Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-fgjc6-write1-task-d26-zfz7c
        Start Time:              2022-06-08T09:09:35Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       write
          Terminated:
            Container ID:  docker://ece943c2b94910178204776fb909a277f23e02a1dca811af0b3b5ae42189e3ea
            Exit Code:     0
            Finished At:   2022-06-08T09:12:10Z
            Reason:        Completed
            Started At:    2022-06-08T09:09:58Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage 
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Setup GCS credentials, bucket, and bucket
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json')
bucket = client.bucket('annastests')
blob = bucket.blob('out1.bin')
# Create data object
string = 'x' * 4294967296
# Store timestamp after data creation 
with open("/local/timestamps/shared_cloud/1to4/4GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on cloud storage
blob.upload_from_string(string)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to4/4GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                    From         Message
  ----    ------     ----                   ----         -------
  Normal  Started    6m51s                  PipelineRun  
  Normal  Running    6m51s                  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    4m13s (x2 over 4m13s)  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    16s                    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    14s                    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    12s                    PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                     PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
