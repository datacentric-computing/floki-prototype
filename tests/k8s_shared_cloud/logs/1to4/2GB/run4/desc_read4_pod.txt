Name:         write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb
Namespace:    default
Priority:     0
Node:         k8s-worker-node5/10.0.26.214
Start Time:   Wed, 08 Jun 2022 07:44:06 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-cloud-pipeline
              tekton.dev/pipelineRun=write-read-array-cloud-pipeline-run-xkb2l
              tekton.dev/pipelineTask=read4-task
              tekton.dev/task=read4
              tekton.dev/taskRun=write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8n
Annotations:  cni.projectcalico.org/containerID: 331a258def55fb743ffad89a89e902bca3e0feaa4ae7cf8676573469ad323ba3
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads an object from cloud
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.43.170
IPs:
  IP:           192.168.43.170
Controlled By:  TaskRun/write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8n
Init Containers:
  place-tools:
    Container ID:  docker://4849b84617c0835fc0d528d57907c2269bbe95aee9d41e15c30d5a0e23718cdd
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:44:08 +0000
      Finished:     Wed, 08 Jun 2022 07:44:08 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://6096245a9a7f723a8ccc2ed051ee11c1d264ba4252079a70e1392df243ee34cf
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-vq87b"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IHN1YnByb2Nlc3MKaW1wb3J0IHN5cwpzdWJwcm9jZXNzLmNoZWNrX2NhbGwoW3N5cy5leGVjdXRhYmxlLCAiLW0iLCAicGlwIiwgImluc3RhbGwiLCAiZ29vZ2xlLWNsb3VkLXN0b3JhZ2UiXSkKc3VicHJvY2Vzcy5jaGVja19jYWxsKFtzeXMuZXhlY3V0YWJsZSwgIi1tIiwgInBpcCIsICJpbnN0YWxsIiwgImdvb2dsZS1yZXN1bWFibGUtbWVkaWE9PTIuMy4zIl0pCmZyb20gZ29vZ2xlLmNsb3VkIGltcG9ydCBzdG9yYWdlCiMgU3RvcmUgaW5pdGlhbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfY2xvdWQvMXRvNC8yR0IvcjRjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X3NfdHM6CiAgIGZwX3Jjb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCmNsaWVudCA9IHN0b3JhZ2UuQ2xpZW50LmZyb21fc2VydmljZV9hY2NvdW50X2pzb24oJy93b3Jrc3BhY2UvY3JlZGVudGlhbHMvc2VydmljZV9hY2NvdW50X2djcy5qc29uJykgCmJ1Y2tldCA9IGNsaWVudC5idWNrZXQoJ2FubmFzdGVzdHMnKQojIFJlYWQgZGF0YSBvYmplY3QgZnJvbSByZW1vdGUgc2hhcmVkIHN0b3JhZ2UKYmxvYjEgPSBidWNrZXQuZ2V0X2Jsb2IoJ291dDEuYmluJykKZGF0YSA9IGJsb2IxLmRvd25sb2FkX2FzX2J5dGVzKGNoZWNrc3VtPSdjcmMzMmMnKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9jbG91ZC8xdG80LzJHQi9yNGNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:44:09 +0000
      Finished:     Wed, 08 Jun 2022 07:44:09 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://8876917109c6a9af76adc2960cfaa0f252650a812236dafc94265a76adcf0cc5
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:44:10 +0000
      Finished:     Wed, 08 Jun 2022 07:44:10 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://c54ff08f9023119e84512206c0e683129c7306aefaa701cfb9b48675a032ef04
    Image:         gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Image ID:      docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-vq87b
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-08T07:44:13.323Z","type":3}]
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:44:11 +0000
      Finished:     Wed, 08 Jun 2022 07:46:15 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-hzxfx (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
      /workspace/credentials from ws-54g4r (rw)
  istio-proxy:
    Container ID:  docker://dd72c5d2c966552291a96001ed5bbf68d6053731ec6b6c9af4db779d95acd5f5
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:46:22 +0000
      Finished:     Wed, 08 Jun 2022 07:46:22 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-54g4r:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  gcs-credentials
    Optional:    false
  ws-hzxfx:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task5-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node5
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                  From               Message
  ----     ------     ----                 ----               -------
  Normal   Scheduled  2m33s                default-scheduler  Successfully assigned default/write-read-array-cloud-pipeline-run-xkb2l-read4-task-k5g8-vmcvb to k8s-worker-node5
  Normal   Pulled     2m31s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    2m31s                kubelet            Created container place-tools
  Normal   Started    2m31s                kubelet            Started container place-tools
  Normal   Pulled     2m31s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    2m31s                kubelet            Created container place-scripts
  Normal   Started    2m30s                kubelet            Started container place-scripts
  Normal   Pulled     2m30s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    2m29s                kubelet            Created container istio-init
  Normal   Started    2m29s                kubelet            Started container istio-init
  Normal   Pulled     2m29s                kubelet            Container image "gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9" already present on machine
  Normal   Created    2m28s                kubelet            Created container step-read
  Normal   Started    2m28s                kubelet            Started container step-read
  Normal   Pulled     2m28s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    23s                  kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  19s (x2 over 21s)    kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Pulled     18s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Created    17s (x2 over 2m28s)  kubelet            Created container istio-proxy
  Normal   Started    17s (x2 over 2m28s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  17s                  kubelet            Readiness probe failed: Get "http://192.168.43.170:15021/healthz/ready": dial tcp 192.168.43.170:15021: connect: connection refused
