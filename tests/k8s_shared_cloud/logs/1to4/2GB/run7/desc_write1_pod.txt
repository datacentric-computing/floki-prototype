Name:         write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Wed, 08 Jun 2022 07:58:02 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-cloud-pipeline
              tekton.dev/pipelineRun=write-read-array-cloud-pipeline-run-29vlm
              tekton.dev/pipelineTask=write1-task
              tekton.dev/task=write1
              tekton.dev/taskRun=write-read-array-cloud-pipeline-run-29vlm-write1-task-gmvq8
Annotations:  cni.projectcalico.org/containerID: b66cc67f356dca23450e1a8057cf0791bf269154692a60d5356dd33cc9c64754
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file to cloud storage
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.50.197
IPs:
  IP:           192.168.50.197
Controlled By:  TaskRun/write-read-array-cloud-pipeline-run-29vlm-write1-task-gmvq8
Init Containers:
  place-tools:
    Container ID:  docker://986f8c6a74a4f1874f6429d1df18576e3e92b4d57ad337c95c91af26033f009b
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:58:27 +0000
      Finished:     Wed, 08 Jun 2022 07:58:30 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://4868876ea7d336b167fc46ffffd92f9b155e86711252a4397d55868f5dcc9e88
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-9bzxs"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MgCmltcG9ydCBzdWJwcm9jZXNzCmltcG9ydCBzeXMKc3VicHJvY2Vzcy5jaGVja19jYWxsKFtzeXMuZXhlY3V0YWJsZSwgIi1tIiwgInBpcCIsICJpbnN0YWxsIiwgImdvb2dsZS1jbG91ZC1zdG9yYWdlIl0pCmZyb20gZ29vZ2xlLmNsb3VkIGltcG9ydCBzdG9yYWdlIAojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2xvY2FsL3RpbWVzdGFtcHMvc2hhcmVkX2Nsb3VkLzF0bzQvMkdCL3cxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9zX3RzOgogICBmcF93Y29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKSAKIyBTZXR1cCBHQ1MgY3JlZGVudGlhbHMsIGJ1Y2tldCwgYW5kIGJ1Y2tldApjbGllbnQgPSBzdG9yYWdlLkNsaWVudC5mcm9tX3NlcnZpY2VfYWNjb3VudF9qc29uKCcvd29ya3NwYWNlL2NyZWRlbnRpYWxzL3NlcnZpY2VfYWNjb3VudF9nY3MuanNvbicpCmJ1Y2tldCA9IGNsaWVudC5idWNrZXQoJ2FubmFzdGVzdHMnKQpibG9iID0gYnVja2V0LmJsb2IoJ291dDEuYmluJykKIyBDcmVhdGUgZGF0YSBvYmplY3QKc3RyaW5nID0gJ3gnICogMjE0NzQ4MzY0OAojIFN0b3JlIHRpbWVzdGFtcCBhZnRlciBkYXRhIGNyZWF0aW9uIAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9jbG91ZC8xdG80LzJHQi93MWNvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiBjbG91ZCBzdG9yYWdlCmJsb2IudXBsb2FkX2Zyb21fc3RyaW5nKHN0cmluZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfY2xvdWQvMXRvNC8yR0IvdzFjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojcHJpbnQoIldSSVRFUjogV3JpdHRlbiBvdXQxISIpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:58:34 +0000
      Finished:     Wed, 08 Jun 2022 07:58:36 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://d5b515bb79499dde0dbe7246652cd79f0151bb8f811883a0f02a511ad704ed88
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:58:38 +0000
      Finished:     Wed, 08 Jun 2022 07:58:39 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://35a3a5a059e23e5a0ad1e03ad28fdba2e1178fc64c6fcc78af361d3a8db5665e
    Image:         gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Image ID:      docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-9bzxs
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-08T07:58:46.096Z","type":3}]
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:58:40 +0000
      Finished:     Wed, 08 Jun 2022 08:00:30 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-ngrpj (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
      /workspace/credentials from ws-jtsrf (rw)
  istio-proxy:
    Container ID:  docker://d8c6fb99b5522d01bf443a0c59819957488f9f26a73b8375e54f89f30d303173
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 08:00:40 +0000
      Finished:     Wed, 08 Jun 2022 08:00:40 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 08 Jun 2022 07:58:41 +0000
      Finished:     Wed, 08 Jun 2022 08:00:39 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-jtsrf:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  gcs-credentials
    Optional:    false
  ws-ngrpj:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason                  Age                 From               Message
  ----     ------                  ----                ----               -------
  Normal   Scheduled               5m6s                default-scheduler  Successfully assigned default/write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9 to k8s-worker-node1
  Warning  FailedCreatePodSandBox  5m                  kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "faa0961065491745703bd6e6db8bbb97502ac22d8008874b4daf014529725c59" network for pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9": networkPlugin cni failed to set up pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Warning  FailedCreatePodSandBox  4m54s               kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "3976489de50e66700edca5b7f2504c8ef4bd0b1e93e0bb4e62f2c36842afbeac" network for pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9": networkPlugin cni failed to set up pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Warning  FailedCreatePodSandBox  4m49s               kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "7f65a29165968facba5e4cfb46ce25c08b1d9b0c2befffe45f0af502c223ea6f" network for pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9": networkPlugin cni failed to set up pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Normal   SandboxChanged          4m46s (x4 over 5m)  kubelet            Pod sandbox changed, it will be killed and re-created.
  Warning  FailedCreatePodSandBox  4m46s               kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to set up sandbox container "bbfdaba91c7ded4d92755607c4ce905e532c4e5858ebe02b176c1d6fba84d08d" network for pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9": networkPlugin cni failed to set up pod "write-read-array-cloud-pipeline-run-29vlm-write1-task-gmv-69dc9_default" network: Calico CNI panicked during ADD: runtime error: invalid memory address or nil pointer dereference
  Normal   Pulled                  4m42s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created                 4m41s               kubelet            Created container place-tools
  Normal   Started                 4m41s               kubelet            Started container place-tools
  Normal   Pulled                  4m35s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created                 4m35s               kubelet            Created container place-scripts
  Normal   Started                 4m34s               kubelet            Started container place-scripts
  Normal   Pulled                  4m31s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created                 4m31s               kubelet            Created container istio-init
  Normal   Started                 4m30s               kubelet            Started container istio-init
  Normal   Pulled                  4m29s               kubelet            Container image "gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9" already present on machine
  Normal   Created                 4m28s               kubelet            Created container step-write
  Normal   Started                 4m28s               kubelet            Started container step-write
  Normal   Pulled                  4m28s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created                 4m27s               kubelet            Created container istio-proxy
  Normal   Started                 4m27s               kubelet            Started container istio-proxy
  Warning  Unhealthy               4m25s               kubelet            Readiness probe failed: Get "http://192.168.50.197:15021/healthz/ready": dial tcp 192.168.50.197:15021: connect: connection refused
  Normal   Killing                 2m35s               kubelet            Container istio-proxy definition changed, will be restarted
