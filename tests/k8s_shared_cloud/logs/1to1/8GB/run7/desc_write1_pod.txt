Name:         write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Fri, 03 Jun 2022 10:15:57 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-cloud-pipeline
              tekton.dev/pipelineRun=write-read-array-cloud-pipeline-run-hflv7
              tekton.dev/pipelineTask=write1-task
              tekton.dev/task=write1
              tekton.dev/taskRun=write-read-array-cloud-pipeline-run-hflv7-write1-task-gkttr
Annotations:  cni.projectcalico.org/containerID: 6ae95b32dcaa9a167a20bc1e4752303d7c65d95ccf3850de6467fafb81bc7f10
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file to cloud storage
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.50.243
IPs:
  IP:           192.168.50.243
Controlled By:  TaskRun/write-read-array-cloud-pipeline-run-hflv7-write1-task-gkttr
Init Containers:
  place-tools:
    Container ID:  docker://b83ead8986b39e1615db3c4143b7c249093c85e76f9f23057648bd003f5abea6
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:16:03 +0000
      Finished:     Fri, 03 Jun 2022 10:16:06 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://57a0ebdb7810e23b6f11ebf37322e55978c0194a9b1f620e3906eec917bc9df7
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-kfrxw"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MgCmltcG9ydCBzdWJwcm9jZXNzCmltcG9ydCBzeXMKc3VicHJvY2Vzcy5jaGVja19jYWxsKFtzeXMuZXhlY3V0YWJsZSwgIi1tIiwgInBpcCIsICJpbnN0YWxsIiwgImdvb2dsZS1jbG91ZC1zdG9yYWdlIl0pCmZyb20gZ29vZ2xlLmNsb3VkIGltcG9ydCBzdG9yYWdlIAojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2xvY2FsL3RpbWVzdGFtcHMvc2hhcmVkX2Nsb3VkLzF0bzEvOEdCL3cxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9zX3RzOgogICBmcF93Y29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKSAKIyBTZXR1cCBHQ1MgY3JlZGVudGlhbHMsIGJ1Y2tldCwgYW5kIGJ1Y2tldApjbGllbnQgPSBzdG9yYWdlLkNsaWVudC5mcm9tX3NlcnZpY2VfYWNjb3VudF9qc29uKCcvd29ya3NwYWNlL2NyZWRlbnRpYWxzL3NlcnZpY2VfYWNjb3VudF9nY3MuanNvbicpCmJ1Y2tldCA9IGNsaWVudC5idWNrZXQoJ2FubmFzdGVzdHMnKQpibG9iID0gYnVja2V0LmJsb2IoJ291dDEuYmluJykKIyBDcmVhdGUgZGF0YSBvYmplY3QKc3RyaW5nID0gJ3gnICogODU4OTkzNDU5MgojIFN0b3JlIHRpbWVzdGFtcCBhZnRlciBkYXRhIGNyZWF0aW9uIAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9jbG91ZC8xdG8xLzhHQi93MWNvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiBjbG91ZCBzdG9yYWdlCmJsb2IudXBsb2FkX2Zyb21fc3RyaW5nKHN0cmluZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfY2xvdWQvMXRvMS84R0IvdzFjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojcHJpbnQoIldSSVRFUjogV3JpdHRlbiBvdXQxISIpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:16:09 +0000
      Finished:     Fri, 03 Jun 2022 10:16:09 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://cf24889eb5094153564fecf7f42182a7c0950d2b13be75e7ce1664f3b7ffc92d
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:16:11 +0000
      Finished:     Fri, 03 Jun 2022 10:16:12 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://7ef14b779e909225420f0bb5d2754c664e67e0298486cd6619bad12058b0df3d
    Image:         gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Image ID:      docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-kfrxw
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-03T10:16:19.311Z","type":3}]
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:16:15 +0000
      Finished:     Fri, 03 Jun 2022 10:20:00 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-rr58p (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
      /workspace/credentials from ws-nvpnf (rw)
  istio-proxy:
    Container ID:  docker://122c6a906d1ad26db80a0c40ea8b1fd4cc933bf2e7373d242bde450a61ad9a53
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:20:09 +0000
      Finished:     Fri, 03 Jun 2022 10:20:09 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 03 Jun 2022 10:16:15 +0000
      Finished:     Fri, 03 Jun 2022 10:20:08 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-nvpnf:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  gcs-credentials
    Optional:    false
  ws-rr58p:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                    From               Message
  ----     ------     ----                   ----               -------
  Normal   Scheduled  8m23s                  default-scheduler  Successfully assigned default/write-read-array-cloud-pipeline-run-hflv7-write1-task-gkt-kcdvp to k8s-worker-node1
  Normal   Pulled     8m18s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    8m18s                  kubelet            Created container place-tools
  Normal   Started    8m17s                  kubelet            Started container place-tools
  Normal   Pulled     8m13s                  kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    8m12s                  kubelet            Created container place-scripts
  Normal   Started    8m11s                  kubelet            Started container place-scripts
  Normal   Pulled     8m10s                  kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    8m10s                  kubelet            Created container istio-init
  Normal   Started    8m9s                   kubelet            Started container istio-init
  Normal   Pulled     8m7s                   kubelet            Container image "gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9" already present on machine
  Normal   Created    8m6s                   kubelet            Created container step-write
  Normal   Started    8m5s                   kubelet            Started container step-write
  Normal   Pulled     8m5s                   kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    4m18s                  kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  4m14s (x2 over 4m16s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    4m12s (x2 over 8m5s)   kubelet            Created container istio-proxy
  Normal   Pulled     4m12s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  4m12s                  kubelet            Readiness probe failed: Get "http://192.168.50.243:15021/healthz/ready": dial tcp 192.168.50.243:15021: connect: connection refused
  Normal   Started    4m11s (x2 over 8m5s)   kubelet            Started container istio-proxy
