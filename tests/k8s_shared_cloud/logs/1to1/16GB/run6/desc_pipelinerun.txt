Name:         write-read-array-cloud-pipeline-run-l9nzb
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-cloud-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-03T14:01:09Z
  Generate Name:       write-read-array-cloud-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-03T14:01:09Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-cloud-pipeline-run-l9nzb-read1-task-7zm5k:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-cloud-pipeline-run-l9nzb-write1-task-zlz89:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-03T14:16:13Z
  Resource Version:  198930257
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-cloud-pipeline-run-l9nzb
  UID:               12f85ded-d340-4a77-848d-643732f2a2b3
Spec:
  Pipeline Ref:
    Name:                write-read-array-cloud-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
  Timeout:                       1h0m0s
  Workspaces:
    Name:  gcs-credentials
    Secret:
      Default Mode:  256
      Secret Name:   gcs-credentials
    Name:            task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
Status:
  Completion Time:  2022-06-03T14:16:13Z
  Conditions:
    Last Transition Time:  2022-06-03T14:16:13Z
    Message:               Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       credentials
        Workspace:  gcs-credentials
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    gcs-credentials
      Name:    task-write1-ws
      Name:    task-read1-ws
  Start Time:  2022-06-03T14:01:09Z
  Task Runs:
    write-read-array-cloud-pipeline-run-l9nzb-read1-task-7zm5k:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-03T14:16:13Z
        Conditions:
          Last Transition Time:  2022-06-03T14:16:13Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-l9nzb-read1-task-7zm5-xqxcw
        Start Time:              2022-06-03T14:08:00Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       read
          Terminated:
            Container ID:  docker://23cd0f2156876c66da1e2ba68f0f18cf903b10b91076906e04ca043e96a010e8
            Exit Code:     0
            Finished At:   2022-06-03T14:16:12Z
            Reason:        Completed
            Started At:    2022-06-03T14:08:07Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to1/16GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json') 
bucket = client.bucket('annastests')
# Read data object from remote shared storage
blob1 = bucket.get_blob('out1.bin')
data = blob1.download_as_string()
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to1/16GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-cloud-pipeline-run-l9nzb-write1-task-zlz89:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-03T14:08:00Z
        Conditions:
          Last Transition Time:  2022-06-03T14:08:00Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-cloud-pipeline-run-l9nzb-write1-task-zlz-bp9kc
        Start Time:              2022-06-03T14:01:09Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://gcr.io/google.com/cloudsdktool/cloud-sdk@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
          Name:       write
          Terminated:
            Container ID:  docker://4a4a360abe8769e17fee9c1dcb994407eff3cc5afe11dbb4372eaac0c7f023f1
            Exit Code:     0
            Finished At:   2022-06-03T14:07:59Z
            Reason:        Completed
            Started At:    2022-06-03T14:01:36Z
        Task Spec:
          Steps:
            Image:              gcr.io/google.com/cloudsdktool/cloud-sdk:379.0.0-slim@sha256:d844877c7aaa06a0072979230c68417ddb0f27087277f29747c7169d6ed0d2b9
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "google-cloud-storage"])
from google.cloud import storage 
# Store initial timestamp
with open("/local/timestamps/shared_cloud/1to1/16GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time()) 
# Setup GCS credentials, bucket, and bucket
client = storage.Client.from_service_account_json('$(workspaces.credentials.path)/service_account_gcs.json')
bucket = client.bucket('annastests')
blob = bucket.blob('out1.bin')
# Create data object
string = 'x' * 17179869184
# Store timestamp after data creation 
with open("/local/timestamps/shared_cloud/1to1/16GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on cloud storage
blob.upload_from_string(string)
# Store final timestamp
with open("/local/timestamps/shared_cloud/1to1/16GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
#print("WRITER: Written out1!")

          Workspaces:
            Name:        credentials
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    15m (x2 over 15m)  PipelineRun  
  Normal  Running    15m (x2 over 15m)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    8m19s              PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  6s                 PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Skipped: 0
