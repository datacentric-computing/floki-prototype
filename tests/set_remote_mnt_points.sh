#!/bin/bash 

# IP addresses
IP_ADDRS=( $(cat ../../cluster_ips.txt | grep 'worker' | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}') )

CONFIGS=(
    'disk'
    'mem'
)

# Create remote in_data and out_data foldes, along with the sync_pipe
for conf in "${CONFIGS[@]}"; do
    echo "Settings for configuration $conf:"
    if [[ $conf == 'mem' ]]; then
        cmd="mkdir -p /mnt/mem/vol2 | mount -t tmpfs -o size=120g tmpfs /mnt/mem/vol2"
        for ip in "${IP_ADDRS[@]}"; do
            sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} ${cmd}
            echo "Created tmpfs for IP $ip!"
        done
    fi
    mnt_path="/mnt/${conf}/vol2"
    cmd="mkdir -p $mnt_path/in_data && mkdir -p $mnt_path/out_data && mkfifo $mnt_path/sync_pipe && mkfifo $mnt_path/data_pipe && mkdir -p $mnt_path/timestamps"
    for ip in "${IP_ADDRS[@]}"; do
        sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} ${cmd}
        echo "Created folders for IP $ip!"
    done
    # Change permissions of vol2 folder
    cmd="chmod -R  777 /mnt/${conf}/vol2"
    for ip in "${IP_ADDRS[@]}"; do
        sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} ${cmd}
        echo "Changes folders permissions for IP  $ip!"
    done
done
# Install g++-9
cmd="apt-get -y install g++ 2> /dev/null && yes | add-apt-repository ppa:ubuntu-toolchain-r/test 2> /dev/null && apt-get update 2> /dev/null && apt-get -y install g++-9 2> /dev/null &&  rm /usr/bin/g++ && ln -s /usr/bin/g++-9 /usr/bin/g++"
for ip in "${IP_ADDRS[@]}"; do
    sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} ${cmd}
    echo "Installed g++-9 on $ip!"
done
