#!/bin/bash

# Store the given parameters
NUM_WTASKS=$1
NUM_RTASKS=$2

# GENERATE THE PIPELINE YAML
# Create temp files directory
mkdir -p temp_files

# Create the temp pipeline yaml
cp templates/template_pipeline.yaml temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml

# Add writers workspaces
for (( w=1; w<=$NUM_WTASKS; w++)); do
    echo "    - name: task-write${w}-ws" >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done

# Add readers workspaces
for (( r=1; r<=$NUM_RTASKS; r++)); do
    echo "    - name: task-read${r}-ws" >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
echo "  tasks:" >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml

# Add writers tasks
for (( w=1; w<=$NUM_WTASKS; w++)); do
    cp templates/template_wtask_pipeline.yaml temp_wtask_pipeline.yaml
    sed -i "s/NUM/${w}/g" temp_wtask_pipeline.yaml
    cat temp_wtask_pipeline.yaml >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_wtask_pipeline.yaml

# Add readers tasks
for (( r=1; r<=$NUM_RTASKS; r++)); do
    cp templates/template_rtask_pipeline.yaml temp_rtask_pipeline.yaml
    sed -i "s/NUM/${r}/g" temp_rtask_pipeline.yaml
    cat temp_rtask_pipeline.yaml >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_rtask_pipeline.yaml

# Add writer tasks definitions
for (( w=1; w<=$NUM_WTASKS; w++)); do
    cp templates/template_wtask.yaml temp_wtask.yaml
    sed -i "s/NUM/${w}/g" temp_wtask.yaml
    sed -i "s/WRITERS/${NUM_WTASKS}/g" temp_wtask.yaml
    sed -i "s/READERS/${NUM_RTASKS}/g" temp_wtask.yaml
    cat temp_wtask.yaml >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_wtask.yaml

# Add reader tasks definitions
for (( w = 1; w <= NUM_WTASKS; w++ )); do
    echo "         data_length_bin = data_fifo.read(8)" >> temp_list_reads.yaml
    echo "         data_length = int.from_bytes(data_length_bin, \"big\")" >> temp_list_reads.yaml
    echo "         data = bytearray(0)" >> temp_list_reads.yaml
    echo "         counter = 0" >> temp_list_reads.yaml
    echo "         while counter < data_length:" >> temp_list_reads.yaml
    echo "            buff = data_fifo.read(buf_sz)" >> temp_list_reads.yaml
    echo "            counter += len(buff)" >> temp_list_reads.yaml
    echo "            data.extend(buff)" >> temp_list_reads.yaml
done
for (( r=1; r<=$NUM_RTASKS; r++)); do
    cp templates/template_rtask.yaml temp_rtask.yaml
    sed -i "s/NUM/${r}/g" temp_rtask.yaml
    sed -i "s/WRITERS/${NUM_WTASKS}/g" temp_rtask.yaml
    sed -i "s/READERS/${NUM_RTASKS}/g" temp_rtask.yaml
    sed -i -e '/LIST_READS/{r temp_list_reads.yaml' -e 'd}' temp_rtask.yaml
    cat temp_rtask.yaml >> temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_rtask.yaml
rm temp_list_reads.yaml

# GENERATE THE PVS AND PVCS YAML
tot_pvs=$((NUM_WTASKS+NUM_RTASKS))
for (( i=1; i<=$tot_pvs; i++)); do
    cp templates/template_pv_node.yaml temp_files/pvNode${i}.yaml
    sed -i "s/NUM/${i}/g" temp_files/pvNode${i}.yaml
    cp templates/template_pvc_node.yaml temp_files/pvcNode${i}.yaml
    sed -i "s/NUM/${i}/g" temp_files/pvcNode${i}.yaml
done

# GENERATE PIPELINERUN YAML
cp templates/template_pipelinerun.yaml temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml
# Add writer tasks definitions
for (( w=1; w<=$NUM_WTASKS; w++)); do
    cp templates/template_wtask_ws_pipelinerun.yaml temp_wtask_ws.yaml
    sed -i "s/NUM/${w}/g" temp_wtask_ws.yaml
    cat temp_wtask_ws.yaml >> temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_wtask_ws.yaml

# Add reader tasks definitions
for (( r=1; r<=$NUM_RTASKS; r++)); do
    cp templates/template_rtask_ws_pipelinerun.yaml temp_rtask_ws.yaml
    sed -i "s/NUM/${r}/g" temp_rtask_ws.yaml
    val=$((r+NUM_WTASKS))
    sed -i "s/ALL/${val}/g" temp_rtask_ws.yaml
    cat temp_rtask_ws.yaml >> temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_rtask_ws.yaml

echo "  taskRunSpecs:" >> temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml

# Add writer tasks run
for (( w=1; w<=$NUM_WTASKS; w++)); do
    cp templates/template_wtask_tr_pipelinerun.yaml temp_wtask_tr.yaml
    sed -i "s/NUM/${w}/g" temp_wtask_tr.yaml
    cat temp_wtask_tr.yaml >> temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_wtask_tr.yaml

# Add reader tasks run
for (( r=1; r<=$NUM_RTASKS; r++)); do
    cp templates/template_rtask_tr_pipelinerun.yaml temp_rtask_tr.yaml
    sed -i "s/NUM/${r}/g" temp_rtask_tr.yaml
    val=$((r+NUM_WTASKS))
    sed -i "s/ALL/${val}/g" temp_rtask_tr.yaml
    cat temp_rtask_tr.yaml >> temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml
done
rm temp_rtask_tr.yaml
