Name:         write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8
Namespace:    default
Priority:     0
Node:         k8s-worker-node2/10.0.26.207
Start Time:   Fri, 18 Mar 2022 10:38:03 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-lkljl
              tekton.dev/pipelineTask=write2-task
              tekton.dev/task=write2
              tekton.dev/taskRun=write-read-array-pipeline-run-lkljl-write2-task-m9qdf
Annotations:  cni.projectcalico.org/containerID: 8f698c9ed17a306336512fe8cff5f537f56d69a5de547b26d91b14cae7d320da
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.219.125
IPs:
  IP:           192.168.219.125
Controlled By:  TaskRun/write-read-array-pipeline-run-lkljl-write2-task-m9qdf
Init Containers:
  place-tools:
    Container ID:  docker://39fa77acd86916bfaaf6ac380c901c805ee35963ce8199c917b1fab262484421
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 10:38:07 +0000
      Finished:     Fri, 18 Mar 2022 10:38:07 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://31ba056425409a42ffd205b38eda5b440f507554fa4b1312241d124a6ca32dfb
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-vj5s9"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXRfcGlwZS82dG8xLzY0TUIvdzJjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X3NfdHM6CiAgIGZwX3djb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiMgQ3JlYXRlIGRhdGEgb2JqZWN0CiMgc3RyaW5nID0gJ3gnICogNjcxMDg4NjQKZGF0YV9hc19ieXRlcyA9IGJ5dGVhcnJheShiJ1x4NzgnKSAqIDY3MTA4ODY0CiNkYXRhX2FzX2J5dGVzID0gc3RyLmVuY29kZShzdHJpbmcpCmRhdGFfZGltID0gNjcxMDg4NjQKZGF0YV9kaW1fYmluID0gZGF0YV9kaW0udG9fYnl0ZXMoOCwgImJpZyIpCiMgU3RvcmUgdGltZXN0YW1wIGFmdGVyIGRhdGEgY3JlYXRpb24Kd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldF9waXBlLzZ0bzEvNjRNQi93MmNvbnRfZGF0YV9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZGF0YV90czoKICAgZnBfd2NvbnRfZGF0YV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBXcml0ZSBkYXRhIG9iamVjdCBvbiBwaXBlCmRhdGFfZmlmbyA9IG9wZW4oJy9kYXRhL2RhdGFfcGlwZScsICd3YicpIApkYXRhX2ZpZm8ud3JpdGUoZGF0YV9kaW1fYmluKQpkYXRhX2ZpZm8ud3JpdGUoZGF0YV9hc19ieXRlcykgCmRhdGFfZmlmby5jbG9zZSgpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXRfcGlwZS82dG8xLzY0TUIvdzJjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 10:38:08 +0000
      Finished:     Fri, 18 Mar 2022 10:38:08 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://17f79fbc4eb069f24e03b08db25be4608bcc5af12a78af0aa63d9b97adea6bee
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 10:38:10 +0000
      Finished:     Fri, 18 Mar 2022 10:38:11 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://8c24b6f0412f9767e696bc46956743501a15369f5555c8ca0cd50716ecc025e3
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-vj5s9
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-18T10:38:15.275Z","type":3}]
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 10:38:12 +0000
      Finished:     Fri, 18 Mar 2022 10:38:16 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-z8h82 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://c858bf514612febc69b22584ca3ba3b2521733d48c8377390a204a5503c7a4a1
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 10:38:24 +0000
      Finished:     Fri, 18 Mar 2022 10:38:24 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-z8h82:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task2-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node2
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  38s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-lkljl-write2-task-m9qdf-pod-kzmk8 to k8s-worker-node2
  Normal   Pulled     35s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    35s                kubelet            Created container place-tools
  Normal   Started    34s                kubelet            Started container place-tools
  Normal   Started    33s                kubelet            Started container place-scripts
  Normal   Created    33s                kubelet            Created container place-scripts
  Normal   Pulled     33s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Pulled     32s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    32s                kubelet            Created container istio-init
  Normal   Started    31s                kubelet            Started container istio-init
  Normal   Created    30s                kubelet            Created container step-write
  Normal   Pulled     30s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    29s                kubelet            Started container step-write
  Normal   Pulled     29s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    23s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  20s (x2 over 22s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Warning  Unhealthy  18s                kubelet            Readiness probe failed: Get "http://192.168.219.125:15021/healthz/ready": read tcp 10.0.2.15:56164->192.168.219.125:15021: read: connection reset by peer
  Normal   Created    17s (x2 over 29s)  kubelet            Created container istio-proxy
  Normal   Started    17s (x2 over 29s)  kubelet            Started container istio-proxy
  Normal   Pulled     17s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  16s                kubelet            Readiness probe failed: Get "http://192.168.219.125:15021/healthz/ready": dial tcp 192.168.219.125:15021: connect: connection refused
