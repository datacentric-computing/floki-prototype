Name:         write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85
Namespace:    default
Priority:     0
Node:         k8s-worker-node3/10.0.26.208
Start Time:   Fri, 18 Mar 2022 12:25:28 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-xh8d7
              tekton.dev/pipelineTask=write3-task
              tekton.dev/task=write3
              tekton.dev/taskRun=write-read-array-pipeline-run-xh8d7-write3-task-tkqqw
Annotations:  cni.projectcalico.org/containerID: 1433e1543bd4984629cb3b521c280b31c8b299b74b26b5a1e48afb0894167be8
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.198.198
IPs:
  IP:           192.168.198.198
Controlled By:  TaskRun/write-read-array-pipeline-run-xh8d7-write3-task-tkqqw
Init Containers:
  place-tools:
    Container ID:  docker://b647fce80ecb303260169b0fa69ec2383303d44bcb1d0cf49a804fb58c104f72
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:25:32 +0000
      Finished:     Fri, 18 Mar 2022 12:25:33 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://1dfdb475337169072e7243f13e082d364745b1e7274b490f60f2ad420da11aa8
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-6gdbk"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQojIFN0b3JlIGluaXRpYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXRfcGlwZS82dG8xLzJHQi93M2NvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfc190czoKICAgZnBfd2NvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBDcmVhdGUgZGF0YSBvYmplY3QKIyBzdHJpbmcgPSAneCcgKiAyMTQ3NDgzNjQ4CmRhdGFfYXNfYnl0ZXMgPSBieXRlYXJyYXkoYidceDc4JykgKiAyMTQ3NDgzNjQ4CiNkYXRhX2FzX2J5dGVzID0gc3RyLmVuY29kZShzdHJpbmcpCmRhdGFfZGltID0gMjE0NzQ4MzY0OApkYXRhX2RpbV9iaW4gPSBkYXRhX2RpbS50b19ieXRlcyg4LCAiYmlnIikKIyBTdG9yZSB0aW1lc3RhbXAgYWZ0ZXIgZGF0YSBjcmVhdGlvbgp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0X3BpcGUvNnRvMS8yR0IvdzNjb250X2RhdGFfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X2RhdGFfdHM6CiAgIGZwX3djb250X2RhdGFfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiMgV3JpdGUgZGF0YSBvYmplY3Qgb24gcGlwZQpkYXRhX2ZpZm8gPSBvcGVuKCcvZGF0YS9kYXRhX3BpcGUnLCAnd2InKSAKZGF0YV9maWZvLndyaXRlKGRhdGFfZGltX2JpbikKZGF0YV9maWZvLndyaXRlKGRhdGFfYXNfYnl0ZXMpIApkYXRhX2ZpZm8uY2xvc2UoKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0X3BpcGUvNnRvMS8yR0IvdzNjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:25:35 +0000
      Finished:     Fri, 18 Mar 2022 12:25:35 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://eaddd07cdf6b80745dab01d76cb11912a9ff7dd5c16fca2966efbe9fcb43dc88
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:25:37 +0000
      Finished:     Fri, 18 Mar 2022 12:25:37 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://6052f33d7d65f6f3fa969d0dc39696a3401f65968ee5da2cbf622312081feac8
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-6gdbk
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-18T12:25:40.633Z","type":3}]
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:25:38 +0000
      Finished:     Fri, 18 Mar 2022 12:26:44 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-kbr4b (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://fc09bfe213859f34e5c81522f7c9cfbcf1e447b61746cbef23f03bb1ed80dd1a
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:26:54 +0000
      Finished:     Fri, 18 Mar 2022 12:26:54 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 12:25:38 +0000
      Finished:     Fri, 18 Mar 2022 12:26:52 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-kbr4b:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task3-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node3
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                  From               Message
  ----     ------     ----                 ----               -------
  Normal   Scheduled  2m31s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-xh8d7-write3-task-tkqqw-pod-smx85 to k8s-worker-node3
  Normal   Pulled     2m29s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    2m28s                kubelet            Created container place-tools
  Normal   Started    2m28s                kubelet            Started container place-tools
  Normal   Pulled     2m26s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    2m25s                kubelet            Created container place-scripts
  Normal   Started    2m25s                kubelet            Started container place-scripts
  Normal   Pulled     2m25s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    2m24s                kubelet            Created container istio-init
  Normal   Started    2m23s                kubelet            Started container istio-init
  Normal   Pulled     2m22s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    2m22s                kubelet            Created container step-write
  Normal   Started    2m22s                kubelet            Started container step-write
  Normal   Pulled     2m22s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Killing    74s                  kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  70s (x3 over 74s)    kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Pulled     67s                  kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Created    66s (x2 over 2m22s)  kubelet            Created container istio-proxy
  Normal   Started    66s (x2 over 2m22s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  66s (x2 over 68s)    kubelet            Readiness probe failed: Get "http://192.168.198.198:15021/healthz/ready": dial tcp 192.168.198.198:15021: connect: connection refused
