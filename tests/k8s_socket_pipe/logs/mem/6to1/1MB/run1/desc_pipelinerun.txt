Name:         write-read-array-pipeline-run-q6v2d
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-18T08:52:03Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-18T08:52:03Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-q6v2d-read1-task-57gp9:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write1-task-8kpq5:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write2-task-b8dqj:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write3-task-ft6xz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write4-task-vs5mf:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write5-task-qq95w:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-q6v2d-write6-task-pnsf2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-18T08:52:17Z
  Resource Version:  28932498
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-q6v2d
  UID:               d0dbfc92-22b1-4b91-af22-ef9f27965ff8
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          write5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          write6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-write5-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-write6-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-03-18T08:52:17Z
  Conditions:
    Last Transition Time:  2022-03-18T08:52:17Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         write5-task
      Task Ref:
        Kind:  Task
        Name:  write5
      Workspaces:
        Name:       write5-ws
        Workspace:  task-write5-ws
      Name:         write6-task
      Task Ref:
        Kind:  Task
        Name:  write6
      Workspaces:
        Name:       write6-ws
        Workspace:  task-write6-ws
      Name:         read1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-write5-ws
      Name:    task-write6-ws
      Name:    task-read1-ws
  Start Time:  2022-03-18T08:52:03Z
  Task Runs:
    write-read-array-pipeline-run-q6v2d-read1-task-57gp9:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-18T08:52:16Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-read1-task-57gp9-pod-8cklk
        Start Time:              2022-03-18T08:52:06Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://3c725afe2451f7b80e8ff7ca4868aeb2407519e966c7763be896bf2dcaf328b6
            Exit Code:     0
            Finished At:   2022-03-18T08:52:16Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:15Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import gc
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-q6v2d-write1-task-8kpq5:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-18T08:52:15Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:15Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write1-task-8kpq5-pod-vn4dl
        Start Time:              2022-03-18T08:52:03Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://1124105a57dd8bdbee4cc698d72ac901d8879fc264ea3de647f64785c95264ca
            Exit Code:     0
            Finished At:   2022-03-18T08:52:15Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:13Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-q6v2d-write2-task-b8dqj:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-18T08:52:16Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write2-task-b8dqj-pod-255dq
        Start Time:              2022-03-18T08:52:03Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://3440a35210aa7da62c51cd404d6bd3aad4c736c936ecc4517c6fca2be63512eb
            Exit Code:     0
            Finished At:   2022-03-18T08:52:15Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-q6v2d-write3-task-ft6xz:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-18T08:52:16Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write3-task-ft6xz-pod-455tk
        Start Time:              2022-03-18T08:52:04Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://3cf7e930e02841d0d4a4206a02eddb48b47d0d7378b6daa89c9ffc802beec6bb
            Exit Code:     0
            Finished At:   2022-03-18T08:52:15Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:15Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
    write-read-array-pipeline-run-q6v2d-write4-task-vs5mf:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-18T08:52:16Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write4-task-vs5mf-pod-n5b6q
        Start Time:              2022-03-18T08:52:04Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://63762db961bc8f9790b8e370735733ecf4a6b5715e03afd8bc79e55b8060e2c8
            Exit Code:     0
            Finished At:   2022-03-18T08:52:16Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:12Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write4-ws
    write-read-array-pipeline-run-q6v2d-write5-task-qq95w:
      Pipeline Task Name:  write5-task
      Status:
        Completion Time:  2022-03-18T08:52:17Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:17Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write5-task-qq95w-pod-zsxjk
        Start Time:              2022-03-18T08:52:04Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://450ca8aa2db523936f50347205a37edc9db7cc6247914c486d35f44531c91c51
            Exit Code:     0
            Finished At:   2022-03-18T08:52:16Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:12Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w5cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w5cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w5cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write5-ws
    write-read-array-pipeline-run-q6v2d-write6-task-pnsf2:
      Pipeline Task Name:  write6-task
      Status:
        Completion Time:  2022-03-18T08:52:16Z
        Conditions:
          Last Transition Time:  2022-03-18T08:52:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-q6v2d-write6-task-pnsf2-pod-ddpr4
        Start Time:              2022-03-18T08:52:05Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://df6eea0e139509082d991e828102c320969c4fb929ffd657e374c78e9fe6b8c7
            Exit Code:     0
            Finished At:   2022-03-18T08:52:16Z
            Reason:        Completed
            Started At:    2022-03-18T08:52:12Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w6cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 1048576
data_as_bytes = bytearray(b'\x78') * 1048576
#data_as_bytes = str.encode(string)
data_dim = 1048576
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/1MB/w6cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/1MB/w6cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write6-ws
Events:
  Type    Reason     Age   From         Message
  ----    ------     ----  ----         -------
  Normal  Started    21s   PipelineRun  
  Normal  Running    21s   PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    9s    PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s    PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
