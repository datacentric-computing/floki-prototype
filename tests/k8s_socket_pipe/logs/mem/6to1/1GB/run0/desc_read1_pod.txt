Name:         write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5
Namespace:    default
Priority:     0
Node:         k8s-worker-node7/10.0.26.216
Start Time:   Fri, 18 Mar 2022 11:45:50 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-nnmfc
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-pipeline-run-nnmfc-read1-task-klcs7
Annotations:  cni.projectcalico.org/containerID: 4780da5647d612668e3e7b39e33fcef2f4ea749f59f04110cb712b21e5689c76
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.30.23
IPs:
  IP:           192.168.30.23
Controlled By:  TaskRun/write-read-array-pipeline-run-nnmfc-read1-task-klcs7
Init Containers:
  place-tools:
    Container ID:  docker://21c3ef6633a59aaa29145b460adb31bb7b0ebb3b114777a181a09f2bbe26adcd
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:45:52 +0000
      Finished:     Fri, 18 Mar 2022 11:45:52 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://c54d5689fd3d48b199e22620bb67a2cc03d26fc4f33897fa86488230ef68cd70
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-6wbgr"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IGdjCiMgU3RvcmUgaW5pdGlhbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvZGF0YS90aW1lc3RhbXBzL3NvY2tldF9waXBlLzZ0bzEvMUdCL3IxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9zX3RzOgogICBmcF9yY29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQpidWZfc3ogPSA2NTUzNgpyZWFkeSA9IGInXHgwMScKc3luY19waXBlID0gb3BlbigiL2RhdGEvc3luY19waXBlIiwgJ3diJykKc3luY19waXBlLndyaXRlKHJlYWR5KQpzeW5jX3BpcGUuY2xvc2UoKQojcHJpbnQoIldyaXR0ZW4gcmVhZHkgc2lnbmFsISIpCiMgUmVhZCBkYXRhIG9iamVjdCBmcm9tIGRhdGEgcGlwZQpkYXRhX2ZpZm8gPSBvcGVuKCcvZGF0YS9kYXRhX3BpcGUnLCAncmInKQpkYXRhX2xlbmd0aF9iaW4gPSBkYXRhX2ZpZm8ucmVhZCg4KQpkYXRhX2xlbmd0aCA9IGludC5mcm9tX2J5dGVzKGRhdGFfbGVuZ3RoX2JpbiwgImJpZyIpCmRhdGEgPSBieXRlYXJyYXkoMCkKY291bnRlciA9IDAKd2hpbGUgY291bnRlciA8IGRhdGFfbGVuZ3RoOgogICBidWZmID0gZGF0YV9maWZvLnJlYWQoYnVmX3N6KQogICBjb3VudGVyICs9IGxlbihidWZmKQogICBkYXRhLmV4dGVuZChidWZmKQpkZWwgZGF0YQpnYy5jb2xsZWN0KCkKZGF0YV9sZW5ndGhfYmluID0gZGF0YV9maWZvLnJlYWQoOCkKZGF0YV9sZW5ndGggPSBpbnQuZnJvbV9ieXRlcyhkYXRhX2xlbmd0aF9iaW4sICJiaWciKQpkYXRhID0gYnl0ZWFycmF5KDApCmNvdW50ZXIgPSAwCndoaWxlIGNvdW50ZXIgPCBkYXRhX2xlbmd0aDoKICAgYnVmZiA9IGRhdGFfZmlmby5yZWFkKGJ1Zl9zeikKICAgY291bnRlciArPSBsZW4oYnVmZikKICAgZGF0YS5leHRlbmQoYnVmZikKZGVsIGRhdGEKZ2MuY29sbGVjdCgpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRlbCBkYXRhCmdjLmNvbGxlY3QoKQpkYXRhX2xlbmd0aF9iaW4gPSBkYXRhX2ZpZm8ucmVhZCg4KQpkYXRhX2xlbmd0aCA9IGludC5mcm9tX2J5dGVzKGRhdGFfbGVuZ3RoX2JpbiwgImJpZyIpCmRhdGEgPSBieXRlYXJyYXkoMCkKY291bnRlciA9IDAKd2hpbGUgY291bnRlciA8IGRhdGFfbGVuZ3RoOgogICBidWZmID0gZGF0YV9maWZvLnJlYWQoYnVmX3N6KQogICBjb3VudGVyICs9IGxlbihidWZmKQogICBkYXRhLmV4dGVuZChidWZmKQpkZWwgZGF0YQpnYy5jb2xsZWN0KCkKZGF0YV9sZW5ndGhfYmluID0gZGF0YV9maWZvLnJlYWQoOCkKZGF0YV9sZW5ndGggPSBpbnQuZnJvbV9ieXRlcyhkYXRhX2xlbmd0aF9iaW4sICJiaWciKQpkYXRhID0gYnl0ZWFycmF5KDApCmNvdW50ZXIgPSAwCndoaWxlIGNvdW50ZXIgPCBkYXRhX2xlbmd0aDoKICAgYnVmZiA9IGRhdGFfZmlmby5yZWFkKGJ1Zl9zeikKICAgY291bnRlciArPSBsZW4oYnVmZikKICAgZGF0YS5leHRlbmQoYnVmZikKZGVsIGRhdGEKZ2MuY29sbGVjdCgpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRlbCBkYXRhCmdjLmNvbGxlY3QoKQpkYXRhX2ZpZm8uY2xvc2UoKQojIFN0b3JlIGZpbmFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0X3BpcGUvNnRvMS8xR0IvcjFjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9lX3RzOgogICBmcF9yY29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojcHJpbnQoIlJFQURFUjogUmVhZCAlZCB4IGNoYXJzIiAlIGxlbihkYXRhKSkK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:45:53 +0000
      Finished:     Fri, 18 Mar 2022 11:45:53 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://2bcc258e7ceaa14d345604ad327facd25d6fe691df64f07147eab0971b8e7406
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:45:54 +0000
      Finished:     Fri, 18 Mar 2022 11:45:54 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://251df246a667d02e71060c07419f82f376772fa60f6545243666abcb36ae5b97
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-6wbgr
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-18T11:45:58.101Z","type":3}]
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:45:55 +0000
      Finished:     Fri, 18 Mar 2022 11:46:57 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-2zjzs (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://7bb12d8a6d59e85647e79a5d72b75af5669fda8d3fd302df8569cb8f64e3b9cd
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:47:04 +0000
      Finished:     Fri, 18 Mar 2022 11:47:04 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Fri, 18 Mar 2022 11:45:55 +0000
      Finished:     Fri, 18 Mar 2022 11:47:04 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5 (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-2zjzs:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task7-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node7
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age               From               Message
  ----     ------     ----              ----               -------
  Normal   Scheduled  82s               default-scheduler  Successfully assigned default/write-read-array-pipeline-run-nnmfc-read1-task-klcs7-pod-mv2q5 to k8s-worker-node7
  Normal   Pulled     81s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    80s               kubelet            Created container place-tools
  Normal   Started    80s               kubelet            Started container place-tools
  Normal   Created    80s               kubelet            Created container place-scripts
  Normal   Pulled     80s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Started    79s               kubelet            Started container place-scripts
  Normal   Pulled     79s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    79s               kubelet            Created container istio-init
  Normal   Created    78s               kubelet            Created container step-read
  Normal   Started    78s               kubelet            Started container istio-init
  Normal   Pulled     78s               kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Started    77s               kubelet            Started container step-read
  Normal   Pulled     77s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    13s               kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  9s (x2 over 11s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    8s (x2 over 77s)  kubelet            Created container istio-proxy
  Normal   Started    8s (x2 over 77s)  kubelet            Started container istio-proxy
  Normal   Pulled     8s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  7s                kubelet            Readiness probe failed: Get "http://192.168.30.23:15021/healthz/ready": dial tcp 192.168.30.23:15021: connect: connection refused
