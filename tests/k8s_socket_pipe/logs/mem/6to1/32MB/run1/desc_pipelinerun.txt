Name:         write-read-array-pipeline-run-rmd57
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-18T10:15:39Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-18T10:15:39Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-rmd57-read1-task-m9mvr:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write1-task-t5qcj:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write2-task-l27nx:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write3-task-lksnt:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write4-task-t8rcd:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write5-task-5rntq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-rmd57-write6-task-7b6nk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-18T10:15:53Z
  Resource Version:  29088710
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-rmd57
  UID:               1e8b860c-d6d1-4d74-9418-3218d2dfe224
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          write5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          write6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-write5-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-write6-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-03-18T10:15:53Z
  Conditions:
    Last Transition Time:  2022-03-18T10:15:53Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         write5-task
      Task Ref:
        Kind:  Task
        Name:  write5
      Workspaces:
        Name:       write5-ws
        Workspace:  task-write5-ws
      Name:         write6-task
      Task Ref:
        Kind:  Task
        Name:  write6
      Workspaces:
        Name:       write6-ws
        Workspace:  task-write6-ws
      Name:         read1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-write5-ws
      Name:    task-write6-ws
      Name:    task-read1-ws
  Start Time:  2022-03-18T10:15:39Z
  Task Runs:
    write-read-array-pipeline-run-rmd57-read1-task-m9mvr:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-18T10:15:52Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-read1-task-m9mvr-pod-fjzcl
        Start Time:              2022-03-18T10:15:41Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://a5af1bd0c471132c3c4f3234cb7381ec216a482a14171450c9aa0d2e1ce771b4
            Exit Code:     0
            Finished At:   2022-03-18T10:15:52Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:50Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import gc
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
del data
gc.collect()
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-rmd57-write1-task-t5qcj:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-18T10:15:52Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write1-task-t5qcj-pod-tvfhw
        Start Time:              2022-03-18T10:15:39Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://628865b65876e8c80418a84510e5ab40fc396087ead8252d59901a920c429572
            Exit Code:     0
            Finished At:   2022-03-18T10:15:49Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:47Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
    write-read-array-pipeline-run-rmd57-write2-task-l27nx:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-03-18T10:15:50Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:50Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write2-task-l27nx-pod-njwz6
        Start Time:              2022-03-18T10:15:39Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://191055ccaf39dd69ba12b8357967999560716130241035ae8329f0e8c4c41e6e
            Exit Code:     0
            Finished At:   2022-03-18T10:15:50Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:50Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write2-ws
    write-read-array-pipeline-run-rmd57-write3-task-lksnt:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-03-18T10:15:52Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write3-task-lksnt-pod-sdx4q
        Start Time:              2022-03-18T10:15:39Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://13e0114e13f7b642aa322d443991054434a0cb17c6fa5445afcf5afa6e06871b
            Exit Code:     0
            Finished At:   2022-03-18T10:15:51Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:50Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write3-ws
    write-read-array-pipeline-run-rmd57-write4-task-t8rcd:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-03-18T10:15:52Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write4-task-t8rcd-pod-7fjcs
        Start Time:              2022-03-18T10:15:40Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://5096ad82f52026d3b73a5f7c9ca1ba8c80cd37f87a32796eb3966b1434ecc398
            Exit Code:     0
            Finished At:   2022-03-18T10:15:51Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:48Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write4-ws
    write-read-array-pipeline-run-rmd57-write5-task-5rntq:
      Pipeline Task Name:  write5-task
      Status:
        Completion Time:  2022-03-18T10:15:53Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:53Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write5-task-5rntq-pod-4n7r2
        Start Time:              2022-03-18T10:15:40Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://022d509696499b6c32180cdb09596c3588676d69240a0b8ed1812c4e3d745732
            Exit Code:     0
            Finished At:   2022-03-18T10:15:52Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:49Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w5cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w5cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w5cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write5-ws
    write-read-array-pipeline-run-rmd57-write6-task-7b6nk:
      Pipeline Task Name:  write6-task
      Status:
        Completion Time:  2022-03-18T10:15:52Z
        Conditions:
          Last Transition Time:  2022-03-18T10:15:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-rmd57-write6-task-7b6nk-pod-nnwpw
        Start Time:              2022-03-18T10:15:40Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://dacd72ae00655c39bf91e1f29adc91a1154124f9aadc1ab080a42c4512ed8788
            Exit Code:     0
            Finished At:   2022-03-18T10:15:52Z
            Reason:        Completed
            Started At:    2022-03-18T10:15:48Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w6cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/6to1/32MB/w6cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/6to1/32MB/w6cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write6-ws
Events:
  Type    Reason     Age   From         Message
  ----    ------     ----  ----         -------
  Normal  Started    21s   PipelineRun  
  Normal  Running    21s   PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    10s   PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    8s    PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s    PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
