Name:         write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb
Namespace:    default
Priority:     0
Node:         k8s-worker-node5/10.0.26.214
Start Time:   Wed, 30 Mar 2022 10:12:39 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-mp24g
              tekton.dev/pipelineTask=read2-task
              tekton.dev/task=read2
              tekton.dev/taskRun=write-read-array-pipeline-run-mp24g-read2-task-4q6f6
Annotations:  cni.projectcalico.org/containerID: ef089d868666c82f2aff90c444a014f8c95f099dc8f14a73b9e2fe520244aee0
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.43.176
IPs:
  IP:           192.168.43.176
Controlled By:  TaskRun/write-read-array-pipeline-run-mp24g-read2-task-4q6f6
Init Containers:
  place-tools:
    Container ID:  docker://e742872899912fcb7dd1543b75e9b3e24828db2c1ce5834396a83e2eb21f9e99
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:12:42 +0000
      Finished:     Wed, 30 Mar 2022 10:12:42 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://d54846fc48d80cc85174188b4665d13779f3c9352a95a191fe5735fb39511f5f
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-xftn6"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0X3BpcGUvM3RvMy8yTUIvcjJjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X3NfdHM6CiAgIGZwX3Jjb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCmJ1Zl9zeiA9IDY1NTM2CnJlYWR5ID0gYidceDAxJwpzeW5jX3BpcGUgPSBvcGVuKCIvZGF0YS9zeW5jX3BpcGUiLCAnd2InKQpzeW5jX3BpcGUud3JpdGUocmVhZHkpCnN5bmNfcGlwZS5jbG9zZSgpCiNwcmludCgiV3JpdHRlbiByZWFkeSBzaWduYWwhIikKIyBSZWFkIGRhdGEgb2JqZWN0IGZyb20gZGF0YSBwaXBlCmRhdGFfZmlmbyA9IG9wZW4oJy9kYXRhL2RhdGFfcGlwZScsICdyYicpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfZmlmby5jbG9zZSgpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXRfcGlwZS8zdG8zLzJNQi9yMmNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:12:43 +0000
      Finished:     Wed, 30 Mar 2022 10:12:43 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://e99ef764358b135a98f0f1df9c228507ecf795318382184a35cc383fd022e8a6
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:12:44 +0000
      Finished:     Wed, 30 Mar 2022 10:12:45 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://38a530f109af2e612c19c2277ff7af738808ec541c8ba6d1db121ed2556dbbfe
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:af220ba516b0c154461915f76b5b170ff8f4fb35a5c80f7e94577117f39b743c
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-xftn6
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-30T10:12:50.462Z","type":3}]
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:12:46 +0000
      Finished:     Wed, 30 Mar 2022 10:12:56 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-k6n2q (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://0dd5ab30e133fa0342230a9276898b05ede6a158ed57dbc35239e2087b2be354
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:13:03 +0000
      Finished:     Wed, 30 Mar 2022 10:13:03 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-k6n2q:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task5-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node5
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  36s                default-scheduler  Successfully assigned default/write-read-array-pipeline-run-mp24g-read2-task-4q6f6-pod-tgcqb to k8s-worker-node5
  Normal   Pulled     34s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    33s                kubelet            Created container place-tools
  Normal   Started    33s                kubelet            Started container place-tools
  Normal   Pulled     33s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    32s                kubelet            Created container place-scripts
  Normal   Started    32s                kubelet            Started container place-scripts
  Normal   Pulled     32s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    31s                kubelet            Created container istio-init
  Normal   Started    31s                kubelet            Started container istio-init
  Normal   Pulled     30s                kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Created    29s                kubelet            Created container step-read
  Normal   Started    29s                kubelet            Started container step-read
  Normal   Pulled     29s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    17s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  14s (x2 over 16s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    12s (x2 over 29s)  kubelet            Created container istio-proxy
  Normal   Started    12s (x2 over 29s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  12s (x2 over 28s)  kubelet            Readiness probe failed: Get "http://192.168.43.176:15021/healthz/ready": dial tcp 192.168.43.176:15021: connect: connection refused
  Normal   Pulled     12s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
