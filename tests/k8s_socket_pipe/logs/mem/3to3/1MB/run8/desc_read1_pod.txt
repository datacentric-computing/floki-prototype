Name:         write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln
Namespace:    default
Priority:     0
Node:         k8s-worker-node4/10.0.26.213
Start Time:   Wed, 30 Mar 2022 10:03:06 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-pipeline
              tekton.dev/pipelineRun=write-read-array-pipeline-run-m6hp5
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-pipeline-run-m6hp5-read1-task-2j8zg
Annotations:  cni.projectcalico.org/containerID: 48ddb8f2baec4959a8d721c49e6cf18d6417bf7f6282788e80544881274251da
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads a file
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.91.125
IPs:
  IP:           192.168.91.125
Controlled By:  TaskRun/write-read-array-pipeline-run-m6hp5-read1-task-2j8zg
Init Containers:
  place-tools:
    Container ID:  docker://fd9ab80d037a1c2c7b19f6e3967639b7f6b448ccd74e3a2ee3b431b2e92902af
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:03:09 +0000
      Finished:     Wed, 30 Mar 2022 10:03:09 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://685872a6e7b5c6d21dabb94aa6674807f5ba5c259045b5f1ec1644af0c3a50db
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-4pd9f"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9kYXRhL3RpbWVzdGFtcHMvc29ja2V0X3BpcGUvM3RvMy8xTUIvcjFjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X3NfdHM6CiAgIGZwX3Jjb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCmJ1Zl9zeiA9IDY1NTM2CnJlYWR5ID0gYidceDAxJwpzeW5jX3BpcGUgPSBvcGVuKCIvZGF0YS9zeW5jX3BpcGUiLCAnd2InKQpzeW5jX3BpcGUud3JpdGUocmVhZHkpCnN5bmNfcGlwZS5jbG9zZSgpCiNwcmludCgiV3JpdHRlbiByZWFkeSBzaWduYWwhIikKIyBSZWFkIGRhdGEgb2JqZWN0IGZyb20gZGF0YSBwaXBlCmRhdGFfZmlmbyA9IG9wZW4oJy9kYXRhL2RhdGFfcGlwZScsICdyYicpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfbGVuZ3RoX2JpbiA9IGRhdGFfZmlmby5yZWFkKDgpCmRhdGFfbGVuZ3RoID0gaW50LmZyb21fYnl0ZXMoZGF0YV9sZW5ndGhfYmluLCAiYmlnIikKZGF0YSA9IGJ5dGVhcnJheSgwKQpjb3VudGVyID0gMAp3aGlsZSBjb3VudGVyIDwgZGF0YV9sZW5ndGg6CiAgIGJ1ZmYgPSBkYXRhX2ZpZm8ucmVhZChidWZfc3opCiAgIGNvdW50ZXIgKz0gbGVuKGJ1ZmYpCiAgIGRhdGEuZXh0ZW5kKGJ1ZmYpCmRhdGFfZmlmby5jbG9zZSgpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2RhdGEvdGltZXN0YW1wcy9zb2NrZXRfcGlwZS8zdG8zLzFNQi9yMWNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:03:10 +0000
      Finished:     Wed, 30 Mar 2022 10:03:10 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://55d7d394fb930788328b601986dabfd831e306179c0d25b3b7811eb0a7af829f
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:03:11 +0000
      Finished:     Wed, 30 Mar 2022 10:03:12 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://574c8dad91badd311848947adeea50575c43a8b48980351cce862e84d3688820
    Image:         jupyter/scipy-notebook
    Image ID:      docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-4pd9f
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-03-30T10:03:16.380Z","type":3}]
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:03:13 +0000
      Finished:     Wed, 30 Mar 2022 10:03:22 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data from ws-52bxf (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://05c2776e4d48f4d278496dddc78c4da98ce5b3d278b1d36d1ba8c997f51a58aa
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 30 Mar 2022 10:03:29 +0000
      Finished:     Wed, 30 Mar 2022 10:03:29 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-52bxf:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task4-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node4
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age               From               Message
  ----     ------     ----              ----               -------
  Normal   Scheduled  30s               default-scheduler  Successfully assigned default/write-read-array-pipeline-run-m6hp5-read1-task-2j8zg-pod-ln4ln to k8s-worker-node4
  Normal   Pulled     28s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    27s               kubelet            Created container place-tools
  Normal   Started    27s               kubelet            Started container place-tools
  Normal   Pulled     27s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    27s               kubelet            Created container place-scripts
  Normal   Started    26s               kubelet            Started container place-scripts
  Normal   Pulled     26s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    25s               kubelet            Created container istio-init
  Normal   Started    25s               kubelet            Started container istio-init
  Normal   Pulled     24s               kubelet            Container image "jupyter/scipy-notebook" already present on machine
  Normal   Created    23s               kubelet            Created container step-read
  Normal   Started    23s               kubelet            Started container step-read
  Normal   Pulled     23s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    12s               kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  9s (x2 over 11s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    7s (x2 over 23s)  kubelet            Created container istio-proxy
  Normal   Started    7s (x2 over 23s)  kubelet            Started container istio-proxy
  Normal   Pulled     7s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Warning  Unhealthy  7s                kubelet            Readiness probe failed: Get "http://192.168.91.125:15021/healthz/ready": dial tcp 192.168.91.125:15021: connect: connection refused
