Name:         write-read-array-pipeline-run-klpc8
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-12T08:37:52Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-12T08:37:52Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-klpc8-read1-task-gkdlk:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-read2-task-bmj27:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-read3-task-7bzcl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-read4-task-cjx2z:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-read5-task-5rjn2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-read6-task-9scjb:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-klpc8-write1-task-cpkq9:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-12T08:38:05Z
  Resource Version:  14962849
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-klpc8
  UID:               b87c6f3c-87be-46c6-8210-3f37fcb52c39
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read5-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read6-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-03-12T08:38:05Z
  Conditions:
    Last Transition Time:  2022-03-12T08:38:05Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       read4-ws
        Workspace:  task-read4-ws
      Name:         read5-task
      Task Ref:
        Kind:  Task
        Name:  read5
      Workspaces:
        Name:       read5-ws
        Workspace:  task-read5-ws
      Name:         read6-task
      Task Ref:
        Kind:  Task
        Name:  read6
      Workspaces:
        Name:       read6-ws
        Workspace:  task-read6-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
      Name:    task-read5-ws
      Name:    task-read6-ws
  Start Time:  2022-03-12T08:37:52Z
  Task Runs:
    write-read-array-pipeline-run-klpc8-read1-task-gkdlk:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-12T08:38:04Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read1-task-gkdlk-pod-tnskr
        Start Time:              2022-03-12T08:37:52Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://2b4483fd3fade81c9bf17fd846734f27f6969acf8e47c4b94fb8f881050ac8a4
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:01Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-klpc8-read2-task-bmj27:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-12T08:38:04Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read2-task-bmj27-pod-gjkd4
        Start Time:              2022-03-12T08:37:53Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://3f4c3c5aba66c3b6a379007be6558add7b0c9934b43aa6c1f447663f1a6d42fa
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:02Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-klpc8-read3-task-7bzcl:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-12T08:38:03Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:03Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read3-task-7bzcl-pod-cc4cq
        Start Time:              2022-03-12T08:37:53Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://be0dadaf990117594dd169130882db61820b4c23b78d9275bb195ec2f3c12926
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:01Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-ws
    write-read-array-pipeline-run-klpc8-read4-task-cjx2z:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-03-12T08:38:04Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read4-task-cjx2z-pod-96wsz
        Start Time:              2022-03-12T08:37:53Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://6852e6381349a61eea7e10f62dcb90984e0282678ddc8ebe5e8eb4e126eacff7
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:01Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read4-ws
    write-read-array-pipeline-run-klpc8-read5-task-5rjn2:
      Pipeline Task Name:  read5-task
      Status:
        Completion Time:  2022-03-12T08:38:05Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:05Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read5-task-5rjn2-pod-gtj9t
        Start Time:              2022-03-12T08:37:54Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://8b2c76c387188d9745c3d09b71896cdc5118a04270a90f72eea6abbb4185ee8a
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:03Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r5cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r5cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read5-ws
    write-read-array-pipeline-run-klpc8-read6-task-9scjb:
      Pipeline Task Name:  read6-task
      Status:
        Completion Time:  2022-03-12T08:38:04Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-read6-task-9scjb-pod-cmjck
        Start Time:              2022-03-12T08:37:54Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://6a95e50e9bd757db70d780ab9e51f33ced7d92b7ff2a24703403ecc7d3aee061
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:02Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r6cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/r6cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read6-ws
    write-read-array-pipeline-run-klpc8-write1-task-cpkq9:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-12T08:38:04Z
        Conditions:
          Last Transition Time:  2022-03-12T08:38:04Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-klpc8-write1-task-cpkq9-pod-vwkxm
        Start Time:              2022-03-12T08:37:52Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://7a08438b3c95fce21be62218ab40dcd8dc7ba8809c5fc52dfb9b79188cc1fd78
            Exit Code:     0
            Finished At:   2022-03-12T08:38:03Z
            Reason:        Completed
            Started At:    2022-03-12T08:38:01Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 33554432
data_as_bytes = bytearray(b'\x78') * 33554432
#data_as_bytes = str.encode(string)
data_dim = 33554432
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/1to6/32MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to6/32MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    21s (x2 over 21s)  PipelineRun  
  Normal  Running    20s (x2 over 21s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    10s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    9s (x2 over 9s)    PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
