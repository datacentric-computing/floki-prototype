Name:         write-read-array-pipeline-run-wk46v
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-03T14:08:24Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-03T14:08:24Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-wk46v-read1-task-dznnt:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-wk46v-read2-task-k8p6g:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-wk46v-write1-task-zl94g:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-03T14:08:43Z
  Resource Version:  4842035
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-wk46v
  UID:               430e0eec-ed0e-499f-87da-955cd8323fb0
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
Status:
  Completion Time:  2022-03-03T14:08:43Z
  Conditions:
    Last Transition Time:  2022-03-03T14:08:43Z
    Message:               Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-03-03T14:08:24Z
  Task Runs:
    write-read-array-pipeline-run-wk46v-read1-task-dznnt:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-03T14:08:43Z
        Conditions:
          Last Transition Time:  2022-03-03T14:08:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-wk46v-read1-task-dznnt-pod-4rvxl
        Start Time:              2022-03-03T14:08:24Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       read
          Terminated:
            Container ID:  docker://f6a94c4d9125c7b1b3b54abf57afab2a7e0ddb9807a665091ba14d75bc02f771
            Exit Code:     0
            Finished At:   2022-03-03T14:08:42Z
            Reason:        Completed
            Started At:    2022-03-03T14:08:31Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-wk46v-read2-task-k8p6g:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-03T14:08:43Z
        Conditions:
          Last Transition Time:  2022-03-03T14:08:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-wk46v-read2-task-k8p6g-pod-mx8lt
        Start Time:              2022-03-03T14:08:24Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       read
          Terminated:
            Container ID:  docker://4dabdfee7b2ff74cdc2fb30a3752e8fd602d302904758cc4b7a8bed461e250e6
            Exit Code:     0
            Finished At:   2022-03-03T14:08:42Z
            Reason:        Completed
            Started At:    2022-03-03T14:08:36Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-wk46v-write1-task-zl94g:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-03T14:08:43Z
        Conditions:
          Last Transition Time:  2022-03-03T14:08:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-wk46v-write1-task-zl94g-pod-6nv9f
        Start Time:              2022-03-03T14:08:24Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:e51cb4700af349c040bbf83c7f7a3c5fb94edb97df3071be48d5eae6c03d2f5b
          Name:       write
          Terminated:
            Container ID:  docker://a6526166b8d93232a3e57ee8d7fd79ffe37ec5c386a682695cf7d91512672a89
            Exit Code:     0
            Finished At:   2022-03-03T14:08:42Z
            Reason:        Completed
            Started At:    2022-03-03T14:08:37Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 536870912
data_as_bytes = bytearray(b'\x78') * 536870912
#data_as_bytes = str.encode(string)
data_dim = 536870912
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/1to2/512MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to2/512MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
Events:
  Type    Reason     Age   From         Message
  ----    ------     ----  ----         -------
  Normal  Started    27s   PipelineRun  
  Normal  Running    26s   PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    7s    PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    7s    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Skipped: 0
