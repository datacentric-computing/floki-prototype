Name:         write-read-array-pipeline-run-5n8g4
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-03-12T00:05:05Z
  Generate Name:       write-read-array-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-03-12T00:05:05Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-pipeline-run-5n8g4-read1-task-l25p6:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-5n8g4-read2-task-v2hq4:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-5n8g4-read3-task-nbndq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-5n8g4-read4-task-gsnbt:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-pipeline-run-5n8g4-write1-task-rkc5p:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-03-12T00:05:18Z
  Resource Version:  14123960
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-pipeline-run-5n8g4
  UID:               0f2db4f2-f994-43c1-8519-ba1ff6359a99
Spec:
  Pipeline Ref:
    Name:                write-read-array-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-03-12T00:05:18Z
  Conditions:
    Last Transition Time:  2022-03-12T00:05:18Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       read4-ws
        Workspace:  task-read4-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
  Start Time:  2022-03-12T00:05:05Z
  Task Runs:
    write-read-array-pipeline-run-5n8g4-read1-task-l25p6:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-03-12T00:05:18Z
        Conditions:
          Last Transition Time:  2022-03-12T00:05:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-5n8g4-read1-task-l25p6-pod-mzvss
        Start Time:              2022-03-12T00:05:06Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://011e7456947f890881753ddee418e42b3a0605c51b0e8078a1b57c039075fc7a
            Exit Code:     0
            Finished At:   2022-03-12T00:05:17Z
            Reason:        Completed
            Started At:    2022-03-12T00:05:12Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read1-ws
    write-read-array-pipeline-run-5n8g4-read2-task-v2hq4:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-03-12T00:05:18Z
        Conditions:
          Last Transition Time:  2022-03-12T00:05:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-5n8g4-read2-task-v2hq4-pod-rklsv
        Start Time:              2022-03-12T00:05:06Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://cd956c98a0d42309e9e70c4c4e8cab06e2ed60a1f4c61fc30d91cb5d6c45bc0e
            Exit Code:     0
            Finished At:   2022-03-12T00:05:17Z
            Reason:        Completed
            Started At:    2022-03-12T00:05:15Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read2-ws
    write-read-array-pipeline-run-5n8g4-read3-task-nbndq:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-03-12T00:05:18Z
        Conditions:
          Last Transition Time:  2022-03-12T00:05:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-5n8g4-read3-task-nbndq-pod-fhtpj
        Start Time:              2022-03-12T00:05:06Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://2ad56e830e228af9630de1f7be8e4cd0f5d622fc7abf494d11016db3e327efe7
            Exit Code:     0
            Finished At:   2022-03-12T00:05:17Z
            Reason:        Completed
            Started At:    2022-03-12T00:05:14Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read3-ws
    write-read-array-pipeline-run-5n8g4-read4-task-gsnbt:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-03-12T00:05:18Z
        Conditions:
          Last Transition Time:  2022-03-12T00:05:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-5n8g4-read4-task-gsnbt-pod-rn57b
        Start Time:              2022-03-12T00:05:06Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       read
          Terminated:
            Container ID:  docker://45065dfcfdac2f7687ff9169aa999185f5a3311fb9595320a620f80ee1defbdf
            Exit Code:     0
            Finished At:   2022-03-12T00:05:17Z
            Reason:        Completed
            Started At:    2022-03-12T00:05:15Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
buf_sz = 65536
ready = b'\x01'
sync_pipe = open("/data/sync_pipe", 'wb')
sync_pipe.write(ready)
sync_pipe.close()
#print("Written ready signal!")
# Read data object from data pipe
data_fifo = open('/data/data_pipe', 'rb')
data_length_bin = data_fifo.read(8)
data_length = int.from_bytes(data_length_bin, "big")
data = bytearray(0)
counter = 0
while counter < data_length:
   buff = data_fifo.read(buf_sz)
   counter += len(buff)
   data.extend(buff)
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /data
            Name:        read4-ws
    write-read-array-pipeline-run-5n8g4-write1-task-rkc5p:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-03-12T00:05:18Z
        Conditions:
          Last Transition Time:  2022-03-12T00:05:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-pipeline-run-5n8g4-write1-task-rkc5p-pod-5v2tc
        Start Time:              2022-03-12T00:05:06Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://jupyter/scipy-notebook@sha256:87b339ec6a62b283d0c2aee1446e7bc2e5ad03a80d49d2dfddb031b233aedd29
          Name:       write
          Terminated:
            Container ID:  docker://d7ad07190ee3d2d9b8fd3a9e8f0e480f9413d0d98109cffd594db8c28a44467c
            Exit Code:     0
            Finished At:   2022-03-12T00:05:17Z
            Reason:        Completed
            Started At:    2022-03-12T00:05:16Z
        Task Spec:
          Steps:
            Image:              jupyter/scipy-notebook
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
# Store initial timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create data object
# string = 'x' * 134217728
data_as_bytes = bytearray(b'\x78') * 134217728
#data_as_bytes = str.encode(string)
data_dim = 134217728
data_dim_bin = data_dim.to_bytes(8, "big")
# Store timestamp after data creation
with open("/data/timestamps/socket_pipe/1to4/128MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on pipe
data_fifo = open('/data/data_pipe', 'wb') 
data_fifo.write(data_dim_bin)
data_fifo.write(data_as_bytes) 
data_fifo.close()
# Store final timestamp
with open("/data/timestamps/socket_pipe/1to4/128MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())

          Workspaces:
            Mount Path:  /data
            Name:        write1-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    21s (x2 over 21s)  PipelineRun  
  Normal  Running    20s (x2 over 21s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    8s                 PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    8s                 PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    8s                 PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    8s                 PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
