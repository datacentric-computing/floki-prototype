#!/bin/bash

# Extract all worker ips
IP_ADDRS=( $(cat ../../../cluster_ips.txt | grep 'worker' | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}') )

# Get first two worker ips
first_worker_ip=${IP_ADDRS[0]}
second_worker_ip=${IP_ADDRS[1]}

# Copy source code to the two worker ips
scp ../writer/test_socket.cpp root@${first_worker_ip}:/home/vagrant
echo "Copied writer test_socket.cpp to ${first_worker_ip}!"
scp ../reader/test_socket.cpp root@${second_worker_ip}:/home/vagrant
echo "Copied reader test_socket.cpp to ${second_worker_ip}!"
