import sys
import getopt
import math
import os
from pathlib import Path
import re
import csv
from statistics import median

dims = [ "1 MB",
        "2 MB",
        "4 MB",
        "8 MB",
        "16 MB",
        "32 MB",
        "64 MB",
        "128 MB",
        "256 MB",
        "512 MB",
        "1 GB",
        "2 GB",
        "4 GB",
        "8 GB",
        "16 GB"
]

NUM_RUNS = 10

NUM_WTASKS = 6
NUM_RTASKS = 1

def usage():
    print("get_stat_timestamps.py -c <configuration> -m <mechanism>")
    print(" -c              <configuration>     specify the configuration   (disk or mem)")
    print(" -m              <mechanism>         specify the mechanism       (shared or socket)")
    print(" --all                           get all statistics")
    print(" --dump                          store statistics on files")

def get_arguments(argv):
    try:
        opts, args = getopt.getopt(argv, "hc:m:", ["help", "configuration=", "mechanism=",  "all", "dump"])
    except getopt.GetoptError:
        print("Error")
        usage()
        sys.exit(2)
    all_mec = False
    dump = False
    conf = []
    mec = []
    # Analyse the given options
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-c", "--configuration"):
            conf = [arg]
        elif opt in ("-m", "--mechanism"):
            mec = [arg]
        elif opt == "--all":
            all_mec = True
        elif opt == "--dump":
            dump = True
    if not all_mec:
        # Check if configuration and mechanism are given
        if len(conf) == 0 or len(mec) == 0:
            print("ERROR: Configuration and mechanism have to be given!")
            exit(1)
        # Check the correctness of the input arguments
        if conf[0] not in ["mem", "disk"]:
            print("ERROR: The configuration can only be 'mem' or 'disk'!")
            exit(1)
        if mec[0] not in ["shared", "shared_minio", "socket", "socket_pipe"]:
            print("ERROR: The mechanism can only be 'shared', 'shared_minio', 'socket' or 'socket_pipe'!")
            exit(1)
        # Check if shared memory configuration is given
        if conf[0] == 'mem' and (mec[0] == 'shared' or mec[0] == 'shared_minio'):
            print("ERROR: Memory is not allowed for the shared mechanism!")
            exit(1)
        if conf[0] == 'disk' and mec[0] == 'socket_pipe':
            print("ERROR: Disk is not allowed for the socket pipe mechanism!")
            exit(1)
    else:
        conf = ["disk", "mem"]
        mec = ["shared", "shared_minio", "socket", "socket_pipe"]
    return conf, mec, dump

def get_remote_timestamps(conf_list, mec_list):
    for dim in dims: 
        print("Getting timestamps from remote for %s data dimension.." % dim)
        for mec in mec_list:
            for conf in conf_list:
                if mec == 'shared' and conf == 'mem':
                    continue
                if mec == 'socket_pipe' and conf == 'disk':
                    continue
                # Get the timestamp folder path
                if mec == 'shared_minio':
                    ts_folder = "k8s_" + mec + "/timestamps/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
                else:
                    ts_folder = "k8s_" + mec + "/timestamps/" + conf + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
                if os.path.isdir(ts_folder):
                    # Delete all files into folder
                    cmd = "rm " + ts_folder + "/*"
                else:
                    # Create folder
                    cmd = "mkdir -p " + ts_folder
                    os.system(cmd)
                # Copy the generated timestamps from remote folders
                #remote_path = "/mnt/" + conf + "/vol2/timestamps/" + mec + "/* "
                remote_path = "/mnt/" + conf + "/vol2/timestamps/" + mec + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "") + "/*"
                ips = []
                with open('../../cluster_ips.txt', 'r') as f:
                    for line in f:
                        strings = line.split()
                        ips.append(strings[0])
                ips = ips[1:]
                for idx in range(NUM_WTASKS+NUM_RTASKS):
                    cmd = "sshpass -p 'root' scp -r root@" + ips[idx] + ":" + remote_path + " " + ts_folder + "/"
                    os.system(cmd)
                    print("Copied %s %s timestamps from node %s!" % (mec, conf, ips[idx]))

def variance(data):
    n = len(data)
    mean = sum(data) / n
    return sum((x - mean) ** 2 for x in data) / n

def stdev(data):
    var = variance(data)
    std_dev = math.sqrt(var)
    return std_dev

def get_stats(values):
    med = median(values)
    #var = variance(values)
    #print("VARIANCE W/R: %f" % var)
    #std_dev = stdev(values)
    #print("STD_DEV W/R: %f" % std_dev) 
    return med

def get_diffs_timestamps(conf, mec, start_ts_file, end_ts_file, dim):
    # Get the timestamp folder path
    if mec == 'shared_minio':
        ts_folder = "k8s_" + mec + "/timestamps/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
    else:
        ts_folder = "k8s_" + mec + "/timestamps/" + conf + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
    diffs = []
    # Open the start and end timestamps files
    with open(ts_folder + "/" + start_ts_file, 'r') as fp_start_ts:
        with open(ts_folder + "/" + end_ts_file, 'r') as fp_end_ts:
            for start_ts, end_ts in zip(fp_start_ts, fp_end_ts):
                diffs.append(abs(float(end_ts)-float(start_ts)))
    return diffs

def get_pattern_diffs_timestamps(conf, mec, dim):
    if mec == 'shared_minio':
        ts_folder = "k8s_" + mec + "/timestamps/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
    else:
        ts_folder = "k8s_" + mec + "/timestamps/" + conf + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "")
    start_ts = []
    end_ts = []
    diffs = []
    for w in range(1, NUM_WTASKS+1):
        with open(ts_folder + "/" + "w" + str(w) + "cont_data_end_ts.txt", 'r') as fp_ts:
            start_ts.append(fp_ts.readlines())
    for r in range(1, NUM_RTASKS+1):
        with open(ts_folder + "/" + "r" + str(r) + "cont_end_ts.txt", 'r') as fp_ts:
            end_ts.append(fp_ts.readlines())
    for i in range(NUM_RUNS):
        run_w_ts = [float(start_ts[k][i][:-2]) for k in range(NUM_WTASKS)]
        min_w = min(run_w_ts)
        run_r_ts = [float(end_ts[k][i][:-2]) for k in range(NUM_RTASKS)]
        max_r = max(run_r_ts)
        diffs.append(max_r - min_w)
    return diffs

# Get statistics on the end-to-end (write, network, read) time
def get_stats_on_end_to_end(conf, mec, dim):
    diffs = []
    start_times = []
    end_times = []
    if NUM_WTASKS + NUM_RTASKS == 1:
        diffs.append(get_diffs_timestamps(conf, mec, "w1cont_data_end_ts.txt", "r1cont_end_ts.txt", dim))
        diffs = [val for sublist in diffs for val in sublist]
    else:
        diffs = get_pattern_diffs_timestamps(conf, mec, dim)
    e2e_avg = get_stats(diffs)
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("End-to-end Average Time:\t\t\t%.5f s" % round(e2e_avg, 5))
    return abs(e2e_avg)

# Get statistics on the network time
def get_stats_on_network(conf, mec, dim):
    diffs = get_diffs_timestamps(conf, mec, "net1_1_start_ts.txt", "net1_1_end_ts.txt", dim)
    net_med = abs(get_stats(diffs))
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("Communication Over Network Median Time:\t%.2f s" % abs(round(net_med, 2)))
    return net_med

# Get statistics on the containers
def get_stats_on_conts(conf, mec, dim):
    w_data_diffs, w_op_diffs, w_sent_diffs = ([] for i in range(3))
    r_data_diffs, r_op_diffs, r_sent_diffs = ([] for i in range(3))
    for w in range(1, NUM_WTASKS+1):
        # Get statistics
        w_data_diffs.append(get_diffs_timestamps(conf, mec, "w" + str(w) + "cont_start_ts.txt", "w" + str(w) + "cont_data_end_ts.txt", dim))
        w_op_diffs.append(get_diffs_timestamps(conf, mec, "w" + str(w) + "cont_data_end_ts.txt", "w" + str(w) + "cont_end_ts.txt", dim))
        if mec == 'socket':
            w_sent_diffs.append(get_diffs_timestamps(conf, mec, "w" + str(w) + "sent_start_ts.txt", "w" + str(w) + "sent_end_ts.txt", dim))

    w_data_diffs = [val for sublist in w_data_diffs for val in sublist]
    med_w_data = get_stats(w_data_diffs)
    w_op_diffs = [val for sublist in w_op_diffs for val in sublist]
    med_w_op = get_stats(w_op_diffs)
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("Write Container Median Data Time:\t%.5f s" % round(med_w_data, 5))
    print("Write Container Median Write Time:\t%.5f s" % round(med_w_op, 5))
    print("Write Container Median Total Time:\t%.5f s" % round(med_w_data+med_w_op, 5))
    if mec == 'socket':
        w_sent_diffs = [val for sublist in w_sent_diffs for val in sublist]
        med_w_sent = get_stats(w_sent_diffs)
        print("Write Container Median Sentinel Time:\t%.5f s" % round(med_w_sent, 5))
    for r in range(1, NUM_RTASKS+1):
        r_op_diffs.append(get_diffs_timestamps(conf, mec, "r" + str(r) + "cont_start_ts.txt", "r" + str(r) + "cont_end_ts.txt", dim))
        if mec == 'socket':
            r_sent_diffs.append(get_diffs_timestamps(conf, mec, "r" + str(r) + "sent_start_ts.txt", "r" + str(r) + "sent_end_ts.txt", dim))
    r_op_diffs = [ val for sublist in r_op_diffs for val in sublist]
    med_r_op = get_stats(r_op_diffs)
    print("Read Container Median Read/Total Time:\t%.5f s" % round(med_r_op, 5))
    if mec == 'socket':
        r_sent_diffs = [val for sublist in r_sent_diffs for val in sublist]
        med_r_sent = get_stats(r_sent_diffs)
        print("Read Container Median Sentinel Time:\t%.5f s" % round(med_r_sent, 5))
        return [med_w_data, med_w_op, med_r_op, med_w_sent, med_r_sent]
    return [med_w_data, med_w_op, med_r_op]

def search_times_into_file(strings_list, f_path, time_format):
    lines_matching = [[] for i in range(len(strings_list))]
    with open(f_path, 'r') as fp:
        for line in fp:
            for idx, string in enumerate(strings_list):
                if string in line:
                    if time_format == 'Tekton':
                        # Time is in Tekton format YYYY-MM-DDTHH:MM:SS
                        T_idxs = [m.start() for m in re.finditer('T', line)]
                        time_only = line[T_idxs[-1]+1:-2]
                    else:
                        # Time is in Kubernetes format Weekday, Day Month YYYY HH:MM:SS
                        line_split = [x for x in re.split(' +', line) if x != '']
                        time_only = line_split[5]
                    # Store found matching
                    lines_matching[idx].append(time_only)
    return lines_matching

def time_to_sec(t):
    time_els = t.split(":")
    hour = int(time_els[0])
    mins = int(time_els[1])
    secs = int(time_els[2])
    return hour*3600 + mins*60 + secs

def get_time_secs_diff(time1, time2):
    if time2 < time1:
        # Completion time is in the next day
        return 86400 - time2 + time1
    else:
        # Start and completion are in the same day
        return time2 - time1

def get_stats_on_pipeline(conf, mec, dim):
    # Get the logs folder path
    if mec == 'shared_minio':
        logs_folder = "k8s_" + mec + "/logs/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "") + "/run"
    else:
        logs_folder = "k8s_" + mec + "/logs/" + conf + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "") + "/run"
    # Define keywords to search in the log files
    search_for = ["Start Time:", "Completion Time:"]
    diffs_twrite, diffs_tread, diffs_pipeline = ([] for i in range(3))
    # For each run store the pipeline, the write task, and the read task time differences 
    for r in range(NUM_RUNS):
        # Get lines matching the searched keywords (0: start time, 1: completion time)
        f_path = logs_folder + str(r) + "/desc_pipelinerun.txt"
        lines_matching = search_times_into_file(search_for, f_path, 'Tekton')
        # Compute pipeline time difference
        start_time_secs = time_to_sec(lines_matching[0][0])
        completion_time_secs = time_to_sec(lines_matching[1][0])
        diffs_pipeline.append(get_time_secs_diff(start_time_secs, completion_time_secs))
        if NUM_WTASKS + NUM_RTASKS == 2:
            # Compute task write time difference
            start_time_secs = time_to_sec(lines_matching[0][2])
            completion_time_secs = time_to_sec(lines_matching[1][2])
            diffs_twrite.append(get_time_secs_diff(start_time_secs, completion_time_secs))
            # Compute task read time difference
            start_time_secs = time_to_sec(lines_matching[0][1])
            completion_time_secs = time_to_sec(lines_matching[1][1])
            diffs_tread.append(get_time_secs_diff(start_time_secs, completion_time_secs))
    # Get statists
    stats_pipeline = get_stats(diffs_pipeline)
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("Pipeline Median Time:\t\t\t%.2f s" % round(stats_pipeline, 2))
    if NUM_WTASKS + NUM_RTASKS == 2:
        stats_twrite = get_stats(diffs_twrite)
        print("Task Write Median Time:\t\t\t%.2f s" % round(stats_twrite, 2))
        stats_tread = get_stats(diffs_tread)
        print("Task Read Median Time:\t\t\t%.2f s" % round(stats_tread, 2))
        return stats_pipeline, stats_twrite, stats_tread
    return stats_pipeline

def get_stats_on_pod(conf, mec, dim, name_pod):
    # Get the logs folder path
    if mec == 'shared_minio':
        logs_folder = "k8s_" + mec + "/logs/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "") + "/run"
    else:
        logs_folder = "k8s_" + mec + "/logs/" + conf + "/" + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + "/" + dim.replace(" ", "") + "/run"
    # Define keywords to search in the log files
    search_for = ["Started:", "Finished:"]
    diffs_init, diffs_cont = ([[]] for i in range(2))
    pod_stats = []
    tot_tasks = NUM_WTASKS if name_pod == "write" else NUM_RTASKS
    # For each run store the init containers and containers (excluding istio-proxy) time differences 
    for r in range(NUM_RUNS):
        temp_init, temp_cont = ([] for i in range(2))
        for nr in range(tot_tasks):
            # Get lines matching the searched keywords (0: start time, 1: end time) 
            f_path = logs_folder + str(r) +  "/desc_" + name_pod + str(nr+1) + "_pod.txt" 
            lines_matching = search_times_into_file(search_for, f_path, 'Kubernetes')
            # Get minimum/maximum of init containers start/end times and take diference
            min_init_start = min([time_to_sec(x) for x in lines_matching[0][:3]])
            max_init_end = max([time_to_sec(x) for x in lines_matching[1][:3]])
            temp_init.append(get_time_secs_diff(min_init_start, max_init_end))
            # Get minimum/maximum of containers start/end times and take diference
            if mec == 'socket':
                min_cont_start = min([time_to_sec(y) for y in lines_matching[0][3:5]])
                max_cont_end = max([time_to_sec(y) for y in lines_matching[1][3:5]])
            else:
                min_cont_start = time_to_sec(lines_matching[0][3])
                max_cont_end = time_to_sec(lines_matching[1][3])
            temp_cont.append(get_time_secs_diff(min_cont_start, max_cont_end))
        diffs_init.append(temp_init)
        diffs_cont.append(temp_cont)
    # Get statistics 
    values = [val for sublist in diffs_init for val in sublist]
    init_med = get_stats(values)
    pod_stats.append(init_med)
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
    print("Pod %s Init Containers Median Time:\t%.2f s" % (name_pod.title(), round(init_med, 2)))
    values = [val for sublist in diffs_cont for val in sublist]
    conts_med = get_stats(values)
    pod_stats.append(conts_med)
    print("Pod %s Containers Median Time:\t%.2f s" % (name_pod.title(), round(conts_med, 2)))
    return pod_stats

def dump_stats_to_files(tekton_stats, e2e_stats, pods_stats = None, conts_stats = None, net_stats = None):
    # Dump statistic to csv files
    with open('stats_files/' + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + '_stats_tekton_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
        if NUM_WTASKS + NUM_RTASKS == 1:
            cols_names = ['data_dim', 'sh_pipeline', 'sh_twrite_time', 'sh_tread_time',
                'so_d_pipeline', 'so_d_twrite_time', 'so_d_tread_time',
                'so_m_pipeline', 'so_m_twrite_time', 'so_m_tread_time',
                'so_p_pipeline', 'so_p_twrite_time', 'so_p_tread_time']
        else:
            cols_names = ['data_dim', 'sh_pipeline', 
                'so_d_pipeline',
                'so_m_pipeline',
                'so_p_pipeline']
        write = csv.writer(fp)
        write.writerow(cols_names)
        write.writerows(tekton_stats)
    with open('stats_files/' + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + '_stats_e2e_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
        cols_names = ['data_dim', 'sh_e2e_time', 
            'so_d_e2e_time',
            'so_m_e2e_time',
            'so_p_e2e_time']
        write = csv.writer(fp)
        write.writerow(cols_names)
        write.writerows(e2e_stats)
    if NUM_WTASKS + NUM_RTASKS == 1:
        with open('stats_files/' + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + '_stats_pods_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
            cols_names = ['data_dim', 'sh_twrite_iconts_time', 'sh_twrite_mconts_time', 'sh_tread_iconts_time', 'sh_tread_mconts_time',
                'so_d_twrite_iconts_time', 'so_d_twrite_mconts_time', 'so_d_tread_iconts_time', 'so_d_tread_mconts_time',
                'so_m_twrite_iconts_time', 'so_m_twrite_mconts_time', 'so_m_tread_iconts_time', 'so_m_tread_mconts_time',
                'so_p_twrite_iconts_time', 'so_p_twrite_mconts_time', 'so_p_tread_iconts_time', 'so_p_tread_mconts_time']
            write = csv.writer(fp)
            write.writerow(cols_names)
            write.writerows(pods_stats)
        with open('stats_files/' + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + '_stats_conts_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
            cols_names = ['data_dim', 'sh_w_data_time', 'sh_w_op_time', 'sh_r_op_time',
                'sh_minio_w_data_time', 'sh_minio_w_op_time', 'sh_minio_r_op_time',
                'so_d_w_data_time', 'so_d_w_op_time', 'so_d_r_op_time', 'so_d_w_sent_time', 'so_d_r_sent_time',
                'so_m_w_data_time', 'so_m_w_op_time', 'so_m_r_op_time', 'so_m_w_sent_time', 'so_m_r_sent_time',
                'so_p_w_data_time', 'so_p_w_op_time', 'so_p_r_op_time']
            write = csv.writer(fp)
            write.writerow(cols_names)
            write.writerows(conts_stats)
        if len(net_stats) > 0:
            with open('stats_files/' + str(NUM_WTASKS) + "to" + str(NUM_RTASKS) + '_stats_net_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
                cols_names = ['data_dim', 'so_d_net_time',
                    'so_m_net_time']
                write = csv.writer(fp)
                write.writerow(cols_names)
                write.writerows(net_stats)
        
def get_all_stats(conf_list, mec_list, dump):
    # Initialize global statistics lists
    tekton_stats, e2e_stats, pods_stats, conts_stats, net_stats = ([] for i in range(5))
    # Get remote timestamps files
    get_remote_timestamps(conf_list, mec_list)
    # For each data dimension compute statistics
    for dim in dims:
        print("******************************** STATISTICS ON %s ********************************" % dim)
        # Initialize dimension lists
        dim_tekton_stats, dim_e2e_stats, dim_pods_stats, dim_conts_stats, dim_net_stats = ([] for i in range(5))
        # For each mechanism and for each configuration get statistics
        for mec in mec_list:
            for conf in conf_list:
                if (mec == 'shared' or mec == 'shared_minio')  and conf == 'mem':
                    continue
                if mec == 'socket_pipe' and conf == 'disk':
                    continue
                # Obtain statistics on the tested data dimensions
                print("----------------------- MECHANISM %s CONFIGURATION: %s ------------------------" % (mec, conf))
                # Get the tekton objects statistics
                if NUM_WTASKS + NUM_RTASKS == 2:
                    stats_pipeline, stats_twrite, stats_tread = get_stats_on_pipeline(conf, mec, dim)
                    dim_tekton_stats.append(stats_pipeline)
                    dim_tekton_stats.append(stats_twrite)
                    dim_tekton_stats.append(stats_tread)
                else:
                    stats_pipeline = get_stats_on_pipeline(conf, mec, dim)
                    dim_tekton_stats.append(stats_pipeline)
                # Get the write, transfer, read ent-to-end statistics
                stats_end_to_end = get_stats_on_end_to_end(conf, mec, dim)
                dim_e2e_stats.append(stats_end_to_end)
                if NUM_WTASKS + NUM_RTASKS == 2:
                    # Get the kubernetes objects statistics
                    stats_pwrite = get_stats_on_pod(conf, mec, dim, 'write')
                    stats_pread = get_stats_on_pod(conf, mec, dim, 'read')
                    dim_pods_stats = dim_pods_stats + stats_pwrite + stats_pread
                    # Get containers statistics
                    dim_conts_stats = dim_conts_stats + [sublist for sublist in get_stats_on_conts(conf, mec, dim)]
                    if mec == 'socket':
                        # Get netwtork statistics
                        dim_net_stats.append(get_stats_on_network(conf, mec, dim))
        # Append dimension statistics to glocal lists
        tekton_stats.append([dim] + dim_tekton_stats)
        e2e_stats.append([dim] + dim_e2e_stats)
        if NUM_WTASKS + NUM_RTASKS == 2:
            pods_stats.append([dim] + dim_pods_stats)
            conts_stats.append([dim] + dim_conts_stats)
            if mec == 'socket':
                net_stats.append([dim] + dim_net_stats)
    # Dump statistics if required
    if dump:
        if NUM_WTASKS + NUM_RTASKS == 2:
            dump_stats_to_files(tekton_stats, e2e_stats, pods_stats, conts_stats, net_stats)
        else:
            dump_stats_to_files(tekton_stats, e2e_stats)

def main(argv):
    # Get arguments
    conf_list, mec_list, dump = get_arguments(argv)
    # Get statistics
    get_all_stats(conf_list, mec_list, dump)

if __name__ == "__main__":
    main(sys.argv[1:])
