Name:         write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb
Namespace:    default
Priority:     0
Node:         k8s-worker-node4/10.0.26.213
Start Time:   Mon, 27 Jun 2022 04:48:32 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-zpngj
              tekton.dev/pipelineTask=read3-task
              tekton.dev/task=read3
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-zpngj-read3-task-kwvpp
Annotations:  cni.projectcalico.org/containerID: 6bddb5514cbed7167d4994a19c0d357f07983cf561854bb7601cb47c39f1d6f2
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads an object from minio
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.91.97
IPs:
  IP:           192.168.91.97
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-zpngj-read3-task-kwvpp
Init Containers:
  place-tools:
    Container ID:  docker://4d527dc7396e2f2f8b78b7a8fdf28f01a3df1e222a91198f2bc0dbc877ff712e
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:34 +0000
      Finished:     Mon, 27 Jun 2022 04:48:34 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://40ba29324a55b092018835affc0e64b2ec42d5cb4c75585f906718b00c840b6b
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-c9prt"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IHN1YnByb2Nlc3MKaW1wb3J0IHN5cwppbXBvcnQgYm90bzMKaW1wb3J0IGlvCmZyb20gYm90b2NvcmUuY29uZmlnIGltcG9ydCBDb25maWcKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby8xdG82LzFHQi9yM2NvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfcmNvbnRfc190czoKICAgZnBfcmNvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKc2Vzc2lvbiA9IGJvdG8zLnNlc3Npb24uU2Vzc2lvbigpCmNvbmZpZyA9IENvbmZpZyhyZXRyaWVzID0gZGljdChtYXhfYXR0ZW1wdHMgPSA1MCkpCnMzX2NsaWVudCA9IHNlc3Npb24uY2xpZW50KAogICBzZXJ2aWNlX25hbWU9J3MzJywKICAgYXdzX2FjY2Vzc19rZXlfaWQ9J2FkbWluJywKICAgYXdzX3NlY3JldF9hY2Nlc3Nfa2V5PSdzZWNyZXRwd2QnLAogICBlbmRwb2ludF91cmw9J2h0dHA6Ly8xMC4wLjI2LjMzOjkwMDAnLAogICBjb25maWc9Y29uZmlnCikKIyBSZWFkIGRhdGEgb2JqZWN0IGZyb20gcmVtb3RlIHNoYXJlZCBzdG9yYWdlCmRhdGFfYmluMSA9IGlvLkJ5dGVzSU8oKQpzM19jbGllbnQuZG93bmxvYWRfZmlsZW9iaihCdWNrZXQ9J2RhdGEnLCBLZXk9J291dDEuYmluJywgRmlsZW9iaj1kYXRhX2JpbjEpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2xvY2FsL3RpbWVzdGFtcHMvc2hhcmVkX21pbmlvLzF0bzYvMUdCL3IzY29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfcmNvbnRfZV90czoKICAgZnBfcmNvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJSRUFERVI6IFJlYWQgJWQgeCBjaGFycyIgJSBsZW4oZGF0YSkpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:35 +0000
      Finished:     Mon, 27 Jun 2022 04:48:35 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://3b3e97fa338d5eb58f14e4189f255e75d802b9b1a6603da62e4177363a280218
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:36 +0000
      Finished:     Mon, 27 Jun 2022 04:48:36 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://996990cce2589d908a64947f6fa47b4718e40bf0c2ecbb8448fec4a1fd4711f2
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-c9prt
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-27T04:48:40.386Z","type":3}]
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:37 +0000
      Finished:     Mon, 27 Jun 2022 04:48:47 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-trc6t (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://ed99701a712437a92240eaf805c706be0557a7dd66ce2d5461317244804aee6c
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:54 +0000
      Finished:     Mon, 27 Jun 2022 04:48:54 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 04:48:37 +0000
      Finished:     Mon, 27 Jun 2022 04:48:54 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-trc6t:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task4-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node4
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  39s                default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-zpngj-read3-task-kwvp-nnwcb to k8s-worker-node4
  Normal   Pulled     37s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    37s                kubelet            Created container place-tools
  Normal   Started    37s                kubelet            Started container place-tools
  Normal   Pulled     36s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    36s                kubelet            Created container place-scripts
  Normal   Started    36s                kubelet            Started container place-scripts
  Normal   Pulled     35s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    35s                kubelet            Created container istio-init
  Normal   Started    35s                kubelet            Started container istio-init
  Normal   Pulled     34s                kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Created    34s                kubelet            Created container step-read
  Normal   Started    34s                kubelet            Started container step-read
  Normal   Pulled     34s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    22s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  18s (x3 over 22s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    17s (x2 over 34s)  kubelet            Created container istio-proxy
  Normal   Started    17s (x2 over 34s)  kubelet            Started container istio-proxy
  Normal   Pulled     17s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
