Name:         write-read-array-minio-pipeline-run-vpsth
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-26T19:30:50Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-26T19:30:50Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-vpsth-read1-task-hr6pj:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-read2-task-568qz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-read3-task-g2rtw:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-read4-task-qtdjg:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-read5-task-t222l:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-read6-task-qw56f:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-vpsth-write1-task-d4h95:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-26T19:34:30Z
  Resource Version:  250586278
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-vpsth
  UID:               42f61512-57c3-4974-9015-8a1d8718fac3
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read5-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read6-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-06-26T19:34:30Z
  Conditions:
    Last Transition Time:  2022-06-26T19:34:30Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       read4-ws
        Workspace:  task-read4-ws
      Name:         read5-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read5
      Workspaces:
        Name:       read5-ws
        Workspace:  task-read5-ws
      Name:         read6-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read6
      Workspaces:
        Name:       read6-ws
        Workspace:  task-read6-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
      Name:    task-read5-ws
      Name:    task-read6-ws
  Start Time:  2022-06-26T19:30:50Z
  Task Runs:
    write-read-array-minio-pipeline-run-vpsth-read1-task-hr6pj:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-26T19:34:29Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read1-task-hr6p-7dwsp
        Start Time:              2022-06-26T19:34:17Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://73a346c8ae29d7e300c28b0665691589cee63db3a757e5fc003eebcd8af8d910
            Exit Code:     0
            Finished At:   2022-06-26T19:34:28Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:27Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-vpsth-read2-task-568qz:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-26T19:34:29Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read2-task-568q-x96tm
        Start Time:              2022-06-26T19:34:17Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://c2d153bace7dbee8bd0ec7bd3e528c82af82c3df1f5f454f03271b6c0d8b8ac2
            Exit Code:     0
            Finished At:   2022-06-26T19:34:28Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:27Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-minio-pipeline-run-vpsth-read3-task-g2rtw:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-26T19:34:25Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:25Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read3-task-g2rt-dqbbx
        Start Time:              2022-06-26T19:34:17Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://897f7aad25c8f0b876bf45a81bc001022ef2e656202cbd1086d2c0eb8c109048
            Exit Code:     0
            Finished At:   2022-06-26T19:34:25Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:25Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-minio-pipeline-run-vpsth-read4-task-qtdjg:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-26T19:34:27Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:27Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read4-task-qtdj-r4wrz
        Start Time:              2022-06-26T19:34:18Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://1d1c39a5901c2bcc0855c00380615f7ccc1407fa920ee82901a83a70de7730f1
            Exit Code:     0
            Finished At:   2022-06-26T19:34:26Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:26Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-minio-pipeline-run-vpsth-read5-task-t222l:
      Pipeline Task Name:  read5-task
      Status:
        Completion Time:  2022-06-26T19:34:29Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read5-task-t222-49v49
        Start Time:              2022-06-26T19:34:18Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://29ecab062dd59a04a92fccfb1479233a666a5b53ef0fb3ffe44edf3c92a8cf3e
            Exit Code:     0
            Finished At:   2022-06-26T19:34:29Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:28Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r5cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r5cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read5-ws
    write-read-array-minio-pipeline-run-vpsth-read6-task-qw56f:
      Pipeline Task Name:  read6-task
      Status:
        Completion Time:  2022-06-26T19:34:30Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:30Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-read6-task-qw56-flcsf
        Start Time:              2022-06-26T19:34:18Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://e6dd10c7ff2a03813edaa4e494c2f1bad6370ac69c866c211f40615726b82cc1
            Exit Code:     0
            Finished At:   2022-06-26T19:34:29Z
            Reason:        Completed
            Started At:    2022-06-26T19:34:28Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r6cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/r6cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read6-ws
    write-read-array-minio-pipeline-run-vpsth-write1-task-d4h95:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-26T19:34:17Z
        Conditions:
          Last Transition Time:  2022-06-26T19:34:17Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-vpsth-write1-task-d4h-wz7fc
        Start Time:              2022-06-26T19:30:50Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://5bda31548e49c4f50b1c227750704df3984521b8fecb4582508125c38eafa0b0
            Exit Code:     0
            Finished At:   2022-06-26T19:34:12Z
            Reason:        Completed
            Started At:    2022-06-26T19:33:55Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1048576
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/1to6/1MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to6/1MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                    From         Message
  ----    ------     ----                   ----         -------
  Normal  Started    3m47s (x2 over 3m47s)  PipelineRun  
  Normal  Running    3m47s (x2 over 3m47s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    20s (x2 over 20s)      PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    12s (x2 over 12s)      PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    10s                    PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    8s                     PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    8s                     PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    8s                     PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s                     PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
