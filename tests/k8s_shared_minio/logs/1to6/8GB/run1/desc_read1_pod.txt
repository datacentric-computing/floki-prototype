Name:         write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w
Namespace:    default
Priority:     0
Node:         k8s-worker-node2/10.0.26.207
Start Time:   Mon, 27 Jun 2022 13:13:12 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-lqkrs
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cdk
Annotations:  cni.projectcalico.org/containerID: 02794452a040491ea1bdbc68514a04a611d89c4de4531bfaf845d6375e073fee
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads an object from minio
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.219.119
IPs:
  IP:           192.168.219.119
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cdk
Init Containers:
  place-tools:
    Container ID:  docker://45659a5779549f9c00a7bb324374233784ce2ed33ae86d844e162ba4c63fd435
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:13:14 +0000
      Finished:     Mon, 27 Jun 2022 13:13:14 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://2e821e55eea1a7aaf21279cb12643dc9443cf4682c82ac5045f0f1309845d4da
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-lsqhd"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IHN1YnByb2Nlc3MKaW1wb3J0IHN5cwppbXBvcnQgYm90bzMKaW1wb3J0IGlvCmZyb20gYm90b2NvcmUuY29uZmlnIGltcG9ydCBDb25maWcKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby8xdG82LzhHQi9yMWNvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfcmNvbnRfc190czoKICAgZnBfcmNvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKc2Vzc2lvbiA9IGJvdG8zLnNlc3Npb24uU2Vzc2lvbigpCmNvbmZpZyA9IENvbmZpZyhyZXRyaWVzID0gZGljdChtYXhfYXR0ZW1wdHMgPSA1MCkpCnMzX2NsaWVudCA9IHNlc3Npb24uY2xpZW50KAogICBzZXJ2aWNlX25hbWU9J3MzJywKICAgYXdzX2FjY2Vzc19rZXlfaWQ9J2FkbWluJywKICAgYXdzX3NlY3JldF9hY2Nlc3Nfa2V5PSdzZWNyZXRwd2QnLAogICBlbmRwb2ludF91cmw9J2h0dHA6Ly8xMC4wLjI2LjMzOjkwMDAnLAogICBjb25maWc9Y29uZmlnCikKIyBSZWFkIGRhdGEgb2JqZWN0IGZyb20gcmVtb3RlIHNoYXJlZCBzdG9yYWdlCmRhdGFfYmluMSA9IGlvLkJ5dGVzSU8oKQpzM19jbGllbnQuZG93bmxvYWRfZmlsZW9iaihCdWNrZXQ9J2RhdGEnLCBLZXk9J291dDEuYmluJywgRmlsZW9iaj1kYXRhX2JpbjEpCiMgU3RvcmUgZmluYWwgdGltZXN0YW1wCndpdGggb3BlbigiL2xvY2FsL3RpbWVzdGFtcHMvc2hhcmVkX21pbmlvLzF0bzYvOEdCL3IxY29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfcmNvbnRfZV90czoKICAgZnBfcmNvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKI3ByaW50KCJSRUFERVI6IFJlYWQgJWQgeCBjaGFycyIgJSBsZW4oZGF0YSkpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:13:16 +0000
      Finished:     Mon, 27 Jun 2022 13:13:16 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://3b697e34b58c0051919e07a40807e2f923869b0809ec1b1b854e0bbfda778321
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:13:17 +0000
      Finished:     Mon, 27 Jun 2022 13:13:17 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://6b6ab6646d2d73d9deadaf7305a484ff3350c32f245e7c20bd52b291e0868ae7
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-lsqhd
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-27T13:13:21.286Z","type":3}]
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:13:18 +0000
      Finished:     Mon, 27 Jun 2022 13:14:00 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-h9clw (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://c11cae31b66aa825f19d8ba3fb1c534b3d9bcc29e912cd76086be70444cfc281
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:14:07 +0000
      Finished:     Mon, 27 Jun 2022 13:14:07 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 27 Jun 2022 13:13:18 +0000
      Finished:     Mon, 27 Jun 2022 13:14:06 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-h9clw:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task2-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node2
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  82s                default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-lqkrs-read1-task-t5cd-gs68w to k8s-worker-node2
  Normal   Pulled     80s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    80s                kubelet            Created container place-tools
  Normal   Started    80s                kubelet            Started container place-tools
  Normal   Pulled     79s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    78s                kubelet            Created container place-scripts
  Normal   Started    78s                kubelet            Started container place-scripts
  Normal   Pulled     78s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    77s                kubelet            Created container istio-init
  Normal   Started    77s                kubelet            Started container istio-init
  Normal   Pulled     77s                kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Created    76s                kubelet            Created container step-read
  Normal   Started    76s                kubelet            Started container step-read
  Normal   Pulled     76s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    33s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  30s (x2 over 32s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Warning  Unhealthy  28s                kubelet            Readiness probe failed: Get "http://192.168.219.119:15021/healthz/ready": dial tcp 192.168.219.119:15021: connect: connection refused
  Normal   Pulled     28s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Created    27s (x2 over 76s)  kubelet            Created container istio-proxy
  Normal   Started    27s (x2 over 76s)  kubelet            Started container istio-proxy
