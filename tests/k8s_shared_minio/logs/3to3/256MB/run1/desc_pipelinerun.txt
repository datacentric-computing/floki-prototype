Name:         write-read-array-minio-pipeline-run-ddb4d
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-30T15:03:12Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-30T15:03:12Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-ddb4d-read1-task-pzgg2:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-ddb4d-read2-task-52vsh:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-ddb4d-read3-task-hk64f:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-ddb4d-write1-task-5dmcz:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-ddb4d-write2-task-tcrvl:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-ddb4d-write3-task-rcdhx:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-30T15:04:29Z
  Resource Version:  259184034
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-ddb4d
  UID:               53ceeefb-03e9-44d8-8d37-6ab2f0902194
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
Status:
  Completion Time:  2022-06-30T15:04:27Z
  Conditions:
    Last Transition Time:  2022-06-30T15:04:27Z
    Message:               Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
        write2-task
        write3-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
  Start Time:  2022-06-30T15:03:12Z
  Task Runs:
    write-read-array-minio-pipeline-run-ddb4d-read1-task-pzgg2:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-30T15:04:26Z
        Conditions:
          Last Transition Time:  2022-06-30T15:04:26Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-read1-task-pzgg-ljmvq
        Start Time:              2022-06-30T15:04:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://4cd890fd4d8871463976c8817a9354ea7be5e53677731ffc2b03d86ffbdbfa25
            Exit Code:     0
            Finished At:   2022-06-30T15:04:26Z
            Reason:        Completed
            Started At:    2022-06-30T15:04:21Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
data_bin3 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out3.bin', Fileobj=data_bin3)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-ddb4d-read2-task-52vsh:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-30T15:04:27Z
        Conditions:
          Last Transition Time:  2022-06-30T15:04:27Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-read2-task-52vs-xzk4k
        Start Time:              2022-06-30T15:04:13Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://6797e44a8e19d34277156bda5fb6e4f36637eeb889f70b69db2f1ccb71a4e4b2
            Exit Code:     0
            Finished At:   2022-06-30T15:04:27Z
            Reason:        Completed
            Started At:    2022-06-30T15:04:22Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
data_bin3 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out3.bin', Fileobj=data_bin3)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-minio-pipeline-run-ddb4d-read3-task-hk64f:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-30T15:04:27Z
        Conditions:
          Last Transition Time:  2022-06-30T15:04:27Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-read3-task-hk64-b9hgd
        Start Time:              2022-06-30T15:04:14Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://611c93f11ee07c532dd45412f88794091a55e504d381999143c375987b7a955d
            Exit Code:     0
            Finished At:   2022-06-30T15:04:28Z
            Reason:        Completed
            Started At:    2022-06-30T15:04:23Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
data_bin3 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out3.bin', Fileobj=data_bin3)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-minio-pipeline-run-ddb4d-write1-task-5dmcz:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-30T15:03:29Z
        Conditions:
          Last Transition Time:  2022-06-30T15:03:29Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-write1-task-5dm-lqzr6
        Start Time:              2022-06-30T15:03:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://edf85c6586756bcfa31274b503e597cef4a9fb95c81d0be50d80aa547dde27ef
            Exit Code:     0
            Finished At:   2022-06-30T15:03:28Z
            Reason:        Completed
            Started At:    2022-06-30T15:03:23Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 268435456
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/3to3/256MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
    write-read-array-minio-pipeline-run-ddb4d-write2-task-tcrvl:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-06-30T15:03:27Z
        Conditions:
          Last Transition Time:  2022-06-30T15:03:27Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-write2-task-tcr-tl4pg
        Start Time:              2022-06-30T15:03:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://428398b51c3e24403656073377aa4e800edfd85e7df66c1b454022c3ca7693af
            Exit Code:     0
            Finished At:   2022-06-30T15:03:26Z
            Reason:        Completed
            Started At:    2022-06-30T15:03:21Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 268435456
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/3to3/256MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out2.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out2.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out2.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write2-ws
    write-read-array-minio-pipeline-run-ddb4d-write3-task-rcdhx:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-06-30T15:04:13Z
        Conditions:
          Last Transition Time:  2022-06-30T15:04:13Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-ddb4d-write3-task-rcd-xqnj6
        Start Time:              2022-06-30T15:03:12Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://6e139d9102644e4ab9073f878426b55b363bf69dd0e651484531419e3e03f612
            Exit Code:     0
            Finished At:   2022-06-30T15:04:11Z
            Reason:        Completed
            Started At:    2022-06-30T15:03:42Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 268435456
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/3to3/256MB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out3.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out3.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out3.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/3to3/256MB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write3-ws
Events:
  Type    Reason     Age                From         Message
  ----    ------     ----               ----         -------
  Normal  Started    85s (x2 over 85s)  PipelineRun  
  Normal  Running    85s (x2 over 85s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    69s                PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    67s                PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    23s (x2 over 23s)  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    10s                PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    9s                 PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                 PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Skipped: 0
