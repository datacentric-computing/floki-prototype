Name:         write-read-array-minio-pipeline-run-l9x8r
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-29T19:18:14Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-29T19:18:14Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-l9x8r-read1-task-lk696:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write1-task-df2l7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write2-task-b2287:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write3-task-d878g:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write4-task-b24mh:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write5-task-rfdjb:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-l9x8r-write6-task-r66dq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-29T19:20:19Z
  Resource Version:  257279779
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-l9x8r
  UID:               048e0978-55e9-414f-a01d-fdf13e3f42a1
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          write3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          write4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          write5-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
    Pipeline Task Name:          write6-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node6
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node7
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-write3-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-write4-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-write5-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
    Name:          task-write6-ws
    Persistent Volume Claim:
      Claim Name:  task6-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task7-pv-claim
Status:
  Completion Time:  2022-06-29T19:20:18Z
  Conditions:
    Last Transition Time:  2022-06-29T19:20:18Z
    Message:               Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         write3-task
      Task Ref:
        Kind:  Task
        Name:  write3
      Workspaces:
        Name:       write3-ws
        Workspace:  task-write3-ws
      Name:         write4-task
      Task Ref:
        Kind:  Task
        Name:  write4
      Workspaces:
        Name:       write4-ws
        Workspace:  task-write4-ws
      Name:         write5-task
      Task Ref:
        Kind:  Task
        Name:  write5
      Workspaces:
        Name:       write5-ws
        Workspace:  task-write5-ws
      Name:         write6-task
      Task Ref:
        Kind:  Task
        Name:  write6
      Workspaces:
        Name:       write6-ws
        Workspace:  task-write6-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
        write3-task
        write4-task
        write5-task
        write6-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-write3-ws
      Name:    task-write4-ws
      Name:    task-write5-ws
      Name:    task-write6-ws
      Name:    task-read1-ws
  Start Time:  2022-06-29T19:18:14Z
  Task Runs:
    write-read-array-minio-pipeline-run-l9x8r-read1-task-lk696:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-29T19:20:18Z
        Conditions:
          Last Transition Time:  2022-06-29T19:20:18Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-read1-task-lk69-67m8z
        Start Time:              2022-06-29T19:19:31Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://528e2897ec6f57788edc25f5c531d6783dd540ea64003a72d45fa10d2d5d9def
            Exit Code:     0
            Finished At:   2022-06-29T19:20:18Z
            Reason:        Completed
            Started At:    2022-06-29T19:19:39Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
data_bin3 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out3.bin', Fileobj=data_bin3)
data_bin4 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out4.bin', Fileobj=data_bin4)
data_bin5 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out5.bin', Fileobj=data_bin5)
data_bin6 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out6.bin', Fileobj=data_bin6)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-l9x8r-write1-task-df2l7:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-29T19:18:40Z
        Conditions:
          Last Transition Time:  2022-06-29T19:18:40Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write1-task-df2-944dn
        Start Time:              2022-06-29T19:18:14Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://2a49bfcbbc7792d9390cbca8b057e4a9d9acecb8e68ba251a2fc7c2074ac935a
            Exit Code:     0
            Finished At:   2022-06-29T19:18:39Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:24Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
    write-read-array-minio-pipeline-run-l9x8r-write2-task-b2287:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-06-29T19:18:43Z
        Conditions:
          Last Transition Time:  2022-06-29T19:18:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write2-task-b22-wt65g
        Start Time:              2022-06-29T19:18:14Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://a37417701cb8051de7b17501454ff87a1be9ab13b4e6cdf8438df96dea4bfe66
            Exit Code:     0
            Finished At:   2022-06-29T19:18:42Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:27Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out2.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out2.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out2.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write2-ws
    write-read-array-minio-pipeline-run-l9x8r-write3-task-d878g:
      Pipeline Task Name:  write3-task
      Status:
        Completion Time:  2022-06-29T19:19:30Z
        Conditions:
          Last Transition Time:  2022-06-29T19:19:30Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write3-task-d87-pwkjd
        Start Time:              2022-06-29T19:18:14Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://f9256ad9c56d567c1a66f3816516879560fcb427519cc6b3c53c234e2a08f323
            Exit Code:     0
            Finished At:   2022-06-29T19:19:29Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:46Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w3cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w3cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out3.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out3.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out3.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w3cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write3-ws
    write-read-array-minio-pipeline-run-l9x8r-write4-task-b24mh:
      Pipeline Task Name:  write4-task
      Status:
        Completion Time:  2022-06-29T19:18:35Z
        Conditions:
          Last Transition Time:  2022-06-29T19:18:35Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write4-task-b24-t6vxr
        Start Time:              2022-06-29T19:18:14Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://67d9ad49485d69918607b5e06ca914b617bcfa8485d2af3ebaed7949bf15cdfa
            Exit Code:     0
            Finished At:   2022-06-29T19:18:34Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:21Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w4cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w4cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out4.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out4.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out4.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w4cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write4-ws
    write-read-array-minio-pipeline-run-l9x8r-write5-task-rfdjb:
      Pipeline Task Name:  write5-task
      Status:
        Completion Time:  2022-06-29T19:18:38Z
        Conditions:
          Last Transition Time:  2022-06-29T19:18:38Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write5-task-rfd-stjlj
        Start Time:              2022-06-29T19:18:15Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://c65068c921ef570c43396d0310e17e966da817d5040a7f6242ce1693ea247716
            Exit Code:     0
            Finished At:   2022-06-29T19:18:38Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:23Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w5cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w5cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out5.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out5.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out5.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w5cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write5-ws
    write-read-array-minio-pipeline-run-l9x8r-write6-task-r66dq:
      Pipeline Task Name:  write6-task
      Status:
        Completion Time:  2022-06-29T19:18:39Z
        Conditions:
          Last Transition Time:  2022-06-29T19:18:39Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-l9x8r-write6-task-r66-xv7tp
        Start Time:              2022-06-29T19:18:15Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://ef6b2de6808751a292040a91a429f2a6545dca0e796eb5f18abab55056691bfb
            Exit Code:     0
            Finished At:   2022-06-29T19:18:38Z
            Reason:        Completed
            Started At:    2022-06-29T19:18:23Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w6cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 1073741824
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/6to1/1GB/w6cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out6.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out6.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out6.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/6to1/1GB/w6cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write6-ws
Events:
  Type    Reason     Age                    From         Message
  ----    ------     ----                   ----         -------
  Normal  Started    2m11s (x2 over 2m11s)  PipelineRun  
  Normal  Running    2m10s (x2 over 2m11s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 7, Skipped: 0
  Normal  Running    110s                   PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 6, Skipped: 0
  Normal  Running    107s (x2 over 107s)    PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    106s                   PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    105s                   PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    102s                   PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    55s (x2 over 55s)      PipelineRun  Tasks Completed: 6 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s                     PipelineRun  Tasks Completed: 7 (Failed: 0, Cancelled 0), Skipped: 0
