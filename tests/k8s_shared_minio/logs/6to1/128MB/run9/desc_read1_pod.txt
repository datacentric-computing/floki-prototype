Name:         write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x
Namespace:    default
Priority:     0
Node:         k8s-worker-node7/10.0.26.216
Start Time:   Wed, 29 Jun 2022 18:10:23 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-fb29q
              tekton.dev/pipelineTask=read1-task
              tekton.dev/task=read1
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-fb29q-read1-task-2grdg
Annotations:  cni.projectcalico.org/containerID: d099141242916caf541566dab904a0dcd958fb218ecc7479b24e27ab4189fbbb
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that reads an object from minio
              kubectl.kubernetes.io/default-container: step-read
              kubectl.kubernetes.io/default-logs-container: step-read
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.30.21
IPs:
  IP:           192.168.30.21
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-fb29q-read1-task-2grdg
Init Containers:
  place-tools:
    Container ID:  docker://faf0be5da121fce9526748dbafdf64fe435a59cc2692a4fb0da41bd2cafbc07a
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 18:10:25 +0000
      Finished:     Wed, 29 Jun 2022 18:10:25 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://6477db1ed707e7e4c5e78c56f1342d1a9be62665197fa938bbc25e896c250ead
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-blg9c"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MKaW1wb3J0IHN1YnByb2Nlc3MKaW1wb3J0IHN5cwppbXBvcnQgYm90bzMKaW1wb3J0IGlvCmZyb20gYm90b2NvcmUuY29uZmlnIGltcG9ydCBDb25maWcKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby82dG8xLzEyOE1CL3IxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF9yY29udF9zX3RzOgogICBmcF9yY29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQpzZXNzaW9uID0gYm90bzMuc2Vzc2lvbi5TZXNzaW9uKCkKY29uZmlnID0gQ29uZmlnKHJldHJpZXMgPSBkaWN0KG1heF9hdHRlbXB0cyA9IDUwKSkKczNfY2xpZW50ID0gc2Vzc2lvbi5jbGllbnQoCiAgIHNlcnZpY2VfbmFtZT0nczMnLAogICBhd3NfYWNjZXNzX2tleV9pZD0nYWRtaW4nLAogICBhd3Nfc2VjcmV0X2FjY2Vzc19rZXk9J3NlY3JldHB3ZCcsCiAgIGVuZHBvaW50X3VybD0naHR0cDovLzEwLjAuMjYuMzM6OTAwMCcsCiAgIGNvbmZpZz1jb25maWcKKQojIFJlYWQgZGF0YSBvYmplY3QgZnJvbSByZW1vdGUgc2hhcmVkIHN0b3JhZ2UKZGF0YV9iaW4xID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0MS5iaW4nLCBGaWxlb2JqPWRhdGFfYmluMSkKZGF0YV9iaW4yID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0Mi5iaW4nLCBGaWxlb2JqPWRhdGFfYmluMikKZGF0YV9iaW4zID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0My5iaW4nLCBGaWxlb2JqPWRhdGFfYmluMykKZGF0YV9iaW40ID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0NC5iaW4nLCBGaWxlb2JqPWRhdGFfYmluNCkKZGF0YV9iaW41ID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0NS5iaW4nLCBGaWxlb2JqPWRhdGFfYmluNSkKZGF0YV9iaW42ID0gaW8uQnl0ZXNJTygpCnMzX2NsaWVudC5kb3dubG9hZF9maWxlb2JqKEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0Ni5iaW4nLCBGaWxlb2JqPWRhdGFfYmluNikKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfbWluaW8vNnRvMS8xMjhNQi9yMWNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3Jjb250X2VfdHM6CiAgIGZwX3Jjb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiNwcmludCgiUkVBREVSOiBSZWFkICVkIHggY2hhcnMiICUgbGVuKGRhdGEpKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 18:10:26 +0000
      Finished:     Wed, 29 Jun 2022 18:10:26 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://464a75c71823e337db9cea167c46a266df664636fd7e1fe036e8566f2ed4ef88
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 18:10:27 +0000
      Finished:     Wed, 29 Jun 2022 18:10:27 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-read:
    Container ID:  docker://a9bf357748de0ba9cc5182814da43b44642069afb15c7c852826cbfd26d24c36
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-read
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-blg9c
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-29T18:10:30.379Z","type":3}]
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 18:10:28 +0000
      Finished:     Wed, 29 Jun 2022 18:10:35 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-pbrsl (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://58b4079b158e0e4ca740cbed99c0c9418caa41dab79b42da1ccdcbdd8c532b18
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 18:10:42 +0000
      Finished:     Wed, 29 Jun 2022 18:10:42 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-read
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-pbrsl:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task7-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node7
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age               From               Message
  ----     ------     ----              ----               -------
  Normal   Scheduled  25s               default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-fb29q-read1-task-2grd-zql9x to k8s-worker-node7
  Normal   Pulled     23s               kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    23s               kubelet            Created container place-tools
  Normal   Started    23s               kubelet            Started container place-tools
  Normal   Pulled     23s               kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    22s               kubelet            Created container place-scripts
  Normal   Started    22s               kubelet            Started container place-scripts
  Normal   Pulled     21s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    21s               kubelet            Created container istio-init
  Normal   Started    21s               kubelet            Started container istio-init
  Normal   Pulled     20s               kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Created    20s               kubelet            Created container step-read
  Normal   Started    20s               kubelet            Started container step-read
  Normal   Pulled     20s               kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Killing    11s               kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  7s (x3 over 11s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    6s (x2 over 20s)  kubelet            Created container istio-proxy
  Normal   Started    6s (x2 over 20s)  kubelet            Started container istio-proxy
  Normal   Pulled     6s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
