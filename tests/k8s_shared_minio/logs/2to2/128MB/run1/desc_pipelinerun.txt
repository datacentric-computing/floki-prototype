Name:         write-read-array-minio-pipeline-run-r257d
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-30T05:11:38Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-30T05:11:38Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-r257d-read1-task-hkqcw:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-r257d-read2-task-lvjlq:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-r257d-write1-task-cl68v:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-r257d-write2-task-crtmr:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-30T05:12:41Z
  Resource Version:  258225131
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-r257d
  UID:               3d6af5f7-fd2a-4e36-a197-c13500212427
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
Status:
  Completion Time:  2022-06-30T05:12:40Z
  Conditions:
    Last Transition Time:  2022-06-30T05:12:40Z
    Message:               Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-06-30T05:11:38Z
  Task Runs:
    write-read-array-minio-pipeline-run-r257d-read1-task-hkqcw:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-30T05:12:40Z
        Conditions:
          Last Transition Time:  2022-06-30T05:12:40Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-r257d-read1-task-hkqc-txbx5
        Start Time:              2022-06-30T05:11:53Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://cf9898feb9bde3fb3de1e110ebd3d147808d36ae536c1d0bdd8ce09614442dc3
            Exit Code:     0
            Finished At:   2022-06-30T05:12:38Z
            Reason:        Completed
            Started At:    2022-06-30T05:12:24Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-r257d-read2-task-lvjlq:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-30T05:12:03Z
        Conditions:
          Last Transition Time:  2022-06-30T05:12:03Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-r257d-read2-task-lvjl-6xqx2
        Start Time:              2022-06-30T05:11:53Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://308e8a7e8fcc553dca42cec3982ca1d7b3c1f3ac30deef74e472907708a2f251
            Exit Code:     0
            Finished At:   2022-06-30T05:12:02Z
            Reason:        Completed
            Started At:    2022-06-30T05:12:01Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-minio-pipeline-run-r257d-write1-task-cl68v:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-30T05:11:52Z
        Conditions:
          Last Transition Time:  2022-06-30T05:11:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-r257d-write1-task-cl6-wqxvd
        Start Time:              2022-06-30T05:11:38Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://50d3e3a3a12f003066832a093697087be9c8635cc966c8b775bcd22208971df8
            Exit Code:     0
            Finished At:   2022-06-30T05:11:52Z
            Reason:        Completed
            Started At:    2022-06-30T05:11:49Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 134217728
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/2to2/128MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
    write-read-array-minio-pipeline-run-r257d-write2-task-crtmr:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-06-30T05:11:52Z
        Conditions:
          Last Transition Time:  2022-06-30T05:11:52Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-r257d-write2-task-crt-gfbw6
        Start Time:              2022-06-30T05:11:38Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://acd596344b058a36975cee9dba18bab682ce49a8358c543e2ee9e07a289681bc
            Exit Code:     0
            Finished At:   2022-06-30T05:11:52Z
            Reason:        Completed
            Started At:    2022-06-30T05:11:49Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 134217728
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/2to2/128MB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out2.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out2.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out2.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/128MB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write2-ws
Events:
  Type    Reason     Age   From         Message
  ----    ------     ----  ----         -------
  Normal  Started    70s   PipelineRun  
  Normal  Running    70s   PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    56s   PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    55s   PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    45s   PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  8s    PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
