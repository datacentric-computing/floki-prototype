Name:         write-read-array-minio-pipeline-run-wzfl5
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-30T07:37:55Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-30T07:37:55Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-wzfl5-read1-task-b2454:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-wzfl5-read2-task-qmxp7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-wzfl5-write1-task-qpx99:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-wzfl5-write2-task-r7wdp:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-30T07:42:00Z
  Resource Version:  258467178
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-wzfl5
  UID:               7d309c71-df2b-4b17-9586-39da4ee4cf43
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          write2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-write2-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
Status:
  Completion Time:  2022-06-30T07:41:59Z
  Conditions:
    Last Transition Time:  2022-06-30T07:41:59Z
    Message:               Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         write2-task
      Task Ref:
        Kind:  Task
        Name:  write2
      Workspaces:
        Name:       write2-ws
        Workspace:  task-write2-ws
      Name:         read1-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
        write2-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-write2-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
  Start Time:  2022-06-30T07:37:55Z
  Task Runs:
    write-read-array-minio-pipeline-run-wzfl5-read1-task-b2454:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-30T07:41:59Z
        Conditions:
          Last Transition Time:  2022-06-30T07:41:59Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-wzfl5-read1-task-b245-s65h5
        Start Time:              2022-06-30T07:39:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://267441deab06199339b2bfe3c7994f6929b06d81e0eb527161c045c24a5ae371
            Exit Code:     0
            Finished At:   2022-06-30T07:41:56Z
            Reason:        Completed
            Started At:    2022-06-30T07:39:43Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-wzfl5-read2-task-qmxp7:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-30T07:40:16Z
        Conditions:
          Last Transition Time:  2022-06-30T07:40:16Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-wzfl5-read2-task-qmxp-2mfh6
        Start Time:              2022-06-30T07:39:12Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://a7bacec627b3984ac85e76153357c83556f48983a72514e5635bab6eafd20789
            Exit Code:     0
            Finished At:   2022-06-30T07:40:17Z
            Reason:        Completed
            Started At:    2022-06-30T07:39:20Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
data_bin2 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out2.bin', Fileobj=data_bin2)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-minio-pipeline-run-wzfl5-write1-task-qpx99:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-30T07:39:03Z
        Conditions:
          Last Transition Time:  2022-06-30T07:39:03Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-wzfl5-write1-task-qpx-t7j44
        Start Time:              2022-06-30T07:37:55Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://f5b1247cf0f6a4ba76edb31613be98036a27846c956ef50743c0dbbd37c70a6c
            Exit Code:     0
            Finished At:   2022-06-30T07:39:03Z
            Reason:        Completed
            Started At:    2022-06-30T07:38:02Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 4294967296
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/2to2/4GB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
    write-read-array-minio-pipeline-run-wzfl5-write2-task-r7wdp:
      Pipeline Task Name:  write2-task
      Status:
        Completion Time:  2022-06-30T07:39:11Z
        Conditions:
          Last Transition Time:  2022-06-30T07:39:11Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-wzfl5-write2-task-r7w-nl682
        Start Time:              2022-06-30T07:37:55Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://e281db0fca457da25aff3a12d89b168af073c25a897e22d4aa61b9278ae0938e
            Exit Code:     0
            Finished At:   2022-06-30T07:39:10Z
            Reason:        Completed
            Started At:    2022-06-30T07:38:04Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/w2cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 4294967296
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/2to2/4GB/w2cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out2.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out2.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out2.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/2to2/4GB/w2cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write2-ws
Events:
  Type    Reason     Age                    From         Message
  ----    ------     ----                   ----         -------
  Normal  Started    4m13s (x2 over 4m13s)  PipelineRun  
  Normal  Running    4m13s (x2 over 4m13s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    3m5s                   PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    2m57s (x2 over 2m57s)  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    112s                   PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  9s                     PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Skipped: 0
