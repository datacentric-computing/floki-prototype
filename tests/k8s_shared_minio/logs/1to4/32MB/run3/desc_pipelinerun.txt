Name:         write-read-array-minio-pipeline-run-m6sbc
Namespace:    default
Labels:       tekton.dev/pipeline=write-read-array-minio-pipeline
Annotations:  <none>
API Version:  tekton.dev/v1beta1
Kind:         PipelineRun
Metadata:
  Creation Timestamp:  2022-06-26T09:18:57Z
  Generate Name:       write-read-array-minio-pipeline-run-
  Generation:          1
  Managed Fields:
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:generateName:
      f:spec:
        .:
        f:pipelineRef:
          .:
          f:name:
        f:taskRunSpecs:
        f:workspaces:
    Manager:      kubectl-create
    Operation:    Update
    Time:         2022-06-26T09:18:57Z
    API Version:  tekton.dev/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:tekton.dev/pipeline:
      f:status:
        .:
        f:completionTime:
        f:conditions:
        f:pipelineSpec:
          .:
          f:tasks:
          f:workspaces:
        f:startTime:
        f:taskRuns:
          .:
          f:write-read-array-minio-pipeline-run-m6sbc-read1-task-q9r6h:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-m6sbc-read2-task-bhrq7:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-m6sbc-read3-task-b6c6q:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-m6sbc-read4-task-k8g7d:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
          f:write-read-array-minio-pipeline-run-m6sbc-write1-task-f6q4t:
            .:
            f:pipelineTaskName:
            f:status:
              .:
              f:completionTime:
              f:conditions:
              f:podName:
              f:startTime:
              f:steps:
              f:taskSpec:
                .:
                f:steps:
                f:workspaces:
    Manager:         controller
    Operation:       Update
    Time:            2022-06-26T09:20:47Z
  Resource Version:  249647813
  Self Link:         /apis/tekton.dev/v1beta1/namespaces/default/pipelineruns/write-read-array-minio-pipeline-run-m6sbc
  UID:               b44343e5-50cc-40c8-8e8d-09827b86cd8d
Spec:
  Pipeline Ref:
    Name:                write-read-array-minio-pipeline
  Service Account Name:  default
  Task Run Specs:
    Pipeline Task Name:  write1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node1
    Pipeline Task Name:          read1-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node2
    Pipeline Task Name:          read2-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node3
    Pipeline Task Name:          read3-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node4
    Pipeline Task Name:          read4-task
    Task Pod Template:
      Node Selector:
        kubernetes.io/hostname:  k8s-worker-node5
  Timeout:                       1h0m0s
  Workspaces:
    Name:  task-write1-ws
    Persistent Volume Claim:
      Claim Name:  task1-pv-claim
    Name:          task-read1-ws
    Persistent Volume Claim:
      Claim Name:  task2-pv-claim
    Name:          task-read2-ws
    Persistent Volume Claim:
      Claim Name:  task3-pv-claim
    Name:          task-read3-ws
    Persistent Volume Claim:
      Claim Name:  task4-pv-claim
    Name:          task-read4-ws
    Persistent Volume Claim:
      Claim Name:  task5-pv-claim
Status:
  Completion Time:  2022-06-26T09:20:47Z
  Conditions:
    Last Transition Time:  2022-06-26T09:20:47Z
    Message:               Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
    Reason:                Succeeded
    Status:                True
    Type:                  Succeeded
  Pipeline Spec:
    Tasks:
      Name:  write1-task
      Task Ref:
        Kind:  Task
        Name:  write1
      Workspaces:
        Name:       write1-ws
        Workspace:  task-write1-ws
      Name:         read1-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read1
      Workspaces:
        Name:       read1-ws
        Workspace:  task-read1-ws
      Name:         read2-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read2
      Workspaces:
        Name:       read2-ws
        Workspace:  task-read2-ws
      Name:         read3-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read3
      Workspaces:
        Name:       read3-ws
        Workspace:  task-read3-ws
      Name:         read4-task
      Run After:
        write1-task
      Task Ref:
        Kind:  Task
        Name:  read4
      Workspaces:
        Name:       read4-ws
        Workspace:  task-read4-ws
    Workspaces:
      Name:    task-write1-ws
      Name:    task-read1-ws
      Name:    task-read2-ws
      Name:    task-read3-ws
      Name:    task-read4-ws
  Start Time:  2022-06-26T09:18:57Z
  Task Runs:
    write-read-array-minio-pipeline-run-m6sbc-read1-task-q9r6h:
      Pipeline Task Name:  read1-task
      Status:
        Completion Time:  2022-06-26T09:20:47Z
        Conditions:
          Last Transition Time:  2022-06-26T09:20:47Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-m6sbc-read1-task-q9r6-qzb89
        Start Time:              2022-06-26T09:20:35Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://d3f6fd3ccd8713ad968c0fb0a6e3e5dfb6ad85b7130c74d4823db331d3a60057
            Exit Code:     0
            Finished At:   2022-06-26T09:20:46Z
            Reason:        Completed
            Started At:    2022-06-26T09:20:45Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r1cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r1cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read1-ws
    write-read-array-minio-pipeline-run-m6sbc-read2-task-bhrq7:
      Pipeline Task Name:  read2-task
      Status:
        Completion Time:  2022-06-26T09:20:46Z
        Conditions:
          Last Transition Time:  2022-06-26T09:20:46Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-m6sbc-read2-task-bhrq-74vt6
        Start Time:              2022-06-26T09:20:35Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://07d644cb4de67624c2ac47ab1a1bef1424c74df7375e45d2fa2f41ad60fa42a7
            Exit Code:     0
            Finished At:   2022-06-26T09:20:46Z
            Reason:        Completed
            Started At:    2022-06-26T09:20:45Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r2cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r2cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read2-ws
    write-read-array-minio-pipeline-run-m6sbc-read3-task-b6c6q:
      Pipeline Task Name:  read3-task
      Status:
        Completion Time:  2022-06-26T09:20:44Z
        Conditions:
          Last Transition Time:  2022-06-26T09:20:44Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-m6sbc-read3-task-b6c6-jcmts
        Start Time:              2022-06-26T09:20:35Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://7237a674d8f2c23b385fddf3fbe5b077eab4705f0e121b07563219bc184af404
            Exit Code:     0
            Finished At:   2022-06-26T09:20:43Z
            Reason:        Completed
            Started At:    2022-06-26T09:20:42Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r3cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r3cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read3-ws
    write-read-array-minio-pipeline-run-m6sbc-read4-task-k8g7d:
      Pipeline Task Name:  read4-task
      Status:
        Completion Time:  2022-06-26T09:20:43Z
        Conditions:
          Last Transition Time:  2022-06-26T09:20:43Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-m6sbc-read4-task-k8g7-wscwj
        Start Time:              2022-06-26T09:20:35Z
        Steps:
          Container:  step-read
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       read
          Terminated:
            Container ID:  docker://223cf66de1203b7e8664ce7b5e6519520aba36278266bfb6a627230f1c370cd2
            Exit Code:     0
            Finished At:   2022-06-26T09:20:43Z
            Reason:        Completed
            Started At:    2022-06-26T09:20:43Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               read
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os
import subprocess
import sys
import boto3
import io
from botocore.config import Config
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r4cont_start_ts.txt", 'a+') as fp_rcont_s_ts:
   fp_rcont_s_ts.write("%f\n" % time.time())
session = boto3.session.Session()
config = Config(retries = dict(max_attempts = 50))
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=config
)
# Read data object from remote shared storage
data_bin1 = io.BytesIO()
s3_client.download_fileobj(Bucket='data', Key='out1.bin', Fileobj=data_bin1)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/r4cont_end_ts.txt", 'a+') as fp_rcont_e_ts:
   fp_rcont_e_ts.write("%f\n" % time.time())
#print("READER: Read %d x chars" % len(data))

          Workspaces:
            Mount Path:  /local
            Name:        read4-ws
    write-read-array-minio-pipeline-run-m6sbc-write1-task-f6q4t:
      Pipeline Task Name:  write1-task
      Status:
        Completion Time:  2022-06-26T09:20:34Z
        Conditions:
          Last Transition Time:  2022-06-26T09:20:34Z
          Message:               All Steps have completed executing
          Reason:                Succeeded
          Status:                True
          Type:                  Succeeded
        Pod Name:                write-read-array-minio-pipeline-run-m6sbc-write1-task-f6q-zcw78
        Start Time:              2022-06-26T09:18:57Z
        Steps:
          Container:  step-write
          Image ID:   docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
          Name:       write
          Terminated:
            Container ID:  docker://6dbc98069feb0bc027a889dd59a9c0d5f05aad3413967aae1c77baf5c06436cc
            Exit Code:     0
            Finished At:   2022-06-26T09:20:32Z
            Reason:        Completed
            Started At:    2022-06-26T09:20:22Z
        Task Spec:
          Steps:
            Image:              amnestorov/minio
            Image Pull Policy:  IfNotPresent
            Name:               write
            Resources:
            Script:  #!/usr/bin/env python3
import time
import os 
import subprocess
import sys
import boto3
import botocore
import io
import boto3.s3.transfer as s3transfer
# Store initial timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/w1cont_start_ts.txt", 'a+') as fp_wcont_s_ts:
   fp_wcont_s_ts.write("%f\n" % time.time())
# Create Boto session and client
session = boto3.session.Session()
botocore_config = botocore.config.Config(
   read_timeout=3600,
   connect_timeout=3600,
   retries={"max_attempts": 0},
   max_pool_connections=20
)
s3_client = session.client(
   service_name='s3',
   aws_access_key_id='admin',
   aws_secret_access_key='secretpwd',
   endpoint_url='http://10.0.26.33:9000',
   config=botocore_config
)
transfer_config = s3transfer.TransferConfig(
   use_threads=True,
   max_concurrency=20
)
s3t = s3transfer.create_transfer_manager(s3_client, transfer_config)
# Create data object
bin_data = bytearray(b'\x01') * 33554432
# Store timestamp after data creation 
with open("/local/timestamps/shared_minio/1to4/32MB/w1cont_data_end_ts.txt", 'a+') as fp_wcont_data_ts:
   fp_wcont_data_ts.write("%f\n" % time.time())
# Write data object on minio storage
#s3_client.put_object(Bucket='data', Key='out1.bin', Body=bin_data)
s3t.upload(io.BytesIO(bin_data), 'data', 'out1.bin')
#s3_client.upload_fileobj(io.BytesIO(bin_data), 'data', 'out1.bin',  Config = config)
# Store final timestamp
with open("/local/timestamps/shared_minio/1to4/32MB/w1cont_end_ts.txt", 'a+') as fp_wcont_e_ts:
   fp_wcont_e_ts.write("%f\n" % time.time())
s3t.shutdown()
#print("WRITER: Written out1!")

          Workspaces:
            Mount Path:  /local
            Name:        write1-ws
Events:
  Type    Reason     Age                  From         Message
  ----    ------     ----                 ----         -------
  Normal  Started    117s (x2 over 117s)  PipelineRun  
  Normal  Running    117s (x2 over 117s)  PipelineRun  Tasks Completed: 0 (Failed: 0, Cancelled 0), Incomplete: 5, Skipped: 0
  Normal  Running    20s                  PipelineRun  Tasks Completed: 1 (Failed: 0, Cancelled 0), Incomplete: 4, Skipped: 0
  Normal  Running    11s                  PipelineRun  Tasks Completed: 2 (Failed: 0, Cancelled 0), Incomplete: 3, Skipped: 0
  Normal  Running    10s                  PipelineRun  Tasks Completed: 3 (Failed: 0, Cancelled 0), Incomplete: 2, Skipped: 0
  Normal  Running    7s                   PipelineRun  Tasks Completed: 4 (Failed: 0, Cancelled 0), Incomplete: 1, Skipped: 0
  Normal  Succeeded  7s                   PipelineRun  Tasks Completed: 5 (Failed: 0, Cancelled 0), Skipped: 0
