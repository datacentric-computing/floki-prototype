Name:         write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn
Namespace:    default
Priority:     0
Node:         k8s-worker-node4/10.0.26.213
Start Time:   Wed, 29 Jun 2022 06:08:50 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-fmkww
              tekton.dev/pipelineTask=write4-task
              tekton.dev/task=write4
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-fmkww-write4-task-vrwnt
Annotations:  cni.projectcalico.org/containerID: 403e4e923c525a546f6b508689efafba1c5c9e605683029c4461f21a5f05dede
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file to minio
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.91.81
IPs:
  IP:           192.168.91.81
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-fmkww-write4-task-vrwnt
Init Containers:
  place-tools:
    Container ID:  docker://69945f65b53eeba3fd566395b5623681bba00cc267bc52b0326c7a99f64146d8
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:08:51 +0000
      Finished:     Wed, 29 Jun 2022 06:08:51 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://fd1c5914b5df21e7010e8605f1ecd9b1629d1d4c668115a2a67b83e8599228ba
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-cztlv"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MgCmltcG9ydCBzdWJwcm9jZXNzCmltcG9ydCBzeXMKaW1wb3J0IGJvdG8zCmltcG9ydCBib3RvY29yZQppbXBvcnQgaW8KaW1wb3J0IGJvdG8zLnMzLnRyYW5zZmVyIGFzIHMzdHJhbnNmZXIKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby80dG8xLzJNQi93NGNvbnRfc3RhcnRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfc190czoKICAgZnBfd2NvbnRfc190cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKIyBDcmVhdGUgQm90byBzZXNzaW9uIGFuZCBjbGllbnQKc2Vzc2lvbiA9IGJvdG8zLnNlc3Npb24uU2Vzc2lvbigpCmJvdG9jb3JlX2NvbmZpZyA9IGJvdG9jb3JlLmNvbmZpZy5Db25maWcoCiAgIHJlYWRfdGltZW91dD0zNjAwLAogICBjb25uZWN0X3RpbWVvdXQ9MzYwMCwKICAgcmV0cmllcz17Im1heF9hdHRlbXB0cyI6IDB9LAogICBtYXhfcG9vbF9jb25uZWN0aW9ucz0yMAopCnMzX2NsaWVudCA9IHNlc3Npb24uY2xpZW50KAogICBzZXJ2aWNlX25hbWU9J3MzJywKICAgYXdzX2FjY2Vzc19rZXlfaWQ9J2FkbWluJywKICAgYXdzX3NlY3JldF9hY2Nlc3Nfa2V5PSdzZWNyZXRwd2QnLAogICBlbmRwb2ludF91cmw9J2h0dHA6Ly8xMC4wLjI2LjMzOjkwMDAnLAogICBjb25maWc9Ym90b2NvcmVfY29uZmlnCikKdHJhbnNmZXJfY29uZmlnID0gczN0cmFuc2Zlci5UcmFuc2ZlckNvbmZpZygKICAgdXNlX3RocmVhZHM9VHJ1ZSwKICAgbWF4X2NvbmN1cnJlbmN5PTIwCikKczN0ID0gczN0cmFuc2Zlci5jcmVhdGVfdHJhbnNmZXJfbWFuYWdlcihzM19jbGllbnQsIHRyYW5zZmVyX2NvbmZpZykKIyBDcmVhdGUgZGF0YSBvYmplY3QKYmluX2RhdGEgPSBieXRlYXJyYXkoYidceDAxJykgKiAyMDk3MTUyCiMgU3RvcmUgdGltZXN0YW1wIGFmdGVyIGRhdGEgY3JlYXRpb24gCndpdGggb3BlbigiL2xvY2FsL3RpbWVzdGFtcHMvc2hhcmVkX21pbmlvLzR0bzEvMk1CL3c0Y29udF9kYXRhX2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9kYXRhX3RzOgogICBmcF93Y29udF9kYXRhX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdyaXRlIGRhdGEgb2JqZWN0IG9uIG1pbmlvIHN0b3JhZ2UKI3MzX2NsaWVudC5wdXRfb2JqZWN0KEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0NC5iaW4nLCBCb2R5PWJpbl9kYXRhKQpzM3QudXBsb2FkKGlvLkJ5dGVzSU8oYmluX2RhdGEpLCAnZGF0YScsICdvdXQ0LmJpbicpCiNzM19jbGllbnQudXBsb2FkX2ZpbGVvYmooaW8uQnl0ZXNJTyhiaW5fZGF0YSksICdkYXRhJywgJ291dDQuYmluJywgIENvbmZpZyA9IGNvbmZpZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfbWluaW8vNHRvMS8yTUIvdzRjb250X2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9lX3RzOgogICBmcF93Y29udF9lX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQpzM3Quc2h1dGRvd24oKQojcHJpbnQoIldSSVRFUjogV3JpdHRlbiBvdXQxISIpCg==
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:08:52 +0000
      Finished:     Wed, 29 Jun 2022 06:08:52 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://a3a8a9c677ec1b40568419a0987f43ef1cd29347f027ebfa65ad8f8bd341e32c
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:08:53 +0000
      Finished:     Wed, 29 Jun 2022 06:08:54 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://72a4e6e28d46df49e3f6caed2cd96b48565ee9d3c6786d91b556b08a163fade3
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-cztlv
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-29T06:08:57.961Z","type":3}]
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:08:54 +0000
      Finished:     Wed, 29 Jun 2022 06:08:58 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-p5dvw (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://4d73125a115fcc80e744014204a56e640337fee8dd0dc07ea32b587ced98cde2
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:09:05 +0000
      Finished:     Wed, 29 Jun 2022 06:09:05 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 29 Jun 2022 06:08:55 +0000
      Finished:     Wed, 29 Jun 2022 06:09:04 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-p5dvw:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task4-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node4
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  64s                default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-fmkww-write4-task-vrw-nl8cn to k8s-worker-node4
  Normal   Pulled     62s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    62s                kubelet            Created container place-tools
  Normal   Started    62s                kubelet            Started container place-tools
  Normal   Pulled     61s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    61s                kubelet            Created container place-scripts
  Normal   Started    61s                kubelet            Started container place-scripts
  Normal   Pulled     60s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    60s                kubelet            Created container istio-init
  Normal   Started    60s                kubelet            Started container istio-init
  Normal   Pulled     59s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    59s                kubelet            Created container step-write
  Normal   Started    59s                kubelet            Started container step-write
  Normal   Pulled     59s                kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Killing    54s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  50s (x2 over 52s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    49s (x2 over 58s)  kubelet            Created container istio-proxy
  Normal   Pulled     49s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Started    48s (x2 over 58s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  48s                kubelet            Readiness probe failed: Get "http://192.168.91.81:15021/healthz/ready": dial tcp 192.168.91.81:15021: connect: connection refused
