Name:         write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Tue, 28 Jun 2022 17:15:12 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-qp9lg
              tekton.dev/pipelineTask=write1-task
              tekton.dev/task=write1
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-qp9lg-write1-task-t2dz9
Annotations:  cni.projectcalico.org/containerID: 905f01dc7fd3e9e38421cac7dca62696302eed60c4c62aa48108b8af6e612a64
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file to minio
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.50.216
IPs:
  IP:           192.168.50.216
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-qp9lg-write1-task-t2dz9
Init Containers:
  place-tools:
    Container ID:  docker://13db6d53056809eb8d2e222a30317801b0d019ff79f2aed6c5b6beb6d881c76f
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 17:15:14 +0000
      Finished:     Tue, 28 Jun 2022 17:15:14 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://c7036e2cfead5bf841778b48b625a376e4fcacd2f188a0f3e76a514a26bac634
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-wvwgq"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MgCmltcG9ydCBzdWJwcm9jZXNzCmltcG9ydCBzeXMKaW1wb3J0IGJvdG8zCmltcG9ydCBib3RvY29yZQppbXBvcnQgaW8KaW1wb3J0IGJvdG8zLnMzLnRyYW5zZmVyIGFzIHMzdHJhbnNmZXIKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby8ydG8xLzI1Nk1CL3cxY29udF9zdGFydF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9zX3RzOgogICBmcF93Y29udF9zX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIENyZWF0ZSBCb3RvIHNlc3Npb24gYW5kIGNsaWVudApzZXNzaW9uID0gYm90bzMuc2Vzc2lvbi5TZXNzaW9uKCkKYm90b2NvcmVfY29uZmlnID0gYm90b2NvcmUuY29uZmlnLkNvbmZpZygKICAgcmVhZF90aW1lb3V0PTM2MDAsCiAgIGNvbm5lY3RfdGltZW91dD0zNjAwLAogICByZXRyaWVzPXsibWF4X2F0dGVtcHRzIjogMH0sCiAgIG1heF9wb29sX2Nvbm5lY3Rpb25zPTIwCikKczNfY2xpZW50ID0gc2Vzc2lvbi5jbGllbnQoCiAgIHNlcnZpY2VfbmFtZT0nczMnLAogICBhd3NfYWNjZXNzX2tleV9pZD0nYWRtaW4nLAogICBhd3Nfc2VjcmV0X2FjY2Vzc19rZXk9J3NlY3JldHB3ZCcsCiAgIGVuZHBvaW50X3VybD0naHR0cDovLzEwLjAuMjYuMzM6OTAwMCcsCiAgIGNvbmZpZz1ib3RvY29yZV9jb25maWcKKQp0cmFuc2Zlcl9jb25maWcgPSBzM3RyYW5zZmVyLlRyYW5zZmVyQ29uZmlnKAogICB1c2VfdGhyZWFkcz1UcnVlLAogICBtYXhfY29uY3VycmVuY3k9MjAKKQpzM3QgPSBzM3RyYW5zZmVyLmNyZWF0ZV90cmFuc2Zlcl9tYW5hZ2VyKHMzX2NsaWVudCwgdHJhbnNmZXJfY29uZmlnKQojIENyZWF0ZSBkYXRhIG9iamVjdApiaW5fZGF0YSA9IGJ5dGVhcnJheShiJ1x4MDEnKSAqIDI2ODQzNTQ1NgojIFN0b3JlIHRpbWVzdGFtcCBhZnRlciBkYXRhIGNyZWF0aW9uIAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby8ydG8xLzI1Nk1CL3cxY29udF9kYXRhX2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9kYXRhX3RzOgogICBmcF93Y29udF9kYXRhX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdyaXRlIGRhdGEgb2JqZWN0IG9uIG1pbmlvIHN0b3JhZ2UKI3MzX2NsaWVudC5wdXRfb2JqZWN0KEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0MS5iaW4nLCBCb2R5PWJpbl9kYXRhKQpzM3QudXBsb2FkKGlvLkJ5dGVzSU8oYmluX2RhdGEpLCAnZGF0YScsICdvdXQxLmJpbicpCiNzM19jbGllbnQudXBsb2FkX2ZpbGVvYmooaW8uQnl0ZXNJTyhiaW5fZGF0YSksICdkYXRhJywgJ291dDEuYmluJywgIENvbmZpZyA9IGNvbmZpZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfbWluaW8vMnRvMS8yNTZNQi93MWNvbnRfZW5kX3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X2VfdHM6CiAgIGZwX3djb250X2VfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCnMzdC5zaHV0ZG93bigpCiNwcmludCgiV1JJVEVSOiBXcml0dGVuIG91dDEhIikK
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 17:15:15 +0000
      Finished:     Tue, 28 Jun 2022 17:15:15 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://d49467243a910caae74a0d51231e3a485c212b48b6bfc7cabde9cdbeab8cf556
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 17:15:16 +0000
      Finished:     Tue, 28 Jun 2022 17:15:17 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://c57fdfd88b33645df3b250f33f9867f4ad9220023111358c5e21a547a9175eb7
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-wvwgq
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-28T17:15:21.816Z","type":3}]
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 17:15:17 +0000
      Finished:     Tue, 28 Jun 2022 17:15:26 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-svk8s (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://a2178634f0d7ca130ade4b5c7e746aa0aed279138058d8aa538af326e6d03c53
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 17:15:34 +0000
      Finished:     Tue, 28 Jun 2022 17:15:34 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-svk8s:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  55s                default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-qp9lg-write1-task-t2d-q5q8w to k8s-worker-node1
  Normal   Pulled     54s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    54s                kubelet            Created container place-tools
  Normal   Started    54s                kubelet            Started container place-tools
  Normal   Pulled     53s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    53s                kubelet            Created container place-scripts
  Normal   Started    53s                kubelet            Started container place-scripts
  Normal   Pulled     52s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    52s                kubelet            Created container istio-init
  Normal   Started    52s                kubelet            Started container istio-init
  Normal   Pulled     51s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    51s                kubelet            Created container step-write
  Normal   Started    51s                kubelet            Started container step-write
  Normal   Pulled     51s                kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Killing    40s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  36s (x3 over 40s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    35s (x2 over 51s)  kubelet            Created container istio-proxy
  Normal   Pulled     35s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Started    34s (x2 over 50s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  34s                kubelet            Readiness probe failed: Get "http://192.168.50.216:15021/healthz/ready": dial tcp 192.168.50.216:15021: connect: connection refused
