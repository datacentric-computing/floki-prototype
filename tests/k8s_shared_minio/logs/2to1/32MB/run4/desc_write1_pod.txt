Name:         write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f
Namespace:    default
Priority:     0
Node:         k8s-worker-node1/10.0.26.206
Start Time:   Tue, 28 Jun 2022 16:37:46 +0000
Labels:       app.kubernetes.io/managed-by=tekton-pipelines
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f
              service.istio.io/canonical-revision=latest
              tekton.dev/memberOf=tasks
              tekton.dev/pipeline=write-read-array-minio-pipeline
              tekton.dev/pipelineRun=write-read-array-minio-pipeline-run-624pd
              tekton.dev/pipelineTask=write1-task
              tekton.dev/task=write1
              tekton.dev/taskRun=write-read-array-minio-pipeline-run-624pd-write1-task-tmkxt
Annotations:  cni.projectcalico.org/containerID: b1624e0d3818e5ae08a34707ca5b200b7bb90c17d560b466c5705c0fc3da9445
              cni.projectcalico.org/podIP: 
              cni.projectcalico.org/podIPs: 
              description: A simple task that writes a file to minio
              kubectl.kubernetes.io/default-container: step-write
              kubectl.kubernetes.io/default-logs-container: step-write
              pipeline.tekton.dev/release: 918ca4f
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-data","istio-podinfo","istiod-ca-cert"],"ima...
              tekton.dev/ready: READY
Status:       Succeeded
IP:           192.168.50.250
IPs:
  IP:           192.168.50.250
Controlled By:  TaskRun/write-read-array-minio-pipeline-run-624pd-write1-task-tmkxt
Init Containers:
  place-tools:
    Container ID:  docker://7662b27e27fb03cbcf0799d0b22a22ab89956834b29667bf60b3d9765a9cf4bc
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561
    Port:          <none>
    Host Port:     <none>
    Command:
      /ko-app/entrypoint
      cp
      /ko-app/entrypoint
      /tekton/bin/entrypoint
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:37:49 +0000
      Finished:     Tue, 28 Jun 2022 16:37:49 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  place-scripts:
    Container ID:  docker://98e4d3bdc60f29a35bdb89c8678406cd038a9c6f7834d9f2fd16f7890bc2f438
    Image:         gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Image ID:      docker-pullable://gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
    Args:
      -c
      scriptfile="/tekton/scripts/script-0-26nfn"
      touch ${scriptfile} && chmod +x ${scriptfile}
      cat > ${scriptfile} << '_EOF_'
      IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgdGltZQppbXBvcnQgb3MgCmltcG9ydCBzdWJwcm9jZXNzCmltcG9ydCBzeXMKaW1wb3J0IGJvdG8zCmltcG9ydCBib3RvY29yZQppbXBvcnQgaW8KaW1wb3J0IGJvdG8zLnMzLnRyYW5zZmVyIGFzIHMzdHJhbnNmZXIKIyBTdG9yZSBpbml0aWFsIHRpbWVzdGFtcAp3aXRoIG9wZW4oIi9sb2NhbC90aW1lc3RhbXBzL3NoYXJlZF9taW5pby8ydG8xLzMyTUIvdzFjb250X3N0YXJ0X3RzLnR4dCIsICdhKycpIGFzIGZwX3djb250X3NfdHM6CiAgIGZwX3djb250X3NfdHMud3JpdGUoIiVmXG4iICUgdGltZS50aW1lKCkpCiMgQ3JlYXRlIEJvdG8gc2Vzc2lvbiBhbmQgY2xpZW50CnNlc3Npb24gPSBib3RvMy5zZXNzaW9uLlNlc3Npb24oKQpib3RvY29yZV9jb25maWcgPSBib3RvY29yZS5jb25maWcuQ29uZmlnKAogICByZWFkX3RpbWVvdXQ9MzYwMCwKICAgY29ubmVjdF90aW1lb3V0PTM2MDAsCiAgIHJldHJpZXM9eyJtYXhfYXR0ZW1wdHMiOiAwfSwKICAgbWF4X3Bvb2xfY29ubmVjdGlvbnM9MjAKKQpzM19jbGllbnQgPSBzZXNzaW9uLmNsaWVudCgKICAgc2VydmljZV9uYW1lPSdzMycsCiAgIGF3c19hY2Nlc3Nfa2V5X2lkPSdhZG1pbicsCiAgIGF3c19zZWNyZXRfYWNjZXNzX2tleT0nc2VjcmV0cHdkJywKICAgZW5kcG9pbnRfdXJsPSdodHRwOi8vMTAuMC4yNi4zMzo5MDAwJywKICAgY29uZmlnPWJvdG9jb3JlX2NvbmZpZwopCnRyYW5zZmVyX2NvbmZpZyA9IHMzdHJhbnNmZXIuVHJhbnNmZXJDb25maWcoCiAgIHVzZV90aHJlYWRzPVRydWUsCiAgIG1heF9jb25jdXJyZW5jeT0yMAopCnMzdCA9IHMzdHJhbnNmZXIuY3JlYXRlX3RyYW5zZmVyX21hbmFnZXIoczNfY2xpZW50LCB0cmFuc2Zlcl9jb25maWcpCiMgQ3JlYXRlIGRhdGEgb2JqZWN0CmJpbl9kYXRhID0gYnl0ZWFycmF5KGInXHgwMScpICogMzM1NTQ0MzIKIyBTdG9yZSB0aW1lc3RhbXAgYWZ0ZXIgZGF0YSBjcmVhdGlvbiAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfbWluaW8vMnRvMS8zMk1CL3cxY29udF9kYXRhX2VuZF90cy50eHQiLCAnYSsnKSBhcyBmcF93Y29udF9kYXRhX3RzOgogICBmcF93Y29udF9kYXRhX3RzLndyaXRlKCIlZlxuIiAlIHRpbWUudGltZSgpKQojIFdyaXRlIGRhdGEgb2JqZWN0IG9uIG1pbmlvIHN0b3JhZ2UKI3MzX2NsaWVudC5wdXRfb2JqZWN0KEJ1Y2tldD0nZGF0YScsIEtleT0nb3V0MS5iaW4nLCBCb2R5PWJpbl9kYXRhKQpzM3QudXBsb2FkKGlvLkJ5dGVzSU8oYmluX2RhdGEpLCAnZGF0YScsICdvdXQxLmJpbicpCiNzM19jbGllbnQudXBsb2FkX2ZpbGVvYmooaW8uQnl0ZXNJTyhiaW5fZGF0YSksICdkYXRhJywgJ291dDEuYmluJywgIENvbmZpZyA9IGNvbmZpZykKIyBTdG9yZSBmaW5hbCB0aW1lc3RhbXAKd2l0aCBvcGVuKCIvbG9jYWwvdGltZXN0YW1wcy9zaGFyZWRfbWluaW8vMnRvMS8zMk1CL3cxY29udF9lbmRfdHMudHh0IiwgJ2ErJykgYXMgZnBfd2NvbnRfZV90czoKICAgZnBfd2NvbnRfZV90cy53cml0ZSgiJWZcbiIgJSB0aW1lLnRpbWUoKSkKczN0LnNodXRkb3duKCkKI3ByaW50KCJXUklURVI6IFdyaXR0ZW4gb3V0MSEiKQo=
      _EOF_
      /tekton/bin/entrypoint decode-script "${scriptfile}"
      
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:37:50 +0000
      Finished:     Tue, 28 Jun 2022 16:37:50 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /tekton/bin from tekton-internal-bin (rw)
      /tekton/scripts from tekton-internal-scripts (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
  istio-init:
    Container ID:  docker://8b550cd2c634b5e03c1bc57a56f1299d913079ec24eefe1e66d5fae7e7216cc4
    Image:         docker.io/istio/proxyv2:1.11.0
    Image ID:      docker-pullable://istio/proxyv2@sha256:f6b384cc9248f299bae2de96dc032a5dfb930d1284aecc0934020eb8044661a0
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:37:51 +0000
      Finished:     Tue, 28 Jun 2022 16:37:51 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        100m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Containers:
  step-write:
    Container ID:  docker://6577568085a44fe474dc825358c28f51a8c4eb34316ea96003f6157ddf0f7ec3
    Image:         amnestorov/minio
    Image ID:      docker-pullable://amnestorov/minio@sha256:384d67ba924edbf872ea1c40bac5f0049d5afc6d962e8f83dde8afa622ab1967
    Port:          <none>
    Host Port:     <none>
    Command:
      /tekton/bin/entrypoint
    Args:
      -wait_file
      /tekton/downward/ready
      -wait_file_content
      -post_file
      /tekton/run/0/out
      -termination_path
      /tekton/termination
      -step_metadata_dir
      /tekton/steps/step-write
      -step_metadata_dir_link
      /tekton/steps/0
      -entrypoint
      /tekton/scripts/script-0-26nfn
      --
    State:          Terminated
      Reason:       Completed
      Message:      [{"key":"StartedAt","value":"2022-06-28T16:37:56.720Z","type":3}]
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:37:52 +0000
      Finished:     Tue, 28 Jun 2022 16:37:58 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /local from ws-6g264 (rw)
      /tekton/bin from tekton-internal-bin (ro)
      /tekton/creds from tekton-creds-init-home-0 (rw)
      /tekton/downward from tekton-internal-downward (ro)
      /tekton/home from tekton-internal-home (rw)
      /tekton/results from tekton-internal-results (rw)
      /tekton/run/0 from tekton-internal-run-0 (rw)
      /tekton/scripts from tekton-internal-scripts (ro)
      /tekton/steps from tekton-internal-steps (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
      /workspace from tekton-internal-workspace (rw)
  istio-proxy:
    Container ID:  docker://1e67c12b1969dc83519356c46483d26f9f1adaa94107614cdd2921cc5c9e7756
    Image:         gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Image ID:      docker-pullable://gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:38:05 +0000
      Finished:     Tue, 28 Jun 2022 16:38:06 +0000
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 28 Jun 2022 16:37:53 +0000
      Finished:     Tue, 28 Jun 2022 16:38:05 +0000
    Ready:          False
    Restart Count:  1
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      100m
      memory:   128Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    first-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                     ]
      ISTIO_META_APP_CONTAINERS:     step-write
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f
      ISTIO_META_OWNER:              kubernetes://apis/v1/namespaces/default/pods/write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-zxbck (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  tekton-internal-workspace:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-home:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-results:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-steps:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-scripts:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-bin:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tekton-internal-downward:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.annotations['tekton.dev/ready'] -> ready
  tekton-creds-init-home-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  tekton-internal-run-0:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  ws-6g264:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  task1-pv-claim
    ReadOnly:   false
  default-token-zxbck:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-zxbck
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  kubernetes.io/hostname=k8s-worker-node1
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                From               Message
  ----     ------     ----               ----               -------
  Normal   Scheduled  46s                default-scheduler  Successfully assigned default/write-read-array-minio-pipeline-run-624pd-write1-task-tmk-cn48f to k8s-worker-node1
  Normal   Pulled     45s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/entrypoint:v0.29.0@sha256:66958b78766741c25e31954f47bc9fd53eaa28263506b262bf2cc6df04f18561" already present on machine
  Normal   Created    45s                kubelet            Created container place-tools
  Normal   Started    44s                kubelet            Started container place-tools
  Normal   Pulled     43s                kubelet            Container image "gcr.io/distroless/base@sha256:aa4fd987555ea10e1a4ec8765da8158b5ffdfef1e72da512c7ede509bc9966c4" already present on machine
  Normal   Created    43s                kubelet            Created container place-scripts
  Normal   Started    43s                kubelet            Started container place-scripts
  Normal   Pulled     42s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    42s                kubelet            Created container istio-init
  Normal   Started    42s                kubelet            Started container istio-init
  Normal   Pulled     41s                kubelet            Container image "docker.io/istio/proxyv2:1.11.0" already present on machine
  Normal   Created    41s                kubelet            Created container step-write
  Normal   Started    41s                kubelet            Started container step-write
  Normal   Pulled     41s                kubelet            Container image "amnestorov/minio" already present on machine
  Normal   Killing    33s                kubelet            Container istio-proxy definition changed, will be restarted
  Warning  Unhealthy  29s (x2 over 31s)  kubelet            Readiness probe failed: HTTP probe failed with statuscode: 503
  Normal   Created    28s (x2 over 41s)  kubelet            Created container istio-proxy
  Normal   Pulled     28s                kubelet            Container image "gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/nop:v0.29.0@sha256:6a037d5ba27d9c6be32a9038bfe676fb67d2e4145b4f53e9c61fb3e69f06e816" already present on machine
  Normal   Started    27s (x2 over 40s)  kubelet            Started container istio-proxy
  Warning  Unhealthy  27s                kubelet            Readiness probe failed: Get "http://192.168.50.250:15021/healthz/ready": dial tcp 192.168.50.250:15021: connect: connection refused
