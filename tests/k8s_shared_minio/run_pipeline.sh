#!/bin/bash

# Set number of parallel tasks
NUM_WTASKS=1
NUM_RTASKS=1

# Number of overall iterations
NUM_RUNS=10

# Set the dimensions to benchmark
dims=( "1 MB" 
        "2 MB"
        "4 MB"
        "8 MB"
        "16 MB"
        "32 MB"
        "64 MB"
        "128 MB"
        "256 MB"
        "512 MB"        
        "1 GB"
        "2 GB"
        "4 GB"
        "8 GB"
        "16 GB"
)

# IP address
IP_ADDRS=( $(cat ../../../cluster_ips.txt | grep 'worker' | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}') )

# Global variables
NUM_BYTES=0

# Set the remote mount point path
REM_MNT_PATH="/mnt/disk/vol2"

# Set the local logs folder path 
LOC_LOGS_FOLD_PATH="logs/"

compute_num_bytes(){
    # Get the value and the dimension
    stringarray=( $1 )
    val=${stringarray[0]}
    dim=${stringarray[1]}
    # Return the actual number of bytes
    case "$dim" in
        "MB") NUM_BYTES=$(($val*1024*1024))
        ;;
        "GB") NUM_BYTES=$(($val*1024*1024*1024))
        ;;
        *) echo "ERROR: Dimension $dim not allowed!" 
        exit 1
        ;;
    esac
}

exec 5>&1

# Setup the pipeline source by modifing the tasks with the proper dimension
setup_and_deploy_pipeline_source(){
    # Setup pipeline source
    compute_num_bytes "$1"
    cp temp_files/pipeline_${NUM_WTASKS}to${NUM_RTASKS}.yaml temp_pipeline.yaml
    sed -i "s/num_bytes/${NUM_BYTES}/g" temp_pipeline.yaml
    dim_nospace=$(echo "$1" | sed "s/ //g")
    sed -i "s/dimension/${dim_nospace}/g" temp_pipeline.yaml
    # Deploy the pipeline
    kubectl apply -f temp_pipeline.yaml
    rm temp_pipeline.yaml
}

# Setup the local folder to store logs 
setup_loc_logs_folder(){
    dim_nospace=$(echo "$1" | sed "s/ //g")
    logs_folder="${LOC_LOGS_FOLD_PATH}/${NUM_WTASKS}to${NUM_RTASKS}/${dim_nospace}"
    if [[ -d "$logs_folder" ]]; then
        # Delete related data dimension logs files
        rm -r ${logs_folder}/* 2> /dev/null
    else
        # Create data dimension folder
        mkdir -p $logs_folder
    fi
}

# Setup the remote folders to store timestamps
setup_remote_folders(){
    dim_nospace=$(echo "$1" | sed "s/ //g")
    father_fold_path="/mnt/disk/vol2/timestamps/shared_minio/${NUM_WTASKS}to${NUM_RTASKS}/${dim_nospace}"
    tot_num=$((NUM_WTASKS+NUM_RTASKS))
    counter=0
    for ip in "${IP_ADDRS[@]}"; do
        # Create folder 
        cmd="mkdir -p ${father_fold_path} && chmod 777 ${father_fold_path}"
        sshpass -p 'root' ssh -o LogLevel=QUIET -o StrictHostKeyChecking=no -t root@${ip} ${cmd}
        # Delete remote files
        cmd="rm ${father_fold_path}/* 2> /dev/null"
        sshpass -p 'root' ssh -o LogLevel=QUIET -t root@${ip} ${cmd}
        counter=$((counter+1))
        if [[ $counter == $tot_num ]]; then
            break
        fi
    done      
}

# Deploy the pvs and the pvcs  
deploy_pvs_and_pvcs(){  
    tot_num=$((NUM_WTASKS+NUM_RTASKS))      
    for (( m=1; m <= ${tot_num}; m++ )); do
        kubectl apply -f temp_files/pvNode${m}.yaml 2> /dev/null    
        kubectl apply -f temp_files/pvcNode${m}.yaml 2> /dev/null  
    done
}

# Deploy and wait the end of pipelinerun
deploy_wait_pipelinerun(){
    # Deploy the pipelinerun
    out=$(kubectl create -f temp_files/pipelinerun_${NUM_WTASKS}to${NUM_RTASKS}.yaml 2> /dev/null | tee >(cat - >&5))
    prun_obj=( $(echo "$out" | cut -d "/" -f2- | awk '{print $1}'))
    # Wait for the pipeline to start
    array=($out)
    while [ ${#array[@]} -lt 8 ]; do
        out=$(kubectl get pipelineruns 2> /dev/null)
        array=($out)
    done
    echo "Pipeline is still running.."
    out=$(kubectl get pipelineruns 2> /dev/null)
    set -- $out
    while [ $7 != 'True' -a $8 != 'Succeded' ]; do
        if [ $8 == 'False' ]; then
            echo "ERROR: Pipelinerun has failed!"
            exit 1
        fi
        out=$(kubectl get pipelineruns 2> /dev/null)
        set -- $out
    done
    echo "Pipeline has finished!"
}

# Store current run logs
store_run_logs(){
    # Create the run logs folder
    dim_nospace=$(echo "$1" | sed "s/ //g")
    logs_folder="${LOC_LOGS_FOLD_PATH}/${NUM_WTASKS}to${NUM_RTASKS}/${dim_nospace}/run$2"
    mkdir $logs_folder
    # Dump pipelinerun description
    prun_obj=$(kubectl get pipelineruns 2> /dev/null | awk '{print $1}' | grep write-read-array-pipeline-run)
    kubectl describe pipelinerun $prun_obj > ${logs_folder}/desc_pipelinerun.txt 2> /dev/null
    # Get pods names
    p_names=($(kubectl get pods --no-headers -o custom-columns=":metadata.name" 2> /dev/null))
    for p in "${p_names[@]}"; do
        # Dump pod description
        pod_name=$(echo $p | cut -d "-" -f8- | cut -d "-" -f1)
        kubectl describe pod ${p} > ${logs_folder}/desc_${pod_name}_pod.txt 2> /dev/null
    done
}

# Delete the pipelinerun
delete_pipelinerun(){
    prun_obj=$(kubectl get pipelineruns 2> /dev/null | awk '{print $1}' | grep write-read-array-minio-pipeline-run)
    kubectl delete pipelinerun $prun_obj 2> /dev/null
}

delete_pvs_and_pvcs(){     
    # Delete the pvs and pvcs 
    tot_num=$((NUM_WTASKS+NUM_RTASKS))    
    for (( n=1; n<=$tot_num; n++)); do
        kubectl delete pvc task${n}-pv-claim 2> /dev/null
        kubectl delete pv tekton-disk-pv-hostpath${n} 2> /dev/null
    done
}

# Delete the run data outcomes from bucket and from nodes folders
delete_run_data(){
    # Delete objects from bucket
     mc rm --force --recursive minio/data
}

# Delete the pipeline along with its tasks
delete_pipeline_and_tasks(){
    kubectl delete pipeline write-read-array-minio-pipeline 2> /dev/null
    for (( t=1; t<=$NUM_WTASKS; t++)); do
        kubectl delete task write$t 2> /dev/null
    done
    for (( t=1; t<=$NUM_RTASKS; t++)); do
        kubectl delete task \read$t 2> /dev/null
    done
}

# Generate yamls
./generate_yamls.sh $NUM_WTASKS $NUM_RTASKS

# Run the tests
for i in "${dims[@]}"; do
    echo "************************** SHARED CLOUD MEC WITH ${CONFIG^^} TEST ON $i **************************"
    # Setup the pipeline source
    setup_and_deploy_pipeline_source "$i"
    # Setup the local logs folder
    setup_loc_logs_folder "$i"
    # Setup the remote timestamps folders
    setup_remote_folders "$i"
    # Run multiple times to get a fair latency value
    for (( j=0; j<$NUM_RUNS; j++)); do
        echo "TEST ON $i: Running $((j+1))/$NUM_RUNS iteration.."
        # Deploy pvs and pvcs 
        deploy_pvs_and_pvcs     
        # Deploy and wait the end of pipelinerun
        deploy_wait_pipelinerun
        # Store run logs
        store_run_logs "$i" $j
        # Delete the pipelinerun
        delete_pipelinerun      
        # Delete pvs and pvcs
        delete_pvs_and_pvcs
        # Delete the run data outcomes
        delete_run_data
    done
    # Delete the pipeline along with its tasks
    delete_pipeline_and_tasks
    # Delete all temporary yaml files
    #rm -rf temp_files
done

# Delete all temporary yamls
rm -r temp_files
