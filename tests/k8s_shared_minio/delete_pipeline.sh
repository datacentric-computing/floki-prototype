NUM_WTASKS=$1
NUM_RTASKS=$2
NUM_TASKS=$(($NUM_WTASKS+$NUM_RTASKS))

# Delete pipelineruns
kubectl delete pipelinerun --all 2> /dev/null

# Delete pipelines
kubectl delete pipeline --all 2> /dev/null

# Delete tasks
for (( t=1; t<=$NUM_WTASKS; t++)); do    
    kubectl delete task write$t 2> /dev/null
done
for (( t=1; t<=$NUM_RTASKS; t++)); do
    kubectl delete task \read$t 2> /dev/null
done

# Delete pvcs and pvs
for (( i=0; i<=$NUM_TASKS; i++)); do
    kubectl delete pvc task${i}-pv-claim 2> /dev/null
    kubectl delete pv tekton-disk-pv-hostpath${i} 2> /dev/null
done
