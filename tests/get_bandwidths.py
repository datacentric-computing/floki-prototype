import sys
import getopt
import math
import os
from pathlib import Path
import re
import csv
from statistics import median

dims = [ "1 MB",
        "2 MB",
        "4 MB",
        "8 MB",
        "16 MB",
        "32 MB",
        "64 MB",
        "128 MB",
        "256 MB",
        "512 MB",
        "1 GB",
        "2 GB",
        "4 GB",
        "8 GB",
        "16 GB"
]

NUM_RUNS = 10

def usage():
    print("get_stat_timestamps.py -c <configuration> -m <mechanism>")
    print(" -c      <configuration>     specify the configuration   (disk or mem)")
    print(" -m      <mechanism>         specify the mechanism       (shared, shared_minio, or socket)")
    print(" --all                       get all statistics")
    print(" --dump                      store statistics on files")

def get_arguments(argv):
    try:
        opts, args = getopt.getopt(argv, "hc:m:", ["help", "configuration=", "mechanism=",  "all", "dump"])
    except getopt.GetoptError:
        print("Error")
        usage()
        sys.exit(2)
    all_mec = False
    dump = False
    conf = []
    mec = []
    # Analyse the given options
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-c", "--configuration"):
            conf = [arg]
        elif opt in ("-m", "--mechanism"):
            mec = [arg]
        elif opt == "--all":
            all_mec = True
        elif opt == "--dump":
            dump = True
    if not all_mec:
        # Check if configuration and mechanism are given
        if len(conf) == 0 or len(mec) == 0:
            print("ERROR: Configuration and mechanism have to be given!")
            exit(1)
        # Check the correctness of the input arguments
        if conf[0] not in ["mem", "disk"]:
            print("ERROR: The configuration can only be 'mem' or 'disk'!")
            exit(1)
        if mec[0] not in ["shared", "shared_minio", "socket"]:
            print("ERROR: The mechanism can only be 'shared' or 'socket'!")
            exit(1)
        # Check if shared memory configuration is given
        if conf[0] == 'mem' and mec[0] == 'shared':
            print("ERROR: Shared memory is not allowed!")
            exit(1)
        if conf[0] == 'mem' and mec[0] == 'shared_minio':
            print("ERROR: Shared minio is only disk-based!")
            exit(1)
    else:
        conf = ["disk", "mem"]
        mec = ["shared", "shared_minio", "socket"]
    return conf, mec, dump

def get_remote_timestamps(conf_list, mec_list, dim):
    for mec in mec_list:
        for conf in conf_list:
            if mec == 'shared' and conf == 'mem':
                continue
            # Get the timestamp folder path
            if mec == 'shared_minio':
                ts_folder = "k8s_" + mec + "/timestamps/1to1/" + dim.replace(" ", "")
            else:
                ts_folder = "k8s_" + mec + "/timestamps/" + conf + "/1to1/" + dim.replace(" ", "")
            if os.path.isdir(ts_folder):
                # Delete all files into folder
                cmd = "rm " + ts_folder + "/*"
            else:
                # Create folder
                cmd = "mkdir -p " + ts_folder
                os.system(cmd)
            # Copy the generated timestamps from remote folders
            #remote_path = "/mnt/" + conf + "/vol2/timestamps/" + mec + "/* "
            remote_path = "/mnt/" + conf + "/vol2/timestamps/" + mec + "/1to1/" + dim.replace(" ", "") + "/*"
            cmd = "sshpass -p 'root' scp -r root@10.0.26.206:" + remote_path + " " + ts_folder + "/"
            os.system(cmd)
            cmd = "sshpass -p 'root' scp -r root@10.0.26.207:" + remote_path + " " + ts_folder + "/"
            os.system(cmd)

def variance(data):
    n = len(data)
    mean = sum(data) / n
    return sum((x - mean) ** 2 for x in data) / n

def stdev(data):
    var = variance(data)
    std_dev = math.sqrt(var)
    return std_dev

def get_stats(values):
    med_time = median(values)
    #var = variance(values)
    #print("VARIANCE W/R: %f" % var)
    #std_dev = stdev(values)
    #print("STD_DEV W/R: %f" % std_dev) 
    return med_time

def get_diffs_timestamps(conf, mec, start_ts_file, end_ts_file, dim):
    # Get the timestamp folder path
    if mec == "shared_minio":
        ts_folder = "k8s_" + mec + "/timestamps/1to1/" + dim.replace(" ", "")
    else:
        ts_folder = "k8s_" + mec + "/timestamps/" + conf + "/1to1/" + dim.replace(" ", "")
    diffs = []
    # Open the start and end timestamps files
    with open(ts_folder + "/" + start_ts_file, 'r') as fp_start_ts:
        with open(ts_folder + "/" + end_ts_file, 'r') as fp_end_ts:
            for start_ts, end_ts in zip(fp_start_ts, fp_end_ts):
                diffs.append(float(end_ts)-float(start_ts))
    return diffs

def get_num_bytes(dim):
    val = int(dim.split()[0])
    unit = dim.split()[1]
    if unit == 'MB':
        num_bytes = val * 1024 * 1024
    if unit == 'GB':
        num_bytes = val * 1024 * 1024 * 1024
    return num_bytes

# Get statistics on bandwidths in MB/s
def get_stats_on_bw(conf, mec, dim):
    num_bytes = get_num_bytes(dim)
    diffs = get_diffs_timestamps(conf, mec, "w1cont_data_end_ts.txt", "w1cont_end_ts.txt", dim)
    med_w_bw = num_bytes/get_stats(diffs)/1024/1024
    print("Median Write Bandwidth:\t%.2f MB/s" % round(med_w_bw, 2))
    diffs = get_diffs_timestamps(conf, mec, "r1cont_start_ts.txt", "r1cont_end_ts.txt", dim)
    med_r_bw = num_bytes/get_stats(diffs)/1024/1024
    print("Median Read Bandwidth:\t%.2f MB/s" % round(med_r_bw, 2))
    return med_w_bw, med_r_bw

def dump_stats_to_files(bw_stats):
    # Dump statistic to csv files
    with open('stats_files/stats_bw_over_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
        cols_names = ['data_dim', 'sh_d_w_bw', 'sh_d_r_bw',
            'sh_minio_w_bw', 'sh_minio_r_bw',
            'so_d_w_bw', 'so_d_r_bw',
            'so_m_w_bw', 'so_m_r_bw']
        write = csv.writer(fp)
        write.writerow(cols_names)
        write.writerows(bw_stats)
        
def get_bandwidths(conf_list, mec_list, dump):
    # Initialize global statistics list
    bw_stats = []
    # For each data dimension compute statistics
    for dim in dims:
        print("******************************** BANDWIDTHS ON %s ********************************" % dim)
        # Get remote timestamps files
        #get_remote_timestamps(conf_list, mec_list, dim)
        # Initialize dimension lists
        dim_stats = []
        # For each mechanism and for each configuration get statistics
        for mec in mec_list:
            for conf in conf_list:
                if (mec == 'shared' or mec == 'shared_minio') and conf == 'mem':
                    continue
                # Obtain statistics on the tested data dimensions
                print("----------------------- MECHANISM %s CONFIGURATION: %s -----------------------" % (mec, conf))
                # Get the bandwidth statistics
                dim_stats.append(get_stats_on_bw(conf, mec, dim))
        # Append dimension statistics to glocal lists
        dim_stats = [val for sub_list in dim_stats for val in sub_list]
        bw_stats.append([dim] + dim_stats)
    # Dump statistics if required
    if dump:
        dump_stats_to_files(bw_stats)

def main(argv):
    # Get arguments
    conf_list, mec_list, dump = get_arguments(argv)
    # Get bandwidths
    get_bandwidths(conf_list, mec_list, dump)

if __name__ == "__main__":
    main(sys.argv[1:])
