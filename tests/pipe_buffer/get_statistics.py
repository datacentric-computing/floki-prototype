import sys
import getopt
import math
import os
from pathlib import Path
import re
import csv
from statistics import median

NUM_RUNS = 100
DUMP = 0 

buff_dims = [
    "4K",
    "8K",
    "16K",
    "32K",
    "64K",
    "128K",
    "256K",
    "512K"
]

def get_diffs_timestamps(start_ts_file, end_ts_file, dim):
    # Get the timestamp folder path
    ts_folder = "timestamps/" + dim
    diffs = []
    # Open the start and end timestamps files
    with open(ts_folder + "/" + start_ts_file, 'r') as fp_start_ts:
        with open(ts_folder + "/" + end_ts_file, 'r') as fp_end_ts:
            for start_ts, end_ts in zip(fp_start_ts, fp_end_ts):
                diffs.append(float(end_ts)-float(start_ts))
    return diffs

# Get statistics on buffer dimension
def get_stats_on_buffer_dimension(dim):
    diffs = get_diffs_timestamps("start_ts.txt", "end_ts.txt", dim)
    buff_avg = sum(diffs)/len(diffs)
    print("Pipe Buffer Dimension %s Average Time: %.5f s" % (dim, round(buff_avg, 5)))
    return [buff_avg]

def dump_stats_to_files(buffer_stats):
    # Dump statistic to csv files
    with open('stats_files/stats_pipe_buffer_dimension_' + str(NUM_RUNS) + 'runs.csv', 'w+', newline = '') as fp:
        cols_names = ['buff_dim', 'avg_time']
        write = csv.writer(fp)
        write.writerow(cols_names)
        write.writerows(buffer_stats)
        
def main(argv):
    # Initialize global statistics lists
    buffer_stats = []
    # For each data dimension compute statistics
    for dim in buff_dims:
        buffer_stats.append([dim] + get_stats_on_buffer_dimension(dim))
    # Dump statistics if required
    if DUMP:
        dump_stats_to_files(buffer_stats)

if __name__ == "__main__":
    main(sys.argv[1:])
